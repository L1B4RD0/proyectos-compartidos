﻿CREATE TABLE [dbo].[PerfilXFuncion] (
  [perf_id] [bigint] NOT NULL,
  [func_id] [bigint] NOT NULL,
  CONSTRAINT [PK_PerfilXFuncion] PRIMARY KEY CLUSTERED ([perf_id], [func_id])
)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[PerfilXFuncion] WITH NOCHECK
  ADD CONSTRAINT [FK_PerfilXFuncion_Funcion] FOREIGN KEY ([perf_id]) REFERENCES [dbo].[Perfil] ([perf_id])
GO

ALTER TABLE [dbo].[PerfilXFuncion] WITH NOCHECK
  ADD CONSTRAINT [FK_PerfilXFuncion_PerfilXFuncion] FOREIGN KEY ([func_id]) REFERENCES [dbo].[Funcion] ([func_id])
GO