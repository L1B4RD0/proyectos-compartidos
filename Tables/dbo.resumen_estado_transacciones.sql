﻿CREATE TABLE [dbo].[resumen_estado_transacciones] (
  [fecha] [datetime] NOT NULL,
  [terminal_id] [varchar](20) NOT NULL,
  [tipo] [varchar](20) NOT NULL,
  [contador] [bigint] NOT NULL,
  CONSTRAINT [resumen_estado_transacciones_pkey] PRIMARY KEY CLUSTERED ([fecha], [terminal_id], [tipo])
)
ON [PRIMARY]
GO