﻿CREATE TABLE [dbo].[TipoLogin] (
  [id] [bigint] IDENTITY,
  [descripcion] [varchar](100) NOT NULL,
  CONSTRAINT [PK_TipoLogin] PRIMARY KEY CLUSTERED ([id])
)
ON [PRIMARY]
GO