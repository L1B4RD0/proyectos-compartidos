﻿CREATE TABLE [dbo].[UsuarioHistoricoClaves] (
  [id] [bigint] IDENTITY,
  [fecha] [datetime] NOT NULL,
  [usu_id] [bigint] NOT NULL,
  [usu_hash_antiguo] [varchar](50) NOT NULL,
  CONSTRAINT [PK_UsuarioHistoricoClaves] PRIMARY KEY CLUSTERED ([id])
)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[UsuarioHistoricoClaves] WITH NOCHECK
  ADD CONSTRAINT [FK_UsuarioHistoricoClaves_Usuario] FOREIGN KEY ([usu_id]) REFERENCES [dbo].[Usuario] ([usu_id])
GO