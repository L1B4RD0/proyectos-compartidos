﻿CREATE TABLE [dbo].[Host_Confi] (
  [Id_Host] [varchar](50) NULL,
  [Nombre_Host] [varchar](50) NULL,
  [Tipo_Comunicacion] [varchar](50) NULL,
  [Formato_Mensaje] [varchar](50) NULL,
  [Reintentos] [varchar](50) NULL,
  [Tiempo_Espera_Conexion] [varchar](50) NULL,
  [Tiempo_Espera_Respuesta] [varchar](50) NULL,
  [NII_Transacciones] [varchar](50) NULL,
  [NII_Cierre] [varchar](50) NULL,
  [NII_Echo_Test] [varchar](50) NULL,
  [Ip_Tran1] [varchar](50) NULL,
  [Ip_Tran2] [varchar](50) NULL,
  [Llave_1] [varchar](50) NULL,
  [Llave_2] [varchar](50) NULL,
  [Llave_Doble] [int] NULL,
  [Dukpt] [int] NULL,
  [No_Tiempo_Host] [int] NULL,
  [Emv] [int] NULL
)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[Host_Confi]
  ADD CONSTRAINT [fkDukpt] FOREIGN KEY ([Dukpt]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO

ALTER TABLE [dbo].[Host_Confi]
  ADD CONSTRAINT [fkEmv] FOREIGN KEY ([Emv]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO

ALTER TABLE [dbo].[Host_Confi]
  ADD CONSTRAINT [fkLlaveDoble] FOREIGN KEY ([Llave_Doble]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO

ALTER TABLE [dbo].[Host_Confi]
  ADD CONSTRAINT [fkNoTiempoHost] FOREIGN KEY ([No_Tiempo_Host]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO