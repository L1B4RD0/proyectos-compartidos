﻿CREATE TABLE [dbo].[Funcion] (
  [func_id] [bigint] IDENTITY,
  [func_descripcion] [varchar](200) NOT NULL,
  [func_objeto_id] [bigint] NOT NULL,
  [func_metodo_id] [bigint] NOT NULL,
  CONSTRAINT [PK_Funcion] PRIMARY KEY CLUSTERED ([func_id]) WITH (FILLFACTOR = 80)
)
ON [PRIMARY]
GO

CREATE UNIQUE INDEX [IX_Funcion]
  ON [dbo].[Funcion] ([func_objeto_id], [func_metodo_id])
  ON [PRIMARY]
GO

ALTER TABLE [dbo].[Funcion] WITH NOCHECK
  ADD CONSTRAINT [FK_Funcion_Metodo] FOREIGN KEY ([func_metodo_id]) REFERENCES [dbo].[Metodo] ([id])
GO

ALTER TABLE [dbo].[Funcion] WITH NOCHECK
  ADD CONSTRAINT [FK_Funcion_Objeto] FOREIGN KEY ([func_objeto_id]) REFERENCES [dbo].[Objeto] ([id])
GO