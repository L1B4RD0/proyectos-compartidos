﻿CREATE TABLE [dbo].[Aplicacion] (
  [apl_id] [bigint] IDENTITY,
  [apl_nombre_archivo_fisico] [varchar](50) NOT NULL,
  [apl_nombre_tms] [varchar](50) NOT NULL,
  [apl_checksum_tms] [varchar](50) NULL,
  [apl_data] [varbinary](max) NOT NULL,
  [apl_descripcion] [varchar](250) NULL,
  [apl_fecha_creacion] [datetime] NOT NULL,
  [apl_fecha_actualizacion] [datetime] NULL,
  [apl_Terminal_Model] [bigint] NULL,
  [ter_codigo_producto] [bigint] NULL,
  CONSTRAINT [PK_Aplicacion] PRIMARY KEY CLUSTERED ([apl_id])
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Application table', 'SCHEMA', N'dbo', 'TABLE', N'Aplicacion'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'primary variable', 'SCHEMA', N'dbo', 'TABLE', N'Aplicacion', 'COLUMN', N'apl_id'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Terminal product code', 'SCHEMA', N'dbo', 'TABLE', N'Aplicacion', 'COLUMN', N'ter_codigo_producto'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Primary key identifier', 'SCHEMA', N'dbo', 'TABLE', N'Aplicacion', 'INDEX', N'PK_Aplicacion'
GO