﻿CREATE TABLE [dbo].[EstadoActualizacion] (
  [id] [bigint] IDENTITY,
  [descripcion] [varchar](50) NOT NULL,
  CONSTRAINT [PK_EstadoActualizacion] PRIMARY KEY CLUSTERED ([id])
)
ON [PRIMARY]
GO