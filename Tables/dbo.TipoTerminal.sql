﻿CREATE TABLE [dbo].[TipoTerminal] (
  [id] [bigint] IDENTITY,
  [descripcion] [varchar](50) NOT NULL,
  CONSTRAINT [PK_TipoTerminal] PRIMARY KEY CLUSTERED ([id])
)
ON [PRIMARY]
GO