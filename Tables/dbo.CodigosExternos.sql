﻿CREATE TABLE [dbo].[CodigosExternos] (
  [cod_ext_id] [bigint] NOT NULL,
  [cod_ext_nombre] [varchar](50) NULL,
  PRIMARY KEY CLUSTERED ([cod_ext_id])
)
ON [PRIMARY]
GO