﻿CREATE TABLE [dbo].[ParametroGeneral] (
  [param_id] [bigint] IDENTITY,
  [param_nombre] [varchar](50) NOT NULL,
  [param_valor] [varchar](200) NOT NULL,
  CONSTRAINT [PK_ParametroGeneral] PRIMARY KEY CLUSTERED ([param_id])
)
ON [PRIMARY]
GO