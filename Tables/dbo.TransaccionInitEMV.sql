﻿CREATE TABLE [dbo].[TransaccionInitEMV] (
  [tra_id] [bigint] IDENTITY,
  [tra_fecha] [datetime] NOT NULL,
  [tra_stan] [varchar](6) NOT NULL,
  [tra_serial_pos] [varchar](20) NOT NULL,
  [tra_terminal_id] [bigint] NULL,
  [tra_message_type] [varchar](4) NOT NULL,
  [tra_processing_code] [varchar](6) NOT NULL,
  [tra_direccion_ip_detectada] [varchar](15) NULL,
  [tra_puerto_tcp_atencion] [varchar](6) NOT NULL,
  [tra_codigo_respuesta] [varchar](2) NOT NULL,
  [tra_mensaje_respuesta] [varchar](128) NOT NULL,
  [tra_campo48_respuesta] [varchar](250) NOT NULL,
  [tra_campo59_respuesta] [varchar](8000) NULL,
  CONSTRAINT [PK_TransaccionEMVInit] PRIMARY KEY CLUSTERED ([tra_id])
)
ON [PRIMARY]
GO