﻿CREATE TABLE [dbo].[Objeto] (
  [id] [bigint] IDENTITY,
  [nombre] [varchar](100) NOT NULL,
  CONSTRAINT [PK_Objeto] PRIMARY KEY CLUSTERED ([id])
)
ON [PRIMARY]
GO