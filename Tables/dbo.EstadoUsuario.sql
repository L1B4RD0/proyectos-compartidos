﻿CREATE TABLE [dbo].[EstadoUsuario] (
  [id] [bigint] IDENTITY,
  [descripcion] [varchar](100) NOT NULL,
  CONSTRAINT [PK_EstadoUsuario] PRIMARY KEY CLUSTERED ([id])
)
ON [PRIMARY]
GO