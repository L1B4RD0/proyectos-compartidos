﻿CREATE TABLE [dbo].[porcentajeDescarga] (
  [por_serialTerminal] [varchar](20) NOT NULL,
  [por_tra_fecha] [datetime] NOT NULL,
  [por_codigo_respuesta] [varchar](2) NOT NULL,
  [por_mensaje_respuesta] [varchar](128) NOT NULL,
  [por_porcentaje_descarga] [varchar](6) NOT NULL,
  [por_nombre_aplicacion] [varchar](50) NULL,
  CONSTRAINT [PK_porcentajeDescarga] PRIMARY KEY CLUSTERED ([por_serialTerminal])
)
ON [PRIMARY]
GO