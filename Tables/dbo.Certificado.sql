﻿CREATE TABLE [dbo].[Certificado] (
  [apl_id] [bigint] IDENTITY,
  [apl_data] [varbinary](max) NOT NULL,
  [apl_descripcion] [varchar](250) NULL,
  [apl_fecha_creacion] [datetime] NOT NULL,
  [apl_fecha_actualizacion] [datetime] NULL,
  CONSTRAINT [PK_Cer] PRIMARY KEY CLUSTERED ([apl_id])
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO