﻿CREATE TABLE [dbo].[LoginInfo] (
  [login_id] [bigint] IDENTITY,
  [login_fecha_inicio] [datetime] NOT NULL,
  [login_fecha_fin] [datetime] NULL,
  [login_tipo_id] [bigint] NOT NULL,
  [login_usuario_id] [bigint] NOT NULL,
  [login_ip] [varchar](20) NOT NULL,
  [login_session_id] [varchar](50) NOT NULL,
  CONSTRAINT [PK_LoginInfo] PRIMARY KEY CLUSTERED ([login_id]) WITH (FILLFACTOR = 80)
)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[LoginInfo] WITH NOCHECK
  ADD CONSTRAINT [FK_LoginInfo_TipoLogin] FOREIGN KEY ([login_tipo_id]) REFERENCES [dbo].[TipoLogin] ([id])
GO