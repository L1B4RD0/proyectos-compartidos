﻿CREATE TABLE [dbo].[Usuario] (
  [usu_id] [bigint] IDENTITY,
  [usu_nombre] [varchar](200) NOT NULL,
  [usu_email] [varchar](100) NOT NULL,
  [usu_login] [varchar](50) NOT NULL,
  [usu_hash] [varchar](150) NOT NULL,
  [usu_perfil_id] [bigint] NOT NULL,
  [usu_estado_usuario_id] [bigint] NOT NULL,
  [usu_fecha_exp_clave] [datetime] NOT NULL,
  [usu_fecha_creacion] [datetime] NOT NULL,
  [usu_fecha_actualizacion] [datetime] NULL,
  CONSTRAINT [PK_Usuario] PRIMARY KEY CLUSTERED ([usu_id]),
  CONSTRAINT [IX_Usuario] UNIQUE ([usu_login]),
  CONSTRAINT [IX_Usuario_Email] UNIQUE ([usu_email])
)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[Usuario] WITH NOCHECK
  ADD CONSTRAINT [FK_Usuario_EstadoUsuario] FOREIGN KEY ([usu_estado_usuario_id]) REFERENCES [dbo].[EstadoUsuario] ([id])
GO

ALTER TABLE [dbo].[Usuario] WITH NOCHECK
  ADD CONSTRAINT [FK_Usuario_Perfil] FOREIGN KEY ([usu_perfil_id]) REFERENCES [dbo].[Perfil] ([perf_id])
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'En esta tabla se registra todos los datos inherente a los usuarios de los cuales ella depende, algunas de las tablas de las que Usuario depende son: 
° EstadoUsuario
° Perfil
Estas estan relacionadas directamente con la tabla Usuario.
Ademas usa 24 Procesos almacenados los cuales se muestran en la parte final del documento.', 'SCHEMA', N'dbo', 'TABLE', N'Usuario'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'llave primaria compuesta', 'SCHEMA', N'dbo', 'TABLE', N'Usuario', 'INDEX', N'IX_Usuario'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'llave primaria compuesta', 'SCHEMA', N'dbo', 'TABLE', N'Usuario', 'INDEX', N'IX_Usuario_Email'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'llave primaria compuesta', 'SCHEMA', N'dbo', 'TABLE', N'Usuario', 'INDEX', N'PK_Usuario'
GO