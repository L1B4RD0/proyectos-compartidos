﻿CREATE TABLE [dbo].[UsuarioXCliente] (
  [cliente_id] [bigint] NOT NULL,
  [usuario_id] [bigint] NOT NULL,
  CONSTRAINT [PK_UsuarioXCliente] PRIMARY KEY CLUSTERED ([cliente_id], [usuario_id])
)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[UsuarioXCliente] WITH NOCHECK
  ADD CONSTRAINT [FK_UsuarioXCliente_Usuario] FOREIGN KEY ([usuario_id]) REFERENCES [dbo].[Usuario] ([usu_id])
GO