﻿CREATE TABLE [dbo].[MarcaTerminal] (
  [id] [bigint] IDENTITY,
  [descripcion] [varchar](50) NOT NULL,
  CONSTRAINT [PK_MarcaTerminal] PRIMARY KEY CLUSTERED ([id])
)
ON [PRIMARY]
GO