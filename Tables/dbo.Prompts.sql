﻿CREATE TABLE [dbo].[Prompts] (
  [Id_Prompts] [varchar](50) NOT NULL,
  [Codigo_Prompts] [varchar](50) NULL,
  [Nombre_Prompts] [varchar](50) NULL,
  [Tipo_Dato] [int] NULL,
  [Longitud_Minima] [varchar](50) NULL,
  [Longitud_Maxima] [varchar](50) NULL,
  [Imprimir_Prompt] [int] NULL,
  [Sumar_Totales] [int] NULL,
  [Valor_Negativo] [int] NULL,
  [Venta] [int] NULL,
  [Venta_Gasolinera] [int] NULL,
  [Diferido] [int] NULL,
  [Pagos_Varios] [int] NULL,
  [PLANTILLA_ID] [varchar](50) NOT NULL,
  PRIMARY KEY CLUSTERED ([PLANTILLA_ID], [Id_Prompts])
)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[Prompts]
  ADD CONSTRAINT [FK_PLANTILLAS_PROMPTS] FOREIGN KEY ([PLANTILLA_ID]) REFERENCES [dbo].[PLANTILLAS_PROMPTS] ([PLANTILLA_ID])
GO

ALTER TABLE [dbo].[Prompts]
  ADD CONSTRAINT [fkDiferido] FOREIGN KEY ([Diferido]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO

ALTER TABLE [dbo].[Prompts]
  ADD CONSTRAINT [fkImprimirPrompt] FOREIGN KEY ([Imprimir_Prompt]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO

ALTER TABLE [dbo].[Prompts]
  ADD CONSTRAINT [fkPagosVarios] FOREIGN KEY ([Pagos_Varios]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO

ALTER TABLE [dbo].[Prompts]
  ADD CONSTRAINT [fkSumarTotales] FOREIGN KEY ([Sumar_Totales]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO

ALTER TABLE [dbo].[Prompts]
  ADD CONSTRAINT [fkTipoDato] FOREIGN KEY ([Tipo_Dato]) REFERENCES [dbo].[TipoDato] ([Id_Tipo_Dato])
GO

ALTER TABLE [dbo].[Prompts]
  ADD CONSTRAINT [fkValorNegativo] FOREIGN KEY ([Valor_Negativo]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO

ALTER TABLE [dbo].[Prompts]
  ADD CONSTRAINT [fkVenta] FOREIGN KEY ([Venta]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO

ALTER TABLE [dbo].[Prompts]
  ADD CONSTRAINT [fkVentaGasolinera] FOREIGN KEY ([Venta_Gasolinera]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO