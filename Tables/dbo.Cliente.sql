﻿CREATE TABLE [dbo].[Cliente] (
  [cli_id] [bigint] IDENTITY,
  [cli_nombre] [varchar](20) NOT NULL,
  [cli_tercero_id] [bigint] NULL,
  [cli_descripcion] [varchar](150) NULL,
  [cli_ip_descarga] [varchar](15) NOT NULL,
  [cli_puerto_descarga] [varchar](5) NOT NULL,
  [cli_tipo_estado_id] [bigint] NOT NULL,
  [cli_fecha_creacion] [datetime] NOT NULL,
  [cli_fecha_actualizacion] [datetime] NULL,
  [cli_autoaprendizaje] [bit] NULL,
  CONSTRAINT [PK_Cliente] PRIMARY KEY CLUSTERED ([cli_id])
)
ON [PRIMARY]
GO

CREATE UNIQUE INDEX [IX_Cliente_Name]
  ON [dbo].[Cliente] ([cli_nombre])
  ON [PRIMARY]
GO

ALTER TABLE [dbo].[Cliente] WITH NOCHECK
  ADD CONSTRAINT [FK_Cliente_Tercero] FOREIGN KEY ([cli_tercero_id]) REFERENCES [dbo].[Tercero] ([terc_id])
GO

ALTER TABLE [dbo].[Cliente] WITH NOCHECK
  ADD CONSTRAINT [FK_Cliente_TipoEstado] FOREIGN KEY ([cli_tipo_estado_id]) REFERENCES [dbo].[TipoEstado] ([id])
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'En esta tabla se registra todos los datos inherente a la tabla Cliente, algunas de las tablas de las que la Cliente depende son: 
° Tercero
° TipoEstado
Estas estan relacionadas directamente con la tabla Cliente.
Ademas usa 15  Procesos almacenados los cuales se muestran en la parte final del documento.', 'SCHEMA', N'dbo', 'TABLE', N'Cliente'
GO