﻿CREATE TABLE [dbo].[PerfilXOpcion] (
  [perf_id] [bigint] NOT NULL,
  [opc_id] [bigint] NOT NULL,
  CONSTRAINT [PK_PerfilXOpcion] PRIMARY KEY CLUSTERED ([perf_id], [opc_id])
)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[PerfilXOpcion] WITH NOCHECK
  ADD CONSTRAINT [FK_PerfilXOpcion_Opcion] FOREIGN KEY ([opc_id]) REFERENCES [dbo].[Opcion] ([opc_id])
GO

ALTER TABLE [dbo].[PerfilXOpcion] WITH NOCHECK
  ADD CONSTRAINT [FK_PerfilXOpcion_Perfil] FOREIGN KEY ([perf_id]) REFERENCES [dbo].[Perfil] ([perf_id])
GO