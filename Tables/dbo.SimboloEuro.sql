﻿CREATE TABLE [dbo].[SimboloEuro] (
  [Id_Simbolo] [int] NOT NULL,
  [Descripcion] [varchar](20) NULL,
  PRIMARY KEY CLUSTERED ([Id_Simbolo])
)
ON [PRIMARY]
GO