﻿CREATE TABLE [dbo].[Tercero] (
  [terc_id] [bigint] IDENTITY,
  [terc_tipo_identificacion_id] [bigint] NOT NULL,
  [terc_numero_identificacion] [varchar](20) NOT NULL,
  [terc_nombre] [varchar](100) NOT NULL,
  [terc_direccion] [varchar](100) NULL,
  [terc_celular] [varchar](10) NULL,
  [terc_telefono_fijo] [varchar](20) NULL,
  [terc_correo_electronico] [varchar](50) NULL,
  [terc_fecha_creacion] [datetime] NOT NULL,
  [terc_fecha_actualizacion] [datetime] NULL,
  CONSTRAINT [PK_Tercero] PRIMARY KEY CLUSTERED ([terc_id])
)
ON [PRIMARY]
GO

CREATE UNIQUE INDEX [IDX_NumeroIdentificacion]
  ON [dbo].[Tercero] ([terc_numero_identificacion])
  ON [PRIMARY]
GO

ALTER TABLE [dbo].[Tercero] WITH NOCHECK
  ADD CONSTRAINT [FK_Tercero_TipoIdentificacion] FOREIGN KEY ([terc_tipo_identificacion_id]) REFERENCES [dbo].[TipoIdentificacion] ([id])
GO