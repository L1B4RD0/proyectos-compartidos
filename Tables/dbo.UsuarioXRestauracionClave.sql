﻿CREATE TABLE [dbo].[UsuarioXRestauracionClave] (
  [rest_id] [bigint] IDENTITY,
  [rest_usuario_id] [bigint] NOT NULL,
  [rest_fecha] [datetime] NOT NULL,
  [rest_ip] [varchar](20) NOT NULL,
  [rest_rand] [varchar](50) NOT NULL,
  [rest_estado] [bit] NOT NULL,
  CONSTRAINT [PK_UsuarioXRestauracionClave] PRIMARY KEY CLUSTERED ([rest_id])
)
ON [PRIMARY]
GO