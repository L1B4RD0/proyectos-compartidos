﻿CREATE TABLE [dbo].[LogErrorBaseDatos] (
  [bderr_id] [bigint] IDENTITY,
  [bderr_fecha] [datetime] NOT NULL,
  [bderr_nombre_funcion] [varchar](250) NOT NULL,
  [bderr_mensaje_error] [varchar](4000) NOT NULL,
  [bderr_datos_adicionales] [varchar](max) NOT NULL,
  CONSTRAINT [PK_LogErrorBaseDatos] PRIMARY KEY CLUSTERED ([bderr_id])
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO