﻿CREATE TABLE [dbo].[TipoMontoFijo] (
  [Id_Tipo_Monto] [int] NOT NULL,
  [Descripcion] [varchar](50) NULL,
  PRIMARY KEY CLUSTERED ([Id_Tipo_Monto])
)
ON [PRIMARY]
GO