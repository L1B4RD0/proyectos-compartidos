﻿CREATE TABLE [dbo].[TerminalXEMVKey] (
  [key_id] [bigint] IDENTITY,
  [key_terminal_id] [bigint] NOT NULL,
  [key_datetime] [datetime] NOT NULL,
  [key_index] [varchar](2) NOT NULL,
  [key_application_id] [varchar](10) NOT NULL,
  [key_exponent] [varchar](8) NOT NULL,
  [key_size] [varchar](8) NOT NULL,
  [key_content] [varchar](512) NOT NULL,
  [key_expiry_date] [varchar](4) NOT NULL,
  [key_effective_date] [varchar](4) NOT NULL,
  [key_checksum] [varchar](2) NOT NULL,
  CONSTRAINT [PK_TerminalXEMVKey] PRIMARY KEY CLUSTERED ([key_id])
)
ON [PRIMARY]
GO