﻿CREATE TABLE [dbo].[TCONF] (
  [COMERCIO_ID] [varchar](50) NULL,
  [NOMBRE_COMERCIO] [varchar](50) NULL,
  [DIRECCION_PRINCIPAL] [varchar](50) NULL,
  [DIRECCION_SECUNDARIA] [varchar](50) NULL,
  [TELEFONO_COMERCIO] [varchar](50) NULL,
  [LINEA_AUX] [varchar](50) NULL,
  [CIUDAD] [varchar](50) NULL,
  [RUC] [varchar](50) NULL,
  [HOST] [varchar](50) NULL,
  [CARD_ACCP_TERM] [varchar](50) NULL,
  [CARD_ACCP_MERCH] [varchar](50) NULL,
  [NUMERO_LOTE] [varchar](50) NULL,
  [MONEDA] [varchar](50) NULL,
  [REVERSO] [int] NULL,
  [HABILITAR_IMPUESTO] [int] NULL,
  [LABEL_IMPUESTO] [varchar](50) NULL,
  [TIPO_IMPUESTO] [varchar](50) NULL,
  [PORCENTAJE_MAXIMO_IMPUESTO] [varchar](50) NULL,
  [TARIFA_CERO] [int] NULL,
  [CAPTURA_ELECTRONICA] [int] NULL,
  [PIN_BYPASS] [int] NULL,
  [CAJA_REGISTRADORA] [int] NULL,
  [ENTRADA_MANUAL_PREAUTO] [int] NULL,
  [MONTO_MAXIMO_TRANSACCION] [varchar](50) NULL,
  [MONTO_MINIMO_TRANSACCION] [varchar](50) NULL,
  [HABILITAR_SERVICIO] [int] NULL,
  [LABEL_SERVICIO] [varchar](50) NULL,
  [TIPO_SERVICIO] [varchar](50) NULL,
  [PORCENTAJE_MAXIMO_SERVICIO] [varchar](50) NULL,
  [HABILITAR_ICE] [int] NULL,
  [LABEL_ICE] [varchar](50) NULL,
  [TIPO_ICE] [varchar](50) NULL,
  [PORCENTAJE_MAXIMO_ICE] [varchar](50) NULL,
  [TRANSACCION_VENTA] [int] NULL,
  [TRANSACCION_DIFERIDO] [int] NULL,
  [TRANSACCION_ANULACION] [int] NULL,
  [TRANSACCION_PAGOS_VARIOS] [int] NULL,
  [TRANSACCION_CASH_OVER] [int] NULL,
  [TRANSACCION_PRE_VOUCHER] [int] NULL,
  [TRANSACCION_PAGOS_ELECTRONICOS] [int] NULL,
  [TRANSACCION_PRE_AUTO] [int] NULL,
  [HABILITAR_PROPINA] [int] NULL,
  [LABEL_PROPINA] [varchar](50) NULL,
  [TIPO_PROPINA] [varchar](50) NULL,
  [PORCENTAJE_MAXIMO_PROPINA] [varchar](50) NULL,
  [HABILITAR_OTROS_IMPUESTOS] [int] NULL,
  [LABEL_OTROS_IMPUESTOS] [varchar](50) NULL,
  [TIPO_OTROS_IMPUESTOS] [varchar](50) NULL,
  [PORCENTAJE_MAXIMO] [varchar](50) NULL,
  [HABILITA_MONTO_FIJO] [int] NULL,
  [VALOR_MONTO_FIJO] [varchar](50) NULL,
  [NO_PERMITIR_2_TRANS_MISMO_TARJ] [int] NULL,
  [HABILITAR_FIRMA] [int] NULL,
  [CLAVE_COMERCIO] [varchar](50) NULL,
  [NUM_SERIAL] [varchar](50) NULL,
  [HEADER_COMERCIO] [varchar](50) NULL,
  [HEADER_DIRECCION_1] [varchar](50) NULL,
  [HEADER_DIRECCION_2] [varchar](50) NULL,
  [HEADER_TELEFONO] [varchar](50) NULL,
  [HEADER_LINEA_AUX] [varchar](50) NULL,
  [FOOTER_LINEA_1] [varchar](50) NULL,
  [FOOTER_LINEA_2] [varchar](50) NULL,
  [FOOTER_LINEA_3] [varchar](50) NULL,
  [FOOTER_LINEA_4] [varchar](50) NULL,
  [FOOTER_LINEA_5] [varchar](50) NULL,
  [FOOTER_LINEA_6] [varchar](50) NULL,
  [FOOTER_LINEA_7] [varchar](50) NULL,
  [FOOTER_LINEA_8] [varchar](50) NULL,
  [FOOTER_LINEA_9] [varchar](50) NULL,
  [FOOTER_LINEA_10] [varchar](50) NULL,
  [CLAVE_TECNICO] [varchar](50) NULL,
  [SIMBOLO_MONEDA_LOCAL] [varchar](50) NULL,
  [SIMBOLO_DOLAR] [varchar](50) NULL,
  [SIMBOLO_EURO] [int] NULL,
  [DIAS_CIERRE] [varchar](50) NULL,
  [HORAS_ECHO] [varchar](50) NULL,
  [HABILITA_CIERRE] [int] NULL,
  [HABILITA_CIERRE_AUTOMATICO_DIA] [int] NULL,
  [HORA_CIERRE] [varchar](50) NULL,
  [HABILITA_PLC] [int] NULL,
  [NOTAS] [varchar](50) NULL,
  [TIPO_INICIALIZACION] [varchar](50) NULL,
  [HABILITA_IMPRIMIR_RECIBO] [int] NULL,
  [HABILITA_IMPRIMIR_CARD_HOLDER] [int] NULL,
  [HABILITA_IMPRIMIR_COD_BARRAS] [int] NULL,
  [TIPO_CODIGO_BARRAS] [int] NULL,
  [COPIA_VOUCHER] [int] NULL,
  [MENSAJE_CIERRE1] [varchar](50) NULL,
  [MENSAJE_CIERRE2] [varchar](50) NULL,
  [PERFIL] [varchar](50) NULL,
  [TID] [varchar](50) NOT NULL,
  [TERMINAL_REC_NO] [varchar](50) NULL,
  PRIMARY KEY CLUSTERED ([TID])
)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[TCONF]
  ADD CONSTRAINT [fkCajaRegistradora] FOREIGN KEY ([CAJA_REGISTRADORA]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO

ALTER TABLE [dbo].[TCONF]
  ADD CONSTRAINT [fkCapturaElectronica] FOREIGN KEY ([CAPTURA_ELECTRONICA]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO

ALTER TABLE [dbo].[TCONF]
  ADD CONSTRAINT [fkCopiaVoucher] FOREIGN KEY ([COPIA_VOUCHER]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO

ALTER TABLE [dbo].[TCONF]
  ADD CONSTRAINT [fkEntradaManualPreauto] FOREIGN KEY ([ENTRADA_MANUAL_PREAUTO]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO

ALTER TABLE [dbo].[TCONF]
  ADD CONSTRAINT [fkHabilitaCierre] FOREIGN KEY ([HABILITA_CIERRE]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO

ALTER TABLE [dbo].[TCONF]
  ADD CONSTRAINT [fkHabilitaCierreAuto] FOREIGN KEY ([HABILITA_CIERRE_AUTOMATICO_DIA]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO

ALTER TABLE [dbo].[TCONF]
  ADD CONSTRAINT [fkHabilitaImprimirCardHolder] FOREIGN KEY ([HABILITA_IMPRIMIR_CARD_HOLDER]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO

ALTER TABLE [dbo].[TCONF]
  ADD CONSTRAINT [fkHabilitaImprimirCodBarras] FOREIGN KEY ([HABILITA_IMPRIMIR_COD_BARRAS]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO

ALTER TABLE [dbo].[TCONF]
  ADD CONSTRAINT [fkHabilitaImprimirRecibo] FOREIGN KEY ([HABILITA_IMPRIMIR_RECIBO]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO

ALTER TABLE [dbo].[TCONF]
  ADD CONSTRAINT [fkHabilitaImpuesto] FOREIGN KEY ([HABILITAR_IMPUESTO]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO

ALTER TABLE [dbo].[TCONF]
  ADD CONSTRAINT [fkHabilitaMontoFijo] FOREIGN KEY ([HABILITA_MONTO_FIJO]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO

ALTER TABLE [dbo].[TCONF]
  ADD CONSTRAINT [fkHabilitaPlc] FOREIGN KEY ([HABILITA_PLC]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO

ALTER TABLE [dbo].[TCONF]
  ADD CONSTRAINT [fkHabilitarFirma] FOREIGN KEY ([HABILITAR_FIRMA]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO

ALTER TABLE [dbo].[TCONF]
  ADD CONSTRAINT [fkHabilitarIce] FOREIGN KEY ([HABILITAR_ICE]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO

ALTER TABLE [dbo].[TCONF]
  ADD CONSTRAINT [fkHabilitarOtrosImpuestos] FOREIGN KEY ([HABILITAR_OTROS_IMPUESTOS]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO

ALTER TABLE [dbo].[TCONF]
  ADD CONSTRAINT [fkHabilitarPropina] FOREIGN KEY ([HABILITAR_PROPINA]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO

ALTER TABLE [dbo].[TCONF]
  ADD CONSTRAINT [fkHabilitarServicio] FOREIGN KEY ([HABILITAR_SERVICIO]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO

ALTER TABLE [dbo].[TCONF]
  ADD CONSTRAINT [fkNoPermitir2Trans] FOREIGN KEY ([NO_PERMITIR_2_TRANS_MISMO_TARJ]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO

ALTER TABLE [dbo].[TCONF]
  ADD CONSTRAINT [fkPerfil] FOREIGN KEY ([PERFIL]) REFERENCES [dbo].[perfiles] ([perfil_id])
GO

ALTER TABLE [dbo].[TCONF]
  ADD CONSTRAINT [fkPinBypass] FOREIGN KEY ([PIN_BYPASS]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO

ALTER TABLE [dbo].[TCONF]
  ADD CONSTRAINT [fkReverso] FOREIGN KEY ([REVERSO]) REFERENCES [dbo].[Reverso] ([Id_reverso])
GO

ALTER TABLE [dbo].[TCONF]
  ADD CONSTRAINT [fkSimboloEuro] FOREIGN KEY ([SIMBOLO_EURO]) REFERENCES [dbo].[SimboloEuro] ([Id_Simbolo])
GO

ALTER TABLE [dbo].[TCONF]
  ADD CONSTRAINT [fkTarifaCero] FOREIGN KEY ([TARIFA_CERO]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO

ALTER TABLE [dbo].[TCONF]
  ADD CONSTRAINT [fkTipoCodigoBarras] FOREIGN KEY ([TIPO_CODIGO_BARRAS]) REFERENCES [dbo].[CodigoBarras] ([Id_Codigo])
GO

ALTER TABLE [dbo].[TCONF]
  ADD CONSTRAINT [fkTipoIce] FOREIGN KEY ([TIPO_ICE]) REFERENCES [dbo].[TipoSeleccion] ([Descripcion])
GO

ALTER TABLE [dbo].[TCONF]
  ADD CONSTRAINT [fkTipoImpuesto] FOREIGN KEY ([TIPO_IMPUESTO]) REFERENCES [dbo].[TipoSeleccion] ([Descripcion])
GO

ALTER TABLE [dbo].[TCONF]
  ADD CONSTRAINT [fkTipoOtrosImpuestos] FOREIGN KEY ([TIPO_OTROS_IMPUESTOS]) REFERENCES [dbo].[TipoSeleccion] ([Descripcion])
GO

ALTER TABLE [dbo].[TCONF]
  ADD CONSTRAINT [fkTipoPropina] FOREIGN KEY ([TIPO_PROPINA]) REFERENCES [dbo].[TipoSeleccion] ([Descripcion])
GO

ALTER TABLE [dbo].[TCONF]
  ADD CONSTRAINT [fkTipoServicio] FOREIGN KEY ([TIPO_SERVICIO]) REFERENCES [dbo].[TipoSeleccion] ([Descripcion])
GO

ALTER TABLE [dbo].[TCONF]
  ADD CONSTRAINT [fkTransaccionAnulacion] FOREIGN KEY ([TRANSACCION_ANULACION]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO

ALTER TABLE [dbo].[TCONF]
  ADD CONSTRAINT [fkTransaccionCashOver] FOREIGN KEY ([TRANSACCION_CASH_OVER]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO

ALTER TABLE [dbo].[TCONF]
  ADD CONSTRAINT [fkTransaccionDiferido] FOREIGN KEY ([TRANSACCION_DIFERIDO]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO

ALTER TABLE [dbo].[TCONF]
  ADD CONSTRAINT [fkTransaccionPagosElectronicos] FOREIGN KEY ([TRANSACCION_PAGOS_ELECTRONICOS]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO

ALTER TABLE [dbo].[TCONF]
  ADD CONSTRAINT [fkTransaccionPagosVarios] FOREIGN KEY ([TRANSACCION_PAGOS_VARIOS]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO

ALTER TABLE [dbo].[TCONF]
  ADD CONSTRAINT [fkTransaccionPreAuto] FOREIGN KEY ([TRANSACCION_PRE_AUTO]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO

ALTER TABLE [dbo].[TCONF]
  ADD CONSTRAINT [fkTransaccionPreVoucher] FOREIGN KEY ([TRANSACCION_PRE_VOUCHER]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO

ALTER TABLE [dbo].[TCONF]
  ADD CONSTRAINT [fkTransaccionVenta] FOREIGN KEY ([TRANSACCION_VENTA]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO