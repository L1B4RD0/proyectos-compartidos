﻿CREATE TABLE [dbo].[TipoDato] (
  [Id_Tipo_Dato] [int] NOT NULL,
  [Descripcion] [varchar](50) NULL,
  PRIMARY KEY CLUSTERED ([Id_Tipo_Dato])
)
ON [PRIMARY]
GO