﻿CREATE TABLE [dbo].[Terminal] (
  [ter_id] [bigint] IDENTITY,
  [ter_serial] [varchar](50) NOT NULL,
  [ter_tipo_terminal_id] [bigint] NOT NULL,
  [ter_marca_terminal_id] [bigint] NOT NULL,
  [ter_modelo_terminal_id] [bigint] NOT NULL,
  [ter_interface_terminal_id] [bigint] NOT NULL,
  [ter_serial_sim] [varchar](50) NULL,
  [ter_telefono_sim] [varchar](50) NULL,
  [ter_imei] [varchar](50) NULL,
  [ter_serial_cargador] [varchar](50) NULL,
  [ter_serial_bateria] [varchar](50) NULL,
  [ter_serial_lector_biometrico] [varchar](50) NULL,
  [ter_descripcion] [varchar](150) NULL,
  [ter_grupo_id] [bigint] NOT NULL,
  [ter_tercero_id] [bigint] NULL,
  [ter_prioridad_grupo] [bit] NOT NULL,
  [ter_actualizar] [bit] NOT NULL,
  [ter_fecha_programada_actualizacion] [datetime] NULL,
  [ter_ip_descarga] [varchar](15) NULL,
  [ter_puerto_descarga] [varchar](5) NULL,
  [ter_estado_actualizacion_id] [bigint] NOT NULL,
  [ter_mensaje_actualizacion] [varchar](1000) NULL,
  [ter_tipo_estado_id] [bigint] NOT NULL,
  [ter_validar_imei] [bit] NOT NULL,
  [ter_validar_sim] [bit] NOT NULL,
  [ter_latitud_gps] [varchar](50) NULL,
  [ter_longitud_gps] [varchar](50) NULL,
  [ter_apn] [varchar](50) NULL,
  [ter_nivel_gprs] [int] NULL,
  [ter_nivel_bateria] [int] NULL,
  [ter_aplicaciones_instaladas] [varchar](1000) NULL,
  [ter_aplicaciones_permitidas] [varchar](500) NULL,
  [ter_ram] [varchar](50) NULL,
  [ter_ip_dispositivo] [varchar](50) NULL,
  [ter_clave_bloqueo] [varchar](50) NULL,
  [ter_mensaje_desplegar] [varchar](50) NULL,
  [ter_bloqueo] [bit] NULL,
  [ter_tiempo] [varchar](50) NULL,
  [ter_id_heracles] [varchar](50) NULL,
  [ter_nombre_heracles] [varchar](200) NULL,
  [ter_ciudad_heracles] [varchar](200) NULL,
  [ter_region_heracles] [varchar](200) NULL,
  [ter_agencia_heracles] [varchar](200) NULL,
  [ter_componente_1] [varchar](100) NULL,
  [ter_componente_1_inyectado] [bit] NULL,
  [ter_fecha_componente1] [datetime] NULL,
  [ter_componente_2] [varchar](100) NULL,
  [ter_componente_2_inyectado] [bit] NULL,
  [ter_fecha_componente2] [datetime] NULL,
  [ter_componentes_inyectados] [bit] NULL CONSTRAINT [DF_Terminal_ter_componentes_inyectados] DEFAULT (0),
  [ter_llave_cargada] [bit] NULL CONSTRAINT [DF_Terminal_ter_llave_cargada] DEFAULT (0),
  [ter_fecha_ultima_carga_llave] [datetime] NULL,
  [ter_recargar_llave] [bit] NULL,
  [ter_fecha_actualizacion_auto] [datetime] NULL,
  [ter_fecha_creacion] [datetime] NOT NULL,
  [ter_fecha_actualizacion] [datetime] NULL,
  [ter_hora] [datetime] NULL,
  [ter_aplicaciones_pendientes_sp] [varchar](999) NULL,
  [ter_fecha_descarga_sp] [varchar](20) NULL,
  [ter_ip_descarga_sp] [varchar](20) NULL,
  [ter_puerto_descarga_sp] [varchar](5) NULL,
  [ter_fecha_consulta_sp] [datetime] NULL,
  [ter_aplicacion_kiosko] [varchar](500) NULL,
  [ter_blocking_mode] [bit] NULL,
  [ter_update_now] [bit] NULL,
  [ter_codigo_producto] [varchar](50) NULL,
  [ter_flag_inicializacion] [bit] NULL,
  [ter_fecha_ultima_inicializacion] [datetime] NULL,
  [ter_mac] [varchar](50) NULL,
  [ter_UID] [varchar](50) NULL,
  [ter_nombre_pos] [varchar](50) NULL,
  CONSTRAINT [PK_Terminal] PRIMARY KEY CLUSTERED ([ter_id]) WITH (FILLFACTOR = 80)
)
ON [PRIMARY]
GO

CREATE UNIQUE INDEX [IDX_Serial]
  ON [dbo].[Terminal] ([ter_serial])
  WITH (FILLFACTOR = 80)
  ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
--Instead of Trigger
create TRIGGER [dbo].[SET_IMEI] ON [dbo].[Terminal]
instead OF INSERT AS
BEGIN

INSERT INTO [dbo].[Terminal]
           ([ter_serial]
           ,[ter_tipo_terminal_id]
           ,[ter_marca_terminal_id]
           ,[ter_modelo_terminal_id]
           ,[ter_interface_terminal_id]
           ,[ter_serial_sim]
           ,[ter_telefono_sim]
           ,[ter_imei]
           ,[ter_serial_cargador]
           ,[ter_serial_bateria]
           ,[ter_serial_lector_biometrico]
           ,[ter_descripcion]
           ,[ter_grupo_id]
           ,[ter_tercero_id]
           ,[ter_prioridad_grupo]
           ,[ter_actualizar]
           ,[ter_fecha_programada_actualizacion]
           ,[ter_ip_descarga]
           ,[ter_puerto_descarga]
           ,[ter_estado_actualizacion_id]
           ,[ter_mensaje_actualizacion]
           ,[ter_tipo_estado_id]
           ,[ter_validar_imei]
           ,[ter_validar_sim]
           ,[ter_latitud_gps]
           ,[ter_longitud_gps]
           ,[ter_apn]
           ,[ter_nivel_gprs]
           ,[ter_nivel_bateria]
           ,[ter_aplicaciones_instaladas]
           ,[ter_aplicaciones_permitidas]
           ,[ter_ram]
           ,[ter_ip_dispositivo]
           ,[ter_clave_bloqueo]
           ,[ter_mensaje_desplegar]
           ,[ter_bloqueo]
           ,[ter_tiempo]
           ,[ter_id_heracles]
           ,[ter_nombre_heracles]
           ,[ter_ciudad_heracles]
           ,[ter_region_heracles]
           ,[ter_agencia_heracles]
           ,[ter_componente_1]
           ,[ter_componente_1_inyectado]
           ,[ter_fecha_componente1]
           ,[ter_componente_2]
           ,[ter_componente_2_inyectado]
           ,[ter_fecha_componente2]
           ,[ter_componentes_inyectados]
           ,[ter_llave_cargada]
           ,[ter_fecha_ultima_carga_llave]
           ,[ter_recargar_llave]
           ,[ter_fecha_actualizacion_auto]
           ,[ter_fecha_creacion]
           ,[ter_fecha_actualizacion]
           ,[ter_hora]
           ,[ter_aplicaciones_pendientes_sp]
           ,[ter_fecha_descarga_sp]
           ,[ter_ip_descarga_sp]
           ,[ter_puerto_descarga_sp]
           ,[ter_fecha_consulta_sp]
           ,[ter_aplicacion_kiosko]
           ,[ter_blocking_mode]
           ,[ter_update_now]
		   ,[ter_mac]
		   ,[ter_UID]
		   ,[ter_nombre_pos])
     SELECT
           inserted.ter_serial
           ,inserted.ter_tipo_terminal_id
           ,inserted.ter_marca_terminal_id
           ,inserted.ter_modelo_terminal_id
           ,inserted.ter_interface_terminal_id
           ,inserted.ter_serial_sim
           ,inserted.ter_telefono_sim
		   ,(CASE when inserted.ter_imei is null then 'T' + inserted.ter_serial
		         when inserted.ter_imei = '' then 'T' + inserted.ter_serial
				 else inserted.ter_imei
		   END)
           ,inserted.ter_serial_cargador
           ,inserted.ter_serial_bateria
           ,inserted.ter_serial_lector_biometrico
           ,inserted.ter_descripcion
           ,inserted.ter_grupo_id
           ,inserted.ter_tercero_id
           ,inserted.ter_prioridad_grupo
           ,inserted.ter_actualizar
           ,inserted.ter_fecha_programada_actualizacion
           ,inserted.ter_ip_descarga
           ,inserted.ter_puerto_descarga
           ,inserted.ter_estado_actualizacion_id
           ,inserted.ter_mensaje_actualizacion
           ,inserted.ter_tipo_estado_id
           ,inserted.ter_validar_imei
           ,inserted.ter_validar_sim
           ,inserted.ter_latitud_gps
           ,inserted.ter_longitud_gps
           ,inserted.ter_apn
           ,inserted.ter_nivel_gprs
           ,inserted.ter_nivel_bateria
           ,inserted.ter_aplicaciones_instaladas
           ,inserted.ter_aplicaciones_permitidas
           ,inserted.ter_ram
           ,inserted.ter_ip_dispositivo
           ,inserted.ter_clave_bloqueo
           ,inserted.ter_mensaje_desplegar
           ,inserted.ter_bloqueo
           ,inserted.ter_tiempo
           ,inserted.ter_id_heracles
           ,inserted.ter_nombre_heracles
           ,inserted.ter_ciudad_heracles
           ,inserted.ter_region_heracles
           ,inserted.ter_agencia_heracles
           ,inserted.ter_componente_1
           ,inserted.ter_componente_1_inyectado
           ,inserted.ter_fecha_componente1
           ,inserted.ter_componente_2
           ,inserted.ter_componente_2_inyectado
           ,inserted.ter_fecha_componente2
           ,inserted.ter_componentes_inyectados
           ,inserted.ter_llave_cargada
           ,inserted.ter_fecha_ultima_carga_llave
           ,inserted.ter_recargar_llave
           ,inserted.ter_fecha_actualizacion_auto
           ,inserted.ter_fecha_creacion
           ,inserted.ter_fecha_actualizacion
           ,inserted.ter_hora
           ,inserted.ter_aplicaciones_pendientes_sp
           ,inserted.ter_fecha_descarga_sp
           ,inserted.ter_ip_descarga_sp
           ,inserted.ter_puerto_descarga_sp
           ,inserted.ter_fecha_consulta_sp
           ,inserted.ter_aplicacion_kiosko
           ,inserted.ter_blocking_mode
           ,inserted.ter_update_now 
		   ,inserted.ter_mac
		   ,inserted.ter_UID
		   ,inserted.ter_nombre_pos
		   FROM inserted


END
GO

ALTER TABLE [dbo].[Terminal] WITH NOCHECK
  ADD CONSTRAINT [FK_Terminal_EstadoActualizacion] FOREIGN KEY ([ter_estado_actualizacion_id]) REFERENCES [dbo].[EstadoActualizacion] ([id])
GO

ALTER TABLE [dbo].[Terminal] WITH NOCHECK
  ADD CONSTRAINT [FK_Terminal_Grupo] FOREIGN KEY ([ter_grupo_id]) REFERENCES [dbo].[Grupo] ([gru_id])
GO

ALTER TABLE [dbo].[Terminal] WITH NOCHECK
  ADD CONSTRAINT [FK_Terminal_InterfaceTerminal] FOREIGN KEY ([ter_interface_terminal_id]) REFERENCES [dbo].[InterfaceTerminal] ([id])
GO

ALTER TABLE [dbo].[Terminal] WITH NOCHECK
  ADD CONSTRAINT [FK_Terminal_MarcaTerminal] FOREIGN KEY ([ter_marca_terminal_id]) REFERENCES [dbo].[MarcaTerminal] ([id])
GO

ALTER TABLE [dbo].[Terminal] WITH NOCHECK
  ADD CONSTRAINT [FK_Terminal_ModeloTerminal] FOREIGN KEY ([ter_modelo_terminal_id]) REFERENCES [dbo].[ModeloTerminal] ([id])
GO

ALTER TABLE [dbo].[Terminal] WITH NOCHECK
  ADD CONSTRAINT [FK_Terminal_Tercero] FOREIGN KEY ([ter_tercero_id]) REFERENCES [dbo].[Tercero] ([terc_id])
GO

ALTER TABLE [dbo].[Terminal] WITH NOCHECK
  ADD CONSTRAINT [FK_Terminal_TipoEstado] FOREIGN KEY ([ter_tipo_estado_id]) REFERENCES [dbo].[TipoEstado] ([id])
GO

ALTER TABLE [dbo].[Terminal] WITH NOCHECK
  ADD CONSTRAINT [FK_Terminal_TipoTerminal] FOREIGN KEY ([ter_tipo_terminal_id]) REFERENCES [dbo].[TipoTerminal] ([id])
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Disparador, se ejecuta al momento de realizar un inserción en la tambla.', 'SCHEMA', N'dbo', 'TABLE', N'Terminal', 'TRIGGER', N'SET_IMEI'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'En esta tabla se registra todos los datos inherente a los terminales de los cuales ella depende, algunas de las tablas de las que terminal depende son: 
° EstadoActualizacion
° Grupo
° InterfaceTerminal
° MarcaTerminal
° ModeloTerminal
° Tercero
° TipoEstado
°  TipoTerminal
Ademas usa 66 Procesos almacenados los cuales se muestran en la parte final del documento.', 'SCHEMA', N'dbo', 'TABLE', N'Terminal'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'identificador serial unico', 'SCHEMA', N'dbo', 'TABLE', N'Terminal', 'INDEX', N'IDX_Serial'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'llave primaria ', 'SCHEMA', N'dbo', 'TABLE', N'Terminal', 'INDEX', N'PK_Terminal'
GO