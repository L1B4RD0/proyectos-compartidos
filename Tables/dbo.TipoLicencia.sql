﻿CREATE TABLE [dbo].[TipoLicencia] (
  [id] [bigint] IDENTITY,
  [descripcion] [varchar](50) NULL,
  [numero_dias] [int] NOT NULL,
  CONSTRAINT [PK_TipoLicencia] PRIMARY KEY CLUSTERED ([id])
)
ON [PRIMARY]
GO