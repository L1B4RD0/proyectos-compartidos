﻿CREATE TABLE [dbo].[CAPKS] (
  [PLANTILLA_ID] [varchar](50) NOT NULL,
  [KEY_ID] [varchar](2) NOT NULL,
  [KEY_RID] [varchar](20) NOT NULL,
  [KEY_EXPONENT] [varchar](20) NOT NULL,
  [KEY_SIZE] [int] NOT NULL,
  [KEY_MODULE] [varchar](4000) NOT NULL,
  [KEY_EXPIRATION_DATE] [date] NOT NULL,
  [KEY_SHA1] [varchar](40) NOT NULL,
  PRIMARY KEY CLUSTERED ([PLANTILLA_ID], [KEY_ID], [KEY_RID])
)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[CAPKS]
  ADD FOREIGN KEY ([KEY_EXPONENT]) REFERENCES [dbo].[CAPKS_EXPONENTS] ([KEY_EXPONENT])
GO

ALTER TABLE [dbo].[CAPKS]
  ADD FOREIGN KEY ([KEY_RID]) REFERENCES [dbo].[CAPKS_RIDS] ([KEY_RID])
GO

ALTER TABLE [dbo].[CAPKS]
  ADD FOREIGN KEY ([KEY_SIZE]) REFERENCES [dbo].[CAPKS_SIZES] ([KEY_SIZE])
GO

ALTER TABLE [dbo].[CAPKS]
  ADD FOREIGN KEY ([PLANTILLA_ID]) REFERENCES [dbo].[PLANTILLAS_CAPKS] ([PLANTILLA_ID])
GO