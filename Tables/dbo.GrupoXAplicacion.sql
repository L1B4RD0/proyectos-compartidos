﻿CREATE TABLE [dbo].[GrupoXAplicacion] (
  [grupo_id] [bigint] NOT NULL,
  [aplicacion_id] [bigint] NOT NULL,
  [Modelo_id] [bigint] NULL,
  CONSTRAINT [PK_GrupoXAplicacion] PRIMARY KEY CLUSTERED ([grupo_id], [aplicacion_id])
)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[GrupoXAplicacion] WITH NOCHECK
  ADD CONSTRAINT [FK_GrupoXAplicacion_Aplicacion] FOREIGN KEY ([aplicacion_id]) REFERENCES [dbo].[Aplicacion] ([apl_id])
GO

ALTER TABLE [dbo].[GrupoXAplicacion] WITH NOCHECK
  ADD CONSTRAINT [Modelo_id] FOREIGN KEY ([grupo_id], [aplicacion_id]) REFERENCES [dbo].[GrupoXAplicacion] ([grupo_id], [aplicacion_id])
GO