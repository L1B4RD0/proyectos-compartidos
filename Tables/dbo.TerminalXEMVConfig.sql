﻿CREATE TABLE [dbo].[TerminalXEMVConfig] (
  [emv_id] [bigint] IDENTITY,
  [emv_terminal_id] [bigint] NOT NULL,
  [emv_datetime] [datetime] NOT NULL,
  [emv_type] [varchar](2) NOT NULL,
  [emv_config] [varchar](2) NOT NULL,
  [emv_threshold_random_selection] [varchar](8) NOT NULL,
  [emv_target_random_selection] [varchar](2) NOT NULL,
  [emv_max_target_random_selection] [varchar](2) NOT NULL,
  [emv_tac_denial] [varchar](10) NOT NULL,
  [emv_tac_online] [varchar](10) NOT NULL,
  [emv_tac_default] [varchar](10) NOT NULL,
  [emv_application_config] [varchar](1024) NOT NULL,
  CONSTRAINT [PK_TerminalXEMVConfig] PRIMARY KEY CLUSTERED ([emv_id])
)
ON [PRIMARY]
GO