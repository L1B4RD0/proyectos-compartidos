﻿CREATE TABLE [dbo].[Cards] (
  [Id_rango] [varchar](10) NOT NULL,
  [Nombre_Rango] [varchar](50) NULL,
  [Identificador_Rango] [varchar](50) NULL,
  [Id_AS400] [varchar](10) NULL,
  [Rango_Min] [varchar](50) NULL,
  [Rango_Max] [varchar](50) NULL,
  [Grupo_AS400] [varchar](10) NULL,
  [Tipo_Cuenta_Default] [varchar](50) NULL,
  [NII] [varchar](10) NULL,
  [Tipo_Mascara] [int] NULL,
  [Grupo_Prompts] [varchar](50) NULL,
  [Tipo_Monto_fijo] [int] NULL,
  [Nombre_Emisor] [varchar](50) NULL,
  [Imagen_Mostrar] [int] NULL,
  [Manual] [int] NULL,
  [Fecha_Exp] [int] NULL,
  [Check_Dig] [int] NULL,
  [V4DBC] [int] NULL,
  [Ultimos_4] [int] NULL,
  [Tarjeta_Cierre] [int] NULL,
  [Inter_Oper] [int] NULL,
  [Debito] [int] NULL,
  [Pin] [int] NULL,
  [Tipo_de_Cuenta] [int] NULL,
  [Pre_Voucher] [int] NULL,
  [Cash_Over] [int] NULL,
  [Omitir_Emv] [int] NULL,
  [Pin_Service_Code] [int] NULL,
  [Permitir_Tarj_Exp] [int] NULL,
  [TID] [varchar](50) NOT NULL,
  [TERMINAL_REC_NO] [varchar](50) NULL,
  PRIMARY KEY CLUSTERED ([TID], [Id_rango])
)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[Cards]
  ADD CONSTRAINT [fkCashOver] FOREIGN KEY ([Cash_Over]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO

ALTER TABLE [dbo].[Cards]
  ADD CONSTRAINT [fkCheckDig] FOREIGN KEY ([Check_Dig]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO

ALTER TABLE [dbo].[Cards]
  ADD CONSTRAINT [fkDebito] FOREIGN KEY ([Debito]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO

ALTER TABLE [dbo].[Cards]
  ADD CONSTRAINT [fkFechaExp] FOREIGN KEY ([Fecha_Exp]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO

ALTER TABLE [dbo].[Cards]
  ADD CONSTRAINT [fkImagenMostrar] FOREIGN KEY ([Imagen_Mostrar]) REFERENCES [dbo].[ImagenMostrar] ([Id_Imagen])
GO

ALTER TABLE [dbo].[Cards]
  ADD CONSTRAINT [fkInter_Oper] FOREIGN KEY ([Inter_Oper]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO

ALTER TABLE [dbo].[Cards]
  ADD CONSTRAINT [fkManual] FOREIGN KEY ([Manual]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO

ALTER TABLE [dbo].[Cards]
  ADD CONSTRAINT [fkOmitirEmv] FOREIGN KEY ([Omitir_Emv]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO

ALTER TABLE [dbo].[Cards]
  ADD CONSTRAINT [fkPermitirTarjExp] FOREIGN KEY ([Permitir_Tarj_Exp]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO

ALTER TABLE [dbo].[Cards]
  ADD CONSTRAINT [fkPin] FOREIGN KEY ([Pin]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO

ALTER TABLE [dbo].[Cards]
  ADD CONSTRAINT [fkPinServiceCode] FOREIGN KEY ([Pin_Service_Code]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO

ALTER TABLE [dbo].[Cards]
  ADD CONSTRAINT [fkTarjeta_Cierre] FOREIGN KEY ([Tarjeta_Cierre]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO

ALTER TABLE [dbo].[Cards]
  ADD CONSTRAINT [fkTerminal_Cards] FOREIGN KEY ([TID]) REFERENCES [dbo].[TCONF] ([TID])
GO

ALTER TABLE [dbo].[Cards]
  ADD CONSTRAINT [fkTipoCuenta] FOREIGN KEY ([Tipo_de_Cuenta]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO

ALTER TABLE [dbo].[Cards]
  ADD CONSTRAINT [fkTipoMascara] FOREIGN KEY ([Tipo_Mascara]) REFERENCES [dbo].[TipoMascara] ([Id_Tipo_Mascara])
GO

ALTER TABLE [dbo].[Cards]
  ADD CONSTRAINT [fkTipoMontoFijo] FOREIGN KEY ([Tipo_Monto_fijo]) REFERENCES [dbo].[TipoMontoFijo] ([Id_Tipo_Monto])
GO

ALTER TABLE [dbo].[Cards]
  ADD CONSTRAINT [fkTPreVoucher] FOREIGN KEY ([Pre_Voucher]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO

ALTER TABLE [dbo].[Cards]
  ADD CONSTRAINT [fkUltimos_4] FOREIGN KEY ([Ultimos_4]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO

ALTER TABLE [dbo].[Cards]
  ADD CONSTRAINT [fkV4DBC] FOREIGN KEY ([V4DBC]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO