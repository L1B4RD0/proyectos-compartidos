﻿CREATE TABLE [dbo].[TransaccionDescarga] (
  [tra_id] [bigint] IDENTITY,
  [tra_fecha] [datetime] NOT NULL,
  [tra_stan] [varchar](6) NOT NULL,
  [tra_serial_pos] [varchar](20) NOT NULL,
  [tra_terminal_id] [bigint] NULL,
  [tra_cliente_pos] [varchar](50) NOT NULL,
  [tra_message_type] [varchar](4) NOT NULL,
  [tra_processing_code] [varchar](6) NOT NULL,
  [tra_codigo_respuesta] [varchar](2) NOT NULL,
  [tra_mensaje_respuesta] [varchar](128) NOT NULL,
  [tra_estado_actualizacion_id] [bigint] NULL,
  [tra_aplicacion_id] [bigint] NULL,
  [tra_direccion_ip_pos] [varchar](15) NULL,
  [tra_direccion_ip_detectada] [varchar](15) NULL,
  [tra_puerto_tcp_atencion] [varchar](6) NOT NULL,
  [tra_imei_pos] [varchar](20) NULL,
  [tra_sim_pos] [varchar](20) NULL,
  [tra_nivel_gprs] [varchar](10) NULL,
  [tra_nivel_bateria] [varchar](10) NULL,
  [tra_version_software_pos] [varchar](50) NULL,
  [tra_porcentaje_descarga] [varchar](6) NULL,
  [tra_campo60_pos] [varchar](50) NULL,
  [tra_campo61_pos] [varchar](250) NULL,
  [tra_processing_code_out] [varchar](6) NOT NULL,
  CONSTRAINT [PK_TransaccionDescarga] PRIMARY KEY CLUSTERED ([tra_id])
)
ON [PRIMARY]
GO