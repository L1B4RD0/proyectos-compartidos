﻿CREATE TABLE [dbo].[ImagenMostrar] (
  [Id_Imagen] [int] NOT NULL,
  [Descripcion] [varchar](50) NULL,
  PRIMARY KEY CLUSTERED ([Id_Imagen])
)
ON [PRIMARY]
GO