﻿CREATE TABLE [dbo].[TipoOpcion] (
  [id] [bigint] IDENTITY,
  [descripcion] [varchar](100) NOT NULL,
  CONSTRAINT [PK_TipoOpcion] PRIMARY KEY CLUSTERED ([id])
)
ON [PRIMARY]
GO