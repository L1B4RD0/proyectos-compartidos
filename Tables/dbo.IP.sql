﻿CREATE TABLE [dbo].[IP] (
  [Id_IP] [varchar](5) NOT NULL,
  [Nombre_IP] [varchar](50) NULL,
  [IP_Host] [varchar](20) NULL,
  [Puerto] [varchar](5) NULL,
  [URL] [varchar](50) NULL,
  [Certificado_Serv] [varchar](50) NULL,
  [Certificado_Cli] [varchar](50) NULL,
  [Agregar_Largo] [int] NULL,
  [Agregar_TPDU] [int] NULL,
  [TLS] [int] NULL,
  [Autenticar_Cliente] [int] NULL,
  PRIMARY KEY CLUSTERED ([Id_IP])
)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[IP]
  ADD CONSTRAINT [fkAgregarLargo] FOREIGN KEY ([Agregar_Largo]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO

ALTER TABLE [dbo].[IP]
  ADD CONSTRAINT [fkAgregarTPDU] FOREIGN KEY ([Agregar_TPDU]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO

ALTER TABLE [dbo].[IP]
  ADD CONSTRAINT [fkAutenticarCliente] FOREIGN KEY ([Autenticar_Cliente]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO

ALTER TABLE [dbo].[IP]
  ADD CONSTRAINT [fkTLS] FOREIGN KEY ([TLS]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO