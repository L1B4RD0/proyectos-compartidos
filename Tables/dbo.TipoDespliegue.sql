﻿CREATE TABLE [dbo].[TipoDespliegue] (
  [id] [bigint] IDENTITY,
  [descripcion] [varchar](100) NOT NULL,
  CONSTRAINT [PK_TipoDespliegue] PRIMARY KEY CLUSTERED ([id])
)
ON [PRIMARY]
GO