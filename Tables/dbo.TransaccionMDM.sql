﻿CREATE TABLE [dbo].[TransaccionMDM] (
  [tra_id] [bigint] IDENTITY,
  [tra_fecha] [datetime] NOT NULL,
  [tra_operacion] [varchar](50) NOT NULL,
  [tra_terminal_id] [bigint] NULL,
  [tra_login] [varchar](50) NULL,
  [tra_pass] [varchar](50) NULL,
  [tra_terminal_ram] [varchar](50) NULL,
  [tra_terminal_bateria] [varchar](50) NULL,
  [tra_terminal_latitud] [varchar](50) NULL,
  [tra_terminal_longitud] [varchar](50) NULL,
  [tra_terminal_apks_instalados] [varchar](1000) NULL,
  [tra_terminal_nombre_grupo] [varchar](50) NULL,
  [tra_terminal_id_device] [varchar](50) NULL,
  [tra_terminal_ip] [varchar](50) NULL,
  [tra_apk_descargado] [varchar](100) NULL,
  [tra_xml_descargado] [varchar](100) NULL,
  [tra_img_descargado] [varchar](100) NULL,
  [tra_estado_final] [varchar](50) NULL,
  CONSTRAINT [PK_TransaccionMDM] PRIMARY KEY CLUSTERED ([tra_id]) WITH (FILLFACTOR = 80)
)
ON [PRIMARY]
GO