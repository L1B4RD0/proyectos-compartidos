﻿CREATE TABLE [dbo].[Perfil] (
  [perf_id] [bigint] IDENTITY,
  [perf_nombre] [varchar](100) NOT NULL,
  CONSTRAINT [PK_Perfil] PRIMARY KEY CLUSTERED ([perf_id])
)
ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[Trigger_AdicionarFuncionesIniciales]
ON [dbo].[Perfil]
AFTER INSERT
AS 
BEGIN

	SET NOCOUNT ON;

	DECLARE @ProfileID AS INT

	SELECT @ProfileID = (SELECT perf_id FROM Inserted)

	INSERT INTO PerfilXFuncion
		SELECT @ProfileID, func_id FROM Funcion WHERE func_id IN (1, 2, 4, 5, 7, 8, 9 , 10)
		
END
GO