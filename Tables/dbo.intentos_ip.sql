﻿CREATE TABLE [dbo].[intentos_ip] (
  [opc_id] [smallint] IDENTITY,
  [opc_intentos] [nchar](50) NOT NULL,
  CONSTRAINT [PK_intentos_ip] PRIMARY KEY CLUSTERED ([opc_id])
)
ON [PRIMARY]
GO