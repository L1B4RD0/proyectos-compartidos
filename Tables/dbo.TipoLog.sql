﻿CREATE TABLE [dbo].[TipoLog] (
  [id] [bigint] IDENTITY,
  [descripcion] [varchar](100) NOT NULL,
  CONSTRAINT [PK_TipoLog] PRIMARY KEY CLUSTERED ([id])
)
ON [PRIMARY]
GO