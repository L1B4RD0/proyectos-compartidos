﻿CREATE TABLE [dbo].[perfiles] (
  [perfil_id] [varchar](50) NOT NULL,
  [descripcion] [varchar](200) NOT NULL,
  [PLANTILLAS_PAGOS_ELEC] [varchar](50) NULL,
  [PLANTILLAS_PAGOS_VAR] [varchar](50) NULL,
  [PLANTILLAS_PROMPTS] [varchar](50) NULL,
  PRIMARY KEY CLUSTERED ([perfil_id])
)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[perfiles]
  ADD CONSTRAINT [FK_PLANTILLAS_PAGOS_ELEC_PERFILES] FOREIGN KEY ([PLANTILLAS_PAGOS_ELEC]) REFERENCES [dbo].[PLANTILLAS_PAGOS_ELEC] ([PLANTILLA_ID])
GO

ALTER TABLE [dbo].[perfiles]
  ADD CONSTRAINT [FK_PLANTILLAS_PAGOS_VAR_PERFILES] FOREIGN KEY ([PLANTILLAS_PAGOS_VAR]) REFERENCES [dbo].[PLANTILLAS_PAGOS_VAR] ([PLANTILLA_ID])
GO

ALTER TABLE [dbo].[perfiles]
  ADD CONSTRAINT [FK_PLANTILLAS_PROMPTS_PERFILES] FOREIGN KEY ([PLANTILLAS_PROMPTS]) REFERENCES [dbo].[PLANTILLAS_PROMPTS] ([PLANTILLA_ID])
GO