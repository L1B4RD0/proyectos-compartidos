﻿CREATE TABLE [dbo].[ClientesEstadisticasTipos] (
  [tipo_id] [bigint] NOT NULL,
  [descripcion] [varchar](100) NOT NULL,
  PRIMARY KEY CLUSTERED ([tipo_id])
)
ON [PRIMARY]
GO