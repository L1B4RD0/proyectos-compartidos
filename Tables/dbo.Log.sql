﻿CREATE TABLE [dbo].[Log] (
  [log_id] [bigint] IDENTITY,
  [log_usuario_id] [bigint] NOT NULL,
  [log_fecha] [datetime] NOT NULL,
  [log_funcion_id] [bigint] NULL,
  [log_tipo_log_id] [bigint] NOT NULL,
  [log_mensaje1] [varchar](1000) NULL,
  [log_mensaje2] [varchar](1000) NULL,
  [log_sessionID] [varchar](50) NULL,
  CONSTRAINT [PK_Log] PRIMARY KEY CLUSTERED ([log_id]) WITH (FILLFACTOR = 80)
)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[Log] WITH NOCHECK
  ADD CONSTRAINT [FK_Log_Log] FOREIGN KEY ([log_tipo_log_id]) REFERENCES [dbo].[TipoLog] ([id])
GO

ALTER TABLE [dbo].[Log] WITH NOCHECK
  ADD CONSTRAINT [FK_Log_Usuario] FOREIGN KEY ([log_usuario_id]) REFERENCES [dbo].[Usuario] ([usu_id])
GO