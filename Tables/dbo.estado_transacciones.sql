﻿CREATE TABLE [dbo].[estado_transacciones] (
  [fecha] [datetime] NOT NULL,
  [terminal_id] [varchar](20) NOT NULL,
  [tipo] [varchar](4) NOT NULL,
  [codigo_proceso] [varchar](6) NOT NULL,
  [codigo_respuesta] [varchar](2) NOT NULL,
  [mensaje_respuesta] [varchar](200) NOT NULL,
  [archivo_descarga] [varchar](200) NOT NULL,
  [porcentaje] [bigint] NOT NULL,
  [responseTime] [bigint] NOT NULL,
  CONSTRAINT [estado_transacciones_pkey] PRIMARY KEY CLUSTERED ([terminal_id])
)
ON [PRIMARY]
GO