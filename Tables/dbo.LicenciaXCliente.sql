﻿CREATE TABLE [dbo].[LicenciaXCliente] (
  [lic_id] [bigint] IDENTITY,
  [lic_fecha_creacion] [datetime] NOT NULL,
  [lic_tipo_estado_id] [bigint] NOT NULL,
  [lic_cliente_id] [bigint] NOT NULL,
  [lic_tipo_licencia_id] [bigint] NOT NULL,
  [lic_key] [varchar](250) NOT NULL,
  CONSTRAINT [PK_LicenciaXCliente] PRIMARY KEY CLUSTERED ([lic_id])
)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[LicenciaXCliente] WITH NOCHECK
  ADD CONSTRAINT [FK_LicenciaXCliente_TipoEstado] FOREIGN KEY ([lic_tipo_estado_id]) REFERENCES [dbo].[TipoEstado] ([id])
GO

ALTER TABLE [dbo].[LicenciaXCliente] WITH NOCHECK
  ADD CONSTRAINT [FK_LicenciaXCliente_TipoLicencia] FOREIGN KEY ([lic_tipo_licencia_id]) REFERENCES [dbo].[TipoLicencia] ([id])
GO