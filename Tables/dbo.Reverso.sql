﻿CREATE TABLE [dbo].[Reverso] (
  [Id_reverso] [int] NOT NULL,
  [Descripcion] [varchar](50) NULL,
  PRIMARY KEY CLUSTERED ([Id_reverso])
)
ON [PRIMARY]
GO