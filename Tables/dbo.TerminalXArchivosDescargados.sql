﻿CREATE TABLE [dbo].[TerminalXArchivosDescargados] (
  [desc_id] [bigint] IDENTITY,
  [desc_fecha] [datetime] NOT NULL,
  [desc_terminal_id] [bigint] NOT NULL,
  [desc_apk_archivo] [varchar](50) NULL,
  [desc_apk_paquete] [varchar](200) NULL,
  [desc_apk_version] [varchar](50) NULL,
  [desc_zip_xml] [varchar](50) NULL,
  [desc_zip_imgs] [varchar](50) NULL,
  CONSTRAINT [PK_TerminalXArchivosDescargados] PRIMARY KEY CLUSTERED ([desc_id]) WITH (FILLFACTOR = 80)
)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[TerminalXArchivosDescargados] WITH NOCHECK
  ADD CONSTRAINT [FK_TerminalXArchivosDescargados_TerminalXArchivosDescargados] FOREIGN KEY ([desc_terminal_id]) REFERENCES [dbo].[Terminal] ([ter_id])
GO