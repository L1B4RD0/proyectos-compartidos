﻿CREATE TABLE [dbo].[LogXObjeto] (
  [log_id] [bigint] NOT NULL,
  [obj_id] [bigint] NOT NULL,
  [mensaje1] [varchar](200) NOT NULL,
  [mensaje2] [varchar](200) NOT NULL,
  CONSTRAINT [PK_LogXObjeto] PRIMARY KEY CLUSTERED ([log_id], [obj_id])
)
ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Este campo se trabajará como una relación Virtual, si el LOG, en su función apunta al Objeto Entidad, entonces este ID corresponderá al Registro Id de la Tabla Entidad.', 'SCHEMA', N'dbo', 'TABLE', N'LogXObjeto', 'COLUMN', N'obj_id'
GO