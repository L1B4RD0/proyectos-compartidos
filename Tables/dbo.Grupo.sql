﻿CREATE TABLE [dbo].[Grupo] (
  [gru_id] [bigint] IDENTITY,
  [gru_nombre] [varchar](50) NOT NULL,
  [gru_descripcion] [varchar](250) NULL,
  [gru_actualizar] [bit] NOT NULL,
  [gru_programar_fecha] [bit] NOT NULL,
  [gru_fecha_programada_actualizacion_ini] [datetime] NULL,
  [gru_fecha_programada_actualizacion_fin] [datetime] NULL,
  [gru_rango_hora_actualizacion_ini] [time] NULL,
  [gru_rango_hora_actualizacion_fin] [time] NULL,
  [gru_ip_descarga] [varchar](15) NULL,
  [gru_puerto_descarga] [varchar](5) NULL,
  [gru_cliente_id] [bigint] NOT NULL,
  [gru_tipo_android] [bit] NULL CONSTRAINT [DF_Grupo_gru_tipo_android] DEFAULT (0),
  [gru_default_login] [varchar](50) NULL,
  [gru_default_password] [varchar](50) NULL,
  [gru_aplicaciones_permitidas] [varchar](500) NULL,
  [gru_numero_consultas] [smallint] NULL,
  [gru_frecuencia_horas] [smallint] NULL,
  [gru_fecha_creacion] [datetime] NOT NULL,
  [gru_fecha_actualizacion] [datetime] NULL,
  [gru_aplicacion_kiosko] [varchar](500) NULL,
  [gru_mensaje_desplegar] [varchar](50) NULL,
  [gru_blocking_mode] [bit] NULL,
  [gru_update_now] [bit] NULL,
  [gru_codigo_externo] [bigint] NULL,
  [gru_clave_configurarMDM] [varchar](50) NULL,
  CONSTRAINT [PK_Grupo] PRIMARY KEY CLUSTERED ([gru_id]) WITH (FILLFACTOR = 80)
)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[Grupo] WITH NOCHECK
  ADD CONSTRAINT [FK_CODIGOS_EXTERNOS] FOREIGN KEY ([gru_id]) REFERENCES [dbo].[Grupo] ([gru_id])
GO

ALTER TABLE [dbo].[Grupo] WITH NOCHECK
  ADD CONSTRAINT [FK_Grupo_Cliente] FOREIGN KEY ([gru_cliente_id]) REFERENCES [dbo].[Cliente] ([cli_id])
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'En esta tabla se registra todos los datos inherente a la tabla Grupo, algunas de las tablas de las que la Grupo depende son: 
° Cliente
° Grupo (Utiliza el identificador para referenciar se ella misma, lo que quiere decir que la llave primaria apunta a la misma tabla, esto garantiza la recursividad) .
Estas estan relacionadas directamente con la tabla Grupo.
Ademas usa 60 Procesos almacenados los cuales se muestran en la parte final del documento.', 'SCHEMA', N'dbo', 'TABLE', N'Grupo'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'llave primaria', 'SCHEMA', N'dbo', 'TABLE', N'Grupo', 'INDEX', N'PK_Grupo'
GO