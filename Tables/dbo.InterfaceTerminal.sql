﻿CREATE TABLE [dbo].[InterfaceTerminal] (
  [id] [bigint] IDENTITY,
  [descripcion] [varchar](50) NOT NULL,
  CONSTRAINT [PK_InterfaceTerminal] PRIMARY KEY CLUSTERED ([id])
)
ON [PRIMARY]
GO