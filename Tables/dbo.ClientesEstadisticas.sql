﻿CREATE TABLE [dbo].[ClientesEstadisticas] (
  [fecha] [date] NOT NULL,
  [cli_id] [bigint] NOT NULL,
  [gru_id] [bigint] NOT NULL,
  [ter_id] [bigint] NOT NULL,
  [tipo_id] [bigint] NOT NULL,
  [valor] [int] NULL,
  CONSTRAINT [ClientesEstadisticas_Pkey] PRIMARY KEY CLUSTERED ([cli_id], [gru_id], [ter_id], [tipo_id], [fecha])
)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[ClientesEstadisticas] WITH NOCHECK
  ADD CONSTRAINT [FK_Estadisticas] FOREIGN KEY ([tipo_id]) REFERENCES [dbo].[ClientesEstadisticasTipos] ([tipo_id])
GO