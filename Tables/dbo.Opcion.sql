﻿CREATE TABLE [dbo].[Opcion] (
  [opc_id] [bigint] IDENTITY,
  [opc_titulo] [varchar](50) NOT NULL,
  [opc_url] [varchar](200) NOT NULL,
  [opc_img_url] [varchar](200) NULL,
  [opc_tipo_opcion_id] [bigint] NOT NULL,
  [opc_opcion_padre_id] [bigint] NULL,
  [opc_orden] [smallint] NOT NULL,
  CONSTRAINT [PK_Opcion] PRIMARY KEY CLUSTERED ([opc_id])
)
ON [PRIMARY]
GO

CREATE UNIQUE INDEX [IX_Opcion]
  ON [dbo].[Opcion] ([opc_opcion_padre_id], [opc_orden])
  ON [PRIMARY]
GO

ALTER TABLE [dbo].[Opcion] WITH NOCHECK
  ADD CONSTRAINT [FK_Opcion_TipoOpcion] FOREIGN KEY ([opc_tipo_opcion_id]) REFERENCES [dbo].[TipoOpcion] ([id])
GO