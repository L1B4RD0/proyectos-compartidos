﻿CREATE TABLE [dbo].[Pagos_Var] (
  [Id_Pagos_Varios] [varchar](50) NOT NULL,
  [Codigo_Pagos_Varios] [varchar](50) NULL,
  [Nombre_Pagos_Varios] [varchar](50) NULL,
  [Texto_Pagos_Varios] [varchar](50) NULL,
  [Grupo_Prompts] [varchar](50) NULL,
  [Bin] [varchar](50) NULL,
  [Longitud_Minima] [varchar](50) NULL,
  [Longitud_Maxima] [varchar](50) NULL,
  [Tiempo_Espera] [varchar](50) NULL,
  [Entrada_Manual_Codigo] [int] NULL,
  [PLANTILLA_ID] [varchar](50) NOT NULL,
  PRIMARY KEY CLUSTERED ([PLANTILLA_ID], [Id_Pagos_Varios])
)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[Pagos_Var]
  ADD CONSTRAINT [FK_PLANTILLAS_PAGOS_VAR] FOREIGN KEY ([PLANTILLA_ID]) REFERENCES [dbo].[PLANTILLAS_PAGOS_VAR] ([PLANTILLA_ID])
GO

ALTER TABLE [dbo].[Pagos_Var]
  ADD CONSTRAINT [fkEntradaManualCodigo] FOREIGN KEY ([Entrada_Manual_Codigo]) REFERENCES [dbo].[Habilitar_Valor] ([Id_Valor])
GO