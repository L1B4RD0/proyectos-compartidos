﻿CREATE TABLE [dbo].[TipoSeleccion] (
  [Id_tipo] [int] NULL,
  [Descripcion] [varchar](50) NOT NULL,
  PRIMARY KEY CLUSTERED ([Descripcion])
)
ON [PRIMARY]
GO