﻿CREATE TABLE [dbo].[Pagos_Elec] (
  [Id_Pagos_Electronicos] [varchar](50) NOT NULL,
  [Nombre_Pago_Electronico] [varchar](50) NULL,
  [Imagen] [varchar](50) NULL,
  [Num_Tarjeta] [varchar](50) NULL,
  [Tiempo_Espera] [varchar](50) NULL,
  [Longitud_Minima] [varchar](50) NULL,
  [Longitud_Maxima] [varchar](50) NULL,
  [PLANTILLA_ID] [varchar](50) NOT NULL,
  PRIMARY KEY CLUSTERED ([PLANTILLA_ID], [Id_Pagos_Electronicos])
)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[Pagos_Elec]
  ADD CONSTRAINT [FK_PLANTILLAS_PAGOS_ELEC] FOREIGN KEY ([PLANTILLA_ID]) REFERENCES [dbo].[PLANTILLAS_PAGOS_ELEC] ([PLANTILLA_ID])
GO