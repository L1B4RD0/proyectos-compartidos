﻿CREATE TABLE [dbo].[TipoEstado] (
  [id] [bigint] IDENTITY,
  [descripcion] [varchar](50) NULL,
  CONSTRAINT [PK_TipoEstado] PRIMARY KEY CLUSTERED ([id])
)
ON [PRIMARY]
GO