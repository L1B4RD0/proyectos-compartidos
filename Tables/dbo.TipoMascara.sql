﻿CREATE TABLE [dbo].[TipoMascara] (
  [Id_Tipo_Mascara] [int] NOT NULL,
  [Descripcion] [varchar](50) NULL,
  PRIMARY KEY CLUSTERED ([Id_Tipo_Mascara])
)
ON [PRIMARY]
GO