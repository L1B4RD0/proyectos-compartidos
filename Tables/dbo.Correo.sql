﻿CREATE TABLE [dbo].[Correo] (
  [correo_id] [bigint] IDENTITY,
  [correo_nombre] [varchar](100) NOT NULL,
  [correo_clave] [varchar](100) NOT NULL,
  CONSTRAINT [PK_Correo] PRIMARY KEY CLUSTERED ([correo_id])
)
ON [PRIMARY]
GO