﻿CREATE TABLE [dbo].[GrupoXArchivosDesplegadosCtl] (
  [desp_id] [bigint] IDENTITY,
  [desp_fecha] [datetime] NOT NULL,
  [desp_nombre_archivo] [varchar](50) NOT NULL,
  [desp_hash] [varchar](500) NULL,
  CONSTRAINT [PK_GrupoXArchivosDesplegadosCTL] PRIMARY KEY CLUSTERED ([desp_id])
)
ON [PRIMARY]
GO