﻿CREATE TABLE [dbo].[GrupoXArchivosDesplegados] (
  [desp_id] [bigint] IDENTITY,
  [desp_fecha] [datetime] NOT NULL,
  [desp_grupo_id] [bigint] NOT NULL,
  [desp_nombre_archivo] [varchar](50) NOT NULL,
  [desp_nombre_paquete] [varchar](50) NULL,
  [desp_version] [varchar](50) NULL,
  [desp_tipo_despliegue_id] [bigint] NOT NULL,
  CONSTRAINT [PK_GrupoXArchivosDesplegados] PRIMARY KEY CLUSTERED ([desp_id]) WITH (FILLFACTOR = 80)
)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[GrupoXArchivosDesplegados] WITH NOCHECK
  ADD CONSTRAINT [FK_GrupoXArchivosDesplegados_TipoDespliegue] FOREIGN KEY ([desp_tipo_despliegue_id]) REFERENCES [dbo].[TipoDespliegue] ([id])
GO