﻿CREATE TABLE [dbo].[terminales] (
  [pos_id] [varchar](50) NULL,
  [pos_tipo] [varchar](50) NULL,
  [pos_marca] [varchar](50) NULL,
  [pos_modelo] [varchar](50) NULL,
  [pos_interface] [varchar](50) NULL,
  [pos_ip] [varchar](50) NULL,
  [pos_mac] [varchar](50) NULL,
  [pos_imei] [varchar](50) NULL,
  [pos_serial_dispositivo] [varchar](50) NOT NULL,
  [pos_serial_sim] [varchar](50) NULL,
  [pos_numero_telefonico_sim] [varchar](50) NULL,
  [pos_serial_cargador] [varchar](50) NULL,
  [pos_serial_bateria] [varchar](50) NULL,
  [pos_serial_lector_biometrico] [varchar](50) NULL,
  [pos_latitud] [varchar](50) NULL,
  [pos_longitud] [varchar](50) NULL,
  [pos_operador] [varchar](50) NULL,
  [pos_apn] [varchar](50) NULL,
  [pos_nivel_gprs] [varchar](50) NULL,
  [pos_nivel_bateria] [varchar](50) NULL,
  [pos_reservado_1] [varchar](50) NULL,
  [pos_reservado_2] [varchar](50) NULL,
  [pos_reservado_3] [varchar](50) NULL,
  [pos_version_software] [varchar](50) NULL,
  [contacto_tipo_documento] [varchar](50) NULL,
  [contacto_numero_documento] [varchar](50) NULL,
  [contacto_nombres] [varchar](50) NULL,
  [contacto_apellidos] [varchar](50) NULL,
  [contacto_celular] [varchar](50) NULL,
  [contacto_telefono] [varchar](50) NULL,
  [contacto_direccion] [varchar](100) NULL,
  [contacto_correo_electronico] [varchar](100) NULL,
  [validacion_tipo] [varchar](30) NOT NULL DEFAULT ('N'),
  [validacion_marca] [varchar](30) NOT NULL DEFAULT ('N'),
  [validacion_modelo] [varchar](30) NOT NULL DEFAULT ('N'),
  [validacion_interface] [varchar](30) NOT NULL DEFAULT ('N'),
  [validacion_ip] [varchar](30) NOT NULL DEFAULT ('N'),
  [validacion_mac] [varchar](30) NOT NULL DEFAULT ('N'),
  [validacion_latitud] [varchar](30) NOT NULL DEFAULT ('N'),
  [validacion_longitud] [varchar](30) NOT NULL DEFAULT ('N'),
  [validacion_operador] [varchar](30) NOT NULL DEFAULT ('N'),
  [validacion_apn] [varchar](30) NOT NULL DEFAULT ('N'),
  [validacion_nivel_gprs] [varchar](30) NOT NULL DEFAULT ('N'),
  [validacion_nivel_bateria] [varchar](30) NOT NULL DEFAULT ('N'),
  [validacion_reservado_1] [varchar](30) NOT NULL DEFAULT ('N'),
  [validacion_reservado_2] [varchar](30) NOT NULL DEFAULT ('N'),
  [validacion_reservado_3] [varchar](30) NOT NULL DEFAULT ('N'),
  [validacion_version_software] [varchar](30) NOT NULL DEFAULT ('N'),
  [host_ip_detectada] [varchar](30) NULL,
  [validacion_ip_detectada] [varchar](30) NULL DEFAULT ('N'),
  [actualizacion_estado] [varchar](30) NOT NULL DEFAULT ('PENDIENTE'),
  [actualizacion_pendiente] [varchar](2000) NULL,
  [actualizacion_fecha] [date] NULL,
  [actualizacion_hora] [time] NULL,
  [actualizacion_ip] [varchar](30) NULL,
  [actualizacion_puerto] [varchar](30) NULL,
  [pos_cliente] [varchar](30) NOT NULL,
  [pos_grupo] [varchar](30) NOT NULL,
  [pos_descripcion] [varchar](2000) NULL,
  [actualizacion_manual] [varchar](30) NOT NULL DEFAULT ('N'),
  [actualizacion_actualizar] [varchar](30) NOT NULL DEFAULT ('N'),
  [estado] [varchar](30) NOT NULL DEFAULT ('Y'),
  [validacion_imei] [varchar](30) NOT NULL DEFAULT ('N'),
  [validacion_serial_dispositivo] [varchar](30) NOT NULL DEFAULT ('Y'),
  [validacion_serial_sim] [varchar](30) NOT NULL DEFAULT ('N'),
  [actualizacion_grupo_prioridad] [varchar](30) NOT NULL DEFAULT ('Y'),
  [actual_nivel_bateria] [varchar](30) NULL,
  [actual_nivel_gprs] [varchar](30) NULL,
  [actual_aplicaciones_instaladas] [varchar](2000) NULL,
  [actual_aplicaciones_pendientes] [varchar](2000) NULL,
  [actual_direccion_ip] [varchar](30) NULL,
  [actual_direccion_ip_detectada] [varchar](30) NULL,
  [actual_imei] [varchar](30) NULL,
  [actual_sim] [varchar](30) NULL,
  [actual_aplicacion_descarga] [varchar](30) NULL,
  [actual_aplicacion_descarga_porcentaje] [varchar](30) NULL,
  [actual_fecha_programada_descarga] [datetime] NULL,
  [actual_ip_programada_descarga] [varchar](30) NULL,
  [actual_puerto_programado_descarga] [varchar](30) NULL,
  [actual_fecha_consulta] [datetime] NULL,
  [actual_fecha_descarga] [datetime] NULL,
  [actual_version_software] [varchar](30) NULL,
  [TRIAL918] [char](1) NULL
)
ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'pos_id'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'pos_tipo'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'pos_marca'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'pos_modelo'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'pos_interface'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'pos_ip'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'pos_mac'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'pos_imei'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'pos_serial_dispositivo'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'pos_serial_sim'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'pos_numero_telefonico_sim'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'pos_serial_cargador'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'pos_serial_bateria'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'pos_serial_lector_biometrico'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'pos_latitud'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'pos_longitud'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'pos_operador'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'pos_apn'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'pos_nivel_gprs'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'pos_nivel_bateria'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'pos_reservado_1'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'pos_reservado_2'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'pos_reservado_3'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'pos_version_software'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'contacto_tipo_documento'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'contacto_numero_documento'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'contacto_nombres'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'contacto_apellidos'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'contacto_celular'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'contacto_telefono'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'contacto_direccion'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'contacto_correo_electronico'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'validacion_tipo'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'validacion_marca'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'validacion_modelo'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'validacion_interface'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'validacion_ip'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'validacion_mac'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'validacion_latitud'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'validacion_longitud'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'validacion_operador'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'validacion_apn'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'validacion_nivel_gprs'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'validacion_nivel_bateria'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'validacion_reservado_1'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'validacion_reservado_2'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'validacion_reservado_3'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'validacion_version_software'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'host_ip_detectada'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'validacion_ip_detectada'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'actualizacion_estado'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'actualizacion_pendiente'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'actualizacion_fecha'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'actualizacion_hora'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'actualizacion_ip'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'actualizacion_puerto'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'pos_cliente'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'pos_grupo'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'pos_descripcion'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'actualizacion_manual'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'actualizacion_actualizar'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'estado'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'validacion_imei'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'validacion_serial_dispositivo'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'validacion_serial_sim'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'actualizacion_grupo_prioridad'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'actual_nivel_bateria'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'actual_nivel_gprs'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'actual_aplicaciones_instaladas'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'actual_aplicaciones_pendientes'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'actual_direccion_ip'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'actual_direccion_ip_detectada'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'actual_imei'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'actual_sim'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'actual_aplicacion_descarga'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'actual_aplicacion_descarga_porcentaje'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'actual_fecha_programada_descarga'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'actual_ip_programada_descarga'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'actual_puerto_programado_descarga'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'actual_fecha_consulta'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'actual_fecha_descarga'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'actual_version_software'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'TRIAL', 'SCHEMA', N'dbo', 'TABLE', N'terminales', 'COLUMN', N'TRIAL918'
GO