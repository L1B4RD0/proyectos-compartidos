﻿CREATE TABLE [dbo].[ModeloTerminal] (
  [id] [bigint] IDENTITY,
  [descripcion] [varchar](50) NOT NULL,
  CONSTRAINT [PK_ModeloTerminal] PRIMARY KEY CLUSTERED ([id])
)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[ModeloTerminal] WITH NOCHECK
  ADD CONSTRAINT [FK_modeloid] FOREIGN KEY ([id]) REFERENCES [dbo].[ModeloTerminal] ([id])
GO