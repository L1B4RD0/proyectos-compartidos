﻿CREATE TABLE [dbo].[receipts] (
  [terminalid] [varchar](50) NOT NULL,
  [stan] [varchar](50) NOT NULL,
  [merchant] [varchar](50) NOT NULL,
  [datecapture] [datetime] NOT NULL,
  [receipt] [text] NOT NULL,
  CONSTRAINT [PK_receipts] PRIMARY KEY CLUSTERED ([terminalid], [stan], [merchant], [datecapture])
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO