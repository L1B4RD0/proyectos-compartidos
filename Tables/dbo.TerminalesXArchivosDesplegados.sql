﻿CREATE TABLE [dbo].[TerminalesXArchivosDesplegados] (
  [desp_id] [bigint] IDENTITY,
  [desp_fecha] [datetime] NOT NULL,
  [desp_terminal_id] [bigint] NOT NULL,
  [desp_nombre_archivo] [varchar](50) NOT NULL,
  [desp_nombre_paquete] [varchar](50) NULL,
  [desp_version] [varchar](50) NULL,
  [desp_tipo_despliegue_id] [bigint] NOT NULL,
  CONSTRAINT [PK_TerminalesXArchivosDesplegados] PRIMARY KEY CLUSTERED ([desp_id])
)
ON [PRIMARY]
GO