﻿CREATE TABLE [dbo].[TipoIdentificacion] (
  [id] [bigint] IDENTITY,
  [descripcion] [varchar](50) NOT NULL,
  CONSTRAINT [PK_TipoIdentificacion] PRIMARY KEY CLUSTERED ([id])
)
ON [PRIMARY]
GO