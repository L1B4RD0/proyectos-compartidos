﻿CREATE TABLE [dbo].[Metodo] (
  [id] [bigint] IDENTITY,
  [nombre] [varchar](100) NOT NULL,
  CONSTRAINT [PK_Metodo] PRIMARY KEY CLUSTERED ([id])
)
ON [PRIMARY]
GO