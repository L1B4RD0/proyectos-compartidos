﻿CREATE TABLE [dbo].[CodigoBarras] (
  [Id_Codigo] [int] NOT NULL,
  [Descripcion] [varchar](20) NULL,
  PRIMARY KEY CLUSTERED ([Id_Codigo])
)
ON [PRIMARY]
GO