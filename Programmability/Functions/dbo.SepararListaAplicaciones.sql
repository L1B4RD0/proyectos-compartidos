﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

-- =============================================
-- Autor:				Elkin Beltrán
-- Fecha Creación:		09/Febrero/2017
-- Histórico Cambios:
------------------------------------------------
--	v1.0	Versión Inicial
------------------------------------------------
-- Campos de Respuesta:		TABLE
-- ID					BIGINT
-- APPNAME				VARCHAR(20)
-- APPCHECKSUM			VARCHAR(10)
-- =============================================

CREATE FUNCTION [dbo].[SepararListaAplicaciones] (
	@appList VARCHAR(1000), @separatorApp VARCHAR(2), @separatorChecksum VARCHAR(2)
)

RETURNS @AppListTable TABLE (Id BIGINT NOT NULL PRIMARY KEY IDENTITY(1,1), AppName VARCHAR(20) NOT NULL, AppChecksum VARCHAR(10) NOT NULL)

AS
BEGIN

    DECLARE @StartingPosition INT;
    DECLARE @AppItem VARCHAR(50);
	DECLARE @AppName VARCHAR(20);
	DECLARE @AppChecksum VARCHAR(10);

    SELECT @StartingPosition = 1;

    IF LEN(@appList) = 0 OR @appList IS NULL RETURN; 

    WHILE @StartingPosition > 0
    BEGIN

		SELECT @AppName = '', @AppChecksum = '';

        SET @StartingPosition = CHARINDEX(@separatorApp,@appList); 

        IF @StartingPosition > 0                
            SET @AppItem = SUBSTRING(@appList,0,@StartingPosition)
        ELSE
            SET @AppItem = @appList;

        IF( LEN(@AppItem) > 0)
		BEGIN

			--Separate App Name / Checksum
			SET @AppName = SUBSTRING(@AppItem, 0, CHARINDEX(@separatorChecksum, @AppItem))
			SET @AppChecksum = SUBSTRING(@AppItem, CHARINDEX(@separatorChecksum, @AppItem) + 1, LEN(@AppItem) - CHARINDEX(@separatorChecksum, @AppItem))

            INSERT INTO @AppListTable(AppName, AppChecksum) VALUES (@AppName, @AppChecksum);

		END

        SET @appList = SUBSTRING(@appList,@StartingPosition + LEN(@separatorApp), LEN(@appList) - @StartingPosition)

        IF LEN(@appList) = 0 BREAK;

    END

    RETURN
END
GO