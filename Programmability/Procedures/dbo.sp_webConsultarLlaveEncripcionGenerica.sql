﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[sp_webConsultarLlaveEncripcionGenerica]
 
AS
BEGIN
	
	SET NOCOUNT ON

	SELECT 
		param_valor AS encryptionKey
	FROM ParametroGeneral 
	WHERE param_nombre = 'EncryptionKey'

END


GO