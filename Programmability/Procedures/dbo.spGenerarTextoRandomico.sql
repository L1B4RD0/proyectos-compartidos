﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

-- =============================================
-- Autor:				Elkin Beltrán
-- Fecha Creación:		06/Junio/2015
-- Histórico Cambios:
------------------------------------------------
--	v1.0	Versión Inicial
------------------------------------------------
-- Campos de Respuesta:		SingleRow
-- randomString		VARCHAR(100)
-- =============================================

CREATE PROCEDURE [dbo].[spGenerarTextoRandomico] (
	@randomString VARCHAR(100) OUTPUT
)
 
AS
BEGIN

	SET NOCOUNT ON

	DECLARE @sLength TINYINT
	DECLARE @counter TINYINT
	DECLARE @nextChar CHAR(1)
	DECLARE @nextCharINT TINYINT
	SET @counter = 1
	SET @randomString = ''
	SET @sLength = 20

	WHILE @counter <= @sLength
	BEGIN
		SELECT @nextCharINT = ASCII(CHAR(ROUND(RAND() * 93 + 33, 0)))
		--SOLO LETRAS(MIN/MAY) Y NÚMEROS

		IF (@nextCharINT >= 65 AND @nextCharINT <= 90) OR (@nextCharINT >= 97 AND @nextCharINT <= 122) OR (@nextCharINT >= 48 AND @nextCharINT <= 57)
		BEGIN
			SELECT @randomString = @randomString + CHAR(@nextCharINT)
			SET @counter = @counter + 1
		END
	END

	/*PAD LEFT AND RIGHT*/
	SET @randomString = SUBSTRING(CAST(RAND() AS VARCHAR(50)),3, 10) + @randomString + SUBSTRING(CAST(RAND() AS VARCHAR(50)),3, 10)

END





GO