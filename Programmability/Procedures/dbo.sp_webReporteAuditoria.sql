﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[sp_webReporteAuditoria](
	@userID AS BIGINT,
	@startDate AS DATETIME,
	@endDate AS DATETIME
)
 
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @endDateReport AS DATETIME	
	SET @endDateReport = DATEADD(DAY, 1, @endDate)

	SELECT 
		L.log_fecha,
		U.usu_login,
		U.usu_nombre,
		O.nombre AS Objeto,
		M.nombre AS Metodo,
		TL.descripcion AS TipoMensaje,
		L.log_mensaje1
	FROM [Log] L
		INNER JOIN Usuario U ON (L.log_usuario_id = U.usu_id)
		INNER JOIN Funcion F ON (L.log_funcion_id = F.func_id)
		INNER JOIN Objeto O ON (F.func_objeto_id = O.id)
		INNER JOIN Metodo M ON (F.func_metodo_id = M.id)
		INNER JOIN TipoLog TL ON (L.log_tipo_log_id = TL.id)
	WHERE L.log_fecha >= @startDate AND L.log_fecha <= @endDateReport
		AND (L.log_usuario_id = @userID OR @userID = -1)
	ORDER BY 1
	
END







GO