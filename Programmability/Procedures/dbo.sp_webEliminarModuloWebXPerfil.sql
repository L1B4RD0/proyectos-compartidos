﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[sp_webEliminarModuloWebXPerfil](
	@profileID INT,
	@opc_id INT
)
 
AS
BEGIN
	
	BEGIN TRANSACTION

	BEGIN TRY	
	
		--ELIMINAR MODULO WEB
		DELETE FROM PerfilXOpcion
		WHERE perf_id = @profileID AND opc_id = @opc_id

		--ELIMINAR OPCIONES
		DELETE FROM PerfilXOpcion
		WHERE perf_id = @profileID AND opc_id IN (SELECT opc_id FROM Opcion WHERE opc_opcion_padre_id = @opc_id)

		COMMIT TRANSACTION

		SELECT 1 /*OK*/

	END TRY
	BEGIN CATCH
		
		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		PRINT(ERROR_MESSAGE())

		SELECT 0 /*ERROR*/

	END CATCH;

END













GO