﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[sp_webEliminarTerminalXGrupo](
	@terminalId INT,
	@groupId INT
)
 
AS
BEGIN
	
	BEGIN TRANSACTION

	BEGIN TRY	
	
		--ELIMINAR ARCHIVOS DESCARGADOS X TERMINAL MDM
		DELETE FROM TerminalXArchivosDescargados WHERE desc_terminal_id = @terminalId

		--ELIMINAR TERMINAL
		DELETE FROM Terminal WHERE ter_id = @terminalId AND ter_grupo_id = @groupId

		COMMIT TRANSACTION

		SELECT 1 /*OK*/

	END TRY
	BEGIN CATCH
		
		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		SELECT 0 /*ERROR*/

	END CATCH;

END



GO