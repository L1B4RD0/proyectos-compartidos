﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[sp_webEliminarArchivoDesplegadoXGrupoAndroid](
	@deployedFileId INT,
	@groupId INT
)
 
AS
BEGIN
	DECLARE @serial varchar(100)
	DECLARE @apps varchar(500)
	DECLARE @Con int
	DECLARE @con2 int
	DECLARE @APP VARCHAR(100)
	DECLARE @validar int
	DECLARE @AppListTableDB TABLE (Id BIGINT NOT NULL PRIMARY KEY IDENTITY(1,1), AppName VARCHAR(100) NOT NULL)

	BEGIN TRANSACTION

	BEGIN TRY	
	
		--ELIMINAR APLICACION
		DELETE FROM GrupoXArchivosDesplegados
		WHERE desp_id = @deployedFileId AND desp_grupo_id = @groupId

		COMMIT TRANSACTION

		--------------------------------------------------------------
		if Exists(select * from GrupoXArchivosDesplegados where desp_grupo_id = @groupID)
			begin
		INSERT INTO @AppListTableDB
						SELECT CONCAT(desp_nombre_paquete, '-', desp_version) 
						FROM GrupoXArchivosDesplegados WHERE desp_grupo_id = @groupID

				DECLARE cursorAplicaciones cursor
					for select ter_serial, ter_aplicaciones_instaladas from Terminal  where ter_grupo_id = @groupID
	
			

				OPEN cursorAplicaciones
				FETCH	NEXT FROM cursorAplicaciones INTO @serial, @apps

				WHILE (@@FETCH_STATUS = 0)

				BEGIN
					SET @con2 = 1
					SET @Con = 0
		
					WHILE (@con2 <= (SELECT MAX(Id) FROM @AppListTableDB))
					BEGIN
						 SELECT  @APP = AppName
						 FROM @AppListTableDB WHERE Id = @con2

						 SELECT @validar = CHARINDEX(@APP, @apps)

						 IF  @validar > 0
						 BEGIN
							SET @Con = @Con + 1
						 END
		 

						 SET @con2 = @con2 + 1
		
		
					END

				IF @Con = (SELECT MAX(Id) FROM @AppListTableDB)
				BEGIN
					UPDATE Terminal SET ter_estado_actualizacion_id = 1 WHERE ter_serial = @serial
				END
				else
					UPDATE Terminal SET ter_estado_actualizacion_id = 2 WHERE ter_serial = @serial

				FETCH NEXT FROM cursorAplicaciones INTO @serial, @apps

			END

			CLOSE cursorAplicaciones
		
			deallocate cursorAplicaciones
		end
		else
			UPDATE Terminal SET ter_estado_actualizacion_id = 1 WHERE ter_grupo_id = @groupID

			-----------------------------------------------------
		SELECT 1 /*OK*/

	END TRY
	BEGIN CATCH
		
		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		SELECT 0 /*ERROR*/

	END CATCH;

END


GO