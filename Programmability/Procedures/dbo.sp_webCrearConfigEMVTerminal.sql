﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO


-- =============================================
-- Autor:				Elkin Beltrán
-- Fecha Creación:		04/Abril/2017
-- Histórico Cambios:
------------------------------------------------
--	v1.0	Versión Inicial
------------------------------------------------
-- Campos de Respuesta:		SingleRow
-- RESPONSECODE		INT
-- =============================================

CREATE PROCEDURE [dbo].[sp_webCrearConfigEMVTerminal](
	@terminalId BIGINT,
	@emvType VARCHAR(2),
	@emvConfig VARCHAR(2),
	@emvThresholdRS VARCHAR(8),
	@emvTargetRS VARCHAR(2),
	@emvMaxTargetRS VARCHAR(2),
	@emvTACDenial VARCHAR(10),
	@emvTACOnline VARCHAR(10),
	@emvTACDefault VARCHAR(10),
	@emvApplicationConfig VARCHAR(1024)
)
 
AS
BEGIN

	SET XACT_ABORT, NOCOUNT ON
	DECLARE @startTranCount INT

	--VARIABLES LOCALES DEL SP
	DECLARE @responseCode AS INT

	BEGIN TRY
		SELECT @startTranCount = @@TRANCOUNT

		IF @startTranCount = 0
		BEGIN
			BEGIN TRANSACTION		
			--INICIO CUERPO DEL SP

			INSERT INTO TerminalXEMVConfig(emv_terminal_id,emv_datetime,emv_type,emv_config,emv_threshold_random_selection,
											emv_target_random_selection, emv_max_target_random_selection, emv_tac_denial,
											emv_tac_online, emv_tac_default, emv_application_config)
				VALUES (@terminalId, GETDATE(), @emvType, @emvConfig, @emvThresholdRS, @emvTargetRS, @emvMaxTargetRS, 
						@emvTACDenial, @emvTACOnline, @emvTACDefault, @emvApplicationConfig)

			SELECT @responseCode = 1

			--FIN CUERPO DEL SP
		END

		IF @startTranCount = 0 
		BEGIN 
			COMMIT TRANSACTION

			--SELECT FINAL
			SELECT @responseCode AS RESPONSECODE
		END
	END TRY
	BEGIN CATCH
		IF XACT_STATE() <> 0 AND @startTranCount = 0 
			ROLLBACK TRANSACTION

		INSERT INTO [dbo].[LogErrorBaseDatos]([bderr_fecha],[bderr_nombre_funcion],[bderr_mensaje_error],[bderr_datos_adicionales])
			 VALUES (GETDATE(), OBJECT_NAME(@@PROCID), ERROR_MESSAGE(), /*Datos Extra, Parámetros del SP*/ 
																		'terminalId = ' + CAST(@terminalId AS VARCHAR) + ', emvType = ' + @emvType + ', emvConfig = ' + @emvConfig + 
																		', emvThresholdRS = ' + @emvThresholdRS + ', emvTargetRS = ' + @emvTargetRS + ', emvMaxTargetRS = ' + @emvMaxTargetRS)

		DECLARE @errMessage AS VARCHAR(4000) = ERROR_MESSAGE()
		DECLARE @errSeverity AS INT = ERROR_SEVERITY()
		DECLARE @errState AS INT = ERROR_STATE()

		RAISERROR (@errMessage, @errSeverity, @errState)
	END CATCH	

END


GO