﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[sp_webConsultarArchivosDesplegadosGrupoAndroidCTL](
	@deployTypeId INT,
	@groupId BIGINT
)
 
AS
BEGIN

	SET NOCOUNT ON;
	
	WITH
cteFilas AS
(
SELECT  desp_nombre_archivo, desp_id,desp_fecha,desp_hash,ROW_NUMBER() OVER(PARTITION BY desp_nombre_archivo ORDER BY desp_nombre_archivo) AS numero_Fila
	FROM [dbo].[GrupoXArchivosDesplegadosCtl] 
	
)
SELECT * FROM cteFilas
WHERE numero_Fila = 1

END




GO