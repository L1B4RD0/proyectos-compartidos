﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[sp_webInsertarMensajeInfoLog](
	@UserID AS INT,
	@LogTypeID AS INT,
	@Message1 AS VARCHAR(1000),
	@Message2 AS VARCHAR(1000),
	@SessionID AS VARCHAR(100)
)
 
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO Log
		(log_usuario_id, log_fecha, log_funcion_id, log_tipo_log_id ,log_mensaje1, log_mensaje2, log_sessionID)
	VALUES
		(@UserID, GETDATE(), 5, @LogTypeID, @Message1, @Message2, @SessionID)
	
END

GO