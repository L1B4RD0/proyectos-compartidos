﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[sp_webConsultarLicenciasCliente](
	@customerId BIGINT
)
 
AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT LxC.lic_id, 
			CONVERT(VARCHAR, LxC.lic_fecha_creacion, 103) + ' ' + SUBSTRING(CONVERT(VARCHAR, LxC.lic_fecha_creacion, 108), 1, 5) AS lic_fecha_creacion, 
			(CASE WHEN LxC.lic_tipo_estado_id = 1 THEN CONVERT(VARCHAR, DATEADD(DAY, TL.numero_dias + 1, LxC.lic_fecha_creacion), 103) + ' ' + SUBSTRING(CONVERT(VARCHAR, DATEADD(DAY, TL.numero_dias + 1, LxC.lic_fecha_creacion), 108), 1, 5) ELSE '-' END) AS lic_fecha_expiracion,
			TL.descripcion AS tipo_licencia, 
			TA.descripcion AS tipo_estado, 
			LxC.lic_key
	FROM LicenciaXCliente LxC
		INNER JOIN TipoEstado TA ON (LxC.lic_tipo_estado_id = TA.id) 
		INNER JOIN TipoLicencia TL ON (LxC.lic_tipo_licencia_id = TL.id) 
	WHERE LxC.lic_cliente_id = @customerId
	ORDER BY LxC.lic_id

END



GO