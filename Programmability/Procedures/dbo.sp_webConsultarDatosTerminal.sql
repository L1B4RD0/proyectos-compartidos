﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[sp_webConsultarDatosTerminal](
	@terminalID BIGINT
)
 
AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT Ter.ter_serial, Ter.ter_tipo_terminal_id, Ter.ter_marca_terminal_id, Ter.ter_modelo_terminal_id, Ter.ter_interface_terminal_id, ISNULL(Ter.ter_serial_sim, ''), ISNULL(Ter.ter_telefono_sim, ''), 
		REPLACE(REPLACE(REPLACE(REPLACE(ISNULL(Ter.ter_imei, ''), CHAR(13),''), CHAR(10),''), CHAR(34),''), CHAR(39),'') AS ter_imei, ISNULL(Ter.ter_serial_cargador, ''), ISNULL(Ter.ter_serial_bateria, ''), ISNULL(Ter.ter_serial_lector_biometrico, ''), ISNULL(Ter.ter_descripcion, ''), Ter.ter_prioridad_grupo, 
		Ter.ter_actualizar, ISNULL(CAST(Ter.ter_fecha_programada_actualizacion AS DATETIME), GETDATE()), ISNULL(Ter.ter_ip_descarga, ''), ISNULL(Ter.ter_puerto_descarga, ''), 
		Ter.ter_tipo_estado_id, Ter.ter_validar_imei, Ter.ter_validar_sim,
		ISNULL((CASE WHEN Ter.ter_latitud_gps = '' THEN '0' ELSE Ter.ter_latitud_gps END), '0'), ISNULL((CASE WHEN Ter.ter_longitud_gps = '' THEN '0' ELSE Ter.ter_longitud_gps END), '0'), ISNULL(Ter.ter_apn, ''), ISNULL(Ter.ter_nivel_bateria, ''), ISNULL(Ter.ter_nivel_gprs, ''), ISNULL(Ter.ter_fecha_actualizacion_auto, CAST('01/01/2001' AS DATETIME)),
		Ter.ter_grupo_id,
		ISNULL(T.terc_numero_identificacion, ''), ISNULL(T.terc_tipo_identificacion_id, -1), ISNULL(T.terc_nombre, ''), ISNULL(T.terc_direccion, ''), ISNULL(T.terc_celular, ''), ISNULL(t.terc_telefono_fijo, ''), ISNULL(T.terc_correo_electronico, ''),
		---CAMPOS MDM
		ISNULL(ter_ip_dispositivo, ''),
		ISNULL(ter_id_heracles, ''),
		ISNULL(ter_nombre_heracles, ''),
		ISNULL(ter_ciudad_heracles, ''),
		ISNULL(ter_region_heracles, ''),
		ISNULL(ter_agencia_heracles, ''),
		G.gru_tipo_android,
		ISNULL(ter_aplicaciones_permitidas, ''),
		ISNULL(ter_aplicaciones_instaladas, ''),
		ISNULL(ter_aplicacion_kiosko, ''),
		ISNULL(ter_mensaje_desplegar, ''),
		ISNULL(Ter.ter_blocking_mode,''),
		ISNULL(Ter.ter_update_now,''),
		ISNULL(Ter.ter_flag_inicializacion,'')
	FROM Terminal Ter
		LEFT OUTER JOIN Tercero T ON(T.terc_id = Ter.ter_tercero_id)
		INNER JOIN Grupo G ON (Ter.ter_grupo_id = G.gru_id)
	WHERE Ter.ter_id = @terminalID

END


GO