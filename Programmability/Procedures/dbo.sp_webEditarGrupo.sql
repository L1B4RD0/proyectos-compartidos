﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[sp_webEditarGrupo](
	@groupID BIGINT,
	@groupName VARCHAR(50),
	--@CodeExtern VARCHAR(50),
	@groupDesc VARCHAR(250),
	@groupClaveMDM VARCHAR(8),
	@groupIP VARCHAR(20),
	@groupPort VARCHAR(10),
	@allowUpdate BIT,
	@useDateRange BIT,
	@RangeDateIni DATETIME,
	@RangeDateEnd DATETIME,
	@RangeHourIni DATETIME,
	@RangeHourEnd DATETIME, 
	@blockingMode BIT,
	@updateNow BIT
	--@Inicializacion BIT
)
 
AS
BEGIN

	SET XACT_ABORT, NOCOUNT ON
	DECLARE @startTranCount INT

	--VARIABLES LOCALES DEL SP
	DECLARE @responseCode AS INT
	DECLARE @contactId AS BIGINT

	BEGIN TRY
		SELECT @startTranCount = @@TRANCOUNT

		IF @startTranCount = 0
		BEGIN
			BEGIN TRANSACTION		
			--INICIO CUERPO DEL SP
	
			--VALIDAR SI USA RANGO DE FECHAS
			IF (@useDateRange = 1)
			BEGIN
				UPDATE Grupo
				SET
					gru_nombre = @groupName,
					--gru_codigo_externo = @CodeExtern,
					gru_descripcion = @groupDesc,
					gru_clave_configurarMDM = @groupClaveMDM,
					gru_ip_descarga = @groupIP,
					gru_puerto_descarga = @groupPort,
					gru_actualizar = @allowUpdate,
					gru_programar_fecha = @useDateRange,
					gru_fecha_programada_actualizacion_ini = @RangeDateIni,
					gru_fecha_programada_actualizacion_fin = @RangeDateEnd,
					gru_rango_hora_actualizacion_ini = NULL,
					gru_rango_hora_actualizacion_fin = NULL,
					gru_blocking_mode = @blockingMode,
					gru_update_now = @updateNow
				WHERE gru_id = @groupID
			END
			ELSE
			BEGIN
				UPDATE Grupo
				SET
					gru_nombre = @groupName,
					--gru_codigo_externo = @CodeExtern,
					gru_descripcion = @groupDesc,
					gru_clave_configurarMDM=@groupClaveMDM,
					gru_ip_descarga = @groupIP,
					gru_puerto_descarga = @groupPort,
					gru_actualizar = @allowUpdate,
					gru_programar_fecha = @useDateRange,
					gru_fecha_programada_actualizacion_ini = NULL,
					gru_fecha_programada_actualizacion_fin = NULL,
					gru_rango_hora_actualizacion_ini = CONVERT(TIME, @RangeHourIni),
					gru_rango_hora_actualizacion_fin = CONVERT(TIME, @RangeHourEnd),
					gru_blocking_mode = @blockingMode,
					gru_update_now = @updateNow
				WHERE gru_id = @groupID			
			END

			
				--UPDATE Terminal
				--SET ter_flag_inicializacion = @Inicializacion
				--WHERE ter_grupo_id = @groupID
			
			SELECT @responseCode = 1

			--FIN CUERPO DEL SP
		END

		IF @startTranCount = 0 
		BEGIN 
			COMMIT TRANSACTION

			--SELECT FINAL
			SELECT @responseCode AS RESPONSECODE
		END
	END TRY
	BEGIN CATCH
		IF XACT_STATE() <> 0 AND @startTranCount = 0 
			ROLLBACK TRANSACTION

		INSERT INTO [dbo].[LogErrorBaseDatos]([bderr_fecha],[bderr_nombre_funcion],[bderr_mensaje_error],[bderr_datos_adicionales])
			 VALUES (GETDATE(), OBJECT_NAME(@@PROCID), ERROR_MESSAGE(), /*Datos Extra, Parámetros del SP*/ 
																		'groupName = ' + @groupName + ', groupDesc = ' + @groupDesc + ', groupIP = ' + @groupIP + 
																		', groupPort = ' + @groupPort)

		DECLARE @errMessage AS VARCHAR(4000) = ERROR_MESSAGE()
		DECLARE @errSeverity AS INT = ERROR_SEVERITY()
		DECLARE @errState AS INT = ERROR_STATE()

		RAISERROR (@errMessage, @errSeverity, @errState)
	END CATCH	

END
GO