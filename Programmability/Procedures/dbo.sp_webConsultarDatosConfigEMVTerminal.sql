﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[sp_webConsultarDatosConfigEMVTerminal](
	@emvConfigId BIGINT
)
 
AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT [emv_type]
      ,[emv_config]
      ,[emv_threshold_random_selection]
      ,[emv_target_random_selection]
      ,[emv_max_target_random_selection]
      ,[emv_tac_denial]
      ,[emv_tac_online]
      ,[emv_tac_default]
      ,[emv_application_config]
	FROM [dbo].[TerminalXEMVConfig]
	WHERE emv_id = @emvConfigId

END



GO