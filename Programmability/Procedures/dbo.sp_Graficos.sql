﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[sp_Graficos]

AS
BEGIN

    SET XACT_ABORT ON
	SET NOCOUNT ON
	DECLARE @startTranCount INT

	--VARIABLES LOCALES DEL SP
    DECLARE @responseCode AS VARCHAR(50)

	BEGIN TRY
	  

	      
	 BEGIN TRANSACTION;  
		create table #tmp1
		(tipo varchar(50),
		valor varchar(50))	;

		DECLARE @CNTCONSULTAS INT
		SET @CNTCONSULTAS = ( (SELECT SUM(C.valor) FROM ClientesEstadisticas C
		INNER JOIN ClientesEstadisticasTipos CT ON CT.tipo_id = C.tipo_id
		WHERE C.tipo_id = '91'));
		if @CNTCONSULTAS is null 
		begin
		set @CNTCONSULTAS = '0'
		end
		INSERT INTO #tmp1 values ('Consultas',@CNTCONSULTAS);

	
		DECLARE @CNTDESCARGARS INT
		SET @CNTDESCARGARS = ( (SELECT SUM(C.valor) FROM ClientesEstadisticas C
		INNER JOIN ClientesEstadisticasTipos CT ON CT.tipo_id = C.tipo_id
		WHERE C.tipo_id = '92'));
		if @CNTDESCARGARS is null 
		begin
		set @CNTDESCARGARS = '0'
		end
		INSERT INTO #tmp1 values ('Descargas',@CNTDESCARGARS);

		DECLARE @CNTTXAPROBADAS INT
		SET @CNTTXAPROBADAS = ( (SELECT SUM(C.valor) FROM ClientesEstadisticas C
		INNER JOIN ClientesEstadisticasTipos CT ON CT.tipo_id = C.tipo_id
		WHERE C.tipo_id = '3'));
		if @CNTTXAPROBADAS is null 
		begin
		set @CNTTXAPROBADAS = '0'
		end
		INSERT INTO #tmp1 values ('tx Aprobadas',@CNTTXAPROBADAS);

		
		DECLARE @CNTTXRECHAZADA INT
		SET @CNTTXRECHAZADA = ( (SELECT SUM(C.valor) FROM ClientesEstadisticas C
		INNER JOIN ClientesEstadisticasTipos CT ON CT.tipo_id = C.tipo_id
		WHERE C.tipo_id = '4'));
		if @CNTTXRECHAZADA is null 
		begin
		set @CNTTXRECHAZADA = '0'
		end
		INSERT INTO #tmp1 values ('tx Rechazadas',@CNTTXRECHAZADA);



		DECLARE @CNTTXREVERSADA INT
		SET @CNTTXREVERSADA = ( (SELECT SUM(C.valor) FROM ClientesEstadisticas C
		INNER JOIN ClientesEstadisticasTipos CT ON CT.tipo_id = C.tipo_id
		WHERE C.tipo_id = '5'));
		if @CNTTXREVERSADA is null 
		begin
		set @CNTTXREVERSADA = '0'
		end
		INSERT INTO #tmp1 values ('tx Reversadas',@CNTTXREVERSADA);

		
		DECLARE @CNTTXSINRESPUESTA INT
		SET @CNTTXSINRESPUESTA = ( (SELECT SUM(C.valor) FROM ClientesEstadisticas C
		INNER JOIN ClientesEstadisticasTipos CT ON CT.tipo_id = C.tipo_id
		WHERE C.tipo_id = '6'));
		if @CNTTXSINRESPUESTA is null 
		begin
		set @CNTTXSINRESPUESTA = '0'
		end
		INSERT INTO #tmp1 values ('tx sin respuesta',@CNTTXSINRESPUESTA);

		DECLARE @ERR_IMPRE INT
		SET @ERR_IMPRE = ( (SELECT SUM(C.valor) FROM ClientesEstadisticas C
		INNER JOIN ClientesEstadisticasTipos CT ON CT.tipo_id = C.tipo_id
		WHERE C.tipo_id = '7'));
		if @ERR_IMPRE is null 
		begin
		set @ERR_IMPRE = '0'
		end
		INSERT INTO #tmp1 values ('Errores Impresion',@ERR_IMPRE);


		DECLARE @ERR_BANDA INT
		SET @ERR_BANDA = ( (SELECT SUM(C.valor) FROM ClientesEstadisticas C
		INNER JOIN ClientesEstadisticasTipos CT ON CT.tipo_id = C.tipo_id
		WHERE C.tipo_id = '8'));
		if @ERR_BANDA is null 
		begin
		set @ERR_BANDA = '0'
		end
		INSERT INTO #tmp1 values ('Errores Banda',@ERR_BANDA);

		
		DECLARE @ERR_CHIP INT
		SET @ERR_CHIP = ( (SELECT SUM(C.valor) FROM ClientesEstadisticas C
		INNER JOIN ClientesEstadisticasTipos CT ON CT.tipo_id = C.tipo_id
		WHERE C.tipo_id = '9'));
		if @ERR_CHIP is null 
		begin
		set @ERR_CHIP = '0'
		end
		INSERT INTO #tmp1 values ('Errores Chip',@ERR_CHIP);

		select * from #tmp1
		Delete from #tmp1

     COMMIT TRANSACTION; 


	END TRY
	BEGIN CATCH

		IF (XACT_STATE()) = -1  
			BEGIN  
				PRINT 'The transaction is in an uncommittable state.' +  
              ' Rolling back transaction.'  
			ROLLBACK TRANSACTION;  
			PRINT @startTranCount
			 INSERT INTO [dbo].[LogErrorBaseDatos]([bderr_fecha],[bderr_nombre_funcion],[bderr_mensaje_error],[bderr_datos_adicionales])
			  VALUES (GETDATE(), OBJECT_NAME(@@PROCID), ERROR_MESSAGE(), /* Datos Extra, Parámetros del SP*/ 
									'tx sin respuesta = ' + @CNTTXSINRESPUESTA + ', Eror impresion = ' + @ERR_IMPRE + ', Error chip' + @ERR_CHIP)

				DECLARE @errMessage AS VARCHAR(4000) = ERROR_MESSAGE()
				DECLARE @errSeverity AS INT = ERROR_SEVERITY()
				DECLARE @errState AS INT = ERROR_STATE()

				RAISERROR (@errMessage, @errSeverity, @errState)
		END;  

		-- Test whether the transaction is active and valid.  
		IF (XACT_STATE()) = 1  
		BEGIN  
			PRINT 'The transaction is committable.' +   
              ' Committing transaction.'  
			COMMIT TRANSACTION;   

		END;  

	END CATCH


END


GO