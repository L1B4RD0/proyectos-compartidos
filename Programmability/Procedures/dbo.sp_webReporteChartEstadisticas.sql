﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[sp_webReporteChartEstadisticas](
	@cli_id AS BIGINT,
	@gru_id AS BIGINT,
	@ter_id AS BIGINT,
    @tipo_id as BIGINT,
	@fechaInicial as varchar(30),
	@fechaFinal as varchar(30)
)
 
AS
BEGIN

	SET NOCOUNT ON;

	if @gru_id = -1 
	begin
	select @gru_id = ter_grupo_id from terminal where ter_id  = @ter_id 
	end

		SELECT convert (varchar(20),  x.fecha) as fecha,
		 convert(varchar(20), x.valor) as valor FROM ClientesEstadisticas x 
		INNER JOIN 
		ClientesEstadisticasTipos y 
		on x.tipo_id = y.tipo_id
		where 
		x.cli_id = @cli_id and
		x.gru_id = @gru_id and
		x.ter_id = @ter_id
		and x.tipo_id = @tipo_id and 
		fecha between convert(datetime, @fechaInicial, 112) and convert(datetime, @fechaFinal, 112) 
		Set @gru_id	= 5
END


GO