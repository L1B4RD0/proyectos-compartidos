﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

-- =============================================
-- Autor:				Elkin Beltrán
-- Fecha Creación:		28/Ene/2018
-- Histórico Cambios:
------------------------------------------------
--	v1.0	Versión Inicial
------------------------------------------------
-- Campos de Respuesta:		SingleRow
-- RESPONSECODE		INT
-- =============================================

CREATE PROCEDURE [dbo].[sp_webEditarGrupoAndroidFrecuenciaConsultas](
	@groupID BIGINT,
	@numberOfQueries SMALLINT,
	@frequency SMALLINT
)
 
AS
BEGIN

	SET XACT_ABORT, NOCOUNT ON
	DECLARE @startTranCount INT

	--VARIABLES LOCALES DEL SP
	DECLARE @responseCode AS INT

	BEGIN TRY
		SELECT @startTranCount = @@TRANCOUNT

		IF @startTranCount = 0
		BEGIN
			BEGIN TRANSACTION		
			--INICIO CUERPO DEL SP
	
			UPDATE Grupo
				SET
					gru_numero_consultas = @numberOfQueries,
					gru_frecuencia_horas = @frequency
				WHERE gru_id = @groupID			

			SELECT @responseCode = 1

			--FIN CUERPO DEL SP
		END

		IF @startTranCount = 0 
		BEGIN 
			COMMIT TRANSACTION

			--SELECT FINAL
			SELECT @responseCode AS RESPONSECODE
		END
	END TRY
	BEGIN CATCH
		IF XACT_STATE() <> 0 AND @startTranCount = 0 
			ROLLBACK TRANSACTION

		INSERT INTO [dbo].[LogErrorBaseDatos]([bderr_fecha],[bderr_nombre_funcion],[bderr_mensaje_error],[bderr_datos_adicionales])
			 VALUES (GETDATE(), OBJECT_NAME(@@PROCID), ERROR_MESSAGE(), /*Datos Extra, Parámetros del SP*/ 
																		'numberOfQueries = ' + CAST(@numberOfQueries AS VARCHAR) + ', frequency = ' + CAST(@frequency AS VARCHAR))

		DECLARE @errMessage AS VARCHAR(4000) = ERROR_MESSAGE()
		DECLARE @errSeverity AS INT = ERROR_SEVERITY()
		DECLARE @errState AS INT = ERROR_STATE()

		RAISERROR (@errMessage, @errSeverity, @errState)
	END CATCH	

END



GO