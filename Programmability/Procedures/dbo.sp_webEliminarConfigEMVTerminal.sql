﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[sp_webEliminarConfigEMVTerminal](
	@emvConfigId INT,
	@terminalId INT
)
 
AS
BEGIN
	
	BEGIN TRANSACTION

	BEGIN TRY	
	
		--ELIMINAR CONFIGURACION
		DELETE FROM TerminalXEMVConfig
		WHERE emv_id = @emvConfigId AND emv_terminal_id = @terminalId

		COMMIT TRANSACTION

		SELECT 1 /*OK*/

	END TRY
	BEGIN CATCH
		
		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		SELECT 0 /*ERROR*/

	END CATCH;

END














GO