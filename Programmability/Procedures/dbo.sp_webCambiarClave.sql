﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[sp_webCambiarClave](
	@userID INT,
	@actualHASH VARCHAR(150),
	@newHASH VARCHAR(150)
)
 
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @status AS INT

	IF NOT EXISTS(SELECT * FROM Usuario U WHERE U.usu_id = @userID AND U.usu_hash = @actualHASH)
		SET @status = 0	/*Error Clave Actual Errónea*/ 
	ELSE
		BEGIN
			/*Validar que No sea la misma clave de 4 cambios anteriores*/
			IF EXISTS(SELECT * FROM (SELECT TOP 4 usu_hash_antiguo FROM UsuarioHistoricoClaves WHERE usu_id = @userID ORDER BY id DESC) AS TMP WHERE TMP.usu_hash_antiguo = @newHASH)
			BEGIN
				SET @status = 2	/* ERROR CLAVES ANTERIORES SIMILARES */
			END
			ELSE
			BEGIN
				/*Clave Actual Correcta, Cambiarla y Actualizar Fecha de Expiración a 3 Meses de la Fecha Actual*/
				UPDATE Usuario
					SET usu_hash = @newHASH,
						usu_fecha_exp_clave = DATEADD(MONTH, 3, GETDATE())	--3 Meses a Partir de Fecha Actual
				WHERE usu_id = @userID AND usu_hash = @actualHASH

				/*Insertar Histórico de Clave*/
				INSERT INTO UsuarioHistoricoClaves(fecha, usu_id, usu_hash_antiguo)
					VALUES(GETDATE(), @userID, @newHASH)

				SET @status = 1	/* OK */			
			END

		END

	SELECT @status AS STATUS

END








GO