﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[sp_webAgregarModuloWebXPerfil](
	@profileID INT,
	@webModuleID INT
)
 
AS
BEGIN
	
	BEGIN TRANSACTION

	BEGIN TRY	
	
		--ADICIONAR MODULO WEB
		INSERT INTO PerfilXOpcion(perf_id, opc_id)
			VALUES (@profileID, @webModuleID)

		--ADICIONAR OPCIONES
		INSERT INTO PerfilXOpcion(perf_id, opc_id)
			SELECT @profileID, opc_id FROM Opcion WHERE opc_opcion_padre_id = @webModuleID

		--ADICIONAR METODO LOAD PARA EL MÓDULO WEB		
		DECLARE @functionID AS INT	
		DECLARE @TMP AS TABLE(opc_url VARCHAR(100))
		
		INSERT INTO @TMP
			SELECT opc_url FROM Opcion WHERE opc_id=@webModuleID
			UNION ALL
			SELECT opc_url FROM Opcion WHERE opc_opcion_padre_id=@webModuleID
				
		DECLARE cFunctions CURSOR FOR
			SELECT func_id FROM Funcion WHERE func_objeto_id IN (SELECT O.id FROM Objeto O INNER JOIN @TMP T ON (T.opc_url = O.nombre)) AND func_metodo_id = 1
			
		OPEN cFunctions

		FETCH cFunctions INTO @functionID

		WHILE (@@FETCH_STATUS = 0 )
		BEGIN
			IF NOT EXISTS(SELECT * FROM PerfilXFuncion WHERE perf_id = @profileID AND func_id = @functionID)
			BEGIN
				INSERT INTO PerfilXFuncion
					VALUES(@profileID,@functionID)		
			END
			
			FETCH cFunctions INTO @functionID
		END
		CLOSE cFunctions
		DEALLOCATE cFunctions		
		
		COMMIT TRANSACTION

		SELECT 1 /*OK*/

	END TRY
	BEGIN CATCH
		
		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		PRINT(ERROR_MESSAGE())

		SELECT 0 /*ERROR*/

	END CATCH;

END













GO