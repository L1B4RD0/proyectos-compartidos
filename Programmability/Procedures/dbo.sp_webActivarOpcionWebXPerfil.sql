﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[sp_webActivarOpcionWebXPerfil](
	@profileID INT,
	@optionID INT,
	@active BIT
)
 
AS
BEGIN

	SET NOCOUNT ON;
	
	BEGIN TRANSACTION

	BEGIN TRY	
	
		IF @active = 1
		BEGIN 
			--INSERTAR
			IF NOT EXISTS(SELECT * FROM PerfilXOpcion WHERE perf_id = @profileID AND opc_id = @optionID)
			BEGIN
				INSERT INTO PerfilXOpcion(perf_id, opc_id)
					VALUES (@profileID, @optionID)
			END
			
		END
		ELSE
		BEGIN
			--ELIMINAR
			DELETE FROM PerfilXOpcion WHERE perf_id = @profileID AND opc_id = @optionID
		END		
		
		COMMIT TRANSACTION

		SELECT 1 /*OK*/

	END TRY
	BEGIN CATCH
		
		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		PRINT(ERROR_MESSAGE())

		SELECT 0 /*ERROR*/

	END CATCH;

END













GO