﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[sp_webReporteTerminalesMDM](
	@groupID AS BIGINT,
	@terminalID AS VARCHAR(50),
	@customerID AS BIGINT
)
 
AS
BEGIN

	SET NOCOUNT ON;
	--VARIABLES LOCALES DEL SP
	DECLARE @defaultPackageName AS VARCHAR(200) = ''; SELECT @defaultPackageName = param_valor FROM ParametroGeneral WHERE param_nombre = 'PackageNameNeoCNB';
	DECLARE @backgroundColorGreen AS VARCHAR(10) = '#dff0d8'
	DECLARE @backgroundColorRed AS VARCHAR(10) = '#e47a7a'
	DECLARE @currentDeployedAPKFileMaxVersion AS VARCHAR(100) = '-'
	DECLARE @currentDeployedXmlFile AS VARCHAR(100) = '-'
	DECLARE @currentDeployedImgFile AS VARCHAR(100) = '-'

	IF @groupID <> -1
	BEGIN
	
		--OBTENER VALORES DE DESPLIEGUE DE ARCHIVOS APK's, XML's E IMG'S POR GRUPO ESPECÍFICO
		SELECT TOP 1 @currentDeployedAPKFileMaxVersion = ISNULL(GXAD.desp_version ,'-') FROM GrupoXArchivosDesplegados GXAD WHERE GXAD.desp_grupo_id = @groupId AND GXAD.desp_tipo_despliegue_id = 1 AND GXAD.desp_nombre_paquete = @defaultPackageName ORDER BY GXAD.desp_id DESC	/*APK's*/
		SELECT TOP 1 @currentDeployedXmlFile = ISNULL(GXAD.desp_nombre_archivo ,'-') FROM GrupoXArchivosDesplegados GXAD WHERE GXAD.desp_grupo_id = @groupId AND GXAD.desp_tipo_despliegue_id = 2 ORDER BY GXAD.desp_id DESC	/*XML's*/
		SELECT TOP 1 @currentDeployedImgFile = ISNULL(GXAD.desp_nombre_archivo ,'-') FROM GrupoXArchivosDesplegados GXAD WHERE GXAD.desp_grupo_id = @groupId AND GXAD.desp_tipo_despliegue_id = 3 ORDER BY GXAD.desp_id DESC	/*IMG's*/

		SELECT 

			(CASE WHEN T.ter_tipo_estado_id = 1 THEN '../img/icons/16x16/android_on.png' ELSE '../img/icons/16x16/android_off.png' END) AS ter_logo,
			T.ter_fecha_consulta_sp as fecha_consulta,
			G.gru_nombre,
			ISNULL(MT.descripcion,'') AS ter_marca_terminal,
			ISNULL(MoT.descripcion, '') AS ter_tipo_terminal,
			T.ter_aplicaciones_instaladas as aplicaciones_instaladas,
			T.ter_aplicaciones_pendientes_sp as aplicaciones_pendientes,
			T.Ter_latitud_gps AS latitud_gps,
			T.ter_longitud_gps AS longitud_gps,
			T.ter_serial,
			EA.descripcion AS ter_estado_actualizacion,
			T.ter_id_heracles,
			T.ter_ip_dispositivo,
			T.ter_nombre_heracles,
			T.ter_ciudad_heracles,
			T.ter_region_heracles,
			T.ter_agencia_heracles,

			--APK's
			CASE WHEN EXISTS( (SELECT TOP 1 TXAD.desc_apk_version FROM TerminalXArchivosDescargados TXAD WHERE TXAD.desc_terminal_id = T.ter_id AND TXAD.desc_zip_imgs IS NULL AND TXAD.desc_zip_xml IS NULL AND TXAD.desc_apk_paquete = @defaultPackageName ORDER BY TXAD.desc_id DESC) ) THEN @defaultPackageName + ' v' + (SELECT TOP 1 TXAD.desc_apk_version FROM TerminalXArchivosDescargados TXAD WHERE TXAD.desc_terminal_id = T.ter_id AND TXAD.desc_zip_imgs IS NULL AND TXAD.desc_zip_xml IS NULL AND TXAD.desc_apk_paquete = @defaultPackageName ORDER BY TXAD.desc_id DESC) ELSE '-' END AS apk_banco,
			CASE WHEN EXISTS( (SELECT TOP 1 TXAD.desc_apk_version FROM TerminalXArchivosDescargados TXAD WHERE TXAD.desc_terminal_id = T.ter_id AND TXAD.desc_zip_imgs IS NULL AND TXAD.desc_zip_xml IS NULL AND TXAD.desc_apk_paquete = @defaultPackageName ORDER BY TXAD.desc_id DESC) ) THEN (CASE WHEN @currentDeployedAPKFileMaxVersion = (SELECT TOP 1 TXAD.desc_apk_version FROM TerminalXArchivosDescargados TXAD WHERE TXAD.desc_terminal_id = T.ter_id AND TXAD.desc_zip_imgs IS NULL AND TXAD.desc_zip_xml IS NULL AND TXAD.desc_apk_paquete = @defaultPackageName ORDER BY TXAD.desc_id DESC) THEN @backgroundColorGreen ELSE @backgroundColorRed END) ELSE @backgroundColorRed END AS apk_banco_color,
			--XML's
			CASE WHEN EXISTS( (SELECT TOP 1 TXAD.desc_zip_xml FROM TerminalXArchivosDescargados TXAD WHERE TXAD.desc_terminal_id = T.ter_id AND TXAD.desc_zip_imgs IS NULL AND TXAD.desc_apk_archivo IS NULL ORDER BY TXAD.desc_id DESC) ) THEN (SELECT TOP 1 TXAD.desc_zip_xml FROM TerminalXArchivosDescargados TXAD WHERE TXAD.desc_terminal_id = T.ter_id AND TXAD.desc_zip_imgs IS NULL AND TXAD.desc_apk_archivo IS NULL ORDER BY TXAD.desc_id DESC) ELSE '-' END AS configuracion,
			CASE WHEN EXISTS( (SELECT TOP 1 (CASE WHEN TXAD.desc_zip_xml = @currentDeployedXmlFile THEN @backgroundColorGreen /*ACTUALIZADO*/ ELSE @backgroundColorRed /*DESACTUALIZADO*/ END) FROM TerminalXArchivosDescargados TXAD WHERE TXAD.desc_terminal_id = T.ter_id AND TXAD.desc_zip_imgs IS NULL AND TXAD.desc_apk_archivo IS NULL ORDER BY TXAD.desc_id DESC) ) THEN (SELECT TOP 1 (CASE WHEN TXAD.desc_zip_xml = @currentDeployedXmlFile THEN @backgroundColorGreen /*ACTUALIZADO*/ ELSE @backgroundColorRed /*DESACTUALIZADO*/ END) FROM TerminalXArchivosDescargados TXAD WHERE TXAD.desc_terminal_id = T.ter_id AND TXAD.desc_zip_imgs IS NULL AND TXAD.desc_apk_archivo IS NULL ORDER BY TXAD.desc_id DESC) ELSE @backgroundColorRed END AS configuracion_color,
			--IMG's
			CASE WHEN EXISTS( (SELECT TOP 1 TXAD.desc_zip_imgs FROM TerminalXArchivosDescargados TXAD WHERE TXAD.desc_terminal_id = T.ter_id AND TXAD.desc_zip_xml IS NULL AND TXAD.desc_apk_archivo IS NULL ORDER BY TXAD.desc_id DESC) ) THEN (SELECT TOP 1 TXAD.desc_zip_imgs FROM TerminalXArchivosDescargados TXAD WHERE TXAD.desc_terminal_id = T.ter_id AND TXAD.desc_zip_xml IS NULL AND TXAD.desc_apk_archivo IS NULL ORDER BY TXAD.desc_id DESC) ELSE '-' END AS imagenes,
			CASE WHEN EXISTS( (SELECT TOP 1 (CASE WHEN TXAD.desc_zip_imgs = @currentDeployedImgFile THEN @backgroundColorGreen /*ACTUALIZADO*/ ELSE @backgroundColorRed /*DESACTUALIZADO*/ END) FROM TerminalXArchivosDescargados TXAD WHERE TXAD.desc_terminal_id = T.ter_id AND TXAD.desc_zip_xml IS NULL AND TXAD.desc_apk_archivo IS NULL ORDER BY TXAD.desc_id DESC) ) THEN (SELECT TOP 1 (CASE WHEN TXAD.desc_zip_imgs = @currentDeployedImgFile THEN @backgroundColorGreen /*ACTUALIZADO*/ ELSE @backgroundColorRed /*DESACTUALIZADO*/ END) FROM TerminalXArchivosDescargados TXAD WHERE TXAD.desc_terminal_id = T.ter_id AND TXAD.desc_zip_xml IS NULL AND TXAD.desc_apk_archivo IS NULL ORDER BY TXAD.desc_id DESC) ELSE @backgroundColorRed END AS imagenes_color,
			--CARGA DE LLAVES
			(CASE WHEN T.ter_llave_cargada = 1 THEN '../img/icons/16x16/key_on.png' ELSE '../img/icons/16x16/key_off.png' END) AS ter_logo_llave,
			CONVERT(VARCHAR, T.ter_fecha_ultima_carga_llave, 103) + ' ' + CONVERT(VARCHAR, T.ter_fecha_ultima_carga_llave, 108) AS ter_fecha_ultima_carga_llave,
			--COMPONENTES
			(CASE WHEN T.ter_componente_1_inyectado = 1 THEN '../img/icons/16x16/key_on.png' ELSE '../img/icons/16x16/key_off.png' END) AS ter_logo_componente1,
			CONVERT(VARCHAR, T.ter_fecha_componente1, 103) + ' ' + CONVERT(VARCHAR, T.ter_fecha_componente1, 108) AS ter_fecha_inyeccion_componente1,
			(CASE WHEN T.ter_componente_2_inyectado = 1 THEN '../img/icons/16x16/key_on.png' ELSE '../img/icons/16x16/key_off.png' END) AS ter_logo_componente2,
			CONVERT(VARCHAR, T.ter_fecha_componente2, 103) + ' ' + CONVERT(VARCHAR, T.ter_fecha_componente2, 108) AS ter_fecha_inyeccion_componente2
		FROM TERMINAL T
			INNER JOIN Grupo G ON (T.ter_grupo_id = G.gru_id)
			INNER JOIN MarcaTerminal MT ON (t.ter_marca_terminal_id = MT.id)
			INNER JOIN ModeloTerminal MoT ON (t.ter_modelo_terminal_id = MoT.id)
			INNER JOIN EstadoActualizacion EA ON (T.ter_estado_actualizacion_id = EA.id)	
		WHERE (T.ter_grupo_id = @groupID OR @groupID = -1) AND G.gru_cliente_id = @customerID AND (T.ter_serial = @terminalID OR @terminalID = '')
				AND G.gru_tipo_android = 1
		ORDER BY T.ter_fecha_creacion
	END
	ELSE
	BEGIN
		--TODOS LOS GRUPOS

		SELECT 
			(CASE WHEN T.ter_tipo_estado_id = 1 THEN '../img/icons/16x16/android_on.png' ELSE '../img/icons/16x16/android_off.png' END) AS ter_logo,
			T.ter_fecha_consulta_sp as fecha_consulta,
			G.gru_nombre,
			ISNULL(MT.descripcion,'') AS ter_marca_terminal,
			ISNULL(MoT.descripcion, '') AS ter_tipo_terminal,
			T.ter_aplicaciones_instaladas as aplicaciones_instaladas,
			T.ter_aplicaciones_pendientes_sp as aplicaciones_pendientes,
			T.Ter_latitud_gps AS latitud_gps,
			T.ter_longitud_gps AS longitud_gps,
			T.ter_serial,
			EA.descripcion AS ter_estado_actualizacion,
			T.ter_id_heracles,
			T.ter_ip_dispositivo,
			T.ter_nombre_heracles,
			T.ter_ciudad_heracles,
			T.ter_region_heracles,
			T.ter_agencia_heracles,

			--APK's
			CASE WHEN EXISTS( (SELECT TOP 1 TXAD.desc_apk_version FROM TerminalXArchivosDescargados TXAD WHERE TXAD.desc_terminal_id = T.ter_id AND TXAD.desc_zip_imgs IS NULL AND TXAD.desc_zip_xml IS NULL AND TXAD.desc_apk_paquete = @defaultPackageName ORDER BY TXAD.desc_id DESC) ) THEN @defaultPackageName + ' v' + (SELECT TOP 1 TXAD.desc_apk_version FROM TerminalXArchivosDescargados TXAD WHERE TXAD.desc_terminal_id = T.ter_id AND TXAD.desc_zip_imgs IS NULL AND TXAD.desc_zip_xml IS NULL AND TXAD.desc_apk_paquete = @defaultPackageName ORDER BY TXAD.desc_id DESC) ELSE '-' END AS apk_banco,
			CASE WHEN EXISTS( (SELECT TOP 1 TXAD.desc_apk_version FROM TerminalXArchivosDescargados TXAD WHERE TXAD.desc_terminal_id = T.ter_id AND TXAD.desc_zip_imgs IS NULL AND TXAD.desc_zip_xml IS NULL AND TXAD.desc_apk_paquete = @defaultPackageName ORDER BY TXAD.desc_id DESC) ) THEN (CASE WHEN (SELECT TOP 1 ISNULL(GXAD.desp_version ,'-') FROM GrupoXArchivosDesplegados GXAD WHERE GXAD.desp_grupo_id = T.ter_grupo_id AND GXAD.desp_tipo_despliegue_id = 1 AND GXAD.desp_nombre_paquete = @defaultPackageName ORDER BY GXAD.desp_id DESC /*APK's*/) = (SELECT TOP 1 TXAD.desc_apk_version FROM TerminalXArchivosDescargados TXAD WHERE TXAD.desc_terminal_id = T.ter_id AND TXAD.desc_zip_imgs IS NULL AND TXAD.desc_zip_xml IS NULL AND TXAD.desc_apk_paquete = @defaultPackageName ORDER BY TXAD.desc_id DESC) THEN @backgroundColorGreen ELSE @backgroundColorRed END) ELSE @backgroundColorRed END AS apk_banco_color,
			--XML's
			CASE WHEN EXISTS( (SELECT TOP 1 TXAD.desc_zip_xml FROM TerminalXArchivosDescargados TXAD WHERE TXAD.desc_terminal_id = T.ter_id AND TXAD.desc_zip_imgs IS NULL AND TXAD.desc_apk_archivo IS NULL ORDER BY TXAD.desc_id DESC) ) THEN (SELECT TOP 1 TXAD.desc_zip_xml FROM TerminalXArchivosDescargados TXAD WHERE TXAD.desc_terminal_id = T.ter_id AND TXAD.desc_zip_imgs IS NULL AND TXAD.desc_apk_archivo IS NULL ORDER BY TXAD.desc_id DESC) ELSE '-' END AS configuracion,
			CASE WHEN EXISTS( (SELECT TOP 1 (CASE WHEN TXAD.desc_zip_xml = (SELECT TOP 1 ISNULL(GXAD.desp_nombre_archivo ,'-') FROM GrupoXArchivosDesplegados GXAD WHERE GXAD.desp_grupo_id = T.ter_grupo_id AND GXAD.desp_tipo_despliegue_id = 2 ORDER BY GXAD.desp_id DESC /*XML's*/) THEN @backgroundColorGreen /*ACTUALIZADO*/ ELSE @backgroundColorRed /*DESACTUALIZADO*/ END) FROM TerminalXArchivosDescargados TXAD WHERE TXAD.desc_terminal_id = T.ter_id AND TXAD.desc_zip_imgs IS NULL AND TXAD.desc_apk_archivo IS NULL ORDER BY TXAD.desc_id DESC) ) THEN (SELECT TOP 1 (CASE WHEN TXAD.desc_zip_xml = (SELECT TOP 1 ISNULL(GXAD.desp_nombre_archivo ,'-') FROM GrupoXArchivosDesplegados GXAD WHERE GXAD.desp_grupo_id = T.ter_grupo_id AND GXAD.desp_tipo_despliegue_id = 2 ORDER BY GXAD.desp_id DESC /*XML's*/) THEN @backgroundColorGreen /*ACTUALIZADO*/ ELSE @backgroundColorRed /*DESACTUALIZADO*/ END) FROM TerminalXArchivosDescargados TXAD WHERE TXAD.desc_terminal_id = T.ter_id AND TXAD.desc_zip_imgs IS NULL AND TXAD.desc_apk_archivo IS NULL ORDER BY TXAD.desc_id DESC) ELSE @backgroundColorRed END AS configuracion_color,
			--IMG's
			CASE WHEN EXISTS( (SELECT TOP 1 TXAD.desc_zip_imgs FROM TerminalXArchivosDescargados TXAD WHERE TXAD.desc_terminal_id = T.ter_id AND TXAD.desc_zip_xml IS NULL AND TXAD.desc_apk_archivo IS NULL ORDER BY TXAD.desc_id DESC) ) THEN (SELECT TOP 1 TXAD.desc_zip_imgs FROM TerminalXArchivosDescargados TXAD WHERE TXAD.desc_terminal_id = T.ter_id AND TXAD.desc_zip_xml IS NULL AND TXAD.desc_apk_archivo IS NULL ORDER BY TXAD.desc_id DESC) ELSE '-' END AS imagenes,
			CASE WHEN EXISTS( (SELECT TOP 1 (CASE WHEN TXAD.desc_zip_imgs = (SELECT TOP 1 ISNULL(GXAD.desp_nombre_archivo ,'-') FROM GrupoXArchivosDesplegados GXAD WHERE GXAD.desp_grupo_id = T.ter_grupo_id AND GXAD.desp_tipo_despliegue_id = 3 ORDER BY GXAD.desp_id DESC /*IMG's*/) THEN @backgroundColorGreen /*ACTUALIZADO*/ ELSE @backgroundColorRed /*DESACTUALIZADO*/ END) FROM TerminalXArchivosDescargados TXAD WHERE TXAD.desc_terminal_id = T.ter_id AND TXAD.desc_zip_xml IS NULL AND TXAD.desc_apk_archivo IS NULL ORDER BY TXAD.desc_id DESC) ) THEN (SELECT TOP 1 (CASE WHEN TXAD.desc_zip_imgs = (SELECT TOP 1 ISNULL(GXAD.desp_nombre_archivo ,'-') FROM GrupoXArchivosDesplegados GXAD WHERE GXAD.desp_grupo_id = T.ter_grupo_id AND GXAD.desp_tipo_despliegue_id = 3 ORDER BY GXAD.desp_id DESC /*IMG's*/) THEN @backgroundColorGreen /*ACTUALIZADO*/ ELSE @backgroundColorRed /*DESACTUALIZADO*/ END) FROM TerminalXArchivosDescargados TXAD WHERE TXAD.desc_terminal_id = T.ter_id AND TXAD.desc_zip_xml IS NULL AND TXAD.desc_apk_archivo IS NULL ORDER BY TXAD.desc_id DESC) ELSE @backgroundColorRed END AS imagenes_color,
			--CARGA DE LLAVES
			(CASE WHEN T.ter_llave_cargada = 1 THEN '../img/icons/16x16/key_on.png' ELSE '../img/icons/16x16/key_off.png' END) AS ter_logo_llave,
			CONVERT(VARCHAR, T.ter_fecha_ultima_carga_llave, 103) + ' ' + CONVERT(VARCHAR, T.ter_fecha_ultima_carga_llave, 108) AS ter_fecha_ultima_carga_llave,
			--COMPONENTES
			(CASE WHEN T.ter_componente_1_inyectado = 1 THEN '../img/icons/16x16/key_on.png' ELSE '../img/icons/16x16/key_off.png' END) AS ter_logo_componente1,
			CONVERT(VARCHAR, T.ter_fecha_componente1, 103) + ' ' + CONVERT(VARCHAR, T.ter_fecha_componente1, 108) AS ter_fecha_inyeccion_componente1,
			(CASE WHEN T.ter_componente_2_inyectado = 1 THEN '../img/icons/16x16/key_on.png' ELSE '../img/icons/16x16/key_off.png' END) AS ter_logo_componente2,
			CONVERT(VARCHAR, T.ter_fecha_componente2, 103) + ' ' + CONVERT(VARCHAR, T.ter_fecha_componente2, 108) AS ter_fecha_inyeccion_componente2
		FROM TERMINAL T
			INNER JOIN MarcaTerminal MT ON (t.ter_marca_terminal_id = MT.id)
			INNER JOIN ModeloTerminal MoT ON (t.ter_modelo_terminal_id = MoT.id)
			INNER JOIN Grupo G ON (T.ter_grupo_id = G.gru_id)
			INNER JOIN EstadoActualizacion EA ON (T.ter_estado_actualizacion_id = EA.id)	
		WHERE (T.ter_grupo_id = @groupID OR @groupID = -1) AND G.gru_cliente_id = @customerID AND (T.ter_serial = @terminalID OR @terminalID = '')
				AND G.gru_tipo_android = 1
		ORDER BY T.ter_fecha_creacion

	END

END


GO