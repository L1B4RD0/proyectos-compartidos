﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

create PROCEDURE [dbo].[sp_webReporteHistoricoDescarga](
	@groupID AS BIGINT,
	@terminalID AS BIGINT,
	@customerID AS BIGINT,
	@startDate AS DATETIME,
	@endDate AS DATETIME
)
 
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @endDateReport AS DATETIME	
	SET @endDateReport = DATEADD(DAY, 1, @endDate)

	SELECT 
		CONVERT(VARCHAR, TD.tra_fecha, 103) + ' ' + CONVERT(VARCHAR, TD.tra_fecha, 108) AS tra_fecha,
		TD.tra_cliente_pos,
		G.gru_nombre,
		TD.tra_serial_pos,
		TD.tra_imei_pos,
		ISNULL(MT.descripcion,'') AS tra_marca_terminal,
		ISNULL(MoT.descripcion, '') AS tra_tipo_terminal,
		TD.tra_codigo_respuesta,
		TD.tra_mensaje_respuesta,
		ISNULL(EA.descripcion, '') AS tra_estado_actualizacion,
		SUBSTRING(TD.tra_campo60_pos, 0, CHARINDEX ( ',' , TD.tra_campo60_pos )) AS apl_nombre_tms,
		TD.tra_porcentaje_descarga + ' %' AS tra_porcentaje_descarga
	FROM TransaccionDescargaHistorico TD
		LEFT OUTER JOIN Terminal T ON (TD.tra_terminal_id = T.ter_id)
		LEFT OUTER JOIN Grupo G ON (T.ter_grupo_id = G.gru_id)
		LEFT OUTER JOIN MarcaTerminal MT ON (t.ter_marca_terminal_id = MT.id)
		LEFT OUTER JOIN ModeloTerminal MoT ON (t.ter_modelo_terminal_id = MoT.id)
		LEFT OUTER JOIN Aplicacion A ON (TD.tra_aplicacion_id = A.apl_id)
		LEFT OUTER JOIN EstadoActualizacion EA ON (TD.tra_estado_actualizacion_id = EA.id)
	WHERE TD.tra_fecha >= @startDate AND TD.tra_fecha <= @endDateReport
		AND (T.ter_grupo_id = @groupID OR @groupID = -1) AND G.gru_cliente_id = @customerID AND (T.ter_id = @terminalID OR @terminalID = -1)
	ORDER BY TD.tra_fecha DESC
	
END

GO