﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[sp_webConsultarAplicacionesGrupo](
	@groupId BIGINT
)
 
AS
BEGIN

	SET NOCOUNT ON;

		SELECT 
    A.apl_id, A.apl_nombre_archivo_fisico, A.apl_nombre_tms, A.apl_checksum_tms, A.apl_descripcion,
	(CASE WHEN MT.descripcion is null THEN 'TODOS LOS MODELOS'
	ELSE
	  MT.descripcion	
	END) as modelo, DATALENGTH(A.apl_data) AS apl_longitud 
	FROM Aplicacion A 
	INNER JOIN 
	GrupoXAplicacion GxA ON (GxA.aplicacion_id = A.apl_id) 
	LEFT JOIN
	ModeloTerminal MT on (GxA.Modelo_id = MT.id)
	WHERE GxA.grupo_id = @groupId ORDER BY A.apl_id

	
	--SELECT A.apl_id, A.apl_nombre_archivo_fisico, A.apl_nombre_tms, A.apl_checksum_tms, A.apl_descripcion, apl_Terminal_Model, DATALENGTH(A.apl_data) AS apl_longitud 
	--FROM Aplicacion A INNER JOIN GrupoXAplicacion GxA ON (GxA.aplicacion_id = A.apl_id) WHERE GxA.grupo_id = @groupId ORDER BY A.apl_id

END
GO