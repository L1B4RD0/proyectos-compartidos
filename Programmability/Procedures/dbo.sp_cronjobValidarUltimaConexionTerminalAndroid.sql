﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

-- =============================================
-- Autor:				Elkin Beltrán
-- Fecha Creación:		15/Febrero/2018
-- Histórico Cambios:
------------------------------------------------
--	v1.0	Versión Inicial		
------------------------------------------------
-- CRONJOB PARA ADMINISTRAR ESTADO DE LOS 
-- DISPOSITIVOS SEGÚN RANGO PARAMETRIZADO EN 
-- MINUTOS
-- =============================================
CREATE PROCEDURE [dbo].[sp_cronjobValidarUltimaConexionTerminalAndroid]
AS
BEGIN
	
	SET NOCOUNT ON

	--DECLARACIÓN DE VARIABLES
	DECLARE @terminalId AS BIGINT
	DECLARE @lastTerminalConnectionDate AS DATETIME
	DECLARE @lastConnectionRange AS BIGINT

	--OBTENER EL VALOR PARAMETRIZADO DE RANGO EN MINUTOS PARA VALIDAR ÚLTIMA CONEXIÓN DEL DISPOSITIVO
	SELECT @lastConnectionRange = param_valor FROM ParametroGeneral WHERE param_nombre = 'ValidateLastConnectionRangeInMinutes'

	--CURSOR CON VISTA DE TERMINALES DE TODOS LOS GRUPOS ANDROID
	DECLARE curTerminals CURSOR FOR
		SELECT T.ter_id, ISNULL(T.ter_fecha_actualizacion_auto, DATEADD(DAY, -1, GETDATE())) 
		FROM Terminal T 
			INNER JOIN Grupo G ON (G.gru_id = T.ter_grupo_id) 
		WHERE G.gru_tipo_android = 1	/*SOLO GRUPOS ANDROID*/

	OPEN curTerminals
	FETCH curTerminals INTO @terminalId, @lastTerminalConnectionDate

	--LOOP PARA VALIDAR ÚLTIMA FECHA DE CONEXIÓN DE LA TERMINAL
	WHILE (@@FETCH_STATUS = 0 )
	BEGIN

		--SI LA DIFERENCIA EN MINUTOS (ÚLTIMA FECHA DE CONEXIÓN REPORTADA VERSUS FECHA ACTUAL) ES MAYOR A LA PARAMETRIZADA CAMBIAR ESTADO AL DISPOSITIVO
		IF (DATEDIFF(MINUTE, @lastTerminalConnectionDate, GETDATE())) > @lastConnectionRange
		BEGIN
			UPDATE Terminal
				SET ter_tipo_estado_id = 2 /*INACTIVO*/
			WHERE ter_id = @terminalId
		END
	
		FETCH curTerminals INTO @terminalId, @lastTerminalConnectionDate
	END

	--CERRAR CURSOR
	CLOSE curTerminals
	DEALLOCATE curTerminals
	
END

GO