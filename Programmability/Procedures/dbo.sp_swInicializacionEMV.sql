﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

-- =============================================
-- Autor:				Elkin Beltrán
-- Fecha Creación:		07/Abril/2017
-- Histórico Cambios:
------------------------------------------------
--	v1.0	Versión Inicial
------------------------------------------------
-- Campos de Respuesta:		
----- OUTPUT PARAMETERS
-- =============================================

CREATE PROCEDURE [dbo].[sp_swInicializacionEMV](
	/*INPUT*/
	@msgType VARCHAR(4),
	@procCode VARCHAR(6),
	@stan VARCHAR(6),
	@serialPos VARCHAR(20),
	@ipDetected VARCHAR(20),
	@tcpPortDetected VARCHAR(6),
	/*OUTPUT*/
	@rspCode VARCHAR(2) OUTPUT,
	@rspMessage VARCHAR(200) OUTPUT,
	@rspField48 VARCHAR(250) OUTPUT,
	@rspField59 VARCHAR(8000) OUTPUT,
	@transactionId VARCHAR(20) OUTPUT,
	@procCodeOut VARCHAR(8) OUTPUT
)
 
AS
BEGIN

	SET XACT_ABORT, NOCOUNT ON
	DECLARE @startTranCount INT

	--VARIABLES LOCALES DEL SP	
	DECLARE @TerminalId BIGINT = NULL
	DECLARE @rspField59_tmp VARCHAR(1024)

	BEGIN TRY
		SELECT @startTranCount = @@TRANCOUNT

		IF @startTranCount = 0
		BEGIN
			BEGIN TRANSACTION		
			--INICIO CUERPO DEL SP

			--INICIALIZAR PARÁMETROS DE SALIDA / VARIABLES LOCALES
			SELECT @rspCode = '', @rspMessage = '', @rspField48 = '', @rspField59 = '', @transactionId = NULL

			--VALIDAR TERMINAL
			IF EXISTS( SELECT * FROM Terminal WHERE ter_serial = @serialPos )
			BEGIN

				--OBTENER DATOS DE LA TERMINAL
				SELECT @TerminalId = T.ter_id
				FROM Terminal T 
					INNER JOIN Grupo G ON (G.gru_id = T.ter_grupo_id)
				WHERE T.ter_serial = @serialPos

				--VALIDAR EL ID DE TABLA A RETORNAR
				IF @msgType = '0800' AND @procCode = '940000'
				BEGIN

					DECLARE cEMVConfig CURSOR FOR
						--TABLA ID 01
						SELECT 
							/*Tag - Table ID*/ '01' +
							/*Length*/ RIGHT(REPLICATE('0', 4) + CAST(LEN(RIGHT(REPLICATE('0', 2) + CAST(ROW_NUMBER() OVER (ORDER BY TEMV.emv_id) AS VARCHAR), 2) + emv_type + emv_config + emv_threshold_random_selection + emv_target_random_selection + emv_max_target_random_selection + emv_tac_denial + emv_tac_online + emv_tac_default + emv_application_config) / 2 AS VARCHAR), 4) +
							/*Value*/
							RIGHT(REPLICATE('0', 2) + CAST(ROW_NUMBER() OVER (ORDER BY TEMV.emv_id) AS VARCHAR), 2) + 
							emv_type + emv_config + emv_threshold_random_selection + emv_target_random_selection + 
							emv_max_target_random_selection + emv_tac_denial + emv_tac_online + emv_tac_default + emv_application_config
						FROM TerminalXEMVConfig TEMV
							INNER JOIN Terminal T ON (T.ter_id = TEMV.emv_terminal_id)
						WHERE T.ter_id = @TerminalId
						ORDER BY TEMV.emv_id;

					OPEN cEMVconfig
					FETCH cEMVconfig INTO @rspField59_tmp

					SET @rspField59 = @rspField59_tmp

					WHILE (@@FETCH_STATUS = 0 )
					BEGIN
						FETCH cEMVconfig INTO @rspField59_tmp
						SET @rspField59 = @rspField59 + @rspField59_tmp
						SET @rspField59_tmp = ''
					END

					CLOSE cEMVConfig
					DEALLOCATE cEMVConfig

					SELECT @rspCode = '00', @rspMessage = 'TRANSACCION EXITOSA OK', @rspField48 = /*Tag*/ '07' + /*Length*/ RIGHT(REPLICATE('0', 4) + CAST(LEN(@rspMessage) AS VARCHAR), 4) + /*Value*/ UPPER(REPLACE(master.dbo.fn_varbintohexstr(CAST(@rspMessage AS VARBINARY)), '0x', '')), /*Proc. Code*/ @procCodeOut = '940001'

				END
				ELSE IF @msgType = '0800' AND @procCode = '940001'
				BEGIN
					DECLARE cEMVKey CURSOR FOR
						--TABLA ID 02
						SELECT 
							/*Tag - Table ID*/ '02' +
							/*Length*/ RIGHT(REPLICATE('0', 4) + CAST(LEN(RIGHT(REPLICATE('0', 2) + CAST(ROW_NUMBER() OVER (ORDER BY TEMV.key_id) AS VARCHAR), 2) + key_index + key_application_id + key_exponent + key_size + key_content + key_effective_date + key_expiry_date + key_checksum) / 2 AS VARCHAR), 4) +
							/*Value*/
							RIGHT(REPLICATE('0', 2) + CAST(ROW_NUMBER() OVER (ORDER BY TEMV.key_id) AS VARCHAR), 2) + 
							key_index + key_application_id + key_exponent + key_size + key_content + key_effective_date + key_expiry_date + key_checksum
						FROM TerminalXEMVKey TEMV
							INNER JOIN Terminal T ON (T.ter_id = TEMV.key_terminal_id)
						WHERE T.ter_id = @TerminalId
						ORDER BY TEMV.key_id;

					OPEN cEMVKey
					FETCH cEMVKey INTO @rspField59_tmp

					SET @rspField59 = @rspField59_tmp

					WHILE (@@FETCH_STATUS = 0 )
					BEGIN
						FETCH cEMVKey INTO @rspField59_tmp
						SET @rspField59 = @rspField59 + @rspField59_tmp
						SET @rspField59_tmp = ''
					END

					CLOSE cEMVKey
					DEALLOCATE cEMVKey

					SELECT @rspCode = '00', @rspMessage = 'TRANSACCION EXITOSA OK', @rspField48 = /*Tag*/ '07' + /*Length*/ RIGHT(REPLICATE('0', 4) + CAST(LEN(@rspMessage) AS VARCHAR), 4) + /*Value*/ UPPER(REPLACE(master.dbo.fn_varbintohexstr(CAST(@rspMessage AS VARBINARY)), '0x', '')), /*Proc. Code*/ @procCodeOut = '940000'

				END
				ELSE
				BEGIN 
					SELECT @rspCode = '05', @rspMessage = 'TRANSACCION NO HABILITADA'
				END

				
			END
			ELSE
			BEGIN
				SELECT @rspCode = '05', @rspMessage = 'TERMINAL NO EXISTE'
			END

			--FIN CUERPO DEL SP

		END

		IF @startTranCount = 0 
		BEGIN

			--REGISTRAR TRANSACCIÓN
			INSERT INTO TransaccionInitEMV( tra_fecha, tra_stan, tra_serial_pos, tra_terminal_id, tra_message_type, tra_processing_code,
						tra_codigo_respuesta, tra_mensaje_respuesta, tra_direccion_ip_detectada, tra_puerto_tcp_atencion,
						tra_campo48_respuesta, tra_campo59_respuesta)
			VALUES (GETDATE(), @stan, @serialPos, @TerminalId, @msgType, @procCode, @rspCode, @rspMessage, @ipDetected, @tcpPortDetected,
						 @rspField48, @rspField59);

			--OBTENER TRANSACCIÓN ID
			SET @transactionId = CAST(SCOPE_IDENTITY() AS VARCHAR(20))

			COMMIT TRANSACTION 
		END
	END TRY
	BEGIN CATCH
		IF XACT_STATE() <> 0 AND @startTranCount = 0 
			ROLLBACK TRANSACTION

		INSERT INTO [dbo].[LogErrorBaseDatos]([bderr_fecha],[bderr_nombre_funcion],[bderr_mensaje_error],[bderr_datos_adicionales])
			 VALUES (GETDATE(), OBJECT_NAME(@@PROCID), ERROR_MESSAGE(), /*Datos Extra, Parámetros del SP*/ 
									'stan = ' + @stan + ', serialPos = ' + @serialPos + ', msgType' + @msgType +
									'procCode = ' + @procCode + ', @pDetected' + @ipDetected + ', tcpPortDetected' + @tcpPortDetected)

		DECLARE @errMessage AS VARCHAR(4000) = ERROR_MESSAGE()
		DECLARE @errSeverity AS INT = ERROR_SEVERITY()
		DECLARE @errState AS INT = ERROR_STATE()

		RAISERROR (@errMessage, @errSeverity, @errState)
	END CATCH

END




GO