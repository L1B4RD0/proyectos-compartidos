﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

-- =============================================
-- Autor:				Elkin Beltrán
-- Fecha Creación:		01/Marzo/2018
-- Histórico Cambios:
------------------------------------------------
--	v1.0	Versión Inicial		
------------------------------------------------
-- Campos de Respuesta:		SingleRow
-- 	ter_componente_1			VARCHAR
-- 	ter_componente_2			VARCHAR
-- 	ter_componentes_inyectados	BIT
-- 	ter_llave_cargada			IT
-- =============================================
CREATE PROCEDURE [dbo].[sp_webConsultarInformacionSeguridadTerminal](
	@terminalID BIGINT
)
 
AS
BEGIN
	
	SET NOCOUNT ON

	SELECT 
		ISNULL(ter_componente_1, '-1') AS ter_componente_1, ISNULL(ter_componente_2, '-1') AS ter_componente_2, ISNULL(ter_componentes_inyectados, 0), ISNULL(ter_llave_cargada, 0)
	FROM Terminal
	WHERE ter_id = @terminalID

END


GO