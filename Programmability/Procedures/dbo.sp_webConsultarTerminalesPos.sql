﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[sp_webConsultarTerminalesPos](
	@customer varchar(20),
	@groupId BIGINT,
	@terminalSerial VARCHAR(20),
	@terminalImei VARCHAR(20)
	--@terminalNombre VARCHAR(50)
	
	
)
AS
BEGIN

------------------------------------------------------------------------------------
	DECLARE @idcliente varchar(20)
	declare @idCustomer varchar(20)

	
	select @idcliente = c.cli_nombre  from Terminal t inner join Grupo g on t.ter_grupo_id = g.gru_id
	inner join Cliente c on c.cli_id=g.gru_cliente_id
	where t.ter_serial =@terminalSerial or ter_imei = @terminalImei 

	

	if @idcliente = @customer
	begin
---------------------------------------------------------------------------------------
		DECLARE @groupName  varchar(20) = 0
		SELECT @groupId=dbo.Grupo.gru_id from dbo.Grupo where dbo.grupo.gru_nombre like (@groupId)
		IF @groupId is null
	    	BEGIN
			SET @groupId = -1
			END

		SET NOCOUNT ON;

		SET @groupId = -1

		
		IF @groupId != -1 
			BEGIN
				SELECT 
				T.ter_id, T.ter_serial, ter_fecha_actualizacion_auto, MT.descripcion AS ter_modelo, T.ter_imei,T.ter_prioridad_grupo, T.ter_actualizar, CASE WHEN T.ter_fecha_programada_actualizacion IS NOT NULL THEN CONVERT(VARCHAR, T.ter_fecha_programada_actualizacion, 103) + ' ' + SUBSTRING(CONVERT(VARCHAR, T.ter_fecha_programada_actualizacion, 108), 1, 5) ELSE '' END AS ter_fecha_programacion, T.ter_ip_descarga, T.ter_puerto_descarga,
				--IMG 'S TERMINAL OG
				(CASE WHEN T.ter_tipo_estado_id = 1 THEN '../img/icons/16x16/pos.png' ELSE '../img/icons/16x16/POS.png' END) AS ter_logo,
				EA.descripcion AS ter_estado_actualizacion, (CASE WHEN T.ter_tipo_estado_id = 1 THEN 1 ELSE 0 END) ter_estado
				FROM Terminal T 
				INNER JOIN ModeloTerminal MT ON (MT.id = T.ter_modelo_terminal_id)
				INNER JOIN EstadoActualizacion EA ON (EA.id = T.ter_estado_actualizacion_id)
				WHERE 
				T.ter_grupo_id = @groupId AND
				T.ter_tipo_terminal_id = 1 AND
				(T.ter_serial LIKE '%' + @terminalSerial + '%' OR @terminalSerial = '-1')
				AND (T.ter_imei LIKE '%' + @terminalImei + '%' OR @terminalImei = '-1')
				ORDER BY T.ter_id 
			END
		ELSE
			BEGIN
				SELECT 
				T.ter_id, T.ter_serial, ter_fecha_actualizacion_auto, MT.descripcion AS ter_modelo, T.ter_imei,T.ter_prioridad_grupo, T.ter_actualizar, CASE WHEN T.ter_fecha_programada_actualizacion IS NOT NULL THEN CONVERT(VARCHAR, T.ter_fecha_programada_actualizacion, 103) + ' ' + SUBSTRING(CONVERT(VARCHAR, T.ter_fecha_programada_actualizacion, 108), 1, 5) ELSE '' END AS ter_fecha_programacion, T.ter_ip_descarga, T.ter_puerto_descarga,
				(CASE WHEN T.ter_tipo_estado_id = 1 THEN '../img/icons/16x16/pos.png' ELSE '../img/icons/16x16/POS.png' END) AS ter_logo,
				EA.descripcion AS ter_estado_actualizacion, (CASE WHEN T.ter_tipo_estado_id = 1 THEN 1 ELSE 0 END) ter_estado
				FROM Terminal T 
				INNER JOIN ModeloTerminal MT ON (MT.id = T.ter_modelo_terminal_id)
				INNER JOIN EstadoActualizacion EA ON (EA.id = T.ter_estado_actualizacion_id)
				inner join Grupo G on T.ter_grupo_id = G.gru_id
				inner join Cliente C on G.gru_cliente_id = C.cli_id
				WHERE 
				T.ter_serial = @terminalSerial  OR 
				T.ter_imei = @terminalImei OR
				T.ter_grupo_id = @groupId AND
				T.ter_tipo_terminal_id = 1 AND
				C.cli_nombre = @customer   and
				(T.ter_serial LIKE '%' + @terminalSerial + '%' OR @terminalSerial = '-1')
				AND (T.ter_imei LIKE '%' + @terminalImei + '%' OR @terminalImei = '-1')
				
				ORDER BY T.ter_id 

			END


		END

end
GO