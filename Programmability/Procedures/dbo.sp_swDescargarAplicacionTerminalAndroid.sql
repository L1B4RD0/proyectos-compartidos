﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[sp_swDescargarAplicacionTerminalAndroid](
	/*INPUT*/
	@msgType VARCHAR(4),
	@procCode VARCHAR(6),
	@stan VARCHAR(6),
	@serialPos VARCHAR(20),
	@appName VARCHAR(200),
	@offset VARCHAR(8),
	@length VARCHAR(8),
	@customerName VARCHAR(50),
	@ipPos VARCHAR(15),
	@ipDetected VARCHAR(15),
	@tcpPortDetected VARCHAR(6),
	@imeiPos VARCHAR(50),
	@simPos VARCHAR(50),
	@signalLevel VARCHAR(50),
	@batteryLevel VARCHAR(50),
	@softwareVersionPos VARCHAR(50),
	@isoField60 VARCHAR(50),
	@isoField61 VARCHAR(250),
	@totalLength VARCHAR(50),
	/*OUTPUT*/
	@rspCode VARCHAR(2) OUTPUT,
	@rspMessage VARCHAR(200) OUTPUT,
	@percentage VARCHAR(6) OUTPUT,
	@transactionId VARCHAR(20) OUTPUT
)
AS
BEGIN

	SET XACT_ABORT, NOCOUNT ON
	DECLARE @startTranCount INT

	--VARIABLES LOCALES DEL SP	
	DECLARE @SeparatorApp VARCHAR(2) = CHAR(10) --\n
	DECLARE @lengthCalculated INT = 0
	DECLARE @percentageD NUMERIC(5,2)
	DECLARE @TerminalId BIGINT = NULL
	DECLARE @ApplicationId BIGINT = NULL
	DECLARE @updateStatus BIGINT = NULL
	DECLARE @terminalStatusDesc VARCHAR(400) = ''
	DECLARE @updateStatusTerminal INT = 0
	DECLARE @ExpLicenseDatetime DATETIME
	DECLARE @GroupId BIGINT = NULL
	DECLARE @CustomerId BIGINT = NULL
	declare @packageName varchar(50) = ''
	declare @packageVersion varchar(10) = ''
	declare @filenameXML VARCHAR(50) = ''
	declare @filenameIMG VARCHAR(50) = ''
	
	BEGIN TRY
		SELECT @startTranCount = @@TRANCOUNT

		IF @startTranCount = 0
		BEGIN
			BEGIN TRANSACTION		
			--INICIO CUERPO DEL SP

			--INICIALIZAR PARÁMETROS DE SALIDA
			SELECT @rspCode = '', @rspMessage = '', @percentage = '', @transactionId = '';
			SELECT @CustomerId = cli_id FROM Cliente WHERE cli_nombre = @customerName;

			--VALIDAR CLIENTE
			IF EXISTS( SELECT * FROM Cliente WHERE cli_nombre = @customerName )
			BEGIN

				SELECT @ExpLicenseDatetime = DATEADD(DAY, TL.numero_dias + 1, lic_fecha_creacion) FROM LicenciaXCliente INNER JOIN TipoLicencia TL ON (lic_tipo_licencia_id = TL.id)  WHERE lic_cliente_id = (SELECT TOP 1 cli_id FROM Cliente WHERE cli_nombre = @customerName) AND lic_tipo_estado_id = 1 /*ACTIVA*/

				--VALIDAR FECHA DE EXPIRACIÓN DE LA LICENCIA POR CLIENTE
				IF NOT @ExpLicenseDatetime IS NULL AND @ExpLicenseDatetime > GETDATE()
				BEGIN

					--VALIDAR TERMINAL
					IF EXISTS( SELECT * FROM Terminal WHERE ter_serial = @serialPos)
					BEGIN

						--VALIDAR TERMINAL / GRUPO / CLIENTE
						IF EXISTS( SELECT * FROM Terminal T INNER JOIN Grupo G ON (G.gru_id = T.ter_grupo_id) INNER JOIN Cliente C ON (C.cli_id = G.gru_cliente_id) WHERE T.ter_serial = @serialPos AND C.cli_nombre = @customerName )
						BEGIN

							--OBTENER EL ID DE LA TERMINAL Y EL GRUPO
							SELECT @TerminalId = ter_id, @GroupId = ter_grupo_id FROM Terminal WHERE ter_serial = @serialPos

							--VALIDAR APLICACIÓN

							DECLARE @GRUPO VARCHAR (100)
							SET @GRUPO = (SELECT TOP 1 CONCAT(G.gru_nombre,'\') FROM [dbo].[GrupoXArchivosDesplegados] GXA INNER JOIN GRUPO G ON G.gru_id = GXA.desp_grupo_id
							WHERE G.gru_id = @GroupId)
							DECLARE @NameApk  VARCHAR(MAX)
							SET @NameApk= (SELECT REPLACE (@appName, @GRUPO,''))
							
							 (select @packageName = desp_nombre_paquete, @packageVersion =desp_version  from GrupoXArchivosDesplegados)

							 --exec [dbo].[sp_webActualizarInfoDescargaTerminalAndroid] @serialPos, @NameApk, @packageName, @packageVersion, @filenameXML,@filenameIMG

							IF @appName = 
							(SELECT STUFF ((SELECT @SeparatorApp  + CONCAT(G.gru_nombre,'\',GXA.desp_nombre_archivo)
							FROM [dbo].[GrupoXArchivosDesplegados] GXA INNER JOIN GRUPO G ON G.gru_id = GXA.desp_grupo_id
							WHERE G.gru_id = @GroupId and GXA.desp_nombre_archivo = @NameApk
							FOR XML PATH ('')),1,1,'')) 
							
							BEGIN
					
								SET @applicationId = (SELECT TOP 1 desp_id from [GrupoXArchivosDesplegados]  GXA INNER JOIN GRUPO G ON G.gru_id = GXA.desp_grupo_id WHERE G.gru_id = @GroupId)

								--VALIDAR PORCIÓN FINAL DEL ARCHIVO
								IF (CAST(@offset AS INT) + CAST(@length AS INT) < @totalLength)
								BEGIN

									SET @percentageD = CAST(ROUND( ((CAST(100 AS DECIMAL) * CAST(@offset AS DECIMAL)) / CAST(@totalLength AS DECIMAL)), 2) AS NUMERIC (5,2))
									SET @percentage = CAST(@percentageD AS VARCHAR(8))
									--SET @lengthCalculated = CAST(@length AS INT)
									SET @transactionId = 1
									SET @updateStatus = 2 /*PENDIENTE*/
									SELECT @rspCode = '00', @rspMessage = 'DESCARGA DE ARCHIVO OK (PENDIENTE)'

									if exists(select * from porcentajeDescarga where por_serialTerminal = @serialPos)
										update porcentajeDescarga set por_tra_fecha = GETDATE(), por_codigo_respuesta = @rspCode, 
											por_mensaje_respuesta = @rspMessage, por_porcentaje_descarga = @percentage,
											por_nombre_aplicacion=@NameApk where por_serialTerminal = @serialPos
									else
										insert into porcentajeDescarga values
											(@serialPos, GETDATE(),@rspCode, @rspMessage,@percentage,@NameApk)
									

								END
								ELSE
								BEGIN
									SET @lengthCalculated = @totalLength - CAST(@offset AS INT)	
									SET @percentage = '100'
									SET @updateStatus = 1 /*ACTUALIZADO*/
									

									SELECT @rspCode = '00', @rspMessage = 'DESCARGA DE ARCHIVO OK (COMPLETADO)'
									

									--ACTUALIZAR ESTADO TERMINAL
									SELECT @terminalStatusDesc = ter_mensaje_actualizacion FROM Terminal T WHERE T.ter_id = @TerminalId

									IF (LEN(@terminalStatusDesc)-LEN(REPLACE(@terminalStatusDesc,'{LF}','')))/LEN('{LF}') <= 1
									BEGIN
										--SIN APLICACIONES PENDIENTE
										SET @terminalStatusDesc = ''
										SET @updateStatusTerminal = 1 /*ACTUALIZADO*/
									END
									ELSE
									BEGIN
										--EXISTE MAS DE 1 APLICACION PENDIENTE POR DESCARGAR
										SET @terminalStatusDesc = REPLACE(@terminalStatusDesc, SUBSTRING(@terminalStatusDesc, CHARINDEX(@appName, @terminalStatusDesc, 0), LEN(@appName) + 9), '')
										SET @updateStatusTerminal = 2 /*PENDIENTE*/
									END

									--CAMBIAR ESTADO A REGISTRO DE TERMINAL
									UPDATE Terminal
									SET ter_estado_actualizacion_id = @updateStatusTerminal, ter_mensaje_actualizacion = @terminalStatusDesc
									WHERE ter_id = @TerminalId

									update porcentajeDescarga set por_tra_fecha = GETDATE(), por_codigo_respuesta = @rspCode, 
											por_mensaje_respuesta = @rspMessage, por_porcentaje_descarga = @percentage,
											por_nombre_aplicacion=@NameApk where por_serialTerminal = @serialPos

									exec [dbo].[sp_webActualizarInfoDescargaTerminalAndroid] @serialPos, @NameApk, @packageName, @packageVersion, @filenameXML,@filenameIMG

								END
							END
							ELSE
							BEGIN
								SELECT @rspCode = '01', @rspMessage = 'APLICACION SOLICITADA NO EXISTE'
							END
						END
						ELSE
						BEGIN
							SELECT @rspCode = '01', @rspMessage = 'TERMINAL NO VALIDA PARA EL GRUPO'
						END

					END
					ELSE
					BEGIN
						SELECT @rspCode = '01', @rspMessage = 'TERMINAL NO EXISTE'
					END

				END
				ELSE
				BEGIN
					SELECT @rspCode = '01', @rspMessage = 'LICENCIA DE USO EXPIRADA'

					--DESACTIVAR LICENCIA
					UPDATE LicenciaXCliente
					SET lic_tipo_estado_id = 2 /*INACTIVA*/
					WHERE lic_cliente_id = (SELECT TOP 1 cli_id FROM Cliente WHERE cli_nombre = @customerName)

				END

			END
			ELSE
			BEGIN
				SELECT @rspCode = '01', @rspMessage = 'CLIENTE NO EXISTE'
			END

			--FIN CUERPO DEL SP

		END

		IF @startTranCount = 0 
		BEGIN

			--REGISTRAR TRANSACCIÓN
			INSERT INTO TransaccionDescarga( tra_fecha, tra_stan, tra_serial_pos, tra_terminal_id, tra_cliente_pos, tra_message_type,tra_processing_code,
						tra_codigo_respuesta, tra_mensaje_respuesta, tra_estado_actualizacion_id, tra_aplicacion_id, tra_direccion_ip_pos, tra_direccion_ip_detectada, tra_puerto_tcp_atencion,
						tra_imei_pos, tra_sim_pos, tra_nivel_gprs, tra_nivel_bateria, tra_version_software_pos, tra_porcentaje_descarga, tra_campo60_pos, tra_campo61_pos, tra_processing_code_out)
			VALUES (GETDATE(), @stan, @serialPos, @TerminalId, @customerName, @msgType, @procCode, @rspCode, @rspMessage, @updateStatus, @applicationId, @ipPos, @ipDetected, @tcpPortDetected,
						 @imeiPos, @simPos, @signalLevel, @batteryLevel, @softwareVersionPos, @percentage, @isoField60, @isoField61, @procCode);

			--OBTENER TRANSACCIÓN ID
			SET @transactionId = CAST(SCOPE_IDENTITY() AS VARCHAR(20))

			

			COMMIT TRANSACTION 
		END
		---ALMACENAMOS EL CONTADOR DE DESCARGAS
		BEGIN TRY
		  DECLARE @statNow varchar(100) = right('0000' + convert(varchar(30), datepart(year,getdate())), 4) + 
											right('00' + convert(varchar(30), datepart(month,getdate())), 2)  +
											right('00' + convert(varchar(30), datepart(day,getdate())), 2) + ',92,I,1'
	      DECLARE @RCs int
		  DECLARE @rss varchar(50)
	      EXECUTE @RCs = [dbo].[sp_swStoreStatistics] 
				@statNow
				,@CustomerId
				,@GroupId
				,@TerminalId
				,@rss OUTPUT
		END TRY
		BEGIN CATCH
			PRINT ''
		END CATCH
	END TRY
	BEGIN CATCH
		IF XACT_STATE() <> 0 AND @startTranCount = 0 
			ROLLBACK TRANSACTION

		INSERT INTO [dbo].[LogErrorBaseDatos]([bderr_fecha],[bderr_nombre_funcion],[bderr_mensaje_error],[bderr_datos_adicionales])
			 VALUES (GETDATE(), OBJECT_NAME(@@PROCID), ERROR_MESSAGE(), /*Datos Extra, Parámetros del SP*/ 
									'stan = ' + @stan + ', serialPos = ' + @serialPos + ', customerName' + @customerName +
									'isoField60 = ' + @isoField60 + ', tcpPortDetected' + @tcpPortDetected)

		DECLARE @errMessage AS VARCHAR(4000) = ERROR_MESSAGE()
		DECLARE @errSeverity AS INT = ERROR_SEVERITY()
		DECLARE @errState AS INT = ERROR_STATE()

		RAISERROR (@errMessage, @errSeverity, @errState)
	END CATCH

END
GO