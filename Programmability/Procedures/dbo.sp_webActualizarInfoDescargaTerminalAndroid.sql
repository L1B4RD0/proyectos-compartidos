﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

-- =============================================
-- Autor:				Elkin Beltrán
-- Fecha Creación:		01/Feb/2018
-- Histórico Cambios:
------------------------------------------------
--	v1.0	Versión Inicial
------------------------------------------------
-- Campos de Respuesta:		SingleRow
-- RESPONSECODE		INT
-- GROUPNAME		VARCHAR
-- =============================================

CREATE PROCEDURE [dbo].[sp_webActualizarInfoDescargaTerminalAndroid](
	@deviceId VARCHAR(50),
	@filenameAPK VARCHAR(50),
	@packageNameAPK VARCHAR(200),
	@packageVersionAPK VARCHAR(50),
	@filenameXML VARCHAR(50),
	@filenameIMG VARCHAR(50)
)
 
AS
BEGIN

	SET XACT_ABORT, NOCOUNT ON
	DECLARE @startTranCount INT

	--VARIABLES LOCALES DEL SP
	DECLARE @responseCode AS INT
	DECLARE @groupName AS VARCHAR(100)
	DECLARE @terminalId AS BIGINT

	BEGIN TRY
		SELECT @startTranCount = @@TRANCOUNT

		IF @startTranCount = 0
		BEGIN
			BEGIN TRANSACTION		
			--INICIO CUERPO DEL SP
	
			--VALIDAR DISPOSITIVO
			IF EXISTS(SELECT * FROM Terminal WHERE ter_serial = @deviceId)
			BEGIN

				--OBTENER VALORES DE NOMBRE DE GRUPO Y TERMINAL ID
				SELECT @terminalId = T.ter_id, @groupName = G.gru_nombre FROM Terminal T INNER JOIN Grupo G ON (T.ter_grupo_id = G.gru_id) WHERE T.ter_serial = @deviceId

				--VALIDAR TIPO DE ARCHIVO
				IF LEN(RTRIM(LTRIM(@filenameAPK))) > 0
				BEGIN
					INSERT INTO TerminalXArchivosDescargados(desc_fecha, desc_terminal_id, desc_apk_archivo, desc_apk_paquete, desc_apk_version) 
					VALUES (GETDATE(), @terminalId, @filenameAPK, @packageNameAPK, @packageVersionAPK)
				END
				ELSE IF LEN(RTRIM(LTRIM(@filenameXML))) > 0
				BEGIN
					INSERT INTO TerminalXArchivosDescargados(desc_fecha, desc_terminal_id, desc_zip_xml) 
					VALUES (GETDATE(), @terminalId, @filenameXML)
				END
				ELSE IF LEN(RTRIM(LTRIM(@filenameIMG))) > 0
				BEGIN
					INSERT INTO TerminalXArchivosDescargados(desc_fecha, desc_terminal_id, desc_zip_imgs) 
					VALUES (GETDATE(), @terminalId, @filenameIMG)
				END

				--DISPOSITIVO EXISTE
				SELECT @responseCode = 1
			END
			ELSE
			BEGIN
				SELECT @responseCode = 0 --DISPOSITIVO NO EXISTE
			END
			
			--FIN CUERPO DEL SP
		END

		IF @startTranCount = 0 
		BEGIN 
			COMMIT TRANSACTION

			--SELECT FINAL
			SELECT @responseCode AS RESPONSECODE, ISNULL(@groupName,'') AS GROUPNAME
		END
	END TRY
	BEGIN CATCH
		IF XACT_STATE() <> 0 AND @startTranCount = 0 
			ROLLBACK TRANSACTION

		INSERT INTO [dbo].[LogErrorBaseDatos]([bderr_fecha],[bderr_nombre_funcion],[bderr_mensaje_error],[bderr_datos_adicionales])
			 VALUES (GETDATE(), OBJECT_NAME(@@PROCID), ERROR_MESSAGE(), /*Datos Extra, Parámetros del SP*/ 
																		'deviceId = ' + @deviceId + ', filenameAPK = ' + @filenameAPK + ', packageNameAPK = ' + @packageNameAPK + ', packageVersionAPK = ' + @packageVersionAPK + ', filenameXML = ' + @filenameXML + 
																		', filenameIMG = ' + @filenameIMG)

		DECLARE @errMessage AS VARCHAR(4000) = ERROR_MESSAGE()
		DECLARE @errSeverity AS INT = ERROR_SEVERITY()
		DECLARE @errState AS INT = ERROR_STATE()

		RAISERROR (@errMessage, @errSeverity, @errState)
	END CATCH	

END

GO