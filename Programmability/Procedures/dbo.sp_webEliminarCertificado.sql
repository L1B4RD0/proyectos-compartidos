﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[sp_webEliminarCertificado](
	@applicationId INT,
	@groupId INT
)
 
AS
BEGIN
	
	BEGIN TRANSACTION

	BEGIN TRY	

		--ELIMINAR APLICACION
		DELETE FROM [dbo].[Certificado]
		WHERE apl_id = @applicationId

		COMMIT TRANSACTION

		SELECT 1 /*OK*/

	END TRY
	BEGIN CATCH
		
		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		SELECT 0 /*ERROR*/

	END CATCH;

END













GO