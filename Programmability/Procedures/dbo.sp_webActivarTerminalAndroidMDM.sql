﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

-- =============================================
-- Autor:				Elkin Beltrán
-- Fecha Creación:		02/Feb/2018
-- Histórico Cambios:
------------------------------------------------
--	v1.0	Versión Inicial
------------------------------------------------
-- Campos de Respuesta:		SingleRow
-- RESPONSECODE		INT
-- DEVICEID			VARCHAR
-- GROUPNAME		VARCHAR
-- =============================================

CREATE PROCEDURE [dbo].[sp_webActivarTerminalAndroidMDM](
	@login VARCHAR(50),
	@pass VARCHAR(50),
	@deviceId VARCHAR(50),
	@deviceIP VARCHAR(50),
	--WS retieve Data Fields
	@heraclesID VARCHAR(50),
	@heraclesName VARCHAR(200),
	@heraclesCity VARCHAR(200),
	@heraclesRegion VARCHAR(200),
	@heraclesAgency VARCHAR(200)
)
 
AS
BEGIN

	SET XACT_ABORT, NOCOUNT ON
	DECLARE @startTranCount INT

	--VARIABLES LOCALES DEL SP
	DECLARE @responseCode AS INT
	DECLARE @terminalId AS BIGINT
	DECLARE @groupId AS BIGINT
	DECLARE @groupNameDB AS VARCHAR(50) = ''
	DECLARE @configBaseFileName AS VARCHAR(100) = 'configuracion_base.zip'
	DECLARE @terminalTypeDefault INT = 1 /*POS*/
	DECLARE @terminalBrandDefault INT = 2 /*NEW POS*/
	DECLARE @terminalModelDefault INT = 26 /*ANDROID-9220*/
	DECLARE @terminalInterfaceDefault INT = 1 /*GPRS*/
	DECLARE @allowedAppsDefaultGroup AS VARCHAR(500) = ''
	DECLARE @fixedGroupName AS VARCHAR(100) = 'DEFAULT_GROUP'

	BEGIN TRY
		SELECT @startTranCount = @@TRANCOUNT

		IF @startTranCount = 0
		BEGIN
			BEGIN TRANSACTION
			--INICIO CUERPO DEL SP
	
			--VALIDAR LOGIN/PASS CONTRA GRUPO POR DEFECTO
			IF EXISTS(SELECT * FROM GRUPO WHERE gru_tipo_android = 0 AND gru_nombre = @fixedGroupName AND gru_default_login = @login AND gru_default_password = @pass)
			BEGIN

				--VALIDAR SI EXISTE DISPOSITIVO
				IF NOT EXISTS(SELECT * FROM Terminal WHERE ter_serial = @deviceId)
				BEGIN

					--DISPOSITIVO NO EXISTE, CREAR NUEVO EN GRUPO POR DEFECTO

					--OBTENER GRUPO_ID y GRUPO NOMBRE POR DEFECTO
					SELECT @groupId = gru_id, @groupNameDB = gru_nombre, @allowedAppsDefaultGroup = gru_aplicaciones_permitidas
					FROM GRUPO WHERE gru_tipo_android = 0 AND gru_nombre = @fixedGroupName

					--INSERTAR NUEVO REGISTRO
					INSERT INTO [dbo].[Terminal]([ter_serial], [ter_tipo_terminal_id], [ter_marca_terminal_id], [ter_modelo_terminal_id], [ter_interface_terminal_id], [ter_grupo_id], 
								[ter_prioridad_grupo], [ter_actualizar], [ter_estado_actualizacion_id], [ter_tipo_estado_id], [ter_validar_imei], [ter_validar_sim], [ter_ip_dispositivo], 
								[ter_aplicaciones_permitidas], [ter_fecha_creacion], [ter_fecha_actualizacion_auto],
								/*Campos Web Service*/
								[ter_id_heracles],[ter_nombre_heracles],[ter_ciudad_heracles],[ter_region_heracles],[ter_agencia_heracles])
					VALUES (@deviceId, @terminalTypeDefault, @terminalBrandDefault, @terminalModelDefault, @terminalInterfaceDefault, @groupID,
							1 /*Default*/, 1 /*Default*/, 1 /*Default*/, 1 /*Default*/, 0 /*Default*/, 0 /*Default*/, @deviceIP, @allowedAppsDefaultGroup, GETDATE(), GETDATE(),
							@heraclesID, @heraclesName, @heraclesCity, @heraclesRegion, @heraclesAgency)

					--DISPOSITIVO CREADO
					SELECT @responseCode = 1
				END
				ELSE
				BEGIN
					SELECT @responseCode = 1 --DISPOSITIVO EXISTE, CONSULTAR NOMBRE DE GRUPO ASOCIADO

					SELECT @groupNameDB = G.gru_nombre
					FROM Terminal T INNER JOIN Grupo G ON (T.ter_grupo_id = G.gru_id) WHERE T.ter_serial = @deviceId

					--ACTUALIZAR DATOS RECIBIDOS POR EL WEB SERVICE
					UPDATE TERMINAL
					SET 
						--ACTUALIZAR PARÁMETROS POR DEFECTO PARA CADA LLAMADA MDM DEL DISPOSITIVO
						ter_fecha_actualizacion_auto = GETDATE(),
						ter_bloqueo = 0,
						ter_tipo_estado_id = 1,
						--ACTUALIZAR DATOS HERACLES RECIBIDOS POR WEB SERVICE
						ter_id_heracles = (CASE WHEN LEN(@heraclesID) > 0 THEN @heraclesID ELSE ter_id_heracles END),
						ter_nombre_heracles = (CASE WHEN LEN(@heraclesName) > 0 THEN @heraclesName ELSE ter_nombre_heracles END),
						ter_ciudad_heracles = (CASE WHEN LEN(@heraclesCity) > 0 THEN @heraclesCity ELSE ter_ciudad_heracles END),
						ter_region_heracles = (CASE WHEN LEN(@heraclesRegion) > 0 THEN @heraclesRegion ELSE ter_region_heracles END),
						ter_agencia_heracles = (CASE WHEN LEN(@heraclesAgency) > 0 THEN @heraclesAgency ELSE ter_agencia_heracles END)
					WHERE ter_serial = @deviceId

				END

			END
			ELSE
			BEGIN
				SELECT @responseCode = 0 --LOGIN/PASS POR DEFECTO ERRADOS
				SET @groupNameDB = ''
			END

			--FIN CUERPO DEL SP
		END

		IF @startTranCount = 0 
		BEGIN 
			COMMIT TRANSACTION

			--SELECT FINAL
			SELECT	@responseCode AS RESPONSECODE, 
					ISNULL(@groupNameDB,'') AS GROUPNAME,
					ISNULL(@deviceId,'') AS DEVICEID

		END
	END TRY
	BEGIN CATCH
		IF XACT_STATE() <> 0 AND @startTranCount = 0 
			ROLLBACK TRANSACTION

		INSERT INTO [dbo].[LogErrorBaseDatos]([bderr_fecha],[bderr_nombre_funcion],[bderr_mensaje_error],[bderr_datos_adicionales])
			 VALUES (GETDATE(), OBJECT_NAME(@@PROCID), ERROR_MESSAGE(), /*Datos Extra, Parámetros del SP*/ 
																		'login = ' + @login + ', pass = ' + @pass + ', ram = ' + ', deviceId = ' + @deviceId +
																		', deviceIP = ' + @deviceIP)

		DECLARE @errMessage AS VARCHAR(4000) = ERROR_MESSAGE()
		DECLARE @errSeverity AS INT = ERROR_SEVERITY()
		DECLARE @errState AS INT = ERROR_STATE()

		RAISERROR (@errMessage, @errSeverity, @errState)
	END CATCH	

END


GO