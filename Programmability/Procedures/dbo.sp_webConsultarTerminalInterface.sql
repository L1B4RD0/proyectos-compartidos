﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

	CREATE PROCEDURE [dbo].[sp_webConsultarTerminalInterface](
	@ID AS BIGINT
)
 
AS
BEGIN
 DECLARE @RESULT AS VARCHAR(50)
	SET NOCOUNT ON;


	SELECT @RESULT = descripcion
	FROM [dbo].[InterfaceTerminal] 
	WHERE id = @ID
	
	SELECT @RESULT

END
GO