﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[sp_webValidarNombreGrupoAndroid](
	@groupName VARCHAR(150)
)
 
AS
BEGIN

	DECLARE @groupID AS INT

	SELECT @groupID = G.gru_id FROM Grupo G WHERE G.gru_nombre = LTRIM(RTRIM(@groupName))

	IF @groupID IS NOT NULL
	BEGIN
		SELECT 0	--ENCONTRADO
	END
	ELSE
	BEGIN
		SELECT 1	--NO EXISTE
	END
	
END


GO