﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[sp_webValidarCargaLlaveTerminal](
	@deviceId VARCHAR(50)
)
 
AS
BEGIN

	SET XACT_ABORT, NOCOUNT ON
	DECLARE @startTranCount INT

	--VARIABLES LOCALES DEL SP
	DECLARE @responseCode AS INT
	DECLARE @component1 AS VARCHAR(100)
	DECLARE @component2 AS VARCHAR(100)
	DECLARE @flagComponentsInjected AS BIT
	DECLARE @terminalID AS BIGINT

	BEGIN TRY
		SELECT @startTranCount = @@TRANCOUNT

		IF @startTranCount = 0
		BEGIN
			BEGIN TRANSACTION		
			--INICIO CUERPO DEL SP

			SET @terminalID = -1

			--VALIDAR SI EXISTE LA TERMINAL
			IF EXISTS(SELECT * FROM Terminal WHERE ter_serial = @deviceId)
			BEGIN

				SELECT @terminalID = ter_id, @component1 = ter_componente_1, @component2 = ter_componente_2, @flagComponentsInjected = ter_componentes_inyectados 
				FROM Terminal 
				WHERE ter_serial = @deviceId

				--VALIDAR FLAG DE COMPONENTES CARGADOS
				IF @flagComponentsInjected = 1
				BEGIN
					--VALIDAR CONTENIDO DE COMPONENTES
					IF NOT @component1 IS NULL AND NOT @component2 IS NULL
					BEGIN
						SELECT @responseCode = 1
					END
					ELSE
					BEGIN
						SELECT @responseCode = 0
					END
				END
				ELSE
				BEGIN
					SELECT @responseCode = 0
				END			
			END
			ELSE
			BEGIN
				SELECT @responseCode = 0
			END			

			--FIN CUERPO DEL SP
		END

		IF @startTranCount = 0 
		BEGIN 
			COMMIT TRANSACTION

			--SELECT FINAL
			SELECT @responseCode AS RESPONSECODE, @terminalID AS TERMINALID
		END
	END TRY
	BEGIN CATCH
		IF XACT_STATE() <> 0 AND @startTranCount = 0 
			ROLLBACK TRANSACTION

		INSERT INTO [dbo].[LogErrorBaseDatos]([bderr_fecha],[bderr_nombre_funcion],[bderr_mensaje_error],[bderr_datos_adicionales])
			 VALUES (GETDATE(), OBJECT_NAME(@@PROCID), ERROR_MESSAGE(), /*Datos Extra, Parámetros del SP*/ 
																		'deviceId = ' + @deviceId)

		DECLARE @errMessage AS VARCHAR(4000) = ERROR_MESSAGE()
		DECLARE @errSeverity AS INT = ERROR_SEVERITY()
		DECLARE @errState AS INT = ERROR_STATE()

		RAISERROR (@errMessage, @errSeverity, @errState)
	END CATCH	

END



GO