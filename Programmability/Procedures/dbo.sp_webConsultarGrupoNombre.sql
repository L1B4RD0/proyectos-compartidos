﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

	create PROCEDURE [dbo].[sp_webConsultarGrupoNombre](
	@ID AS BIGINT
)
 
AS
BEGIN
 DECLARE @RESULT AS VARCHAR(50)
	SET NOCOUNT ON;


	SELECT @RESULT = gru_nombre
	FROM [dbo].[Grupo] 
	WHERE gru_id = @ID
	
	SELECT @RESULT

END

GO