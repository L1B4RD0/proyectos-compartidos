﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[sp_webConsultarArchivosDesplegadosTerminalAndroid](
	@terminalId BIGINT,
	@deployTypeId INT
)
 
AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT desp_id, desp_fecha, desp_nombre_archivo, desp_nombre_paquete, desp_version
	FROM [dbo].[TerminalesXArchivosDesplegados]
	WHERE desp_terminal_id = @terminalId AND desp_tipo_despliegue_id = @deployTypeId
	ORDER BY desp_id DESC

END



GO