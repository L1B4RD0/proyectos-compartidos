﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[sp_webConsultarGrupos]
(
	@customerID BIGINT,
	@groupName VARCHAR(50),
	@groupDesc VARCHAR(100)
)
 
AS
BEGIN
	
	SET NOCOUNT ON

	SELECT 
		G.gru_id,
		C.cli_nombre,
		G.gru_nombre, 
		(SELECT COUNT(*) FROM Terminal T WHERE T.ter_grupo_id = G.gru_id) AS gru_total_terminales,
		G.gru_descripcion,
		(CASE WHEN G.gru_actualizar = 1 THEN 1 ELSE 0 END) AS gru_actualizar,
		(CASE WHEN G.gru_programar_fecha = 1 THEN 1 ELSE 0 END) AS gru_programar_fecha,
		CONVERT(VARCHAR, G.gru_fecha_programada_actualizacion_ini, 103) + ' ' + SUBSTRING(CONVERT(VARCHAR, G.gru_fecha_programada_actualizacion_ini, 108), 1, 5) AS gru_fecha_programada_actualizacion_ini,
		CONVERT(VARCHAR, G.gru_fecha_programada_actualizacion_fin, 103) + ' ' + SUBSTRING(CONVERT(VARCHAR, G.gru_fecha_programada_actualizacion_fin, 108), 1, 5) AS gru_fecha_programada_actualizacion_fin,
		SUBSTRING(CONVERT(VARCHAR, G.gru_rango_hora_actualizacion_ini, 108), 1, 5) AS gru_rango_hora_actualizacion_ini,
		SUBSTRING(CONVERT(VARCHAR, G.gru_rango_hora_actualizacion_fin, 108), 1, 5) AS gru_rango_hora_actualizacion_fin,
		G.gru_ip_descarga,
		G.gru_puerto_descarga
	FROM Grupo G
		INNER JOIN cliente C ON (C.cli_id = G.gru_cliente_id)
	WHERE C.cli_id = @customerID AND G.gru_tipo_android = 0 /*NO Android*/ AND (G.gru_nombre LIKE '%' + @groupName + '%' OR @groupName = '-1')
			AND (G.gru_descripcion LIKE '%' + @groupDesc + '%' OR @groupDesc = '-1')

END



GO