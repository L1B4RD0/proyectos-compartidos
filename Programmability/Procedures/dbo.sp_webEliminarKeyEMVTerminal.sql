﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[sp_webEliminarKeyEMVTerminal](
	@emvKeyId INT,
	@terminalId INT
)
 
AS
BEGIN
	
	BEGIN TRANSACTION

	BEGIN TRY	
	
		--ELIMINAR LLAVE
		DELETE FROM TerminalXEMVKey
		WHERE key_id = @emvKeyId AND key_terminal_id = @terminalId

		COMMIT TRANSACTION

		SELECT 1 /*OK*/

	END TRY
	BEGIN CATCH
		
		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		SELECT 0 /*ERROR*/

	END CATCH;

END














GO