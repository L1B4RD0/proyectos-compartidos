﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO


-- =============================================
-- Autor:				Elkin Beltrán
-- Fecha Creación:		04/Abril/2017
-- Histórico Cambios:
------------------------------------------------
--	v1.0	Versión Inicial
------------------------------------------------
-- Campos de Respuesta:		SingleRow
-- RESPONSECODE		INT
-- =============================================

CREATE PROCEDURE [dbo].[sp_webEditarKeyEMVTerminal](
	@emvKeyId BIGINT,
	@terminalId BIGINT,	
	@keyIndex VARCHAR(2),
	@keyApplicationId VARCHAR(10),
	@keyExponent VARCHAR(8),
	@keySize VARCHAR(8),
	@keyContent VARCHAR(512),
	@keyExpiryDate VARCHAR(4),
	@keyEffectiveDate VARCHAR(4),
	@keyChecksum VARCHAR(2)
)
 
AS
BEGIN

	SET XACT_ABORT, NOCOUNT ON
	DECLARE @startTranCount INT

	--VARIABLES LOCALES DEL SP
	DECLARE @responseCode AS INT

	BEGIN TRY
		SELECT @startTranCount = @@TRANCOUNT

		IF @startTranCount = 0
		BEGIN
			BEGIN TRANSACTION		
			--INICIO CUERPO DEL SP

			UPDATE TerminalXEMVKey
			SET 
				[key_index] = @keyIndex,
				[key_application_id] = @keyApplicationId,
				[key_exponent] = @keyExponent,
				[key_size] = @keySize,
				[key_content] = @keyContent,
				[key_expiry_date] = @keyExpiryDate,
				[key_effective_date] = @keyEffectiveDate,
				[key_checksum] = @keyChecksum
			WHERE [key_id] = @emvKeyId AND key_terminal_id = @terminalId

			SELECT @responseCode = 1

			--FIN CUERPO DEL SP
		END

		IF @startTranCount = 0 
		BEGIN 
			COMMIT TRANSACTION

			--SELECT FINAL
			SELECT @responseCode AS RESPONSECODE
		END
	END TRY
	BEGIN CATCH
		IF XACT_STATE() <> 0 AND @startTranCount = 0 
			ROLLBACK TRANSACTION

		INSERT INTO [dbo].[LogErrorBaseDatos]([bderr_fecha],[bderr_nombre_funcion],[bderr_mensaje_error],[bderr_datos_adicionales])
			 VALUES (GETDATE(), OBJECT_NAME(@@PROCID), ERROR_MESSAGE(), /*Datos Extra, Parámetros del SP*/ 
																		'terminalId = ' + CAST(@terminalId AS VARCHAR) + ', keyIndex = ' + @keyIndex + ', keyApplicationId = ' + @keyApplicationId + 
																		', keyExponent = ' + @keyExponent + ', keySize = ' + @keySize + ', keyExpiryDate = ' + @keyExpiryDate  + ', keyEffectiveDate = ' + @keyEffectiveDate)

		DECLARE @errMessage AS VARCHAR(4000) = ERROR_MESSAGE()
		DECLARE @errSeverity AS INT = ERROR_SEVERITY()
		DECLARE @errState AS INT = ERROR_STATE()

		RAISERROR (@errMessage, @errSeverity, @errState)
	END CATCH	

END


GO