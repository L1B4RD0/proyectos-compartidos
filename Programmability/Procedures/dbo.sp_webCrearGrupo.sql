﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[sp_webCrearGrupo](
	@groupName VARCHAR(50),
	--@CodeExtern VARCHAR(50),
	@groupDesc VARCHAR(250),
	@groupClaveMDM VARCHAR(8),
	@groupIP VARCHAR(20),
	@groupPort VARCHAR(10),
	@allowUpdate BIT,
	@useDateRange BIT,
	@RangeDateIni DATETIME,
	@RangeDateEnd DATETIME,
	@RangeHourIni DATETIME,
	@RangeHourEnd DATETIME,
	@customerID BIGINT,
	@blockingMode BIT,
	@updateNow BIT,
	--MDM Fields
	@isAndroidGroup BIT = FALSE,
	@defaultApplications VARCHAR(500) = NULL,
	@numberOfQueries SMALLINT = NULL,
	@frequency SMALLINT = NULL
)
 
AS
BEGIN

	SET XACT_ABORT, NOCOUNT ON
	DECLARE @startTranCount INT

	--VARIABLES LOCALES DEL SP
	DECLARE @responseCode AS INT

	BEGIN TRY
		SELECT @startTranCount = @@TRANCOUNT

		IF @startTranCount = 0
		BEGIN
			BEGIN TRANSACTION		
			--INICIO CUERPO DEL SP
	
			IF @useDateRange = 1
			BEGIN
				--INSERTAR NUEVO GRUPO - CON USO DE RANGO DE FECHAS
				INSERT INTO Grupo(gru_nombre, /**** gru_codigo_externo */ gru_descripcion, gru_ip_descarga, gru_puerto_descarga, gru_actualizar, gru_programar_fecha, gru_fecha_programada_actualizacion_ini, gru_fecha_programada_actualizacion_fin, gru_rango_hora_actualizacion_ini, gru_rango_hora_actualizacion_fin, gru_cliente_id, gru_blocking_mode, gru_update_now, gru_tipo_android, gru_aplicaciones_permitidas, gru_numero_consultas, gru_frecuencia_horas, gru_fecha_creacion, gru_clave_configurarMDM)
						VALUES (@groupName,/*@CodeExtern*/ @groupDesc, @groupIP, @groupPort, @allowUpdate, @useDateRange, @RangeDateIni, @RangeDateEnd, NULL, NULL, @customerID, @blockingMode, @updateNow, @isAndroidGroup, @defaultApplications, @numberOfQueries, @frequency, GETDATE(),@groupClaveMDM)
			END
			ELSE
			BEGIN
			--INSERTAR NUEVO GRUPO
			INSERT INTO Grupo(gru_nombre, /*gru_codigo_externo*/gru_descripcion, gru_ip_descarga, gru_puerto_descarga, gru_actualizar, gru_programar_fecha, gru_fecha_programada_actualizacion_ini, gru_fecha_programada_actualizacion_fin, gru_rango_hora_actualizacion_ini, gru_rango_hora_actualizacion_fin, gru_cliente_id, gru_blocking_mode, gru_update_now, gru_tipo_android, gru_aplicaciones_permitidas, gru_numero_consultas, gru_frecuencia_horas, gru_fecha_creacion,gru_clave_configurarMDM)
					VALUES (@groupName, /*@CodeExtern*/@groupDesc, @groupIP, @groupPort, @allowUpdate, @useDateRange, NULL, NULL, CONVERT(TIME, @RangeHourIni), CONVERT(TIME, @RangeHourEnd), @customerID, @blockingMode, @updateNow, @isAndroidGroup, @defaultApplications, @numberOfQueries, @frequency, GETDATE(),@groupClaveMDM)
			END

			SELECT @responseCode = 1

			--FIN CUERPO DEL SP
		END

		IF @startTranCount = 0 
		BEGIN 
			COMMIT TRANSACTION

			--SELECT FINAL
			SELECT @responseCode AS RESPONSECODE
		END
	END TRY
	BEGIN CATCH
		IF XACT_STATE() <> 0 AND @startTranCount = 0 
			ROLLBACK TRANSACTION

		INSERT INTO [dbo].[LogErrorBaseDatos]([bderr_fecha],[bderr_nombre_funcion],[bderr_mensaje_error],[bderr_datos_adicionales])
			 VALUES (GETDATE(), OBJECT_NAME(@@PROCID), ERROR_MESSAGE(), /*Datos Extra, Parámetros del SP*/ 
																		'groupName = ' + @groupName + ', groupDesc = ' + @groupDesc + ', groupIP = ' + @groupIP + 
																		', groupPort = ' + @groupPort)

		DECLARE @errMessage AS VARCHAR(4000) = ERROR_MESSAGE()
		DECLARE @errSeverity AS INT = ERROR_SEVERITY()
		DECLARE @errState AS INT = ERROR_STATE()

		RAISERROR (@errMessage, @errSeverity, @errState)
	END CATCH	

END
GO