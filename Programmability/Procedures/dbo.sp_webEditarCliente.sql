﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[sp_webEditarCliente](
	@customerID BIGINT,
	@customerName VARCHAR(50),
	@customerDesc VARCHAR(250),
	@customerIP VARCHAR(20),
	@customerPort VARCHAR(10),
	@customerStatus BIGINT,
	@learningMode BIT,
	--Contact Data
	@contactIDNumber VARCHAR(20),
	@contactIDType INT,
	@contactName VARCHAR(50),
	@contactAddress VARCHAR(100),
	@contactMobile VARCHAR(20),
	@contactPhone VARCHAR(20),
	@contactEmail VARCHAR(50)	
)
 
AS
BEGIN

	SET XACT_ABORT, NOCOUNT ON
	DECLARE @startTranCount INT

	--VARIABLES LOCALES DEL SP
	DECLARE @responseCode AS INT
	DECLARE @contactId AS BIGINT
	DECLARE @msg AS VARCHAR(500)
	DECLARE @autoLearning BIT
	DECLARE @groupNumber INT

	BEGIN TRY

	    SET @responseCode = 0

	    --VALIDAMOS EL MODO AUTOAPRENDIZAJE
		--IF @learningMode = 1 
		--BEGIN
		--   --VALIDAR QUE EXISTA EL GRUPO INVENTARIO EN AUTOLEARNING, EN CASO CONTRARIO DETENER EL PROCESO
		--   SELECT @groupNumber = count(*) FROM Grupo G INNER JOIN Cliente C ON (C.cli_id = G.gru_cliente_id) WHERE (C.cli_nombre = @customerName AND G.gru_tipo_android = 0) AND G.gru_nombre = 'INVENTARIO'
		--   IF @groupNumber > 1 
		--   BEGIN
		--	SET @responseCode = 2
		--   END
		--END
        
		IF @learningMode = 1 
		BEGIN
		   --VALIDAR QUE EXISTA EL GRUPO INVENTARIO EN AUTOLEARNING, EN CASO CONTRARIO DETENER EL PROCESO
		   SELECT @groupNumber = count(*) FROM Grupo G INNER JOIN Cliente C ON (C.cli_id = G.gru_cliente_id) WHERE (C.cli_nombre = @customerName AND G.gru_tipo_android = 0) AND G.gru_nombre = 'INVENTARIO'
		   IF @groupNumber != 1 
		   BEGIN
			SET @responseCode = 2
		   END
		END


		SELECT @startTranCount = @@TRANCOUNT

		IF @responseCode = 0

		BEGIN
			IF @startTranCount = 0
			BEGIN
				BEGIN TRANSACTION		
				--INICIO CUERPO DEL SP
	
				--VALIDAR CREACIÓN DE CONTACTO
				IF (LEN(@contactIDNumber) <= 0)










				BEGIN
					--SIN DATOS DE CONTACTO



					SET @contactId = NULL
				END
				ELSE
				BEGIN
					--CON DATOS DE CONTACTO, VALIDAR SI ES EXISTENTE O ES NUEVO
					SELECT @contactId = T.terc_id FROM Tercero T WHERE T.terc_numero_identificacion = LTRIM(RTRIM(@contactIDNumber))

					IF @contactId IS NULL
					BEGIN
						--ES NUEVO CONTACTO, CREARLO
						INSERT INTO Tercero(terc_tipo_identificacion_id, terc_numero_identificacion, terc_nombre, terc_direccion, terc_celular, terc_telefono_fijo, terc_correo_electronico, terc_fecha_creacion)
							VALUES (@contactIDType, @contactIDNumber, @contactName, @contactAddress, @contactMobile, @contactPhone, @contactEmail, GETDATE())

						SET @contactId = SCOPE_IDENTITY()
					END
					ELSE
					BEGIN
						--EXISTE, ACTUALIZAR CONTACTO
						UPDATE Tercero
						SET 

							terc_tipo_identificacion_id = @contactIDType,
							terc_numero_identificacion = @contactIDNumber,
							terc_nombre = @contactName,
							terc_direccion = @contactAddress,
							terc_celular = @contactMobile,
							terc_telefono_fijo = @contactPhone,
							terc_correo_electronico = @contactEmail,
							terc_fecha_actualizacion = GETDATE()
						WHERE terc_id = @contactId
					END
				END

				--EDITAR CLIENTE
				UPDATE cliente
				SET
					cli_nombre = @customerName,
					cli_tercero_id = @contactId,
					cli_descripcion = @customerDesc,
					cli_ip_descarga = @customerIP,
					cli_puerto_descarga = @customerPort,
					cli_tipo_estado_id = @customerStatus,
					cli_autoaprendizaje = @learningMode,
					cli_fecha_actualizacion = GETDATE()
				WHERE cli_id = @customerID


				SELECT @responseCode = 1












				--FIN CUERPO DEL SP
			END


			IF @startTranCount = 0 
			BEGIN 
				COMMIT TRANSACTION
			END

		END
		

			--SELECT FINAL
			IF @responseCode = 1 
			BEGIN
			   SET @msg = 'CLIENTE EDITADO EN FORMA CORRECTA'
			END
			ELSE IF @responseCode = 2
			BEGIN
			   SET @responseCode = 0
			   --SET @msg = 'NO SE PUEDE HABILITAR EL MODO DE AUTO APRENDIZAJE - SE DEBE CONFIGURAR UN SOLO GRUPO POR CLIENTE'
			   SET @msg = 'NO SE PUEDE HABILITAR EL MODO DE AUTO APRENDIZAJE - SE DEBE CONFIGURAR EL GRUPO INVENTARIO'
			END
			ELSE 

			BEGIN
			   SET @responseCode = 0
			   SET @msg = 'CLIENTE NO FUE EDITADO EN FORMA CORRECTA'
			END
			
			SELECT @responseCode AS RESPONSECODE, @msg as MESSAGE 





	END TRY
	BEGIN CATCH
		IF XACT_STATE() <> 0 AND @startTranCount = 0 
			ROLLBACK TRANSACTION

		INSERT INTO [dbo].[LogErrorBaseDatos]([bderr_fecha],[bderr_nombre_funcion],[bderr_mensaje_error],[bderr_datos_adicionales])
			 VALUES (GETDATE(), OBJECT_NAME(@@PROCID), ERROR_MESSAGE(), /*Datos Extra, Parámetros del SP*/ 
																		'customerName = ' + @customerName + ', customerDesc = ' + @customerDesc + ', customerIP = ' + @customerIP + 
																		', customerPort = ' + @customerPort)

		DECLARE @errMessage AS VARCHAR(4000) = ERROR_MESSAGE()
		DECLARE @errSeverity AS INT = ERROR_SEVERITY()
		DECLARE @errState AS INT = ERROR_STATE()

		RAISERROR (@errMessage, @errSeverity, @errState)
	END CATCH	

END



GO