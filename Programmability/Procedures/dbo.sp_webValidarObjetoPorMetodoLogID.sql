﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[sp_webValidarObjetoPorMetodoLogID](
	@UserID AS INT,
	@Object AS VARCHAR(100),
	@Method AS VARCHAR(100),
	@SessionID AS VARCHAR(100)
)
 
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @FuncID AS INT
	DECLARE @LogID AS BIGINT

	SELECT @FuncID = F.func_id
	FROM Usuario U
		INNER JOIN Perfil P ON (U.usu_perfil_id = P.perf_id)
		INNER JOIN PerfilXFuncion PXF ON (P.perf_id = PXF.perf_id)
		INNER JOIN Funcion F ON (F.func_id = PXF.func_id)
		INNER JOIN Objeto O ON (O.id = F.func_objeto_id)
		INNER JOIN Metodo M ON (M.id = F.func_metodo_id)
	WHERE 
		U.usu_id = @UserID AND
		O.nombre = @Object AND
		M.nombre = @Method 

	IF @FuncID IS NOT NULL
	BEGIN
		INSERT INTO Log
			(log_usuario_id, log_fecha, log_funcion_id, log_tipo_log_id ,log_mensaje1, log_mensaje2, log_sessionID)
		VALUES
			(@UserID, GETDATE(), @FuncID, 5, '', '', @SessionID)
						
		SELECT @LogID = SCOPE_IDENTITY()
			
		SELECT @FuncID AS FuncID, @LogID AS LogID
	END
	ELSE
	BEGIN
		SELECT @FuncID = F.func_id
		FROM Funcion F 
			INNER JOIN Objeto O ON (O.id = F.func_objeto_id)
			INNER JOIN Metodo M ON (M.id = F.func_metodo_id)
		WHERE 
			O.nombre = @Object AND
			M.nombre = @Method 		
			
		IF @FuncID IS NOT NULL		
		BEGIN
			INSERT INTO Log
				(log_usuario_id, log_fecha, log_funcion_id, log_tipo_log_id ,log_mensaje1, log_mensaje2, log_sessionID)
			VALUES
				(@UserID, GETDATE(), @FuncID, 3, '', '', @SessionID)
				
			SELECT @LogID = SCOPE_IDENTITY()				
				
			SELECT NULL AS FuncID, @LogID AS LogID		
		END
		ELSE
		BEGIN
			INSERT INTO Log
				(log_usuario_id, log_fecha, log_funcion_id, log_tipo_log_id ,log_mensaje1, log_mensaje2, log_sessionID)
			VALUES
				(@UserID, GETDATE(), @FuncID, 3, 'Invalid Object.Method (' + ISNULL(@Object, '') + '.' + ISNULL(@Method, '') + ')', '', @SessionID)
			
			SELECT @LogID = SCOPE_IDENTITY()			
				
			SELECT NULL AS FuncID, @LogID AS LogID
		END
				
	END
	
END







GO