﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

	CREATE PROCEDURE [dbo].[sp_webConsultarTerminalBrand](
	@ID AS BIGINT
)
 
AS
BEGIN
 DECLARE @RESULT AS VARCHAR(50)
	SET NOCOUNT ON;


	SELECT @RESULT = descripcion
	FROM [Polaris].[MarcaTerminal] 
	WHERE id = @ID
	
	SELECT @RESULT

END
   
GO