﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[sp_webEliminarArchivoDesplegadoXGrupoAndroidCTL](
	@filename VARCHAR(520),
	@deployedFileId  VARCHAR(520)
)
 
AS
BEGIN
	
	BEGIN TRANSACTION

	BEGIN TRY	
	
		--ELIMINAR APLICACION
		DELETE FROM GrupoXArchivosDesplegadosCtl
		WHERE desp_nombre_archivo = @filename  and desp_id = @deployedFileId

		COMMIT TRANSACTION

		SELECT 1 /*OK*/

	END TRY
	BEGIN CATCH
		
		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		SELECT 0 /*ERROR*/

	END CATCH;

END


GO