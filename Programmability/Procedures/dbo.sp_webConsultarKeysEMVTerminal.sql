﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[sp_webConsultarKeysEMVTerminal](
	@terminalId BIGINT
)
 
AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT 
		TEMV.key_id, TEMV.key_index, TEMV.key_application_id, TEMV.key_exponent,
		TEMV.key_size, TEMV.key_expiry_date, TEMV.key_effective_date, TEMV.key_checksum
	FROM TerminalXEMVKey TEMV
		INNER JOIN Terminal T ON (T.ter_id = TEMV.key_terminal_id)
	WHERE T.ter_id = @terminalId;

END



GO