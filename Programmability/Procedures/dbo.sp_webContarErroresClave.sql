﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[sp_webContarErroresClave](
	@userID INT
)
 
AS
BEGIN

	DECLARE @errorCounter AS INT

	SELECT @errorCounter = COUNT(*) FROM LoginInfo WHERE login_tipo_id = 3 AND login_usuario_id = @userID AND login_fecha_inicio BETWEEN DATEADD(MINUTE,-30,GETDATE()) AND GETDATE()

	SELECT @errorCounter AS ERRORCOUNTER

END

GO