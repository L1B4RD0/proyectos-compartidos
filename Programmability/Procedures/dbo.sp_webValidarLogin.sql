﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[sp_webValidarLogin](
	@userID VARCHAR(150),
	@passwordID VARCHAR(150),
	@IP VARCHAR(20),
	@sessionID VARCHAR(50),
	@webEntryType INT	/*1) Administración Transaccional(PHP-Admin) , 2) Usuarios WEB (ASP .Net)*/
)
 
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @loginID AS INT
	DECLARE @loginType AS INT
	DECLARE @userID_DB AS INT
	DECLARE @userIDInfo_DB AS INT
	DECLARE @userType AS INT
	DECLARE @userName AS VARCHAR(150)
	DECLARE @entityName AS VARCHAR(150)	DECLARE @expDatetime AS DATETIME
								 
	DECLARE @errorCounter AS INT
	DECLARE @userStatus AS INT
	DECLARE @userCustomer AS INT
	DECLARE @customerName AS VARCHAR(150)
	DECLARE @userPerfil AS INT
	DECLARE @licenseType AS INT
	DECLARE @licenseStatus AS INT
	DECLARE @licenseExpiryDate AS DATETIME

	IF EXISTS(SELECT * FROM Usuario U WHERE U.usu_login COLLATE Latin1_General_CS_AS = @userID)
	BEGIN
		--Nombre de Usuario Existe

		--Validar si existen Sesiones Activas de ese usuario para la fecha actual
		--IF NOT EXISTS(SELECT * FROM LoginInfo 
		--				WHERE login_usuario_id = (SELECT U.usu_id FROM Usuario U WHERE U.usu_login COLLATE Latin1_General_CS_AS = @userID) AND CONVERT(VARCHAR, login_fecha_inicio, 103) = CONVERT(VARCHAR, GETDATE(), 103) AND
		--						login_tipo_id = 1 AND login_fecha_fin IS NULL
		--	)
		--BEGIN
			--Validar Password
			IF EXISTS(SELECT * FROM Usuario U WHERE U.usu_login COLLATE Latin1_General_CS_AS = @userID AND U.usu_hash = @passwordID)
			BEGIN
				--Clave Correcta
				
				IF @webEntryType = 1	/*Administración Transaccional*/
					BEGIN
						SELECT TOP 1 @userID_DB = U.usu_id, @userType = U.usu_perfil_id, 
						@userName = U.usu_nombre, @entityName = P.perf_nombre, 
						@expDatetime = U.usu_fecha_exp_clave, 
						@userStatus = U.usu_estado_usuario_id,
						@userCustomer = CASE WHEN UXC.cliente_id IS NULL THEN 0 ELSE UXC.cliente_id END,
						@customerName = CASE WHEN C.cli_id IS NULL THEN '-' ELSE C.cli_nombre END,
						@userPerfil = P.perf_id
						FROM Usuario U
							INNER JOIN Perfil P ON (U.usu_perfil_id = P.perf_id)
							LEFT JOIN UsuarioXCliente UXC ON(U.usu_id = UXC.usuario_id)
							LEFT JOIN Cliente C ON (C.cli_id = UXC.cliente_id)
						WHERE U.usu_login COLLATE Latin1_General_CS_AS = @userID AND U.usu_hash = @passwordID AND U.usu_perfil_id = 6			
					END

				ELSE IF @webEntryType = 2	/*Usuarios WEB (ASP .Net)*/
					BEGIN
						SELECT TOP 1 @userID_DB = U.usu_id, @userType = U.usu_perfil_id, 
						@userName = U.usu_nombre, @entityName = P.perf_nombre, 
						@expDatetime = U.usu_fecha_exp_clave, 
						@userStatus = U.usu_estado_usuario_id, 
						@userCustomer = CASE WHEN UXC.cliente_id IS NULL THEN 0 ELSE UXC.cliente_id END,
						@customerName = CASE WHEN C.cli_id IS NULL THEN '-' ELSE C.cli_nombre END,
						@userPerfil = P.perf_id
						FROM Usuario U
							INNER JOIN Perfil P ON (U.usu_perfil_id = P.perf_id)
							LEFT JOIN UsuarioXCliente UXC ON(U.usu_id = UXC.usuario_id)
							LEFT JOIN Cliente C ON (C.cli_id = UXC.cliente_id)
						WHERE U.usu_login COLLATE Latin1_General_CS_AS = @userID AND U.usu_hash = @passwordID AND U.usu_perfil_id <> 6			
					END

				IF NOT @userID_DB IS NULL
				BEGIN

					--VALIDAR ESTADO DE USUARIO
					IF @userStatus = 1
					BEGIN
						--ACTIVADO
						SELECT @loginType = 1, @userIDInfo_DB = @userID_DB

						--VALIDAR LICENCIA DEL USUARIO CLIENTE
						IF @userPerfil = 8 /*CLIENTE*/
						BEGIN

							SET DATEFORMAT DMY
						
							--VALIDAR LICENCIA POR CLIENTE POR USUARIO
							SELECT TOP 1 @licenseType = LxC.lic_tipo_licencia_id, @licenseStatus = LxC.lic_tipo_estado_id, @licenseExpiryDate = (CASE WHEN LxC.lic_tipo_estado_id = 1 THEN CONVERT(VARCHAR, DATEADD(DAY, TL.numero_dias + 1, LxC.lic_fecha_creacion), 103) + ' ' + SUBSTRING(CONVERT(VARCHAR, DATEADD(DAY, TL.numero_dias + 1, LxC.lic_fecha_creacion), 108), 1, 5) ELSE DATEADD(DAY, -1, GETDATE()) END) FROM LicenciaXCliente LxC INNER JOIN Cliente C ON (C.cli_id = LxC.lic_cliente_id) INNER JOIN UsuarioXCliente UxC ON (UxC.cliente_id = C.cli_id) INNER JOIN TipoLicencia TL ON (LxC.lic_tipo_licencia_id = TL.id) WHERE UxC.usuario_id = @userID_DB ORDER BY LxC.lic_id DESC

							IF @licenseType IS NULL
							BEGIN
								--LICENCIA NO EXISTE PARA EL USUARIO CLIENTE
								SELECT @loginType = 7, @userIDInfo_DB = @userID_DB
							END
							ELSE
							BEGIN
								--VALIDAR ESTADO DE LICENCIA
								IF @licenseStatus = 2 /*INACTIVO - EXPIRADA*/
								BEGIN
									SELECT @loginType = 8, @userIDInfo_DB = @userID_DB	
								END
							END

							SET DATEFORMAT MDY

						END

					END
					ELSE
					BEGIN
						--DESACTIVADO
						SELECT @loginType = 6, @userIDInfo_DB = @userID_DB
					END

				END
				ELSE
				BEGIN
					SELECT @loginType = 5, @userID_DB = -1, @userIDInfo_DB = -1, @userType=-1, @userName = '', @entityName = '', @expDatetime = GETDATE(), @userPerfil = -1
				END
			
			END
			ELSE
			BEGIN
			
				--VALIDAR ESTADO
				SELECT @userStatus = U.usu_estado_usuario_id
				FROM Usuario U
				WHERE U.usu_login = @userID
			
				IF @userStatus = 1
				BEGIN
					--CLAVE ERRÓNEA
					SELECT @loginType = 3, @userID_DB = -1, @userIDInfo_DB = (SELECT U.usu_id FROM Usuario U WHERE U.usu_login = @userID), @userType=-1, @userName = '', @entityName = '', @expDatetime = GETDATE(), @userPerfil = -1
				END
				ELSE
				BEGIN
					--DESACTIVADO
					SELECT @loginType = 6, @userID_DB = -1, @userIDInfo_DB = (SELECT U.usu_id FROM Usuario U WHERE U.usu_login = @userID), @userType=-1, @userName = '', @entityName = '', @expDatetime = GETDATE(), @userPerfil = -1	
				END		
			END		
		--END
		--ELSE
		--BEGIN
		--	--Sesiones Activas Encontradas
		--	SELECT @loginType = 4, @userID_DB = -1, @userIDInfo_DB = (SELECT U.usu_id FROM Usuario U WHERE U.usu_login = @userID), @userType=-1, @userName = '', @entityName = '', @expDatetime = GETDATE()			
		--END
	END
	ELSE
	BEGIN
		SELECT @loginType = 2, @userID_DB = -1, @userIDInfo_DB = -1, @userType=-1, @userName = '', @entityName = '', @expDatetime = GETDATE(), @userPerfil = -1
	END

	--Datos de la Sesion
	INSERT INTO LoginInfo(login_fecha_inicio, login_tipo_id, login_usuario_id, login_ip, login_session_id)
		VALUES (GETDATE(), @loginType, @userIDInfo_DB, @IP, @sessionID)					

	SELECT @loginID = SCOPE_IDENTITY()

	--Validar Nro. de Intentos Erroneos, Si Mayor a 5 Desactivar Usuario
	IF @loginType = 3
	BEGIN
		SELECT @errorCounter = COUNT(*) FROM LoginInfo WHERE login_tipo_id = 3 AND login_usuario_id = @userIDInfo_DB AND login_fecha_inicio BETWEEN DATEADD(MINUTE,-30,GETDATE()) AND GETDATE()
		
		IF @errorCounter >= 3
		BEGIN
			UPDATE Usuario
			SET usu_estado_usuario_id = 2
			WHERE usu_id = @userIDInfo_DB
		END
		
	END 

	--Select Final
	SELECT @loginID AS LOGINID, @userIDInfo_DB AS USERID, @userType AS USERTYPE, @userName AS USERNAME, @entityName AS ENTITYNAME, @expDatetime AS EXPPASSWORDDATETIME, @loginType AS ERRORCODE, ISNULL(@userCustomer,'0') AS CUSTOMERUSER_ID, @userPerfil AS PERFIL_ID, ISNULL(@customerName,'0') AS CUSTOMER_NAME, ISNULL(@licenseType,0) AS LICENSE_TYPE, ISNULL(@licenseStatus,0) AS LICENSE_STATUS, ISNULL(@licenseExpiryDate, GETDATE()) AS LICENSE_EXPIRY_DATE

END
GO