﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[sp_webReporteChartTerminales](
	@customerID AS BIGINT,
	@groupID AS BIGINT
)
 
AS
BEGIN

	SET NOCOUNT ON;

	SELECT SUM(ter_actualizadas), SUM(ter_pendientes), SUM(ter_totales) FROM 
	(
		SELECT 
			 SUM((CASE WHEN T.ter_estado_actualizacion_id = 1 THEN 1 ELSE 0 END)) AS ter_actualizadas,
			 SUM((CASE WHEN T.ter_estado_actualizacion_id = 2 THEN 1 ELSE 0 END)) AS ter_pendientes,
			 COUNT(*) AS ter_totales
		FROM Terminal T
			INNER JOIN Grupo G ON (T.ter_grupo_id = G.gru_id)
		--WHERE (G.gru_id = @groupID OR @groupID = -1) AND G.gru_cliente_id = @customerID
	--	GROUP BY T.ter_estado_actualizacion_id
	) AS REP
	
END
GO