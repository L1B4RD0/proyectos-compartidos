﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[sp_swValidarInventarioAppsTerminal](
	/*INPUT*/
	@msgType VARCHAR(4),
	@procCode VARCHAR(6),
	@stan VARCHAR(6),
	@serialPos VARCHAR(20),
	@customerName VARCHAR(50),
	@applicationListPos VARCHAR(1000),
	@ipPos VARCHAR(15),
	@ipDetected VARCHAR(15),
	@tcpPortDetected VARCHAR(6),
	@imeiPos VARCHAR(50),
	@simPos VARCHAR(50),
	@latitude VARCHAR(50),
	@longitude VARCHAR(50),
	@apn VARCHAR(50),
	@signalLevel VARCHAR(50),
	@batteryLevel VARCHAR(50),
	@reservedUse1 VARCHAR(250),
	@reservedUse2 VARCHAR(250),
	@reservedUse3 VARCHAR(250),
	@softwareVersionPos VARCHAR(50),
	@isoField61 VARCHAR(250),
	/*OUTPUT*/
	@rspCode VARCHAR(2) OUTPUT,
	@rspMessage VARCHAR(200) OUTPUT,
	@dloadScheduledDatetime VARCHAR(20) OUTPUT,
	@dloadIp VARCHAR(15) OUTPUT,
	@dloadPort VARCHAR(6) OUTPUT,
	@dloadApplicationList VARCHAR(200) OUTPUT,
	@transactionId VARCHAR(20) OUTPUT
)
 
AS
BEGIN

	SET XACT_ABORT, NOCOUNT ON
	DECLARE @startTranCount INT

	--VARIABLES LOCALES DEL SP	
	DECLARE @SeparatorApp VARCHAR(2) = CHAR(10) --\n
	DECLARE @SeparatorChecksum VARCHAR(2) = '-'
	DECLARE @Counter INT
	DECLARE @AppNameDB VARCHAR(100)
	DECLARE @AppChecksumDB VARCHAR(100)
	DECLARE @AppListTablePos TABLE (Id BIGINT NOT NULL PRIMARY KEY, AppName VARCHAR(100) NOT NULL, AppChecksum VARCHAR(100) NOT NULL)
	DECLARE @AppListTableDB TABLE (Id BIGINT NOT NULL PRIMARY KEY IDENTITY(1,1), AppName VARCHAR(100) NOT NULL, AppChecksum VARCHAR(100) NOT NULL)
	DECLARE @TerminalId BIGINT = NULL
	DECLARE @UpdateStatus BIGINT = NULL
	DECLARE @AppListStringDB VARCHAR(1000) = ''
	DECLARE @FlagUpdTerminal BIT
	DECLARE @GrupoId BIGINT
	DECLARE @FlagUpdGrupo BIT
	DECLARE @FlagPrioridadGrupo BIT
	DECLARE @FlagProgramarFecha BIT
	DECLARE @GrupoActualizarFechaIni DATETIME
	DECLARE @GrupoActualizarFechaFin DATETIME
	DECLARE @GrupoRangoHoraIni TIME
	DECLARE @GrupoRangoHoraFin TIME
	DECLARE @FlagCheckForNewApps BIT
	DECLARE @TerminalStatus INT
	DECLARE @FlagValidateImei BIT
	DECLARE @TerminalImei VARCHAR(50)
	DECLARE @FlagValidateSim BIT
	DECLARE @TerminalSim VARCHAR(50)
	DECLARE @FlagContinueProcess BIT
	DECLARE @ExpLicenseDatetime DATETIME
	DECLARE @CustomerId BIGINT = NULL
	DECLARE @NextString NVARCHAR(40)
	DECLARE @Pos INT
	DECLARE @NextPos INT
	DECLARE @String NVARCHAR(250)
	DECLARE @Delimiter NVARCHAR(2) = '|'
	DECLARE @counterIdx INT
	DECLARE @posType VARCHAR(50)
	DECLARE @posBrand VARCHAR(50)
	DECLARE @posModel VARCHAR(50)
	DECLARE @posCommType VARCHAR(50)
	DECLARE @posTypeId INT
	DECLARE @posBrandId INT
	DECLARE @posModelId INT
	DECLARE @posCommTypeId INT
	DECLARE @autoLearning BIT
	DECLARE @groupNumber INT
	DECLARE @groupUpdateNow BIT
	DECLARE @terminalUpdateNow BIT
	DECLARE @imeiNumber INT
	DECLARE @ter_codigo_producto varchar(50)
	
	BEGIN TRY
		SELECT @startTranCount = @@TRANCOUNT

		IF @startTranCount = 0
		BEGIN
			BEGIN TRANSACTION	
			
			--Capturamos el codigo de producto
			SET @ter_codigo_producto = @reservedUse2	

			--CORREGIR ERROR DEL IMEI

			IF @imeiPos is null
			BEGIN
				SET @imeiPos = 'T' + @serialPos
			END 
				ELSE IF @imeiPos = ''
			BEGIN
				SET @imeiPos = 'T' + @serialPos
			END
			ELSE IF CHARINDEX('+', @imeiPos) != 0
			BEGIN
				SET @imeiPos = 'E' + @serialPos
			END
			ELSE IF ISNUMERIC(@imeiPos) = 0
			BEGIN
			SET @imeiPos = 'E' + @serialPos
			END
			

			--INICIO CUERPO DEL SP

			--INICIALIZAR PARÁMETROS DE SALIDA / VARIABLES LOCALES
			SELECT @rspCode = '', @rspMessage = '', @dloadScheduledDatetime = '', @dloadIp = '', @dloadPort = '', @dloadApplicationList = '', @transactionId = NULL, @FlagCheckForNewApps = 0, @FlagContinueProcess = 0;

			--SETEAR AUTOLEARNING FLAG
			--SELECT @autoLearning = 1
			SELECT @autoLearning = cli_autoaprendizaje, @CustomerId = cli_id FROM Cliente WHERE cli_nombre = @customerName;

			--VALIDAR CLIENTE
			IF EXISTS( SELECT * FROM Cliente WHERE cli_nombre = @customerName )
			BEGIN

				SELECT @ExpLicenseDatetime = DATEADD(DAY, TL.numero_dias + 1, lic_fecha_creacion) FROM LicenciaXCliente INNER JOIN TipoLicencia TL ON (lic_tipo_licencia_id = TL.id)  WHERE lic_cliente_id = (SELECT TOP 1 cli_id FROM Cliente WHERE cli_nombre = @customerName) AND lic_tipo_estado_id = 1 /*ACTIVA*/

				--VALIDAR FECHA DE EXPIRACIÓN DE LA LICENCIA POR CLIENTE
				IF NOT @ExpLicenseDatetime IS NULL AND @ExpLicenseDatetime > GETDATE()
				BEGIN
				    
					SET @FlagContinueProcess = 1	--CONTINUAR PROCESO

					--AUTOAPRENDIZAJE, VALIDAR FLAG SETEADO POR VARIABLE
					IF NOT EXISTS( SELECT * FROM Terminal WHERE ter_serial = @serialPos ) AND @autoLearning = 1
					BEGIN
						--CREAR TERMINAL SI EXISTE AL MENOS UN GRUPO CREADO PARA EL CLIENTE
						SELECT @CustomerId = cli_id FROM Cliente WHERE cli_nombre = @customerName

						--VALIDAR CLIENTE
						IF NOT @CustomerId IS NULL
						BEGIN


						    --VALIDAR QUE SOLO EXISTA UN GRUPO EN AUTOLEARNING, EN CASO CONTRARIO DETENER EL PROCESO
							SELECT @groupNumber = count(*) FROM Grupo G INNER JOIN Cliente C ON (C.cli_id = G.gru_cliente_id) WHERE (C.cli_nombre = @customerName AND G.gru_tipo_android = 0) AND G.gru_nombre = 'INVENTARIO'
							IF @groupNumber != 1 
							BEGIN
							  SELECT @rspCode = '01', @rspMessage = 'ACTUALIZACION DECLINADA - MODO APRENDIZAJE CONFIGURADO, SE DEBE CONFIGURAR EL GRUPO INVENTARIO'
							  SET @FlagCheckForNewApps = 0
							  SET @FlagContinueProcess = 0	--DETENER PROCESO
							  SET @GrupoId = NULL
							END
							ELSE
							BEGIN
							  	--OBTENER DATOS DEL GRUPO
							    SELECT TOP 1 @GrupoId = G.gru_id FROM Grupo G INNER JOIN Cliente C ON (C.cli_id = G.gru_cliente_id) WHERE (C.cli_nombre = @customerName AND G.gru_tipo_android = 0) AND G.gru_nombre = 'INVENTARIO'
							END							
							
							--SE CREA TERMINAL SOLO SI EXISTE AL MENOS UN GRUPO CREADO PARA EL CLIENTE
							IF NOT @GrupoId IS NULL
							BEGIN 
								--CONSULTAR DATOS DE TIPO/MARCA/MODELO/TIPO COMUNICACIÓN REPORTADO POR LA TERMINAL
								SET @String = @isoField61
								SET @String = @String + @Delimiter
								SET @Pos = CHARINDEX(@Delimiter,@String)
								SET @counterIdx = 0

								WHILE (@pos <> 0)
								BEGIN
									SET @NextString = SUBSTRING(@String,1,@Pos - 1)
									SET @String = SUBSTRING(@String,@pos+1,LEN(@String))
									SET @pos = CHARINDEX(@Delimiter,@String)
									SET @counterIdx = @counterIdx + 1
									IF @counterIdx = 2
										SET @posType = @NextString
									ELSE IF @counterIdx = 3
										SET @posBrand = @NextString
									ELSE IF @counterIdx = 4
										SET @posModel = @NextString
									ELSE IF @counterIdx = 5
										SET @posCommType = @NextString
								END

								SELECT @posTypeId = id FROM TipoTerminal WHERE descripcion = @posType
								SELECT @posBrandId = id FROM MarcaTerminal WHERE descripcion = @posBrand
								SELECT @posModelId = id FROM ModeloTerminal WHERE descripcion = @posModel
								SELECT @posCommTypeId = id FROM InterfaceTerminal WHERE descripcion = @posCommType

								IF @posModelId IS NULL
								BEGIN
								    SELECT @rspCode = '01', @rspMessage = 'ACTUALIZACION DECLINADA - MODELO DE TERMINAL ' + @posModel + ' NO EXISTE'
									SET @FlagCheckForNewApps = 0
									SET @FlagContinueProcess = 0	--DETENER PROCESO
									SET @GrupoId = NULL
								END


								--VALORES POR DEFECTO
								IF @posTypeId IS NULL SET @posTypeId = 1 /*POS*/
								IF @posBrandId IS NULL SET @posBrandId = 1 /*SPECTRA*/
								--IF @posModelId IS NULL SET @posModelId = 3 /*T1000*/
								IF @posCommTypeId IS NULL SET @posCommTypeId = 1 /*GPRS*/

								--VALIDAR QUE EL IMEI NO ESTE REPETIDO
								select @imeiNumber = count(*) from dbo.terminal where ter_serial != @serialPos and ter_imei = @imeiPos
								IF @imeiNumber != 0
								BEGIN
									SELECT @rspCode = '01', @rspMessage = 'ACTUALIZACION DECLINADA - MODO APRENDIZAJE CONFIGURADO, EL IMEI: ' + @imeiPos + ' YA EXISTE'
									SET @FlagCheckForNewApps = 0
									SET @FlagContinueProcess = 0	--DETENER PROCESO
									SET @GrupoId = NULL
								END
								ELSE
								BEGIN
								  --CREAR TERMINAL X GRUPO
								  INSERT INTO [dbo].[Terminal]([ter_serial], [ter_tipo_terminal_id], [ter_marca_terminal_id], [ter_modelo_terminal_id], [ter_interface_terminal_id], [ter_serial_sim], [ter_telefono_sim], [ter_imei], [ter_serial_cargador]
											,[ter_serial_bateria], [ter_serial_lector_biometrico], [ter_descripcion], [ter_grupo_id], [ter_tercero_id], [ter_prioridad_grupo], [ter_actualizar], [ter_fecha_programada_actualizacion]
											,[ter_ip_descarga], [ter_puerto_descarga], [ter_estado_actualizacion_id], [ter_mensaje_actualizacion], [ter_tipo_estado_id], [ter_validar_imei], [ter_validar_sim], [ter_fecha_creacion], [ter_codigo_producto])
								  VALUES (@serialPos, @posTypeId, @posBrandId, @posModelId, @posCommTypeId, @simPos, '', @imeiPos, '', '', '', '', @GrupoId, NULL, 1,
										0, NULL, '', '', 1, '', 1, 0, 0, GETDATE(), @ter_codigo_producto)
								END

							END

						END

					END

					--CONSULTAR DATOS DE TIPO/MARCA/MODELO/TIPO COMUNICACIÓN REPORTADO POR LA TERMINAL
					SET @String = @isoField61
					SET @String = @String + @Delimiter
					SET @Pos = CHARINDEX(@Delimiter,@String)
					SET @counterIdx = 0

					WHILE (@pos <> 0)
					BEGIN
						SET @NextString = SUBSTRING(@String,1,@Pos - 1)
						SET @String = SUBSTRING(@String,@pos+1,LEN(@String))
						SET @pos = CHARINDEX(@Delimiter,@String)
						SET @counterIdx = @counterIdx + 1
						IF @counterIdx = 2
							SET @posType = @NextString
						ELSE IF @counterIdx = 3
							SET @posBrand = @NextString
						ELSE IF @counterIdx = 4
							SET @posModel = @NextString
						ELSE IF @counterIdx = 5
							SET @posCommType = @NextString
					END

					SELECT @posModelId = id FROM ModeloTerminal WHERE descripcion = @posModel
		


					--VALIDAR QUE EXISTA EL MODELO.  SI NO EXISTE EL MODELO SE DECLINA LA TRANSACCION
					SELECT @posModelId = id FROM ModeloTerminal WHERE descripcion = @posModel
					IF @posModelId IS NULL
					BEGIN
						SELECT @rspCode = '01', @rspMessage = 'ACTUALIZACION DECLINADA - MODELO DE TERMINAL ' + @posModel + ' NO EXISTE'
						SET @FlagCheckForNewApps = 0
						SET @FlagContinueProcess = 0	--DETENER PROCESO
						SET @GrupoId = NULL
					END

					--VALIDAR QUE EL IMEI NO ESTE REPETIDO
					select @imeiNumber = count(*) from dbo.terminal where  ter_serial != @serialPos and ter_imei = @imeiPos
					IF @imeiNumber != 0
					BEGIN
						SELECT @rspCode = '01', @rspMessage = 'ACTUALIZACION DECLINADA - EL IMEI: ' + @imeiPos + ' YA EXISTE'
						SET @FlagCheckForNewApps = 0
						SET @FlagContinueProcess = 0	--DETENER PROCESO
						SET @GrupoId = NULL
					END

					--VALIDAR TERMINAL
					IF @FlagContinueProcess = 1 
					BEGIN
						IF (EXISTS( SELECT * FROM Terminal WHERE ter_serial = @serialPos ))
						BEGIN

							--ACTUALIZAR VALORES REPORTADOS POR EL AGENTE DEL POS SIEMPRE QUE NO ESTÉN VACÍOS
							UPDATE Terminal 
								SET ter_imei = (CASE WHEN @imeiPos = '' THEN ter_imei ELSE @imeiPos END),
									ter_serial_sim = (CASE WHEN @simPos = '' THEN ter_serial_sim ELSE @simPos END),
									ter_latitud_gps = (CASE WHEN @latitude = '' THEN ter_latitud_gps ELSE @latitude END),
									ter_longitud_gps = (CASE WHEN @longitude = '' THEN ter_longitud_gps ELSE @longitude END),
									ter_apn = (CASE WHEN @apn = '' THEN ter_apn ELSE @apn END),
									ter_nivel_gprs = (CASE WHEN @signalLevel = '' THEN ter_nivel_gprs ELSE @signalLevel END),
									ter_nivel_bateria = (CASE WHEN @batteryLevel = '' THEN ter_nivel_bateria ELSE @batteryLevel END),
									ter_aplicaciones_instaladas = (CASE WHEN @applicationListPos = '' THEN ter_aplicaciones_instaladas ELSE @applicationListPos END),
									ter_fecha_actualizacion_auto = GETDATE(),
									ter_codigo_producto = (CASE WHEN @ter_codigo_producto = '' THEN ter_codigo_producto ELSE @ter_codigo_producto END)
							WHERE ter_serial = @serialPos

							--VALIDAR TERMINAL / GRUPO / CLIENTE
							IF EXISTS( SELECT * FROM Terminal T INNER JOIN Grupo G ON (G.gru_id = T.ter_grupo_id) INNER JOIN Cliente C ON (C.cli_id = G.gru_cliente_id) WHERE T.ter_serial = @serialPos AND C.cli_nombre = @customerName )
							BEGIN

								--OBTENER DATOS DEL GRUPO Y DE LA TERMINAL
								SELECT @TerminalId = T.ter_id, @FlagUpdTerminal = T.ter_actualizar, 
										@GrupoId = G.gru_id, @FlagUpdGrupo = G.gru_actualizar, @groupUpdateNow = G.gru_update_now, @terminalUpdateNow = T.ter_update_now,
										@FlagPrioridadGrupo = T.ter_prioridad_grupo,
										@FlagProgramarFecha = G.gru_programar_fecha,
										@GrupoActualizarFechaIni = G.gru_fecha_programada_actualizacion_ini,
										@GrupoActualizarFechaFin = G.gru_fecha_programada_actualizacion_fin,
										@GrupoRangoHoraIni = G.gru_rango_hora_actualizacion_ini,
										@GrupoRangoHoraFin = G.gru_rango_hora_actualizacion_fin,
										@TerminalStatus = T.ter_tipo_estado_id,
										@FlagValidateImei = T.ter_validar_imei,
										@FlagValidateSim = T.ter_validar_sim,
										@TerminalImei = T.ter_imei,
										@TerminalSim = T.ter_serial_sim
								FROM Terminal T 
									INNER JOIN Grupo G ON (G.gru_id = T.ter_grupo_id)
								WHERE T.ter_serial = @serialPos

								--VALIDAR FLAGS DE LA TERMINAL
								IF @TerminalStatus = 1
								BEGIN

									--SET @FlagContinueProcess = 1	--CONTINUAR PROCESO

									IF @FlagValidateImei = 1
									BEGIN
										IF LTRIM(RTRIM(@TerminalImei)) = LTRIM(RTRIM(@imeiPos))
										BEGIN
											SET @FlagContinueProcess = 1	--CONTINUAR PROCESO
										END
										ELSE
										BEGIN
											SELECT @rspCode = '01', @rspMessage = 'ACTUALIZACION DECLINADA - VALOR DE IMEI ERRADO'
											SET @FlagCheckForNewApps = 0
											SET @FlagContinueProcess = 0	--DETENER PROCESO
										END
									END
							
									IF @FlagValidateSim = 1 AND @FlagContinueProcess = 1
									BEGIN
										IF LTRIM(RTRIM(@TerminalSim)) = LTRIM(RTRIM(@simPos))
										BEGIN
											SET @FlagContinueProcess = 1	--CONTINUAR PROCESO
										END
										ELSE
										BEGIN
											SELECT @rspCode = '01', @rspMessage = 'ACTUALIZACION DECLINADA - VALOR DE SERIAL SIM ERRADO'
											SET @FlagCheckForNewApps = 0
											SET @FlagContinueProcess = 0	--DETENER PROCESO
										END
									END	
								END
								ELSE
								BEGIN
									SELECT @rspCode = '01', @rspMessage = 'ACTUALIZACION DECLINADA - ESTADO TERMINAL INACTIVO'
									SET @FlagCheckForNewApps = 0
									SET @FlagContinueProcess = 0	--DETENER PROCESO
								END

								--VALIDAR SI SE DEBE CONTINUAR CON EL PROCESO
								IF @FlagContinueProcess = 1
								BEGIN												

									--VALIDAR SI TIENE PRIORIDAD EL GRUPO EN LA TERMINAL
									IF @FlagPrioridadGrupo = 1
									BEGIN 
										--VALIDAR DATOS DE GRUPO
										IF @FlagUpdGrupo = 1
										BEGIN
											--VALIDAR SI ESTÁ ACTIVADA OPCIÓN POR FECHAS
											IF @FlagProgramarFecha = 1
											BEGIN
												--FECHA_HORA ACTUAL ESTÁ DENTRO DEL RANGO PARA ACTUALIZAR
												SET @FlagCheckForNewApps = 1

												--VALIDAR FECHA_INI Y FECHA_FIN CON RANGO DE HORAS
												IF CONVERT(DATE,GETDATE()) BETWEEN CONVERT(DATE, @GrupoActualizarFechaIni) AND CONVERT(DATE,@GrupoActualizarFechaFin) AND
													CONVERT(TIME,GETDATE()) BETWEEN CONVERT(TIME, @GrupoActualizarFechaIni) AND CONVERT(TIME,@GrupoActualizarFechaFin)
												BEGIN
													--FECHA_HORA ACTUAL ESTÁ DENTRO DEL RANGO PARA ACTUALIZAR
													SET @FlagCheckForNewApps = 1
												END
												ELSE IF CONVERT(DATE,GETDATE()) > CONVERT(DATE,@GrupoActualizarFechaFin)
												BEGIN
													SELECT @rspCode = '01', @rspMessage = 'ACTUALIZACION DECLINADA CONFIGURACION X GRUPO - FECHA EXPIRO'
													SET @FlagCheckForNewApps = 0
												END
												ELSE
												BEGIN
													SELECT @rspCode = '01', @rspMessage = 'ACTUALIZACION DECLINADA CONFIGURACION X GRUPO - FECHA_HORA FUERA DE RANGO'
													SET @FlagCheckForNewApps = 0
												END
											END
											ELSE
											BEGIN
												--VALIDAR SOLO RANGO DE HORAS
												IF CONVERT(TIME, GETDATE()) BETWEEN @GrupoRangoHoraIni AND @GrupoRangoHoraFin
												BEGIN
													--HORA ACTUAL ESTÁ DENTRO DEL RANGO PARA ACTUALIZAR
													SET @FlagCheckForNewApps = 1
												END
												ELSE
												BEGIN
													SELECT @rspCode = '01', @rspMessage = 'ACTUALIZACION DECLINADA CONFIGURACION X GRUPO - HORA FUERA DE RANGO'
													SET @FlagCheckForNewApps = 0
												END
											END
										END
										ELSE
										BEGIN
											SELECT @rspCode = '01', @rspMessage = 'GRUPO CONFIGURADO PARA NO ACTUALIZAR'
											SET @FlagCheckForNewApps = 0
										END
									END
									ELSE
									BEGIN
										--VALIDAR DATOS DE TERMINAL

										---VALIDAR SI LA TERMINAL ESTÁ ACTIVA PARA ACTUALIZAR
										IF @FlagUpdTerminal = 1
										BEGIN
											--SE DEBE ACTUALIZAR TERMINAL, COMPARAR APLICACIONES Y CHECKSUM
											SET @FlagCheckForNewApps = 1
										END
										ELSE
										BEGIN
											SELECT @rspCode = '01', @rspMessage = 'TERMINAL CONFIGURADO PARA NO ACTUALIZAR'
											SET @FlagCheckForNewApps = 0
										END
									END

									--VALIDAR SI SE DEBE BUSCAR NUEVAS APLICACIONES A INSTALAR
									IF @FlagCheckForNewApps = 1
									BEGIN
										--DESEMPAQUETAR EN TABLA LISTA DE APLICACIONES REPORTADAS POR EL POS
										INSERT INTO @AppListTablePos
											SELECT * FROM SepararListaAplicaciones(@applicationListPos, @separatorApp, @separatorChecksum);

										--CONSULTAR APLICACIONES X GRUPO SEGÚN TERMINAL SERIAL
										INSERT INTO @AppListTableDB
											SELECT A.apl_nombre_tms, A.apl_checksum_tms FROM Aplicacion A 
												INNER JOIN 
												(SELECT grupo_id, aplicacion_id FROM GrupoXAplicacion where Modelo_id = @posModelId or Modelo_id is null) GA
												ON (GA.aplicacion_id = A.apl_id)
												INNER JOIN Grupo G ON (G.gru_id = GA.grupo_id)
												INNER JOIN Terminal T ON (T.ter_grupo_id = G.gru_id)
											WHERE T.ter_id = @TerminalId;

										--COMPARAR NOMBRE DE APLICACIÓN Y CHECKSUM
										SET @Counter = 1

										WHILE (@Counter <= (SELECT MAX(Id) FROM @AppListTableDB))
										BEGIN
							
											SELECT @AppNameDB = AppName,
											 @AppChecksumDB = AppChecksum,
											 @AppListStringDB = @AppListStringDB + AppName + @SeparatorChecksum + AppChecksum 
											 FROM @AppListTableDB
											 WHERE Id = @Counter

											--BUSCAR POR NOMBRE DE APLICACIÓN EN LA LISTA DEL POS
											IF EXISTS(SELECT * FROM @AppListTablePos LP WHERE LP.AppName = @AppNameDB)
											BEGIN

												--ENCONTRADO, COMPARAR CHECKSUM
												IF (SELECT LP.AppChecksum FROM @AppListTablePos LP WHERE LP.AppName = @AppNameDB) <> @AppChecksumDB
												BEGIN
													--CHECKSUM DIFERENTES, ACTUALIZAR APPLICACION
													SET @dloadApplicationList = @dloadApplicationList + @AppNameDB + @SeparatorChecksum + @AppChecksumDB + @SeparatorApp
												END
											END
											ELSE
											BEGIN
												--NO EXISTE ACTUALIZACIÓN EN LISTA DE POS, ACTUALIZARLA
												SET @dloadApplicationList = @dloadApplicationList + @AppNameDB + @SeparatorChecksum + @AppChecksumDB + @SeparatorApp
											END

											SET @Counter = @Counter + 1
										END

										--VALIDAR SI HAY APLICACIONES POR ACTUALIZAR PARA LA TERMINAL
										IF @dloadApplicationList IS NULL
										BEGIN
											SET @dloadApplicationList = ''
										END

										IF LEN(@dloadApplicationList) > 0
										BEGIN

											--VALIDAR SI LA PROGRAMACIÓN ES POR GRUPO O POR TERMINAL
											IF @FlagPrioridadGrupo = 1
											BEGIN
												--BUSCAR DATOS DE PROGRAMACIÓN / IP / PUERTO EN GRUPO
												SELECT @rspCode = '00', @rspMessage = 'ACTUALIZACION APROBADA CONFIGURACION X GRUPO', @UpdateStatus = 2 /*Pendiente*/

												--BUSCAR FECHA HORA, IP Y PUERTO DE DESCARGA POR GRUPO
												IF @groupUpdateNow = 0
												BEGIN
												  SELECT 
													--POR RANGO DE HORAS -> DESPUES DE 5 MINUTOS SE PROGRAMA LA DESCARGA - SEGÚN HORA ACTUAL DE CONSULTA
													--POR RANGO DE FECHAS -> AL SIGUIENTE DÍA SE PROGRAMA LA DESCARGA - SEGÚN HORA ACTUAL DE CONSULTA
													   @dloadScheduledDatetime = (CASE WHEN @FlagProgramarFecha = 1 AND GETDATE() BETWEEN G.gru_fecha_programada_actualizacion_ini AND G.gru_fecha_programada_actualizacion_fin THEN (CONVERT(VARCHAR, DATEADD(DAY, 1, GETDATE()), 112) + REPLACE(CONVERT(VARCHAR, DATEADD(DAY, 1, GETDATE()), 108), ':', '')) WHEN @FlagProgramarFecha = 0 AND CONVERT(TIME, GETDATE()) BETWEEN gru_rango_hora_actualizacion_ini AND gru_rango_hora_actualizacion_fin THEN (CONVERT(VARCHAR, DATEADD(MI,5,GETDATE()), 112) + REPLACE(CONVERT(VARCHAR, DATEADD(MI,5,GETDATE()), 108), ':', '')) ELSE 'NA' END),
													   @dloadIp = G.gru_ip_descarga,
													   @dloadPort = G.gru_puerto_descarga
												  FROM Terminal T 
													INNER JOIN Grupo G ON (G.gru_id = T.ter_grupo_id)
												  WHERE T.ter_id = @TerminalId
												END
												ELSE
												BEGIN
												  SELECT 
													--POR RANGO DE HORAS -> DESPUES DE 5 MINUTOS SE PROGRAMA LA DESCARGA - SEGÚN HORA ACTUAL DE CONSULTA
													--POR RANGO DE FECHAS -> AL SIGUIENTE DÍA SE PROGRAMA LA DESCARGA - SEGÚN HORA ACTUAL DE CONSULTA
													   @dloadScheduledDatetime = 'NA',
													   @dloadIp = G.gru_ip_descarga,
													   @dloadPort = G.gru_puerto_descarga
												  FROM Terminal T 
													INNER JOIN Grupo G ON (G.gru_id = T.ter_grupo_id)
												   WHERE T.ter_id = @TerminalId
												END
												
											END
											ELSE
											BEGIN
												--BUSCAR DATOS DE PROGRAMACIÓN / IP / PUERTO EN TERMINAL
												SELECT @rspCode = '00', @rspMessage = 'ACTUALIZACION APROBADA CONFIGURACION X TERMINAL', @UpdateStatus = 2 /*Pendiente*/

												--BUSCAR FECHA HORA, IP Y PUERTO DE DESCARGA POR TERMINAL
												IF @terminalUpdateNow = 0
												BEGIN
												  SELECT 
													@dloadScheduledDatetime = (CASE WHEN T.ter_fecha_programada_actualizacion < GETDATE() THEN 'NA' ELSE (CONVERT(VARCHAR, T.ter_fecha_programada_actualizacion, 112) + REPLACE(CONVERT(VARCHAR, T.ter_fecha_programada_actualizacion, 108), ':', '')) END), 
													@dloadIp = T.ter_ip_descarga,
													@dloadPort = T.ter_puerto_descarga
												  FROM Terminal T WHERE T.ter_id = @TerminalId
												END
												ELSE
												BEGIN
												 SELECT 
													@dloadScheduledDatetime = 'NA', 
													@dloadIp = T.ter_ip_descarga,
													@dloadPort = T.ter_puerto_descarga
												  FROM Terminal T WHERE T.ter_id = @TerminalId
												END
												
											END

											--CAMBIAR ESTADO A REGISTRO DE TERMINAL
											UPDATE Terminal
											SET ter_estado_actualizacion_id = 2 /*Pendiente*/, ter_mensaje_actualizacion = 'APLICACIONES PENDIENTES: ' + REPLACE(@dloadApplicationList, CHAR(10), '{LF}')
											WHERE ter_id = @TerminalId

										END
										ELSE
										BEGIN
											SELECT @rspCode = '00', @rspMessage = 'TERMINAL ACTUALIZADO', @updateStatus = 1 /*Actualizado*/

											--CAMBIAR ESTADO A REGISTRO DE TERMINAL
											UPDATE Terminal
											SET ter_estado_actualizacion_id = 1 /*Actualizado*/, ter_mensaje_actualizacion = ''
											WHERE ter_id = @TerminalId

										END							
									END
								END
							END
							ELSE
							BEGIN
								SELECT @rspCode = '01', @rspMessage = 'TERMINAL NO VALIDA PARA EL GRUPO'
							END

						END
						ELSE
						BEGIN
							SELECT @rspCode = '01', @rspMessage = 'TERMINAL NO EXISTE'
						END
					END
					

				END
				ELSE
				BEGIN
					SELECT @rspCode = '01', @rspMessage = 'LICENCIA DE USO EXPIRADA'

					--DESACTIVAR LICENCIA
					UPDATE LicenciaXCliente
					SET lic_tipo_estado_id = 2 /*INACTIVA*/
					WHERE lic_cliente_id = (SELECT TOP 1 cli_id FROM Cliente WHERE cli_nombre = @customerName)

				END

			END
			ELSE
			BEGIN
				SELECT @rspCode = '01', @rspMessage = 'CLIENTE NO EXISTE'
			END

			--FIN CUERPO DEL SP

		END

		IF @startTranCount = 0 
		BEGIN

			--REGISTRAR TRANSACCIÓN
			INSERT INTO TransaccionInventario( tra_fecha, tra_stan, tra_serial_pos, tra_terminal_id, tra_cliente_pos, tra_message_type, tra_processing_code,
						tra_codigo_respuesta, tra_mensaje_respuesta, tra_estado_actualizacion_id, tra_direccion_ip_pos, tra_direccion_ip_detectada, tra_puerto_tcp_atencion,
						tra_imei_pos, tra_sim_pos, tra_nivel_gprs, tra_nivel_bateria, tra_version_software_pos, tra_aplicaciones_pos, tra_aplicaciones_pendientes,
						tra_aplicaciones_a_instalar, tra_fecha_descarga, tra_ip_descarga, tra_puerto_descarga, tra_campo61_pos, tra_codigo_producto)
			VALUES (GETDATE(), @stan, @serialPos, @TerminalId, @customerName, @msgType, @procCode, @rspCode, @rspMessage, @UpdateStatus, @ipPos, @ipDetected, @tcpPortDetected,
						 @imeiPos, @simPos, @signalLevel, @batteryLevel, @softwareVersionPos, REPLACE(@applicationListPos, CHAR(10), '{LF}') , @AppListStringDB, @dloadApplicationList, @dloadScheduledDatetime, @dloadIp,
						 @dloadPort, @isoField61, @ter_codigo_producto);

			--REGISTRAR DATOS ADICIONALES EN LA TABLA DE TERMINALES
			UPDATE Terminal set 
					ter_fecha_consulta_sp = GETDATE(), 
					ter_aplicaciones_pendientes_sp = @AppListStringDB, 
					ter_ip_descarga_sp = @dloadIp, 
					ter_puerto_descarga_sp = @dloadPort, 
					ter_fecha_descarga_sp = @dloadScheduledDatetime 
			WHERE ter_id = @TerminalId;

			--OBTENER TRANSACCIÓN ID
			SET @transactionId = CAST(SCOPE_IDENTITY() AS VARCHAR(20))

			COMMIT TRANSACTION 
		END

	    ---ALMACENAMOS EL CONTADOR DE CONSULTAS
		BEGIN TRY
		  DECLARE @statNow varchar(100) = right('0000' + convert(varchar(30), datepart(year,getdate())), 4) + 
											right('00' + convert(varchar(30), datepart(month,getdate())), 2)  +
											right('00' + convert(varchar(30), datepart(day,getdate())), 2) + ',91,I,1'
	      DECLARE @RCs int
		  DECLARE @rss varchar(50)
	      EXECUTE @RCs = [dbo].[sp_swStoreStatistics] 
				@statNow
				,@CustomerId
				,@GrupoId
				,@TerminalId
				,@rss OUTPUT
		END TRY
		BEGIN CATCH
			PRINT ''
		END CATCH


	END TRY
	BEGIN CATCH
		IF XACT_STATE() <> 0 AND @startTranCount = 0 
			ROLLBACK TRANSACTION

		INSERT INTO [dbo].[LogErrorBaseDatos]([bderr_fecha],[bderr_nombre_funcion],[bderr_mensaje_error],[bderr_datos_adicionales])
			 VALUES (GETDATE(), OBJECT_NAME(@@PROCID), ERROR_MESSAGE(), /*Datos Extra, Parámetros del SP*/ 
									'stan = ' + @stan + ', serialPos = ' + @serialPos + ', customerName' + @customerName +
									'applicationListPos = ' + @applicationListPos + ', tcpPortDetected' + @tcpPortDetected)

		DECLARE @errMessage AS VARCHAR(4000) = ERROR_MESSAGE()
		DECLARE @errSeverity AS INT = ERROR_SEVERITY()
		DECLARE @errState AS INT = ERROR_STATE()

		RAISERROR (@errMessage, @errSeverity, @errState)
	END CATCH

END




GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Se realizan todas las validaciones relevantes para la transacción, si es válida, se registran los datos y la transacción se realiza en los terminales , de lo contrario la transacción no es efectiva.', 'SCHEMA', N'dbo', 'PROCEDURE', N'sp_swValidarInventarioAppsTerminal'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'input values', 'SCHEMA', N'dbo', 'PROCEDURE', N'sp_swValidarInventarioAppsTerminal', 'PARAMETER', N'@msgType'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'input values', 'SCHEMA', N'dbo', 'PROCEDURE', N'sp_swValidarInventarioAppsTerminal', 'PARAMETER', N'@procCode'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'input values', 'SCHEMA', N'dbo', 'PROCEDURE', N'sp_swValidarInventarioAppsTerminal', 'PARAMETER', N'@stan'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'input values', 'SCHEMA', N'dbo', 'PROCEDURE', N'sp_swValidarInventarioAppsTerminal', 'PARAMETER', N'@serialPos'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'input values', 'SCHEMA', N'dbo', 'PROCEDURE', N'sp_swValidarInventarioAppsTerminal', 'PARAMETER', N'@customerName'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'input values', 'SCHEMA', N'dbo', 'PROCEDURE', N'sp_swValidarInventarioAppsTerminal', 'PARAMETER', N'@applicationListPos'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'input values', 'SCHEMA', N'dbo', 'PROCEDURE', N'sp_swValidarInventarioAppsTerminal', 'PARAMETER', N'@ipPos'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'input values', 'SCHEMA', N'dbo', 'PROCEDURE', N'sp_swValidarInventarioAppsTerminal', 'PARAMETER', N'@ipDetected'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'input values', 'SCHEMA', N'dbo', 'PROCEDURE', N'sp_swValidarInventarioAppsTerminal', 'PARAMETER', N'@tcpPortDetected'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'input values', 'SCHEMA', N'dbo', 'PROCEDURE', N'sp_swValidarInventarioAppsTerminal', 'PARAMETER', N'@imeiPos'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'input values', 'SCHEMA', N'dbo', 'PROCEDURE', N'sp_swValidarInventarioAppsTerminal', 'PARAMETER', N'@simPos'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'input values', 'SCHEMA', N'dbo', 'PROCEDURE', N'sp_swValidarInventarioAppsTerminal', 'PARAMETER', N'@latitude'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'input values', 'SCHEMA', N'dbo', 'PROCEDURE', N'sp_swValidarInventarioAppsTerminal', 'PARAMETER', N'@longitude'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'input values', 'SCHEMA', N'dbo', 'PROCEDURE', N'sp_swValidarInventarioAppsTerminal', 'PARAMETER', N'@apn'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'input values', 'SCHEMA', N'dbo', 'PROCEDURE', N'sp_swValidarInventarioAppsTerminal', 'PARAMETER', N'@signalLevel'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'input values', 'SCHEMA', N'dbo', 'PROCEDURE', N'sp_swValidarInventarioAppsTerminal', 'PARAMETER', N'@batteryLevel'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'input values', 'SCHEMA', N'dbo', 'PROCEDURE', N'sp_swValidarInventarioAppsTerminal', 'PARAMETER', N'@reservedUse1'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'input values', 'SCHEMA', N'dbo', 'PROCEDURE', N'sp_swValidarInventarioAppsTerminal', 'PARAMETER', N'@reservedUse2'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'input values', 'SCHEMA', N'dbo', 'PROCEDURE', N'sp_swValidarInventarioAppsTerminal', 'PARAMETER', N'@reservedUse3'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'input values', 'SCHEMA', N'dbo', 'PROCEDURE', N'sp_swValidarInventarioAppsTerminal', 'PARAMETER', N'@softwareVersionPos'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'input values', 'SCHEMA', N'dbo', 'PROCEDURE', N'sp_swValidarInventarioAppsTerminal', 'PARAMETER', N'@isoField61'
GO