﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

-- =============================================
-- Autor:				Elkin Beltrán
-- Fecha Creación:		16/Febrero/2017
-- Histórico Cambios:
------------------------------------------------
--	v1.0	Versión Inicial		
------------------------------------------------
-- Campos de Respuesta:		SingleRow
-- usu_nombre				VARCHAR(100)
-- usu_email				VARCHAR(50)
-- usu_login				VARCHAR(50)
-- perf_nombre				VARCHAR(50)
-- usu_estado				BIT
-- cli_nombre				VARCHAR(50)
-- =============================================
CREATE PROCEDURE [dbo].[sp_webConsultarUsuarios]
(
	@userName VARCHAR(100),
	@login VARCHAR(50)
)
 
AS
BEGIN
	
	SET NOCOUNT ON

	SELECT 
		U.usu_nombre,
		U.usu_email,
		U.usu_login,
		P.perf_nombre,
		(CASE WHEN usu_estado_usuario_id = 1 THEN 1 ELSE 0 END) usu_estado,
		C.cli_nombre
	FROM Usuario U
		INNER JOIN Perfil P ON (U.usu_perfil_id = P.perf_id)
		INNER JOIN UsuarioXCliente UxC ON (UxC.usuario_id = U.usu_id)
		INNER JOIN Cliente C ON (C.cli_id = UxC.cliente_id)
	WHERE U.usu_perfil_id NOT IN (7) AND (U.usu_nombre LIKE '%' + @userName + '%' OR @userName = '-1')
			AND (U.usu_login LIKE '%' + @login + '%' OR @login = '-1')

END



GO