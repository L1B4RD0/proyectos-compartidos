﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

-- =============================================
-- Autor:				Elkin Beltrán
-- Fecha Creación:		06/Jun/2015
-- Histórico Cambios:
------------------------------------------------
--	v1.0	Versión Inicial
------------------------------------------------
-- Campos de Respuesta:		SingleRow
-- STATUS				INT
-- =============================================

CREATE PROCEDURE [dbo].[sp_webTerminarProcesoRestauracionClave](
	@activationCode VARCHAR(100),
	@passwordID VARCHAR(100)
)
 
AS
BEGIN

	SET XACT_ABORT, NOCOUNT ON
	DECLARE @startTranCount INT

	--VARIABLES LOCALES DEL SP
	DECLARE @userID_DB AS INT
	DECLARE @status AS INT
	DECLARE @restID AS INT

	BEGIN TRY
		SELECT @startTranCount = @@TRANCOUNT

		IF @startTranCount = 0
		BEGIN
			BEGIN TRANSACTION		
			--INICIO CUERPO DEL SP

			--VERIFICAR QUE EXISTA UN PREPROCESO PARA REACTIVACIÓN DE CLAVE
			SELECT @restID = rest_id, @userID_DB = rest_usuario_id FROM UsuarioXRestauracionClave WHERE rest_rand = @activationCode AND rest_estado = 1

			IF @restID IS NOT NULL AND @userID_DB IS NOT NULL
			BEGIN

				/*ACTUALIZAR ESTADO DEL PROCESO DE RESTAURACION*/
				UPDATE UsuarioXRestauracionClave
					SET rest_estado = 0
				WHERE rest_id = @restID

				/*ACTUALIZAR CLAVE DEL USUARIO Y EXTENDER PERÍODO DE VALIDEZ DE CLAVE*/

				UPDATE Usuario
					SET usu_hash = @passwordID, usu_fecha_exp_clave = DATEADD(MONTH, 1, GETDATE())
				WHERE usu_id = @userID_DB

				SET @status = 1	/*OK*/
			END
			ELSE
			BEGIN
				SET @status = 0	/*ERROR*/ 
			END

			--FIN CUERPO DEL SP
		END

		IF @startTranCount = 0 
		BEGIN 
			COMMIT TRANSACTION 

			--SELECT FINAL
			SELECT @status AS STATUS
		END
	END TRY
	BEGIN CATCH
		IF XACT_STATE() <> 0 AND @startTranCount = 0 
			ROLLBACK TRANSACTION

		INSERT INTO [dbo].[LogErrorBaseDatos]([bderr_fecha],[bderr_nombre_funcion],[bderr_mensaje_error],[bderr_datos_adicionales])
			 VALUES (GETDATE(), OBJECT_NAME(@@PROCID), ERROR_MESSAGE(), /*Datos Extra, Parámetros del SP*/ 'activationCode = ' + @activationCode + ', passwordID = ' + @passwordID)

		DECLARE @errMessage AS VARCHAR(4000) = ERROR_MESSAGE()
		DECLARE @errSeverity AS INT = ERROR_SEVERITY()
		DECLARE @errState AS INT = ERROR_STATE()

		RAISERROR (@errMessage, @errSeverity, @errState)
	END CATCH

END

GO