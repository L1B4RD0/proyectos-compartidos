﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[sp_webReporteCargaLlaves](
	@groupID AS BIGINT,
	@terminalID AS BIGINT,
	@customerID AS BIGINT,
	@startDate AS DATETIME,
	@endDate AS DATETIME
)
 
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @endDateReport AS DATETIME	
	SET @endDateReport = DATEADD(DAY, 1, @endDate)

	SELECT 
		CONVERT(VARCHAR, TMDM.tra_fecha, 103) + ' ' + CONVERT(VARCHAR, TMDM.tra_fecha, 108) AS tra_fecha,
		UPPER(TMDM.tra_operacion) AS tra_operacion,
		TMDM.tra_terminal_id_device,
		G.gru_nombre,
		TMDM.tra_terminal_ip,
		TMDM.tra_estado_final 
	FROM TransaccionMDM TMDM
		LEFT OUTER JOIN Terminal T ON (TMDM.tra_terminal_id = T.ter_id)
		LEFT OUTER JOIN Grupo G ON (T.ter_grupo_id = G.gru_id)
	WHERE TMDM.tra_fecha >= @startDate AND TMDM.tra_fecha <= @endDateReport
		AND (T.ter_grupo_id = @groupID OR @groupID = -1) AND G.gru_cliente_id = @customerID AND (T.ter_id = @terminalID OR @terminalID = -1)
		AND UPPER(TMDM.tra_operacion) IN (UPPER('solicitud_llave'), UPPER('notificar_carga_llave'), UPPER('cargar_componente_1'), UPPER('cargar_componente_2'))
	ORDER BY TMDM.tra_fecha DESC
	
END


GO