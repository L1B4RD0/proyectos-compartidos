﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[sp_webEditarGrupoAndroidAcciones](
	@groupID BIGINT,
	@flagchangePassword BIT,
	@newPassword VARCHAR(50),
	@newMessage VARCHAR(100),
	@lockTerminals BIT
)
 
AS
BEGIN

	SET XACT_ABORT, NOCOUNT ON
	DECLARE @startTranCount INT

	--VARIABLES LOCALES DEL SP
	DECLARE @responseCode AS INT

	BEGIN TRY
		SELECT @startTranCount = @@TRANCOUNT

		IF @startTranCount = 0
		BEGIN
			BEGIN TRANSACTION		
			--INICIO CUERPO DEL SP
	
			--VALIDAR CAMBIO DE CLAVE A TERMINALES
			IF @flagchangePassword = 1
			BEGIN
				UPDATE Terminal SET ter_clave_bloqueo = @newPassword WHERE ter_grupo_id = @groupID
			END
			

			--VALIDAR NUEVO MENSAJE
			IF LEN(@newMessage) > 0
			BEGIN
				UPDATE Terminal SET ter_mensaje_desplegar = @newMessage WHERE ter_grupo_id = @groupID
				UPDATE Grupo SET gru_mensaje_desplegar = @newMessage WHERE gru_id = @groupID
			END

			--VALIDAR BLOQUEO DE TERMINALES
			IF @lockTerminals = 1
			BEGIN
				UPDATE Terminal SET ter_bloqueo = @lockTerminals WHERE ter_grupo_id = @groupID
			END
			else
				UPDATE Terminal SET ter_bloqueo = @lockTerminals WHERE ter_grupo_id = @groupID

			SELECT @responseCode = 1

			--FIN CUERPO DEL SP
		END

		IF @startTranCount = 0 
		BEGIN 
			COMMIT TRANSACTION

			--SELECT FINAL
			SELECT @responseCode AS RESPONSECODE
		END
	END TRY
	BEGIN CATCH
		IF XACT_STATE() <> 0 AND @startTranCount = 0 
			ROLLBACK TRANSACTION

		INSERT INTO [dbo].[LogErrorBaseDatos]([bderr_fecha],[bderr_nombre_funcion],[bderr_mensaje_error],[bderr_datos_adicionales])
			 VALUES (GETDATE(), OBJECT_NAME(@@PROCID), ERROR_MESSAGE(), /*Datos Extra, Parámetros del SP*/ 
																		'flagchangePassword = ' + CAST(@flagchangePassword AS VARCHAR) + 
																		', newPassword = ' + @newPassword + 
																		', newMessage = ' + @newMessage +
																		', lockTerminals = ' + CAST(@lockTerminals AS VARCHAR))

		DECLARE @errMessage AS VARCHAR(4000) = ERROR_MESSAGE()
		DECLARE @errSeverity AS INT = ERROR_SEVERITY()
		DECLARE @errState AS INT = ERROR_STATE()

		RAISERROR (@errMessage, @errSeverity, @errState)
	END CATCH	

END


GO