﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[sp_webConsultarTerminalesGrupo](
	@groupId BIGINT,
	@terminalSerial VARCHAR(20),
	@terminalImei VARCHAR(20)
)
 
AS
BEGIN

SET NOCOUNT ON;
	SELECT 
		T.ter_id, T.ter_serial,T.ter_fecha_actualizacion_auto, MT.descripcion AS ter_modelo, T.ter_imei,T.ter_codigo_producto, T.ter_mac, T.ter_prioridad_grupo, T.ter_actualizar, CASE WHEN T.ter_fecha_programada_actualizacion IS NOT NULL THEN CONVERT(VARCHAR, T.ter_fecha_programada_actualizacion, 103) + ' ' + SUBSTRING(CONVERT(VARCHAR, T.ter_fecha_programada_actualizacion, 108), 1, 5) ELSE '' END AS ter_fecha_programacion, T.ter_ip_descarga, T.ter_puerto_descarga,
		--IMG 'S FROM TERMINAL OG
		(CASE WHEN T.ter_tipo_estado_id = 1 THEN '../img/icons/16x16/pos.png' ELSE '../img/icons/16x16/POS.png' END) AS ter_logo,
		(CASE WHEN T.ter_estado_actualizacion_id = 1 THEN '../img/icons/16x16/key_on.png' WHEN T.ter_estado_actualizacion_id = 3 THEN '../img/icons/16x16/key_download.png' ELSE  '../img/icons/16x16/key_off.png' END) AS ter_estado_actualizacion,
		EA.descripcion as 	estadoTerminal,
		 (CASE WHEN T.ter_tipo_estado_id = 1 THEN 1 ELSE 0 END) ter_estado
	FROM Terminal T 
		INNER JOIN ModeloTerminal MT ON (MT.id = T.ter_modelo_terminal_id)
		INNER JOIN EstadoActualizacion EA ON (EA.id = T.ter_estado_actualizacion_id)
	where
	-- ter_serial =  @terminalSerial AND
    ter_grupo_id =  @groupId AND
	(T.ter_serial LIKE '%' + @terminalSerial + '%' OR @terminalSerial = '-1')
	AND (T.ter_imei LIKE '%' + @terminalImei + '%' OR @terminalImei = '-1') 
	ORDER BY T.ter_id 
	END 



GO