﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[sp_webReporteChartTransacciones](
    @tipo_id as BIGINT,
	@fechaInicial as varchar(30),
	@fechaFinal as varchar(30)
)
 
AS
BEGIN

	SET NOCOUNT ON;

	select convert (varchar(20), fecha) as fecha, convert(varchar(20), sum(valor)) as valor 
	from ClientesEstadisticas
    group by fecha, tipo_id having tipo_id = @tipo_id and 
    fecha between convert(datetime, @fechaInicial, 112) and convert(datetime, @fechaFinal, 112) 


END

GO