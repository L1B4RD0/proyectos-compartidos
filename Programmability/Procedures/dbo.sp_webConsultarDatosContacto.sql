﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[sp_webConsultarDatosContacto](
	@contactNumID VARCHAR(50)
)
 
AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT T.terc_numero_identificacion, T.terc_tipo_identificacion_id, T.terc_nombre, T.terc_direccion, T.terc_celular, T.terc_telefono_fijo, T.terc_correo_electronico
	FROM Tercero T
	WHERE T.terc_numero_identificacion = @contactNumID

END







EXEC sp_webConsultarDatosContacto '88254312'

GO