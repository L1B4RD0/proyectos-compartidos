﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[sp_webConsultarConfigEMVTerminal](
	@terminalId BIGINT
)
 
AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT 
		TEMV.emv_id, TEMV.emv_type, TEMV.emv_config, TEMV.emv_threshold_random_selection, 
		TEMV.emv_target_random_selection, TEMV.emv_max_target_random_selection, TEMV.emv_tac_denial, 
		TEMV.emv_tac_online, TEMV.emv_tac_default
	FROM TerminalXEMVConfig TEMV
		INNER JOIN Terminal T ON (T.ter_id = TEMV.emv_terminal_id)
	WHERE T.ter_id = @terminalId;

END



GO