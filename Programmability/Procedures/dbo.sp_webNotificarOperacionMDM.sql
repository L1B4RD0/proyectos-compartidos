﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[sp_webNotificarOperacionMDM](
	/*INPUT*/
	@operationName VARCHAR(50),
	@login VARCHAR(50),
	@pass VARCHAR(50),
	@terminalRam VARCHAR(50),
	@terminalBattery VARCHAR(50),
	@terminalLatitude VARCHAR(50),
	@terminalLongitude VARCHAR(50),
	@installedAPKs VARCHAR(1000),
	@groupName VARCHAR(50),
	@terminalDeviceId VARCHAR(50),
	@terminalIP VARCHAR(50),
	@terminalDownloadedAPK VARCHAR(200),
	@terminalDownloadedXML VARCHAR(200),
	@terminalDownloadedIMG VARCHAR(200)
)
 
AS
BEGIN

	SET XACT_ABORT, NOCOUNT ON
	DECLARE @startTranCount INT

	--VARIABLES LOCALES DEL SP	
	DECLARE @responseCode AS INT
	DECLARE @terminalId AS BIGINT
	DECLARE @transactionId AS BIGINT
	
	BEGIN TRY
		SELECT @startTranCount = @@TRANCOUNT

		IF @startTranCount = 0
		BEGIN
			BEGIN TRANSACTION		
			--INICIO CUERPO DEL SP

			SELECT @terminalId = ter_id FROM Terminal WHERE ter_serial = @terminalDeviceId

			--REGISTRAR TRANSACCIÓN

			INSERT INTO TransaccionMDM( [tra_fecha],[tra_operacion],[tra_terminal_id],[tra_login],[tra_pass],[tra_terminal_ram],[tra_terminal_bateria],[tra_terminal_latitud],[tra_terminal_longitud],
										[tra_terminal_apks_instalados],[tra_terminal_nombre_grupo],[tra_terminal_id_device],[tra_terminal_ip],[tra_apk_descargado],[tra_xml_descargado],[tra_img_descargado])
			VALUES (GETDATE(), @operationName, @terminalId, @login, @pass, @terminalRam, @terminalBattery, @terminalLatitude, @terminalLongitude, @installedAPKs, @groupName, @terminalDeviceId, @terminalIP, @terminalDownloadedAPK,
						 @terminalDownloadedXML, @terminalDownloadedIMG);

			SET @transactionId = SCOPE_IDENTITY()

			--ALMACENADO CORRECTAMENTE
			SET @responseCode = 1

			--FIN CUERPO DEL SP
		END

		IF @startTranCount = 0 
		BEGIN

			COMMIT TRANSACTION

			--SELECT FINAL
			SELECT @responseCode AS RESPONSECODE, @transactionId AS TRANSACTIONID

		END
	END TRY
	BEGIN CATCH
		IF XACT_STATE() <> 0 AND @startTranCount = 0 
			ROLLBACK TRANSACTION

		INSERT INTO [dbo].[LogErrorBaseDatos]([bderr_fecha],[bderr_nombre_funcion],[bderr_mensaje_error],[bderr_datos_adicionales])
			 VALUES (GETDATE(), OBJECT_NAME(@@PROCID), ERROR_MESSAGE(), /*Datos Extra, Parámetros del SP*/ 
									'operationName = ' + @operationName + ', login = ' + @login + ', pass' + @pass +
									'terminalRam = ' + @terminalRam + ', terminalBattery' + @terminalBattery + 
									'terminalLatitude = ' + @terminalLatitude + ', terminalLongitude' + @terminalLongitude +
									'installedAPKs = ' + @installedAPKs + ', groupName' + @groupName +
									'terminalDeviceId = ' + @terminalDeviceId + ', terminalIP' + @terminalIP +
									'terminalDownloadedAPK = ' + @terminalDownloadedAPK + ', terminalDownloadedXML' + @terminalDownloadedXML +
									', terminalDownloadedIMG' + @terminalDownloadedIMG)

		DECLARE @errMessage AS VARCHAR(4000) = ERROR_MESSAGE()
		DECLARE @errSeverity AS INT = ERROR_SEVERITY()
		DECLARE @errState AS INT = ERROR_STATE()

		RAISERROR (@errMessage, @errSeverity, @errState)
	END CATCH

END


GO