﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

-- =============================================
-- Autor:				Elkin Beltrán
-- Fecha Creación:		05/Mar/2017
-- Histórico Cambios:
------------------------------------------------
--	v1.0	Versión Inicial
------------------------------------------------
-- Campos de Respuesta:		SingleRow
-- RESPONSECODE		INT
-- =============================================

CREATE PROCEDURE [dbo].[sp_webCrearLicenciaCliente](
	@customerId BIGINT,
	@licenseType BIGINT,
	@licenseKey VARCHAR(250)
)
 
AS
BEGIN

	SET XACT_ABORT, NOCOUNT ON
	DECLARE @startTranCount INT

	--VARIABLES LOCALES DEL SP
	DECLARE @responseCode AS INT

	BEGIN TRY
		SELECT @startTranCount = @@TRANCOUNT

		IF @startTranCount = 0
		BEGIN
			BEGIN TRANSACTION		
			--INICIO CUERPO DEL SP
	
			IF NOT EXISTS(SELECT * FROM LicenciaXCliente WHERE lic_cliente_id = @customerId AND lic_tipo_estado_id = 1 /*ACTIVA*/)
			BEGIN
				INSERT INTO LicenciaXCliente(lic_fecha_creacion, lic_tipo_estado_id, lic_cliente_id, lic_tipo_licencia_id, lic_key)
					VALUES (GETDATE(), 1 /*ACTIVA*/, @customerId, @licenseType, @licenseKey)

				SELECT @responseCode = 1
			END
			ELSE
			BEGIN
				--CLIENTE CON LICENCIA YA ACTIVA
				SELECT @responseCode = 0
			END

			--FIN CUERPO DEL SP
		END

		IF @startTranCount = 0 
		BEGIN 
			COMMIT TRANSACTION

			--SELECT FINAL
			SELECT @responseCode AS RESPONSECODE
		END
	END TRY
	BEGIN CATCH
		IF XACT_STATE() <> 0 AND @startTranCount = 0 
			ROLLBACK TRANSACTION

		INSERT INTO [dbo].[LogErrorBaseDatos]([bderr_fecha],[bderr_nombre_funcion],[bderr_mensaje_error],[bderr_datos_adicionales])
			 VALUES (GETDATE(), OBJECT_NAME(@@PROCID), ERROR_MESSAGE(), /*Datos Extra, Parámetros del SP*/ 
																		'customerId = ' + @customerId + ', licenseType = ' + @licenseType + ', licenseKey = ' + @licenseKey)

		DECLARE @errMessage AS VARCHAR(4000) = ERROR_MESSAGE()
		DECLARE @errSeverity AS INT = ERROR_SEVERITY()
		DECLARE @errState AS INT = ERROR_STATE()

		RAISERROR (@errMessage, @errSeverity, @errState)
	END CATCH	

END

GO