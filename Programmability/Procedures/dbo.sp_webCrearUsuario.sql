﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

-- =============================================
-- Autor:				Elkin Beltrán
-- Fecha Creación:		06/Junio/2015
-- Histórico Cambios:
------------------------------------------------
--	v1.0	Versión Inicial
------------------------------------------------
-- Campos de Respuesta:		SingleRow
-- RSPCODE				INT
-- =============================================

CREATE PROCEDURE [dbo].[sp_webCrearUsuario](
	@userName VARCHAR(200),
	@userID VARCHAR(100),
	@userEmail VARCHAR(100),
	@userHash VARCHAR(100),
	@userProfileID INT,
	@userCustomerID INT
)
 
AS
BEGIN

	SET XACT_ABORT, NOCOUNT ON
	DECLARE @startTranCount INT

	--VARIABLES LOCALES DEL SP
	DECLARE @rspCode AS INT
	DECLARE @userIndex AS BIGINT

	BEGIN TRY
		SELECT @startTranCount = @@TRANCOUNT

		IF @startTranCount = 0
		BEGIN
			BEGIN TRANSACTION		

			--INICIO CUERPO DEL SP
			INSERT INTO Usuario(usu_nombre, usu_email, usu_login, usu_hash, usu_perfil_id, usu_estado_usuario_id, usu_fecha_exp_clave, usu_fecha_creacion)
				VALUES (@userName, @userEmail, @userID, @userHash, @userProfileID, 1/*Activo*/, DATEADD(DAY, -1, GETDATE()), GETDATE())

			--RELACIONAR USUARIO / CLIENTE SI EL PERFIL ES DIFERENTE A ADMINISTRADOR LICENCIAS = 7 Y OFICIAL DE SEGURIDAD = 18
			IF @userProfileID <> 7 AND @userProfileID <> 18
			BEGIN
				SET @userIndex = SCOPE_IDENTITY()
				INSERT INTO UsuarioXCliente(cliente_id, usuario_id) VALUES (@userCustomerID, @userIndex)
			END

			SELECT @rspCode = 1 /*OK*/
			--FIN CUERPO DEL SP
		END

		IF @startTranCount = 0 
		BEGIN 
			COMMIT TRANSACTION 

			--SELECT FINAL
			SELECT @rspCode AS RSPCODE
		END
	END TRY
	BEGIN CATCH
		IF XACT_STATE() <> 0 AND @startTranCount = 0 
			ROLLBACK TRANSACTION

		INSERT INTO [dbo].[LogErrorBaseDatos]([bderr_fecha],[bderr_nombre_funcion],[bderr_mensaje_error],[bderr_datos_adicionales])
			 VALUES (GETDATE(), OBJECT_NAME(@@PROCID), ERROR_MESSAGE(), /*Datos Extra, Parámetros del SP*/ 
																		'userName = ' + @userName + ', userID = ' + @userID + ', userEmail = ' + @userEmail + 
																		', userHash = ' + @userHash + ', userProfileID = ' + CAST(@userProfileID AS VARCHAR(10))
																		+ ', userCustomerID = ' + CAST(@userCustomerID AS VARCHAR(10)))

		DECLARE @errMessage AS VARCHAR(4000) = ERROR_MESSAGE()
		DECLARE @errSeverity AS INT = ERROR_SEVERITY()
		DECLARE @errState AS INT = ERROR_STATE()

		RAISERROR (@errMessage, @errSeverity, @errState)
	END CATCH

END












GO