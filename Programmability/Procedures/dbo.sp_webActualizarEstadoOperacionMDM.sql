﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[sp_webActualizarEstadoOperacionMDM](
	/*INPUT*/
	@operacionId BIGINT,
	@finalstatus VARCHAR(50)
)
 
AS
BEGIN

	SET XACT_ABORT, NOCOUNT ON
	DECLARE @startTranCount INT

	--VARIABLES LOCALES DEL SP	
	DECLARE @responseCode AS INT
	
	BEGIN TRY
		SELECT @startTranCount = @@TRANCOUNT

		IF @startTranCount = 0
		BEGIN
			BEGIN TRANSACTION		
			--INICIO CUERPO DEL SP

			--ACTUALIZAR TRANSACCIÓN

			UPDATE TransaccionMDM
			SET tra_estado_final = @finalstatus
			WHERE tra_id = @operacionId

			--ACTUALIZADO CORRECTAMENTE
			SET @responseCode = 1

			--FIN CUERPO DEL SP
		END

		IF @startTranCount = 0 
		BEGIN

			COMMIT TRANSACTION

			--SELECT FINAL
			SELECT @responseCode AS RESPONSECODE

		END
	END TRY
	BEGIN CATCH
		IF XACT_STATE() <> 0 AND @startTranCount = 0 
			ROLLBACK TRANSACTION

		INSERT INTO [dbo].[LogErrorBaseDatos]([bderr_fecha],[bderr_nombre_funcion],[bderr_mensaje_error],[bderr_datos_adicionales])
			 VALUES (GETDATE(), OBJECT_NAME(@@PROCID), ERROR_MESSAGE(), /*Datos Extra, Parámetros del SP*/ 
									'operacionId = ' + CAST(@operacionId AS VARCHAR) + ', finalstatus = ' + @finalstatus)

		DECLARE @errMessage AS VARCHAR(4000) = ERROR_MESSAGE()
		DECLARE @errSeverity AS INT = ERROR_SEVERITY()
		DECLARE @errState AS INT = ERROR_STATE()

		RAISERROR (@errMessage, @errSeverity, @errState)
	END CATCH

END;


GO