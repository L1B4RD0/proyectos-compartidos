﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[sp_webReporteTerminalesDetallado](
	@groupID AS BIGINT,
	@terminalID AS BIGINT,
	@customerID AS BIGINT
)
 
AS
BEGIN

	SET NOCOUNT ON;

	SELECT 
		CONVERT(VARCHAR, T.ter_fecha_creacion, 103) + ' ' + CONVERT(VARCHAR, T.ter_fecha_creacion, 108) AS ter_fecha_creacion,
		T.ter_fecha_consulta_sp as fecha_consulta,
		T.ter_aplicaciones_instaladas as aplicaciones_instaladas,
		T.ter_aplicaciones_pendientes_sp as aplicaciones_pendientes,
		T.ter_latitud_gps AS latitud_gps,
		T.ter_longitud_gps AS longitud_gps,
		G.gru_nombre,
		T.ter_serial,
		ISNULL(TT.descripcion,'') AS ter_tipo_terminal,
		ISNULL(MT.descripcion,'') AS ter_marca_terminal,
		ISNULL(MoT.descripcion,'') AS ter_modelo_terminal,
		ISNULL(IT.descripcion, '') AS ter_tipo_interface_terminal,
		T.ter_serial_sim,
		T.ter_telefono_sim,
		T.ter_imei,
		T.ter_mac,
		T.ter_UID,
		T.ter_serial_cargador,
		T.ter_serial_bateria,
		T.ter_serial_lector_biometrico,
		T.ter_descripcion,
		EA.descripcion AS ter_estado_actualizacion,
		T.ter_mensaje_actualizacion,
		(CASE WHEN T.ter_tipo_estado_id = 1 THEN 'Activa' ELSE 'Inactiva' END) AS ter_estado,
		(CASE WHEN T.ter_prioridad_grupo = 1 THEN 'Sí' ELSE 'No' END) AS ter_prioridad_grupo,
		(CASE WHEN T.ter_actualizar = 1 THEN 'Sí' ELSE 'No' END) AS ter_actualizar,
		CONVERT(VARCHAR, T.ter_fecha_programada_actualizacion, 103) + ' ' + SUBSTRING(CONVERT(VARCHAR, T.ter_fecha_programada_actualizacion, 108), 1, 5) AS ter_fecha_programacion,
		(CASE WHEN T.ter_ip_descarga = '' THEN '' ELSE T.ter_ip_descarga + ':' + T.ter_puerto_descarga END) AS ter_datos_descarga,
		(CASE WHEN T.ter_validar_imei = 1 THEN 'Sí' ELSE 'No' END) AS ter_validar_imei,
		(CASE WHEN T.ter_validar_sim = 1 THEN 'Sí' ELSE 'No' END) AS ter_validar_sim,
		T.ter_nivel_gprs,
		T.ter_nivel_bateria,
		REPLACE(T.ter_aplicaciones_instaladas,CHAR(10),'#') AS ter_aplicaciones_instaladas,
		ISNULL(TI.descripcion, '') AS tipo_identificacion_tercero,
		ISNULL(TER.terc_numero_identificacion, '') AS terc_numero_identificacion,
		ISNULL(Ter.terc_nombre, '') AS terc_nombre,
		ISNULL(Ter.terc_direccion, '') AS terc_direccion,
		ISNULL(Ter.terc_celular, '') AS terc_celular,
		ISNULL(Ter.terc_telefono_fijo, '') AS terc_telefono_fijo,
		ISNULL(Ter.terc_correo_electronico, '') AS terc_correo_electronico
	FROM TERMINAL T
		INNER JOIN Grupo G ON (T.ter_grupo_id = G.gru_id)
		INNER JOIN TipoTerminal TT ON (T.ter_tipo_terminal_id = TT.id)
		INNER JOIN MarcaTerminal MT ON (t.ter_marca_terminal_id = MT.id)
		INNER JOIN ModeloTerminal MoT ON (t.ter_modelo_terminal_id = MoT.id)
		INNER JOIN InterfaceTerminal IT ON (T.ter_interface_terminal_id = IT.id)
		INNER JOIN EstadoActualizacion EA ON (T.ter_estado_actualizacion_id = EA.id)	
		LEFT OUTER JOIN Tercero TER ON (T.ter_tercero_id = TER.terc_id)
		LEFT OUTER JOIN TipoIdentificacion TI ON (TER.terc_tipo_identificacion_id = TI.id)
	WHERE (T.ter_grupo_id = @groupID OR @groupID = -1) AND G.gru_cliente_id = @customerID AND (T.ter_id = @terminalID OR @terminalID = -1)
	ORDER BY T.ter_fecha_creacion
	
END




GO