﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[sp_webCerrarSesionWeb](
	@loginID AS INT
)
 
AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE LoginInfo
	SET
		login_fecha_fin = GETDATE()
	WHERE 
		login_id = @loginID

END










GO