﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[sp_webReporteTransactions]
(
	@tid AS VARCHAR(255),
	@merchantId AS VARCHAR(255),
	@codTranMod AS VARCHAR(255),
	@usuario AS VARCHAR(255),
	@startDate AS DATETIME,
	@endDate AS DATETIME
)
 
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @endDateReport AS DATETIME	
	SET @endDateReport = DATEADD(DAY, 1, @endDate)
	SELECT 
	[ct_id],  
	CONVERT(VARCHAR, ct_fecha_hora, 103) + ' ' + CONVERT(VARCHAR, ct_fecha_hora, 108) AS  ct_fecha_hora, 
	[ct_ip] ,
	[ct_puerto], 
	[ct_incoming_data],
	[ct_outgoing_data],
	[fi_type] ,
	[fi_msgType],
	[fi_pcc] ,
	[fi_amount],
	[fi_stan_11], 
	[fi_stan_62],
	[fi_expirationDate], 
	[fi_posentrymode],
	[fi_track2],
	[fi_pan],
	[fi_tid] ,
	[fi_merchantId],
	[fi_emvData] ,
	[fi_pinBlock] ,
	[fi_cvc],
	[fi_nombreTarjeta], 
	[fi_encodedBlock1] ,
	[fi_encodedBlock2] ,
	[fi_encodedBlock3] ,
	[fi_encodedBlock4],
	[fi_encodedBlock5] ,
	[fi_encodedBlock6] ,
	[fi_encodedBlock7] ,
	[fi_encodedBlock8] ,
	[fi_encodedBlock9],
	[fi_encodedBlock10], 
	[bi_businessData],
	[bi_CodigoTransaccion], 
	[bi_CodigoTransaccionModificado], 
	[bi_Usuario],
	[bi_FechaHora], 
	[bi_TipoVenta], 
	[bi_IdTransaction], 
	[bi_Agency], 
	[bi_Monto] ,
	[bi_Plan] ,
	[bi_CodigoEmisor],
	[bi_TipoCredito], 
	[bi_NombreTipoCredito],
	[bi_NombreAerolinea],
	[bi_DateTime], 
	[bi_IdRecord], 
	[bi_IdSecuencialCliente], 
	[bi_Operador], 
	[bi_CodigoEstablecimiento], 
	[bi_IVA] ,
	[bi_Num_Voucher], 
	[bi_Ciudad],
	[bi_ValorServicio],
	[bi_ValorPropina],
	[bi_ValorMontoFijo],
	[bi_Tarifa],
	[bi_Impuestos], 
	[bi_Interes], 
	[bi_Boletos], 
	[bi_NombrePAX], 
	[vtc_json_request], 
	[vtc_json_response], 
	[fo_rspCode],
	[fo_authorization], 
	[fo_msgResponse], 
	[fo_emvData],
	[fo_encoded] ,
	[bo_businessData],
	[bo_ReturnCode], 
	[bo_Description], 
	[bo_DateTimeReturn], 
	[bo_Autorization], 
	[bo_Emisor], 
	[bo_NumeroTarjeta], 
	[bo_NumeroTarjetaEditada],
	[bo_FechaExpiracion], 
	[bo_Nombre],
	[bo_CodigoEmisor],
	[bo_TipoConvenio], 
	[bo_IdRecord], 
	[bo_Tarifa], 
	[bo_Impuestos], 
	[bo_Interes], 
	[bo_Total], 
	[bo_Mensaje], 
	[bo_Comision], 
	[bo_Boletos], 
	[bo_NombrePAX], 
	[db_rsp_code], 
	[db_msg],
	[db_error_code], 
	[db_error_msg]
	FROM  [dbo].[EASYSOFT_TRANSACCIONES]
	WHERE  ct_fecha_hora >= @startDate AND ct_fecha_hora <= @endDateReport
	AND (fi_merchantId = @merchantId OR @merchantId = '- Todos los Merchantid -') AND (bi_CodigoTransaccionModificado = @codTranMod OR @codTranMod =  '- Todos los Codigos -') AND (bi_Usuario = @usuario OR @usuario = '- Todos los Usuarios -')AND (fi_tid = @tid OR @tid = '- Todos los TID -')
	ORDER BY [ct_id] ASC
END




GO