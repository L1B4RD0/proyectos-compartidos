﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[sp_webConsultarDatosGrupoAndroid](
	@groupID BIGINT
)
 
AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT	G.gru_nombre, 
			G.gru_descripcion, 
			
			G.gru_ip_descarga, 
			G.gru_puerto_descarga,
			G.gru_blocking_mode,
			G.gru_update_now, 
			G.gru_actualizar, 
			G.gru_programar_fecha, 
			ISNULL(G.gru_fecha_programada_actualizacion_ini, GETDATE()), 
			ISNULL(G.gru_fecha_programada_actualizacion_fin, GETDATE()),
			ISNULL(CAST(G.gru_rango_hora_actualizacion_ini AS DATETIME), 
			GETDATE()), 
			ISNULL(CAST(G.gru_rango_hora_actualizacion_fin AS DATETIME), GETDATE()), 
			G.gru_aplicaciones_permitidas, 
			G.gru_numero_consultas, 
			G.gru_frecuencia_horas,
			G.gru_aplicacion_kiosko,
			G.gru_mensaje_desplegar
		
	FROM Grupo G
	WHERE G.gru_id = @groupID

END

GO