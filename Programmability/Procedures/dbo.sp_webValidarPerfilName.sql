﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[sp_webValidarPerfilName](
	/*INPUT*/
	@perfilID VARCHAR(50)
	
)
AS
BEGIN

    SET XACT_ABORT ON
	SET NOCOUNT ON
	DECLARE @startTranCount INT

	--VARIABLES LOCALES DEL SP
    DECLARE @responseCode AS VARCHAR(50)

	BEGIN TRY
	  SELECT @startTranCount = @@TRANCOUNT
	  PRINT @startTranCount
	      
	 BEGIN TRANSACTION;  
     IF EXISTS(SELECT * FROM PERFIL WHERE perf_nombre = @perfilID )
	select resCode = 1
else 
	select resCode = 0
     COMMIT TRANSACTION; 

	 PRINT @startTranCount

	END TRY
	BEGIN CATCH

		IF (XACT_STATE()) = -1  
			BEGIN  
				PRINT 'The transaction is in an uncommittable state.' +  
              ' Rolling back transaction.'  
			ROLLBACK TRANSACTION;  
			PRINT @startTranCount
			 INSERT INTO [dbo].[LogErrorBaseDatos]([bderr_fecha],[bderr_nombre_funcion],[bderr_mensaje_error],[bderr_datos_adicionales])
			  VALUES (GETDATE(), OBJECT_NAME(@@PROCID), ERROR_MESSAGE(), /*Datos Extra, Parámetros del SP*/ 
									'PERFIL = ' + @perfilID)

				DECLARE @errMessage AS VARCHAR(4000) = ERROR_MESSAGE()
				DECLARE @errSeverity AS INT = ERROR_SEVERITY()
				DECLARE @errState AS INT = ERROR_STATE()

				RAISERROR (@errMessage, @errSeverity, @errState)
		END;  

		-- Test whether the transaction is active and valid.  
		IF (XACT_STATE()) = 1  
		BEGIN  
			PRINT 'The transaction is committable.' +   
              ' Committing transaction.'  
			COMMIT TRANSACTION;   
			PRINT @startTranCount  
		END;  

	END CATCH


END

GO