﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

-- =============================================
-- Autor:				Elkin Beltrán
-- Fecha Creación:		17/Febrero/2017
-- Histórico Cambios:
------------------------------------------------
--	v1.0	Versión Inicial		
------------------------------------------------
-- Campos de Respuesta:		SingleRow
-- cli_id					BIGINT
-- cli_nombre				VARCHAR(100)
-- cli_descripcion			VARCHAR(50)
-- cli_ip_descarga			VARCHAR(50)
-- cli_puerto_descarga		VARCHAR(50)
-- cli_estado				BIT
-- =============================================
CREATE PROCEDURE [dbo].[sp_webConsultarClientes]
(
	@customerName VARCHAR(50),
	@customerDesc VARCHAR(100)
)
 
AS
BEGIN
	
	SET NOCOUNT ON

	SELECT 
		C.cli_id,
		C.cli_nombre, 
		C.cli_descripcion, 
		C.cli_ip_descarga, 
		C.cli_puerto_descarga,
		(CASE WHEN C.cli_tipo_estado_id = 1 THEN 1 ELSE 0 END) cli_estado
	FROM Cliente C
	WHERE (C.cli_nombre LIKE '%' + @customerName + '%' OR @customerName = '-1')
			AND (C.cli_descripcion LIKE '%' + @customerDesc + '%' OR @customerDesc = '-1')

END



GO