﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[sp_webConsultarUbicacionTerminalesGrupo](
	@groupId BIGINT
)
 
AS
BEGIN

	DECLARE @latDefault AS VARCHAR(50) = '21.983801'
	DECLARE @lngDefault AS VARCHAR(50) = '-40.385742'

	SET NOCOUNT ON;
	
	SELECT 
		T.ter_id, (CASE WHEN G.gru_tipo_android IS NULL OR G.gru_tipo_android = 1 THEN T.ter_serial + ' - ' + T.ter_id_heracles ELSE T.ter_serial END) AS serial, ISNULL((CASE WHEN T.ter_latitud_gps = '' Or T.ter_latitud_gps = '0' Or T.ter_latitud_gps IS NULL THEN @latDefault ELSE T.ter_latitud_gps END), @latDefault), ISNULL((CASE WHEN T.ter_longitud_gps = '' Or T.ter_longitud_gps = '0' Or T.ter_longitud_gps IS NULL THEN @lngDefault ELSE T.ter_longitud_gps END), @lngDefault), (CASE WHEN ABS(DATEDIFF(HOUR, GETDATE(), ISNULL(T.ter_fecha_actualizacion_auto, DATEADD(DAY, -1, GETDATE())))) > 24 THEN '../img/markerRed.png' ELSE '../img/markerBlue.png' END) 
	FROM Terminal T 
		INNER JOIN Grupo G ON (G.gru_id = T.ter_grupo_id)
	WHERE T.ter_grupo_id = @groupId
	ORDER BY T.ter_id 

END


GO