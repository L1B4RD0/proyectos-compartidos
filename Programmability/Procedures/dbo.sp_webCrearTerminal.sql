﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[sp_webCrearTerminal](
	@groupID AS INT,
	@terminalSerial VARCHAR(50),
	@terminalType INT,
	@terminalBrand INT,
	@terminalModel INT,
	@terminalInterface INT,
	@serialSIM VARCHAR(50),
	@mobileNumberSIM VARCHAR(50),
	@IMEI VARCHAR(50),
	@chargerSerial VARCHAR(50),
	@batterySerial VARCHAR(50),
	@biometricSerial VARCHAR(50),
	@terminalDesc VARCHAR(250),
	@groupPriority BIT,
	@updateTerminal BIT,
	@scheduleDate DATETIME,
	@updateIP VARCHAR(20),
	@updatePort VARCHAR(10),
	@updateBlockingMode BIT,
	@updateUpdateNow BIT,
	@validateIMEI BIT,
	@validateSerialSIM BIT,
	--Contact Data
	@contactIDNumber VARCHAR(20),
	@contactIDType INT,
	@contactName VARCHAR(50),
	@contactAddress VARCHAR(100),
	@contactMobile VARCHAR(20),
	@contactPhone VARCHAR(20),
	@contactEmail VARCHAR(50)
)
 
AS
BEGIN

	SET XACT_ABORT, NOCOUNT ON
	DECLARE @startTranCount INT

	--VARIABLES LOCALES DEL SP
	DECLARE @responseCode AS INT
	DECLARE @responseMsg AS VARCHAR(500)
	DECLARE @contactId AS BIGINT
	DECLARE @imeiNumber INT
	DECLARE @serialIMEIDuplicado AS VARCHAR(500)

	BEGIN TRY
	    --VALIDAR QUE EL IMEI NO ESTE REPETIDO
		select @imeiNumber = count(*) from dbo.terminal where ter_serial != @terminalSerial and ter_imei = @IMEI 
		IF @imeiNumber = 0
		BEGIN


					SELECT @startTranCount = @@TRANCOUNT

					IF @startTranCount = 0
					BEGIN
						BEGIN TRANSACTION		
						--INICIO CUERPO DEL SP
	
						--VALIDAR CREACIÓN DE CONTACTO
						IF (LEN(@contactIDNumber) <= 0)
						BEGIN
							--SIN DATOS DE CONTACTO
							SET @contactId = NULL
						END

						ELSE
						BEGIN
							--CON DATOS DE CONTACTO, VALIDAR SI ES EXISTENTE O ES NUEVO
							SELECT @contactId = T.terc_id FROM Tercero T WHERE T.terc_numero_identificacion = LTRIM(RTRIM(@contactIDNumber))

							IF @contactId IS NULL
							BEGIN
								--ES NUEVO CONTACTO, CREARLO
								INSERT INTO Tercero(terc_tipo_identificacion_id, terc_numero_identificacion, terc_nombre, terc_direccion, terc_celular, terc_telefono_fijo, terc_correo_electronico, terc_fecha_creacion)
									VALUES (@contactIDType, @contactIDNumber, @contactName, @contactAddress, @contactMobile, @contactPhone, @contactEmail, GETDATE())

								SET @contactId = SCOPE_IDENTITY()






							END
						END


						--INSERTAR NUEVA TERMINAL EN GRUPO
						IF @groupPriority = 1
						BEGIN
							--ACTUALIZAR TERMINAL X GRUPO
							INSERT INTO [dbo].[Terminal]([ter_serial], [ter_tipo_terminal_id], [ter_marca_terminal_id], [ter_modelo_terminal_id], [ter_interface_terminal_id], [ter_serial_sim], [ter_telefono_sim], [ter_imei], [ter_serial_cargador]
										,[ter_serial_bateria], [ter_serial_lector_biometrico], [ter_descripcion], [ter_grupo_id], [ter_tercero_id], [ter_prioridad_grupo], [ter_actualizar], [ter_fecha_programada_actualizacion]
										,[ter_ip_descarga], [ter_puerto_descarga], [ter_blocking_mode], [ter_update_now], [ter_estado_actualizacion_id], [ter_mensaje_actualizacion], [ter_tipo_estado_id], [ter_validar_imei], [ter_validar_sim], [ter_fecha_creacion])
							VALUES (@terminalSerial, @terminalType, @terminalBrand, @terminalModel, @terminalInterface, @serialSIM, @mobileNumberSIM, @IMEI, @chargerSerial, @batterySerial, @biometricSerial, @terminalDesc, @groupID, @contactId, 1,
									0, NULL, '', '', 0, 0, 1, '', 1, @validateIMEI, @validateSerialSIM, GETDATE())
						END
						ELSE
						BEGIN
							IF @updateTerminal = 1 
							BEGIN
								--ACTUALIZAR TERMINAL MANUAL
								INSERT INTO [dbo].[Terminal]([ter_serial], [ter_tipo_terminal_id], [ter_marca_terminal_id], [ter_modelo_terminal_id], [ter_interface_terminal_id], [ter_serial_sim], [ter_telefono_sim], [ter_imei], [ter_serial_cargador]
											,[ter_serial_bateria], [ter_serial_lector_biometrico], [ter_descripcion], [ter_grupo_id], [ter_tercero_id], [ter_prioridad_grupo], [ter_actualizar], [ter_fecha_programada_actualizacion]
											,[ter_ip_descarga], [ter_puerto_descarga], [ter_blocking_mode], [ter_update_now], [ter_estado_actualizacion_id], [ter_mensaje_actualizacion], [ter_tipo_estado_id], [ter_validar_imei], [ter_validar_sim], [ter_fecha_creacion])
								VALUES (@terminalSerial, @terminalType, @terminalBrand, @terminalModel, @terminalInterface, @serialSIM, @mobileNumberSIM, @IMEI, @chargerSerial, @batterySerial, @biometricSerial, @terminalDesc, @groupID, @contactId, 0,
										1, @scheduleDate, @updateIP, @updatePort, @updateBlockingMode, @updateUpdateNow, 1, '', 1, @validateIMEI, @validateSerialSIM, GETDATE())
							END
							ELSE
							BEGIN
								--NO ACTUALIZAR TERMINAL
								INSERT INTO [dbo].[Terminal]([ter_serial], [ter_tipo_terminal_id], [ter_marca_terminal_id], [ter_modelo_terminal_id], [ter_interface_terminal_id], [ter_serial_sim], [ter_telefono_sim], [ter_imei], [ter_serial_cargador]
											,[ter_serial_bateria], [ter_serial_lector_biometrico], [ter_descripcion], [ter_grupo_id], [ter_tercero_id], [ter_prioridad_grupo], [ter_actualizar], [ter_fecha_programada_actualizacion]
											,[ter_ip_descarga], [ter_puerto_descarga], [ter_blocking_mode], [ter_update_now], [ter_estado_actualizacion_id], [ter_mensaje_actualizacion], [ter_tipo_estado_id], [ter_validar_imei], [ter_validar_sim], [ter_fecha_creacion])
								VALUES (@terminalSerial, @terminalType, @terminalBrand, @terminalModel, @terminalInterface, @serialSIM, @mobileNumberSIM, @IMEI, @chargerSerial, @batterySerial, @biometricSerial, @terminalDesc, @groupID, @contactId, 0,
										0, NULL, '', '', 0, 0, 1, '', 1, @validateIMEI, @validateSerialSIM, GETDATE())
							END
						END



						SELECT @responseCode = 1
				        SELECT @responseMsg = 'Terminal creada correctamente.'
































						--FIN CUERPO DEL SP
					END


					IF @startTranCount = 0 
					BEGIN 
						COMMIT TRANSACTION



				        --SELECT FINAL
				        SELECT @responseCode AS RESPONSECODE, @responseMsg AS RESPONSEMSG
					END




		END
		ELSE
		BEGIN
		    SELECT @serialIMEIDuplicado = ter_serial from dbo.terminal where ter_imei = @IMEI
			SET @responseCode = 0
			SET @responseMsg = 'Terminal no creado de forma correcta - El IMEI: ' + @IMEI + ' ya existe asociado al terminal: ' + @serialIMEIDuplicado
			--SELECT FINAL
			SELECT @responseCode AS RESPONSECODE, @responseMsg AS RESPONSEMSG
	
		END


		
	END TRY
	BEGIN CATCH

		SELECT @responseCode = 0
		SELECT @responseMsg = 'Terminal no creado de forma correcta.'
		SELECT @responseCode AS RESPONSECODE, @responseMsg AS RESPONSEMSG

		IF XACT_STATE() <> 0 AND @startTranCount = 0 
			ROLLBACK TRANSACTION

		INSERT INTO [dbo].[LogErrorBaseDatos]([bderr_fecha],[bderr_nombre_funcion],[bderr_mensaje_error],[bderr_datos_adicionales])
			 VALUES (GETDATE(), OBJECT_NAME(@@PROCID), ERROR_MESSAGE(), /*Datos Extra, Parámetros del SP*/ 
																		'terminalSerial = ' + @terminalSerial + ', serialSIM = ' + @serialSIM + ', IMEI = ' + @IMEI + 
																		', updateIP = ' + @updateIP + ', updatePort = ' + @updatePort)

		DECLARE @errMessage AS VARCHAR(4000) = ERROR_MESSAGE()
		DECLARE @errSeverity AS INT = ERROR_SEVERITY()
		DECLARE @errState AS INT = ERROR_STATE()

		RAISERROR (@errMessage, @errSeverity, @errState)
	END CATCH	

END



GO