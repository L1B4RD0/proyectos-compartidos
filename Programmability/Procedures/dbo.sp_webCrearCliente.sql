﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[sp_webCrearCliente](
	@customerName VARCHAR(50),
	@customerDesc VARCHAR(250),
	@customerIP VARCHAR(20),
	@customerPort VARCHAR(10),
	@learningMode BIT,
	--Contact Data
	@contactIDNumber VARCHAR(20),
	@contactIDType INT,
	@contactName VARCHAR(50),
	@contactAddress VARCHAR(100),
	@contactMobile VARCHAR(20),
	@contactPhone VARCHAR(20),
	@contactEmail VARCHAR(50)	
)
 
AS
BEGIN

	SET XACT_ABORT, NOCOUNT ON
	DECLARE @startTranCount INT

	--VARIABLES LOCALES DEL SP
	DECLARE @responseCode AS INT
	DECLARE @contactId AS BIGINT

	BEGIN TRY
		SELECT @startTranCount = @@TRANCOUNT

		IF @startTranCount = 0
		BEGIN
			BEGIN TRANSACTION		
			--INICIO CUERPO DEL SP
	
			--VALIDAR CREACIÓN DE CONTACTO
			IF (LEN(@contactIDNumber) <= 0)
			BEGIN
				--SIN DATOS DE CONTACTO
				SET @contactId = NULL
			END
			ELSE
			BEGIN
				--CON DATOS DE CONTACTO, VALIDAR SI ES EXISTENTE O ES NUEVO
				SELECT @contactId = T.terc_id FROM Tercero T WHERE T.terc_numero_identificacion = LTRIM(RTRIM(@contactIDNumber))

				IF @contactId IS NULL
				BEGIN
					--ES NUEVO CONTACTO, CREARLO
					INSERT INTO Tercero(terc_tipo_identificacion_id, terc_numero_identificacion, terc_nombre, terc_direccion, terc_celular, terc_telefono_fijo, terc_correo_electronico, terc_fecha_creacion)
						VALUES (@contactIDType, @contactIDNumber, @contactName, @contactAddress, @contactMobile, @contactPhone, @contactEmail, GETDATE())

					SET @contactId = SCOPE_IDENTITY()

				END
			END

			--INSERTAR NUEVO CLIENTE
			INSERT INTO Cliente(cli_nombre, cli_tercero_id, cli_descripcion, cli_ip_descarga, cli_puerto_descarga, cli_autoaprendizaje, cli_tipo_estado_id, cli_fecha_creacion)
					VALUES (@customerName, @contactId, @customerDesc, @customerIP, @customerPort, @learningMode, 1 /*Activo*/, GETDATE())

			SELECT @responseCode = 1

			--FIN CUERPO DEL SP
		END

		IF @startTranCount = 0 
		BEGIN 
			COMMIT TRANSACTION

			--SELECT FINAL
			SELECT @responseCode AS RESPONSECODE
		END
	END TRY
	BEGIN CATCH
		IF XACT_STATE() <> 0 AND @startTranCount = 0 
			ROLLBACK TRANSACTION

		INSERT INTO [dbo].[LogErrorBaseDatos]([bderr_fecha],[bderr_nombre_funcion],[bderr_mensaje_error],[bderr_datos_adicionales])
			 VALUES (GETDATE(), OBJECT_NAME(@@PROCID), ERROR_MESSAGE(), /*Datos Extra, Parámetros del SP*/ 
																		'customerName = ' + @customerName + ', customerDesc = ' + @customerDesc + ', customerIP = ' + @customerIP + 
																		', customerPort = ' + @customerPort)

		DECLARE @errMessage AS VARCHAR(4000) = ERROR_MESSAGE()
		DECLARE @errSeverity AS INT = ERROR_SEVERITY()
		DECLARE @errState AS INT = ERROR_STATE()

		RAISERROR (@errMessage, @errSeverity, @errState)
	END CATCH	

END



GO