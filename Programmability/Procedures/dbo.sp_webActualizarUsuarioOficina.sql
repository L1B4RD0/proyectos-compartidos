﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

-- =============================================
-- Autor:				Sixto Contreras
-- Fecha Creación:		28/Mayo/2015
-- Histórico Cambios:
------------------------------------------------
--	v1.0	Versión Inicial		28/Mayo/2015
------------------------------------------------
-- Campos de Respuesta:		SingleRow
-- RESULTADO	INT
-- =============================================
CREATE PROCEDURE [dbo].[sp_webActualizarUsuarioOficina](
	@UsuarioOficinaId INT,
	@MontoBase NUMERIC
)
 
AS
BEGIN
	SET XACT_ABORT ON
	DECLARE @startTranCount INT

	--VARIABLES LOCALES DEL SP

	BEGIN TRY
		SELECT @startTranCount = @@TRANCOUNT

		IF @startTranCount = 0
		BEGIN
			BEGIN TRANSACTION		
			--INICIO CUERPO DEL SP

			--ACTUALIZAR VALOR MONTO DE CAJA DEL USUARIO OFICINA
			UPDATE UsuarioXOficina
			SET 
				monto_base_caja = @MontoBase
			WHERE
				id = @UsuarioOficinaId
			
			--FIN CUERPO DEL SP			
					
		END

		IF @startTranCount = 0 
		BEGIN 
			COMMIT TRANSACTION 
			
			--PARAMETROS DE RETORNO
			SELECT 1 /*OK*/
		END
	END TRY
	BEGIN CATCH
		IF XACT_STATE() <> 0 AND @startTranCount = 0 
			ROLLBACK TRANSACTION

		INSERT INTO [dbo].[LogErrorBaseDatos]([bderr_fecha],[bderr_nombre_funcion],[bderr_mensaje_error],[bderr_datos_adicionales])
			 VALUES (GETDATE(), OBJECT_NAME(@@PROCID), ERROR_MESSAGE(), /*Datos Extra, Parámetros del SP*/ 'UsuarioOficinaId = ' + CAST(@UsuarioOficinaId AS VARCHAR(10)) + ', MontoBase = ' + @MontoBase)

		DECLARE @errMessage AS VARCHAR(4000) = ERROR_MESSAGE()
		DECLARE @errSeverity AS INT = ERROR_SEVERITY()
		DECLARE @errState AS INT = ERROR_STATE()

		RAISERROR (@errMessage, @errSeverity, @errState)
	END CATCH
END


GO