﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[sp_webConsultarDatosUsuarioPorLogin](
	@userID VARCHAR(100)
)
 
AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT U.usu_login, U.usu_nombre, U.usu_email, U.usu_estado_usuario_id, U.usu_perfil_id, CASE WHEN UXC.cliente_id IS NULL THEN -1 ELSE UXC.cliente_id END AS usu_cliente_id
	FROM Usuario U 
		LEFT JOIN UsuarioXCliente UXC ON(U.usu_id = UXC.usuario_id)
	WHERE U.usu_login = @userID

END







GO