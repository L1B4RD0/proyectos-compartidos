﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[sp_webCrearOpcion](
	@optionTitle VARCHAR(100),
	@optionURL VARCHAR(100),
	@optionIMGURL VARCHAR(100),
	@optionType INT,
	@optionParentID INT
)
 
AS
BEGIN

	DECLARE @optionOrder AS INT

	IF @optionType = 1	
		SELECT @optionOrder = MAX(opc_orden) + 1 FROM Opcion WHERE opc_tipo_opcion_id = @optionType AND opc_opcion_padre_id IS NULL
	ELSE IF @optionType = 2
		SELECT @optionOrder = MAX(opc_orden) + 1 FROM Opcion WHERE opc_tipo_opcion_id = @optionType AND opc_opcion_padre_id = @optionParentID
	 
	IF @optionOrder IS NULL
		SET @optionOrder = 1
	
	BEGIN TRANSACTION

	BEGIN TRY	
	
		INSERT INTO Opcion(opc_titulo, opc_url, opc_img_url, opc_tipo_opcion_id, opc_opcion_padre_id, opc_orden)
			VALUES (@optionTitle, @optionURL, @optionIMGURL, @optionType, @optionParentID, @optionOrder)

		COMMIT TRANSACTION

		SELECT 1 /*OK*/

	END TRY
	BEGIN CATCH
		
		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		PRINT(ERROR_MESSAGE())

		SELECT 0 /*ERROR*/

	END CATCH;

END













GO