﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[sp_webConsultarXMLsTerminalAndroid](
	@terminalId BIGINT
)
 
AS
BEGIN

	SET NOCOUNT ON;
	
	--VARIABLES LOCALES DEL SP
	DECLARE @backgroundColorGreen AS VARCHAR(10) = '#dff0d8'
	DECLARE @backgroundColorRed AS VARCHAR(10) = '#e47a7a'
	DECLARE @currentDeployedXmlFile AS VARCHAR(100) = '-'

	----OBTENER VALORES DE DESPLIEGUE DE ARCHIVOS XML's
	SELECT TOP 1 @currentDeployedXmlFile = ISNULL(GXAD.desp_nombre_archivo ,'-') 
	FROM GrupoXArchivosDesplegados GXAD 
		INNER JOIN Grupo G ON (GXAD.desp_grupo_id = G.gru_id)
		INNER JOIN Terminal T ON (T.ter_grupo_id = G.gru_id)
	WHERE T.ter_id = @terminalId AND GXAD.desp_tipo_despliegue_id = 2 ORDER BY GXAD.desp_id DESC	/*XML's*/

	SELECT 
		desc_id, 
		desc_fecha, 
		desc_zip_xml,
		CASE WHEN desc_zip_xml = @currentDeployedXmlFile THEN @backgroundColorGreen ELSE @backgroundColorRed END AS desc_color
	FROM TerminalXArchivosDescargados 
	WHERE desc_terminal_id = @terminalId AND desc_zip_imgs IS NULL AND desc_apk_archivo IS NULL 
	ORDER BY 1 DESC

END



GO