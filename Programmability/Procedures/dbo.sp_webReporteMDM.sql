﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[sp_webReporteMDM](
	@groupID AS BIGINT,
	@terminalID AS BIGINT,
	@customerID AS BIGINT,
	@startDate AS DATETIME,
	@endDate AS DATETIME
)
AS
BEGIN

	SET NOCOUNT ON;
		--VARIABLES LOCALES DEL SP
	DECLARE @defaultPackageName AS VARCHAR(200) = ''; SELECT @defaultPackageName = param_valor FROM ParametroGeneral WHERE param_nombre = 'PackageNameNeoCNB';
	DECLARE @backgroundColorGreen AS VARCHAR(10) = '#dff0d8'
	DECLARE @backgroundColorRed AS VARCHAR(10) = '#e47a7a'
	DECLARE @currentDeployedAPKFileMaxVersion AS VARCHAR(100) = '-'
	DECLARE @currentDeployedXmlFile AS VARCHAR(100) = '-'
	DECLARE @currentDeployedImgFile AS VARCHAR(100) = '-'


	--OBTENER VALORES DE DESPLIEGUE DE ARCHIVOS APK's, XML's E IMG'S
	SELECT TOP 1 @currentDeployedAPKFileMaxVersion = ISNULL(GXAD.desp_version ,'-') FROM GrupoXArchivosDesplegados GXAD WHERE GXAD.desp_grupo_id = @groupId AND GXAD.desp_tipo_despliegue_id = 1 AND GXAD.desp_nombre_paquete = @defaultPackageName ORDER BY GXAD.desp_id DESC	/*APK's*/
	SELECT TOP 1 @currentDeployedXmlFile = ISNULL(GXAD.desp_nombre_archivo ,'-') FROM GrupoXArchivosDesplegados GXAD WHERE GXAD.desp_grupo_id = @groupId AND GXAD.desp_tipo_despliegue_id = 2 ORDER BY GXAD.desp_id DESC	/*XML's*/
	SELECT TOP 1 @currentDeployedImgFile = ISNULL(GXAD.desp_nombre_archivo ,'-') FROM GrupoXArchivosDesplegados GXAD WHERE GXAD.desp_grupo_id = @groupId AND GXAD.desp_tipo_despliegue_id = 3 ORDER BY GXAD.desp_id DESC	/*IMG's*/



	DECLARE @endDateReport AS DATETIME	
	SET @endDateReport = DATEADD(DAY, 1, @endDate)

	SELECT 
		CONVERT(VARCHAR, TMDM.tra_fecha, 103) + ' ' + CONVERT(VARCHAR, TMDM.tra_fecha, 108) AS tra_fecha,
		UPPER(TMDM.tra_operacion) AS tra_operacion,
		TMDM.tra_terminal_id_device,
		G.gru_nombre,
		TMDM.tra_terminal_ip,
		REPLACE(TMDM.tra_terminal_apks_instalados,',', CHAR(13) + CHAR(10)) AS tra_terminal_apks_instalados,
		TMDM.tra_apk_descargado,
		TMDM.tra_xml_descargado,
		TMDM.tra_img_descargado,
		TMDM.tra_estado_final,
			--APK's
		CASE WHEN EXISTS( (SELECT TOP 1 TXAD.desc_apk_version FROM TerminalXArchivosDescargados TXAD WHERE TXAD.desc_terminal_id = T.ter_id AND TXAD.desc_zip_imgs IS NULL AND TXAD.desc_zip_xml IS NULL AND TXAD.desc_apk_paquete = @defaultPackageName ORDER BY TXAD.desc_id DESC) ) THEN @defaultPackageName + ' v' + (SELECT TOP 1 TXAD.desc_apk_version FROM TerminalXArchivosDescargados TXAD WHERE TXAD.desc_terminal_id = T.ter_id AND TXAD.desc_zip_imgs IS NULL AND TXAD.desc_zip_xml IS NULL AND TXAD.desc_apk_paquete = @defaultPackageName ORDER BY TXAD.desc_id DESC) ELSE '-' END AS apk_banco,
		CASE WHEN EXISTS( (SELECT TOP 1 TXAD.desc_apk_version FROM TerminalXArchivosDescargados TXAD WHERE TXAD.desc_terminal_id = T.ter_id AND TXAD.desc_zip_imgs IS NULL AND TXAD.desc_zip_xml IS NULL AND TXAD.desc_apk_paquete = @defaultPackageName ORDER BY TXAD.desc_id DESC) ) THEN (CASE WHEN @currentDeployedAPKFileMaxVersion = (SELECT TOP 1 TXAD.desc_apk_version FROM TerminalXArchivosDescargados TXAD WHERE TXAD.desc_terminal_id = T.ter_id AND TXAD.desc_zip_imgs IS NULL AND TXAD.desc_zip_xml IS NULL AND TXAD.desc_apk_paquete = @defaultPackageName ORDER BY TXAD.desc_id DESC) THEN @backgroundColorGreen ELSE @backgroundColorRed END) ELSE @backgroundColorRed END AS apk_banco_color,
		--XML's
		CASE WHEN EXISTS( (SELECT TOP 1 TXAD.desc_zip_xml FROM TerminalXArchivosDescargados TXAD WHERE TXAD.desc_terminal_id = T.ter_id AND TXAD.desc_zip_imgs IS NULL AND TXAD.desc_apk_archivo IS NULL ORDER BY TXAD.desc_id DESC) ) THEN (SELECT TOP 1 TXAD.desc_zip_xml FROM TerminalXArchivosDescargados TXAD WHERE TXAD.desc_terminal_id = T.ter_id AND TXAD.desc_zip_imgs IS NULL AND TXAD.desc_apk_archivo IS NULL ORDER BY TXAD.desc_id DESC) ELSE '-' END AS configuracion,
		CASE WHEN EXISTS( (SELECT TOP 1 (CASE WHEN TXAD.desc_zip_xml = @currentDeployedXmlFile THEN @backgroundColorGreen /*ACTUALIZADO*/ ELSE @backgroundColorRed /*DESACTUALIZADO*/ END) FROM TerminalXArchivosDescargados TXAD WHERE TXAD.desc_terminal_id = T.ter_id AND TXAD.desc_zip_imgs IS NULL AND TXAD.desc_apk_archivo IS NULL ORDER BY TXAD.desc_id DESC) ) THEN (SELECT TOP 1 (CASE WHEN TXAD.desc_zip_xml = @currentDeployedXmlFile THEN @backgroundColorGreen /*ACTUALIZADO*/ ELSE @backgroundColorRed /*DESACTUALIZADO*/ END) FROM TerminalXArchivosDescargados TXAD WHERE TXAD.desc_terminal_id = T.ter_id AND TXAD.desc_zip_imgs IS NULL AND TXAD.desc_apk_archivo IS NULL ORDER BY TXAD.desc_id DESC) ELSE @backgroundColorRed END AS configuracion_color,
		--IMG's
		CASE WHEN EXISTS( (SELECT TOP 1 TXAD.desc_zip_imgs FROM TerminalXArchivosDescargados TXAD WHERE TXAD.desc_terminal_id = T.ter_id AND TXAD.desc_zip_xml IS NULL AND TXAD.desc_apk_archivo IS NULL ORDER BY TXAD.desc_id DESC) ) THEN (SELECT TOP 1 TXAD.desc_zip_imgs FROM TerminalXArchivosDescargados TXAD WHERE TXAD.desc_terminal_id = T.ter_id AND TXAD.desc_zip_xml IS NULL AND TXAD.desc_apk_archivo IS NULL ORDER BY TXAD.desc_id DESC) ELSE '-' END AS imagenes,
		CASE WHEN EXISTS( (SELECT TOP 1 (CASE WHEN TXAD.desc_zip_imgs = @currentDeployedImgFile THEN @backgroundColorGreen /*ACTUALIZADO*/ ELSE @backgroundColorRed /*DESACTUALIZADO*/ END) FROM TerminalXArchivosDescargados TXAD WHERE TXAD.desc_terminal_id = T.ter_id AND TXAD.desc_zip_xml IS NULL AND TXAD.desc_apk_archivo IS NULL ORDER BY TXAD.desc_id DESC) ) THEN (SELECT TOP 1 (CASE WHEN TXAD.desc_zip_imgs = @currentDeployedImgFile THEN @backgroundColorGreen /*ACTUALIZADO*/ ELSE @backgroundColorRed /*DESACTUALIZADO*/ END) FROM TerminalXArchivosDescargados TXAD WHERE TXAD.desc_terminal_id = T.ter_id AND TXAD.desc_zip_xml IS NULL AND TXAD.desc_apk_archivo IS NULL ORDER BY TXAD.desc_id DESC) ELSE @backgroundColorRed END AS imagenes_color
		--COMPONENTES 
	FROM TransaccionMDM TMDM
		LEFT OUTER JOIN Terminal T ON (TMDM.tra_terminal_id = T.ter_id)
		LEFT OUTER JOIN Grupo G ON (T.ter_grupo_id = G.gru_id)
	WHERE TMDM.tra_fecha >= @startDate AND TMDM.tra_fecha <= @endDateReport
		AND (T.ter_grupo_id = @groupID OR @groupID = -1) AND G.gru_cliente_id = @customerID AND (T.ter_id = @terminalID OR @terminalID = -1)
		AND UPPER(TMDM.tra_operacion) NOT IN (UPPER('solicitud_llave'), UPPER('notificar_carga_llave'), UPPER('cargar_componente_1'), UPPER('cargar_componente_2'))
	ORDER BY TMDM.tra_fecha DESC
	
END



GO