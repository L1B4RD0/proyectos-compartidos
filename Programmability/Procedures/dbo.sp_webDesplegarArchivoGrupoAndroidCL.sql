﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[sp_webDesplegarArchivoGrupoAndroidCL](
	/*INPUT*/
	@filename VARCHAR(100)
)

 
AS
BEGIN

    SET XACT_ABORT ON
	SET NOCOUNT ON
	DECLARE @startTranCount INT

	--VARIABLES LOCALES DEL SP
    DECLARE @responseCode AS VARCHAR(50)

	BEGIN TRY
	  SELECT @startTranCount = @@TRANCOUNT
	  PRINT @startTranCount
	      
	 BEGIN TRANSACTION;  
       
	   	--INSERTAR NUEVO ARCHIVO CTL DESPLEGADO POR GRUPO
					INSERT INTO GrupoXArchivosDesplegadosCtl(desp_fecha, desp_nombre_archivo, desp_hash)
							VALUES (GETDATE(), @filename, '')

							select @responseCode = 1

     COMMIT TRANSACTION; 
	 	--SELECT FINAL
			SELECT @responseCode AS RESPONSECODE
	 PRINT @startTranCount

	END TRY
	BEGIN CATCH


		IF (XACT_STATE()) = -1  
			BEGIN  
				PRINT 'The transaction is in an uncommittable state.' +  
              ' Rolling back transaction.'  
			ROLLBACK TRANSACTION;  
			PRINT @startTranCount
			 INSERT INTO [dbo].[LogErrorBaseDatos]([bderr_fecha],[bderr_nombre_funcion],[bderr_mensaje_error],[bderr_datos_adicionales])
			  VALUES (GETDATE(), OBJECT_NAME(@@PROCID), ERROR_MESSAGE(), /*Datos Extra, Parámetros del SP*/ 
									'FileName = ' + @filename )

				DECLARE @errMessage AS VARCHAR(4000) = ERROR_MESSAGE()
				DECLARE @errSeverity AS INT = ERROR_SEVERITY()
				DECLARE @errState AS INT = ERROR_STATE()

				RAISERROR (@errMessage, @errSeverity, @errState)
		END;  

		-- Test whether the transaction is active and valid.  
		IF (XACT_STATE()) = 1  
		BEGIN  
			PRINT 'The transaction is committable.' +   
              ' Committing transaction.'  
			COMMIT TRANSACTION;   
			PRINT @startTranCount  
		END;  

	END CATCH


END

GO