﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[sp_webGrabarDatosUsuario](
	@userID INT, /*User ID Logeado*/
	@userName VARCHAR(200),
	@userEmail VARCHAR(100),
	@PerfilUsuario VARCHAR(100)
)
 
AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE Usuario
	SET usu_nombre = @userName, usu_email = @userEmail,
	usu_perfil_id = @PerfilUsuario
	WHERE usu_id = @userID	

END













GO