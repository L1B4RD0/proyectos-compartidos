﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[sp_webExpirarLicenciaXCliente](
	@licenseId INT,
	@customerId INT
)
 
AS
BEGIN
	
	BEGIN TRANSACTION

	BEGIN TRY	
	
		--EXPIRAR LICENCIA
		UPDATE LicenciaXCliente
		SET lic_tipo_estado_id = 2 /*Invalidar Licencia*/
		WHERE lic_id = @licenseId AND lic_cliente_id = @customerId

		COMMIT TRANSACTION

		SELECT 1 /*OK*/

	END TRY
	BEGIN CATCH
		
		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		SELECT 0 /*ERROR*/

	END CATCH;

END


GO