﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[sp_webActualizarUsuario](
	@userName VARCHAR(200),
	@userID VARCHAR(100),
	@userEmail VARCHAR(100),
	@userHash VARCHAR(100),
	@userStatusID INT,
	@Perfil int

)
 
AS
BEGIN

	SET XACT_ABORT, NOCOUNT ON
	DECLARE @startTranCount INT

	--VARIABLES LOCALES DEL SP
	DECLARE @rspCode AS INT

	BEGIN TRY
		SELECT @startTranCount = @@TRANCOUNT

		IF @startTranCount = 0
		BEGIN
			BEGIN TRANSACTION		

			--INICIO CUERPO DEL SP

			IF @userHash = '' 
			BEGIN
				--EDICIÓN SIN NUEVA CLAVE
				UPDATE Usuario
				SET 
					usu_nombre = @userName, 
					usu_email = @userEmail, 
					usu_estado_usuario_id = @userStatusID,
					usu_fecha_actualizacion = GETDATE(),
					usu_perfil_id = @Perfil
				WHERE
					usu_login = @userID		
			END
			ELSE
			BEGIN
				--EDICIÓN CON NUEVA CLAVE
				UPDATE Usuario
				SET 
					usu_nombre = @userName, 
					usu_email = @userEmail, 
					usu_hash = @userHash, 
					usu_estado_usuario_id = @userStatusID,
					usu_fecha_exp_clave = DATEADD(MONTH, -1, GETDATE()),
					usu_fecha_actualizacion = GETDATE(),
					usu_perfil_id = @Perfil
				WHERE
					usu_login = @userID				
			END	

			SELECT @rspCode = 1 /*OK*/

			--FIN CUERPO DEL SP
		END

		IF @startTranCount = 0 
		BEGIN 
			COMMIT TRANSACTION 

			--SELECT FINAL
			SELECT @rspCode AS RSPCODE
		END
	END TRY
	BEGIN CATCH
		IF XACT_STATE() <> 0 AND @startTranCount = 0 
			ROLLBACK TRANSACTION

		INSERT INTO [dbo].[LogErrorBaseDatos]([bderr_fecha],[bderr_nombre_funcion],[bderr_mensaje_error],[bderr_datos_adicionales])
			 VALUES (GETDATE(), OBJECT_NAME(@@PROCID), ERROR_MESSAGE(), /*Datos Extra, Parámetros del SP*/ 
																		'userName = ' + @userName + ', userID = ' + @userID + ', userEmail = ' + @userEmail + 
																		', userHash = ' + @userHash + ', userStatusID = ' + CAST(@userStatusID AS VARCHAR(10)))

		DECLARE @errMessage AS VARCHAR(4000) = ERROR_MESSAGE()
		DECLARE @errSeverity AS INT = ERROR_SEVERITY()
		DECLARE @errState AS INT = ERROR_STATE()

		RAISERROR (@errMessage, @errSeverity, @errState)
	END CATCH

END
GO