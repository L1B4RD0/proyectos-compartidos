﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[sp_webReporteTerminales](
	@groupID AS BIGINT,
	@terminalID AS BIGINT,
	@customerID AS BIGINT
)
 
AS
BEGIN

	SET NOCOUNT ON;

	SELECT 
		T.ter_fecha_creacion,
		T.ter_fecha_consulta_sp as fecha_consulta,
		T.ter_aplicaciones_instaladas as aplicaciones_instaladas,
		T.ter_aplicaciones_pendientes_sp as aplicaciones_pendientes,
		T.Ter_latitud_gps AS latitud_gps,
		T.ter_longitud_gps AS longitud_gps,
		G.gru_nombre,
		T.ter_serial,
		T.ter_mac,
		T.ter_UID,
		T.ter_fecha_ultima_inicializacion,
		CASE WHEN T.ter_flag_inicializacion = 1 THEN 'Activo' 
		WHEN ter_flag_inicializacion = 0 THEN 'Inactivo' END as ter_flag_inicializacion,
		T.ter_imei,
		ISNULL(MT.descripcion,'') AS ter_marca_terminal,
		ISNULL(MoT.descripcion, '') AS ter_tipo_terminal,
		T.ter_serial_sim,
		EA.descripcion AS ter_estado_actualizacion,
		T.ter_mensaje_actualizacion,
		(CASE WHEN T.ter_tipo_estado_id = 1 THEN 1 ELSE 0 END) AS ter_estado,
		T.ter_prioridad_grupo,
		T.ter_actualizar,
		(CASE WHEN T.ter_prioridad_grupo = 0 THEN 
		   CONVERT(VARCHAR, T.ter_fecha_programada_actualizacion, 111) + ' ' + SUBSTRING(CONVERT(VARCHAR, T.ter_fecha_programada_actualizacion, 108), 1, 5)
		 ELSE
		   (CASE WHEN LEN(RTRIM(T.ter_fecha_descarga_sp)) = 14 THEN 
			  CONCAT(SUBSTRING(T.ter_fecha_descarga_sp, 1, 4), 
			                         '/', 
					 SUBSTRING(T.ter_fecha_descarga_sp, 5, 2), 
			                         '/', 
					 SUBSTRING(T.ter_fecha_descarga_sp, 7, 2), 
											' ', 
					 SUBSTRING(T.ter_fecha_descarga_sp, 9, 2),
											':',
		             SUBSTRING(T.ter_fecha_descarga_sp, 11, 2),
											':',
					 SUBSTRING(T.ter_fecha_descarga_sp, 13, 2))
		    ELSE
			  T.ter_fecha_descarga_sp
            END)
		 END) as ter_fecha_programacion,
		 (CASE WHEN T.ter_prioridad_grupo = 0 THEN 
		   (CASE WHEN T.ter_ip_descarga = '' THEN '' ELSE T.ter_ip_descarga + ':' + T.ter_puerto_descarga END)
		 ELSE
		   (CASE WHEN T.ter_ip_descarga_sp = '' THEN '' ELSE T.ter_ip_descarga_sp + ':' + T.ter_puerto_descarga_sp END)
		 END) as ter_datos_descarga
	FROM TERMINAL T
		INNER JOIN Grupo G ON (T.ter_grupo_id = G.gru_id)
		INNER JOIN MarcaTerminal MT ON (t.ter_marca_terminal_id = MT.id)
		INNER JOIN ModeloTerminal MoT ON (t.ter_modelo_terminal_id = MoT.id)
		INNER JOIN EstadoActualizacion EA ON (T.ter_estado_actualizacion_id = EA.id)	
	WHERE (T.ter_grupo_id = @groupID OR @groupID = -1) AND G.gru_cliente_id = @customerID AND (T.ter_id = @terminalID OR @terminalID = -1)
	ORDER BY T.ter_fecha_creacion

END




GO