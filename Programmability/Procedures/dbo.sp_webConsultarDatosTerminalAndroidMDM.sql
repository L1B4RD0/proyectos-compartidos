﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

-- =============================================
-- Autor:				Elkin Beltrán
-- Fecha Creación:		01/Feb/2018
-- Histórico Cambios:
------------------------------------------------
--	v1.0	Versión Inicial
--	v1.1	Se Agrega el retorno del campo 
--			ter_recargar_llave
------------------------------------------------
-- Campos de Respuesta:		SingleRow
-- RESPONSECODE		INT
-- GROUPNAME		VARCHAR
-- LOGIN			VARCHAR
-- PASS				VARCHAR
-- MESSAGE			VARCHAR
-- LOCKTERMINAL		VARCHAR
-- LOCKPASSWORD		VARCHAR
-- DEVICEID			VARCHAR
-- QUERIES			VARCHAR
-- FREQUENCY		VARCHAR
-- ALLOWEDAPPS		VARCHAR
-- APKFILE			VARCHAR
-- APKPACKAGE		VARCHAR
-- APKVERSION		VARCHAR
-- XMLFILE			VARCHAR
-- IMGFILE			VARCHAR
-- =============================================

CREATE PROCEDURE [dbo].[sp_webConsultarDatosTerminalAndroidMDM](
	@login VARCHAR(50),
	@pass VARCHAR(50),
	@ram VARCHAR(100),
	@battery VARCHAR(50),
	@latitude VARCHAR(50),
	@longitude VARCHAR(50),
	@installedApps VARCHAR(50),
	@groupName VARCHAR(50),
	@deviceId VARCHAR(50),
	@deviceIP VARCHAR(50),
	--WS retieve Data Fields
	@heraclesID VARCHAR(50),
	@heraclesName VARCHAR(200),
	@heraclesCity VARCHAR(200),
	@heraclesRegion VARCHAR(200),
	@heraclesAgency VARCHAR(200)
)
 
AS
BEGIN

	SET XACT_ABORT, NOCOUNT ON
	DECLARE @startTranCount INT

	--VARIABLES LOCALES DEL SP
	DECLARE @responseCode AS INT
	DECLARE @terminalId AS BIGINT
	DECLARE @groupId AS BIGINT
	DECLARE @groupNameDB AS VARCHAR(50) = ''
	DECLARE @groupLoginDB AS VARCHAR(50) = ''
	DECLARE @groupPassDB AS VARCHAR(50) = ''
	DECLARE @messageDB AS VARCHAR(250) = ''
	DECLARE @blockTerminalDB AS VARCHAR(50) = ''
	DECLARE @lockPasswordDB AS VARCHAR(50) = ''
	DECLARE @deviceIdDB AS VARCHAR(50) = ''
	DECLARE @queriesDB AS VARCHAR(50) = ''
	DECLARE @frequencyDB AS VARCHAR(50) = ''
	DECLARE @allowedApps AS VARCHAR(500) = ''
	DECLARE @kioskoApp AS VARCHAR(500) = ''

	DECLARE @currentDeployedTerminalXmlFile AS VARCHAR(100)
	--DECLARE @configBaseTerminalFileName AS VARCHAR(100) = 'configuracion_terminal_base.zip'

	DECLARE @currentDeployedAPKFileName AS VARCHAR(100) = ''
	DECLARE @currentDeployedAPKPackageName AS VARCHAR(100) = ''
	DECLARE @currentDeployedAPKFileVersion AS VARCHAR(100) = ''
	DECLARE @currentDeployedXmlFile AS VARCHAR(100) = ''
	DECLARE @currentDeployedImgFile AS VARCHAR(100) = ''
	DECLARE @configBaseFileName AS VARCHAR(100) = 'configuracion_base.zip'
	DECLARE @imagesBaseFileName AS VARCHAR(100) = 'imagenes_base.zip'
	DECLARE @reloadKeyDB AS VARCHAR(50) = ''

	BEGIN TRY
		SELECT @startTranCount = @@TRANCOUNT

		IF @startTranCount = 0
		BEGIN
			BEGIN TRANSACTION
			--INICIO CUERPO DEL SP
	
			--VALIDAR DISPOSITIVO
			IF EXISTS(SELECT * FROM Terminal WHERE ter_serial = @deviceId)
			BEGIN

				--OBTENER VALORES DE GRUPO Y TERMINAL
				SELECT 
					@terminalId = T.ter_id,
					@groupId = G.gru_id, 
					@groupNameDB = G.gru_nombre, 
					@groupLoginDB = G.gru_default_login, 
					@groupPassDB = G.gru_default_password,
					@messageDB = T.ter_mensaje_desplegar,
					@blockTerminalDB = (CASE WHEN T.ter_bloqueo = 1 THEN '1' ELSE '0' END),
					@lockPasswordDB = T.ter_clave_bloqueo,
					@deviceIdDB = T.ter_serial,
					@queriesDB = G.gru_numero_consultas,
					@frequencyDB = G.gru_frecuencia_horas,
					@allowedApps = T.ter_aplicaciones_permitidas,					
					@reloadKeyDB = (CASE WHEN T.ter_recargar_llave = 1 THEN '1' ELSE '0' END),
					@kioskoApp = T.ter_aplicacion_kiosko
				FROM Terminal T INNER JOIN Grupo G ON (T.ter_grupo_id = G.gru_id) WHERE T.ter_serial = @deviceId

				--OBTENER VALORES DE DESPLIEGUE DE ARCHIVOS APK's, XML's E IMG'S
				SELECT TOP 1 @currentDeployedAPKFileName = ISNULL(GXAD.desp_nombre_archivo ,''), @currentDeployedAPKPackageName = ISNULL(GXAD.desp_nombre_paquete ,''), @currentDeployedAPKFileVersion = ISNULL(GXAD.desp_version ,'') FROM GrupoXArchivosDesplegados GXAD WHERE GXAD.desp_grupo_id = @groupId AND GXAD.desp_tipo_despliegue_id = 1 ORDER BY GXAD.desp_id DESC	/*APK's*/
				SELECT TOP 1 @currentDeployedXmlFile = ISNULL(GXAD.desp_nombre_archivo ,'') FROM GrupoXArchivosDesplegados GXAD WHERE GXAD.desp_grupo_id = @groupId AND GXAD.desp_tipo_despliegue_id = 2 ORDER BY GXAD.desp_id DESC	/*XML's*/
				SELECT TOP 1 @currentDeployedTerminalXmlFile = ISNULL(TXAD.desp_nombre_archivo ,'') FROM TerminalesXArchivosDesplegados TXAD WHERE TXAD.desp_terminal_id = @terminalId AND TXAD.desp_tipo_despliegue_id = 2 ORDER BY TXAD.desp_id DESC	/*XML's*/
				SELECT TOP 1 @currentDeployedImgFile = ISNULL(GXAD.desp_nombre_archivo ,'') FROM GrupoXArchivosDesplegados GXAD WHERE GXAD.desp_grupo_id = @groupId AND GXAD.desp_tipo_despliegue_id = 3 ORDER BY GXAD.desp_id DESC	/*IMG's*/

				--VALIDAR DESCARGAS DE ARCHIVOS BASE DE CONFIGURACION E IMAGENES
				SELECT @currentDeployedXmlFile = (CASE WHEN COUNT(*) = 0 THEN @configBaseFileName ELSE @currentDeployedXmlFile END) FROM TerminalXArchivosDescargados WHERE desc_terminal_id = @terminalId AND desc_zip_xml = @configBaseFileName
				--SELECT @currentDeployedTerminalXmlFile = (CASE WHEN COUNT(*) = 0 THEN @configBaseTerminalFileName ELSE @currentDeployedTerminalXmlFile END) FROM TerminalXArchivosDescargados WHERE desc_terminal_id = @terminalId AND desc_zip_xml = @configBaseTerminalFileName
				SELECT @currentDeployedImgFile = (CASE WHEN COUNT(*) = 0 THEN @imagesBaseFileName ELSE @currentDeployedImgFile END) FROM TerminalXArchivosDescargados WHERE desc_terminal_id = @terminalId AND desc_zip_imgs = @imagesBaseFileName

				--ACTUALIZAR DATOS ENVIADOS POR EL DISPOSITIVO
				UPDATE TERMINAL
				SET 
					ter_ram = @ram,
					ter_nivel_bateria = @battery,
					ter_latitud_gps = (CASE WHEN @latitude = '0' Or @latitude = '' Or @latitude IS NULL THEN ter_latitud_gps ELSE @latitude END),
					ter_longitud_gps = (CASE WHEN @longitude = '0' Or @longitude = '' THEN ter_longitud_gps ELSE @longitude END),
					ter_aplicaciones_instaladas = @installedApps,
					ter_ip_dispositivo = @deviceIP,
					--ACTUALIZAR PARÁMETROS POR DEFECTO PARA CADA LLAMADA MDM DEL DISPOSITIVO
					ter_fecha_actualizacion_auto = GETDATE(),
					ter_bloqueo = 0,
					ter_tipo_estado_id = 1,
					ter_recargar_llave = 0,
					--ACTUALIZAR DATOS HERACLES RECIBIDOS POR WEB SERVICE
					ter_id_heracles = (CASE WHEN LEN(@heraclesID) > 0 THEN @heraclesID ELSE ter_id_heracles END),
					ter_nombre_heracles = (CASE WHEN LEN(@heraclesName) > 0 THEN @heraclesName ELSE ter_nombre_heracles END),
					ter_ciudad_heracles = (CASE WHEN LEN(@heraclesCity) > 0 THEN @heraclesCity ELSE ter_ciudad_heracles END),
					ter_region_heracles = (CASE WHEN LEN(@heraclesRegion) > 0 THEN @heraclesRegion ELSE ter_region_heracles END),
					ter_agencia_heracles = (CASE WHEN LEN(@heraclesAgency) > 0 THEN @heraclesAgency ELSE ter_agencia_heracles END)
				WHERE ter_serial = @deviceId

				--DISPOSITIVO EXISTE
				SELECT @responseCode = 1
			END
			ELSE
			BEGIN
				SELECT @responseCode = 0 --DISPOSITIVO NO EXISTE
			END
			
			--FIN CUERPO DEL SP
		END

		IF @startTranCount = 0 
		BEGIN 
			COMMIT TRANSACTION

			--SELECT FINAL
			SELECT	@responseCode AS RESPONSECODE, 
					ISNULL(@groupNameDB,'') AS GROUPNAME,
					ISNULL(@groupLoginDB,'') AS [LOGIN],
					ISNULL(@groupPassDB,'') AS PASS,
					ISNULL(@messageDB,'') AS [MESSAGE],
					ISNULL(@blockTerminalDB,'') AS LOCKTERMINAL,
					ISNULL(@lockPasswordDB,'') AS LOCKPASSWORD,
					ISNULL(@deviceIdDB,'') AS DEVICEID,
					ISNULL(@queriesDB,'') AS QUERIES,
					ISNULL(@frequencyDB,'') AS FREQUENCY,
					ISNULL(@allowedApps,'') AS ALLOWEDAPPS,					
					ISNULL(@currentDeployedAPKFileName,'') AS APKFILE,
					ISNULL(@currentDeployedAPKPackageName,'') AS APKPACKAGE,
					ISNULL(@currentDeployedAPKFileVersion,'') AS APKVERSION,
					ISNULL(@currentDeployedXmlFile,'') AS XMLFILE,
					ISNULL(@currentDeployedTerminalXmlFile,'') AS XMLTERMINALFILE,
					ISNULL(@currentDeployedImgFile,'') AS IMGFILE,
					ISNULL(@reloadKeyDB,'') AS RELOADKEY,
					ISNULL(@kioskoApp,'') AS KIOSKOAPP

		END
	END TRY
	BEGIN CATCH
		IF XACT_STATE() <> 0 AND @startTranCount = 0 
			ROLLBACK TRANSACTION

		INSERT INTO [dbo].[LogErrorBaseDatos]([bderr_fecha],[bderr_nombre_funcion],[bderr_mensaje_error],[bderr_datos_adicionales])
			 VALUES (GETDATE(), OBJECT_NAME(@@PROCID), ERROR_MESSAGE(), /*Datos Extra, Parámetros del SP*/ 
																		'login = ' + @login + ', pass = ' + @pass + ', ram = ' + @ram + ', battery = ' + @battery + ', latitude = ' + @latitude + 
																		', longitude = ' + @longitude + ', installedApps = ' + @installedApps + ', groupName = ' + @groupName + ', deviceId = ' + @deviceId +
																		', deviceIP = ' + @deviceIP + ', heraclesID  = ' + @heraclesID  + ', heraclesName  = ' + @heraclesName  + ', @heraclesCity = ' + @heraclesCity  + 
																		', heraclesRegion  = ' + @heraclesRegion  + ', heraclesAgency  = ' + @heraclesAgency )

		DECLARE @errMessage AS VARCHAR(4000) = ERROR_MESSAGE()
		DECLARE @errSeverity AS INT = ERROR_SEVERITY()
		DECLARE @errState AS INT = ERROR_STATE()

		RAISERROR (@errMessage, @errSeverity, @errState)
	END CATCH	

END


GO