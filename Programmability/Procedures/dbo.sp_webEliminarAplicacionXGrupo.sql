﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[sp_webEliminarAplicacionXGrupo](
	@applicationId INT,
	@groupId INT
)
 
AS
BEGIN
	
	BEGIN TRANSACTION

	BEGIN TRY	
	
		--ELIMINAR ASOCIACIÓN GRUPOXAPLICACIÓN
		DELETE FROM GrupoXAplicacion 
		WHERE grupo_id = @groupId AND aplicacion_id = @applicationId

		--ELIMINAR APLICACION
		DELETE FROM Aplicacion
		WHERE apl_id = @applicationId

		COMMIT TRANSACTION

		SELECT 1 /*OK*/

	END TRY
	BEGIN CATCH
		
		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		SELECT 0 /*ERROR*/

	END CATCH;

END













GO