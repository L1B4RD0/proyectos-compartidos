﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

	CREATE PROCEDURE [dbo].[sp_webConsultarTerminalModel](
	@ID AS BIGINT
)
 
AS
BEGIN
 DECLARE @RESULT AS VARCHAR(50)
	SET NOCOUNT ON;


	SELECT @RESULT = descripcion
	FROM [dbo].[ModeloTerminal] 
	WHERE id = @ID
	
	SELECT @RESULT

END
GO