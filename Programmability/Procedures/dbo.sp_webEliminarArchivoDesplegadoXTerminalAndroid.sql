﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[sp_webEliminarArchivoDesplegadoXTerminalAndroid](
	@deployedFileId INT,
	@terminalId INT
)
 
AS
BEGIN
	
	BEGIN TRANSACTION

	BEGIN TRY	
	
		--ELIMINAR APLICACION
		DELETE FROM [dbo].[TerminalesXArchivosDesplegados]
		WHERE desp_id = @deployedFileId AND desp_terminal_id = @terminalId

		COMMIT TRANSACTION

		SELECT 1 /*OK*/

	END TRY
	BEGIN CATCH
		
		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		SELECT 0 /*ERROR*/

	END CATCH;

END

GO