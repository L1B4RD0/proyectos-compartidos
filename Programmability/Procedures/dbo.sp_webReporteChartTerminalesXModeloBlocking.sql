﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[sp_webReporteChartTerminalesXModeloBlocking](
	@customerID AS BIGINT,
	@groupID AS BIGINT
)
 
AS
BEGIN

	SET NOCOUNT ON;

	SELECT 
		 MT.descripcion AS terminal_tipo, 
		 CAST(COUNT(*) AS VARCHAR(10)) AS ter_totales
	FROM Terminal T
		INNER JOIN Grupo G ON (T.ter_grupo_id = G.gru_id)
		INNER JOIN ModeloTerminal MT ON (T.ter_modelo_terminal_id = MT.id)
	WHERE (G.gru_id = @groupID OR @groupID = -1) AND G.gru_cliente_id = @customerID AND ter_blocking_mode = 1
		GROUP BY MT.descripcion
END


GO