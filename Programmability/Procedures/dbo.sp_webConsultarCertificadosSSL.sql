﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[sp_webConsultarCertificadosSSL](
	@groupId BIGINT
)
 
AS
BEGIN

	SET NOCOUNT ON;

		SELECT 
    C.apl_id, C.apl_descripcion,
	DATALENGTH(C.apl_data) AS apl_longitud 
	FROM [dbo].[Certificado] C
	ORDER BY C.apl_id

END
GO