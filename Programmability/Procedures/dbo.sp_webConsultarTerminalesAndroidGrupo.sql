﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[sp_webConsultarTerminalesAndroidGrupo](
	@groupId BIGINT,
	@terminalHeraclesId VARCHAR(20),
	@terminalIPAddress VARCHAR(20),
	@terminalName VARCHAR(20),
	@terminalSerial VARCHAR(20)
)
 
AS
BEGIN

	SET NOCOUNT ON;
	
	--VARIABLES LOCALES DEL SP
	DECLARE @defaultPackageName AS VARCHAR(200) = ''; SELECT @defaultPackageName = param_valor FROM ParametroGeneral WHERE param_nombre = 'PackageNameNeoCNB';
	DECLARE @backgroundColorGreen AS VARCHAR(10) = '#dff0d8'
	DECLARE @backgroundColorRed AS VARCHAR(10) = '#e47a7a'
	DECLARE @currentDeployedAPKFileMaxVersion AS VARCHAR(100) = '-'
	DECLARE @currentDeployedXmlFile AS VARCHAR(100) = '-'
	DECLARE @currentDeployedImgFile AS VARCHAR(100) = '-'

	--OBTENER VALORES DE DESPLIEGUE DE ARCHIVOS APK's, XML's E IMG'S
	SELECT TOP 1 @currentDeployedAPKFileMaxVersion = ISNULL(GXAD.desp_version ,'-') FROM GrupoXArchivosDesplegados GXAD WHERE GXAD.desp_grupo_id = @groupId AND GXAD.desp_tipo_despliegue_id = 1 AND GXAD.desp_nombre_paquete = @defaultPackageName ORDER BY GXAD.desp_id DESC	/*APK's*/
	SELECT TOP 1 @currentDeployedXmlFile = ISNULL(GXAD.desp_nombre_archivo ,'-') FROM GrupoXArchivosDesplegados GXAD WHERE GXAD.desp_grupo_id = @groupId AND GXAD.desp_tipo_despliegue_id = 2 ORDER BY GXAD.desp_id DESC	/*XML's*/
	SELECT TOP 1 @currentDeployedImgFile = ISNULL(GXAD.desp_nombre_archivo ,'-') FROM GrupoXArchivosDesplegados GXAD WHERE GXAD.desp_grupo_id = @groupId AND GXAD.desp_tipo_despliegue_id = 3 ORDER BY GXAD.desp_id DESC	/*IMG's*/

	SELECT 
		T.ter_id, 
		T.ter_serial + ' - ' + ISNULL(T.ter_id_heracles, '') AS ter_dispositivo_id, 
		T.ter_fecha_actualizacion_auto,
		ISNULL(T.ter_ip_dispositivo, '') AS ter_ip_dispositivo,
		ISNULL(T.ter_nombre_heracles, '') AS ter_nombre_heracles,
		ISNULL(T.ter_ciudad_heracles, '') AS ter_ciudad_heracles,
		ISNULL(T.ter_region_heracles, '') AS ter_region_heracles,
		ISNULL(T.ter_agencia_heracles, '') AS ter_agencia_heracles,
		--APK's
		CASE WHEN EXISTS( (SELECT TOP 1 TXAD.desc_apk_version FROM TerminalXArchivosDescargados TXAD WHERE TXAD.desc_terminal_id = T.ter_id AND TXAD.desc_zip_imgs IS NULL AND TXAD.desc_zip_xml IS NULL AND TXAD.desc_apk_paquete = @defaultPackageName ORDER BY TXAD.desc_id DESC) ) THEN @defaultPackageName + ' v' + (SELECT TOP 1 TXAD.desc_apk_version FROM TerminalXArchivosDescargados TXAD WHERE TXAD.desc_terminal_id = T.ter_id AND TXAD.desc_zip_imgs IS NULL AND TXAD.desc_zip_xml IS NULL AND TXAD.desc_apk_paquete = @defaultPackageName ORDER BY TXAD.desc_id DESC) ELSE '-' END AS apk_banco,
		CASE WHEN EXISTS( (SELECT TOP 1 TXAD.desc_apk_version FROM TerminalXArchivosDescargados TXAD WHERE TXAD.desc_terminal_id = T.ter_id AND TXAD.desc_zip_imgs IS NULL AND TXAD.desc_zip_xml IS NULL AND TXAD.desc_apk_paquete = @defaultPackageName ORDER BY TXAD.desc_id DESC) ) THEN (CASE WHEN @currentDeployedAPKFileMaxVersion = (SELECT TOP 1 TXAD.desc_apk_version FROM TerminalXArchivosDescargados TXAD WHERE TXAD.desc_terminal_id = T.ter_id AND TXAD.desc_zip_imgs IS NULL AND TXAD.desc_zip_xml IS NULL AND TXAD.desc_apk_paquete = @defaultPackageName ORDER BY TXAD.desc_id DESC) THEN @backgroundColorGreen ELSE @backgroundColorRed END) ELSE @backgroundColorRed END AS apk_banco_color,
		--XML's
		CASE WHEN EXISTS( (SELECT TOP 1 TXAD.desc_zip_xml FROM TerminalXArchivosDescargados TXAD WHERE TXAD.desc_terminal_id = T.ter_id AND TXAD.desc_zip_imgs IS NULL AND TXAD.desc_apk_archivo IS NULL ORDER BY TXAD.desc_id DESC) ) THEN (SELECT TOP 1 TXAD.desc_zip_xml FROM TerminalXArchivosDescargados TXAD WHERE TXAD.desc_terminal_id = T.ter_id AND TXAD.desc_zip_imgs IS NULL AND TXAD.desc_apk_archivo IS NULL ORDER BY TXAD.desc_id DESC) ELSE '-' END AS configuracion,
		CASE WHEN EXISTS( (SELECT TOP 1 (CASE WHEN TXAD.desc_zip_xml = @currentDeployedXmlFile THEN @backgroundColorGreen /*ACTUALIZADO*/ ELSE @backgroundColorRed /*DESACTUALIZADO*/ END) FROM TerminalXArchivosDescargados TXAD WHERE TXAD.desc_terminal_id = T.ter_id AND TXAD.desc_zip_imgs IS NULL AND TXAD.desc_apk_archivo IS NULL ORDER BY TXAD.desc_id DESC) ) THEN (SELECT TOP 1 (CASE WHEN TXAD.desc_zip_xml = @currentDeployedXmlFile THEN @backgroundColorGreen /*ACTUALIZADO*/ ELSE @backgroundColorRed /*DESACTUALIZADO*/ END) FROM TerminalXArchivosDescargados TXAD WHERE TXAD.desc_terminal_id = T.ter_id AND TXAD.desc_zip_imgs IS NULL AND TXAD.desc_apk_archivo IS NULL ORDER BY TXAD.desc_id DESC) ELSE @backgroundColorRed END AS configuracion_color,
		--IMG's
		CASE WHEN EXISTS( (SELECT TOP 1 TXAD.desc_zip_imgs FROM TerminalXArchivosDescargados TXAD WHERE TXAD.desc_terminal_id = T.ter_id AND TXAD.desc_zip_xml IS NULL AND TXAD.desc_apk_archivo IS NULL ORDER BY TXAD.desc_id DESC) ) THEN (SELECT TOP 1 TXAD.desc_zip_imgs FROM TerminalXArchivosDescargados TXAD WHERE TXAD.desc_terminal_id = T.ter_id AND TXAD.desc_zip_xml IS NULL AND TXAD.desc_apk_archivo IS NULL ORDER BY TXAD.desc_id DESC) ELSE '-' END AS imagenes,
		CASE WHEN EXISTS( (SELECT TOP 1 (CASE WHEN TXAD.desc_zip_imgs = @currentDeployedImgFile THEN @backgroundColorGreen /*ACTUALIZADO*/ ELSE @backgroundColorRed /*DESACTUALIZADO*/ END) FROM TerminalXArchivosDescargados TXAD WHERE TXAD.desc_terminal_id = T.ter_id AND TXAD.desc_zip_xml IS NULL AND TXAD.desc_apk_archivo IS NULL ORDER BY TXAD.desc_id DESC) ) THEN (SELECT TOP 1 (CASE WHEN TXAD.desc_zip_imgs = @currentDeployedImgFile THEN @backgroundColorGreen /*ACTUALIZADO*/ ELSE @backgroundColorRed /*DESACTUALIZADO*/ END) FROM TerminalXArchivosDescargados TXAD WHERE TXAD.desc_terminal_id = T.ter_id AND TXAD.desc_zip_xml IS NULL AND TXAD.desc_apk_archivo IS NULL ORDER BY TXAD.desc_id DESC) ELSE @backgroundColorRed END AS imagenes_color,
		(CASE WHEN T.ter_tipo_estado_id = 1 THEN '../img/icons/16x16/android_on.png' ELSE '../img/icons/16x16/android_off.png' END) AS ter_logo,
		--COMPONENTES
		(CASE WHEN T.ter_componente_1_inyectado = 1 THEN '../img/icons/16x16/component1_on.png' ELSE '../img/icons/16x16/component1_off.png' END) AS ter_logo_estado_componente1,
		CONVERT(VARCHAR, T.ter_fecha_componente1, 103) + ' ' + CONVERT(VARCHAR, T.ter_fecha_componente1, 108) AS ter_desc_estado_componente1,
		(CASE WHEN T.ter_componente_2_inyectado = 1 THEN '../img/icons/16x16/component2_on.png' ELSE '../img/icons/16x16/component2_off.png' END) AS ter_logo_estado_componente2,
		CONVERT(VARCHAR, T.ter_fecha_componente2, 103) + ' ' + CONVERT(VARCHAR, T.ter_fecha_componente2, 108) AS ter_desc_estado_componente2
	FROM Terminal T 
	WHERE T.ter_grupo_id = @groupId AND
		(T.ter_id_heracles LIKE '%' + @terminalHeraclesId + '%' OR @terminalHeraclesId = '-1')
		AND (T.ter_ip_dispositivo LIKE '%' + @terminalIPAddress + '%' OR @terminalIPAddress = '-1')
		AND (T.ter_nombre_heracles LIKE '%' + @terminalName + '%' OR @terminalName = '-1')
		AND (T.ter_serial LIKE '%' + @terminalSerial + '%' OR @terminalSerial = '-1')
	ORDER BY T.ter_id 

END



GO