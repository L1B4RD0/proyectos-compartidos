﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[sp_webEliminarGrupo](
	@groupId INT
)
 
AS
BEGIN
	
	--VARIABLES LOCALES DEL SP	
	DECLARE @CounterTerminals INT
	DECLARE @CounterAppsPOS INT
	DECLARE @CounterDeployedFiles INT

	BEGIN TRANSACTION

	BEGIN TRY	
	
		SELECT @CounterTerminals = COUNT(*) FROM Terminal T WHERE T.ter_grupo_id = @groupId
		SELECT @CounterAppsPOS = COUNT(*) FROM GrupoXAplicacion GXA WHERE GXA.grupo_id = @groupId
		SELECT @CounterDeployedFiles = COUNT(*) FROM GrupoXArchivosDesplegados GXAD WHERE GXAD.desp_grupo_id = @groupId

		--ELIMINAR GRUPO SI NO TIENE TERMINALES, NI APLICACIONES, NI ARCHIVOS DESPLEGADOS ASOCIADOS
		IF @CounterTerminals > 0 
		BEGIN
			SELECT 2 /*ERROR*/
		END
		ELSE IF @CounterAppsPOS > 0 
		BEGIN
			SELECT 3 /*ERROR*/
		END
		ELSE IF @CounterDeployedFiles > 0 
		BEGIN
			SELECT 4 /*ERROR*/
		END
		ELSE
		BEGIN
			DELETE FROM Grupo WHERE gru_id = @groupId

			COMMIT TRANSACTION

			SELECT 1 /*OK*/

		END		

	END TRY
	BEGIN CATCH
		
		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		SELECT 5 /*ERROR*/

	END CATCH;

END



GO