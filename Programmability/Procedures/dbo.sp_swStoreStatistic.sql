﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[sp_swStoreStatistic](

	@date VARCHAR(50),
	@tipo_id BIGINT,
	@event VARCHAR(50),
	@valor INT,
	@cli_id BIGINT,
	@gru_id BIGINT,
	@ter_id BIGINT,
	@rspCode VARCHAR(50) OUTPUT
)
 
AS
BEGIN

    SET XACT_ABORT ON
	SET NOCOUNT ON
	

	--VARIABLES LOCALES DEL SP
	DECLARE @startTranCount INT
	DECLARE @count INT

	BEGIN TRY
	  SELECT @startTranCount = @@TRANCOUNT
	  BEGIN TRANSACTION;

	  --VERIFICAMOS SI YA EXISTE LA ESTADISTICA
	 SELECT 
      @count = count(*)
        FROM [dbo].[ClientesEstadisticas] where
	  [cli_id] = @cli_id
        and [gru_id] = @gru_id
        and [ter_id] = @ter_id
        and [tipo_id] = @tipo_id
        and [fecha] = convert(datetime, @date, 112)
	  IF @event = 'I'
	  BEGIN
	  	   IF @count = 0 
	        BEGIN
	          PRINT 'No hay registro'
			  INSERT INTO [dbo].[ClientesEstadisticas]
				([cli_id]
				,[gru_id]
				,[ter_id]
				,[tipo_id]
				,[fecha]
				,[valor])
			  VALUES
				(@cli_id
				,@gru_id
				,@ter_id
				,@tipo_id
				,convert(datetime, @date, 112)
				,@valor)

	        END
	      ELSE
	      BEGIN
	         PRINT 'Si hay registro'
			 UPDATE [dbo].[ClientesEstadisticas]
				SET [valor] = [valor] + @valor
			WHERE [cli_id] = @cli_id
				 and [gru_id] = @gru_id
				 and [ter_id] = @ter_id
				 and [tipo_id] = @tipo_id
				 and [fecha] = convert(datetime, @date, 112)
	      END  
	  END
	  ELSE IF @event = 'U'
	  	  IF @count = 0 
	        BEGIN
	          PRINT 'No hay registro'
			  INSERT INTO [dbo].[ClientesEstadisticas]
				([cli_id]
				,[gru_id]
				,[ter_id]
				,[tipo_id]
				,[fecha]
				,[valor])
			  VALUES
				(@cli_id
				,@gru_id
				,@ter_id
				,@tipo_id
				,convert(datetime, @date, 112)
				,@valor)

	        END
	      ELSE
	      BEGIN
	         PRINT 'Si hay registro'
			 UPDATE [dbo].[ClientesEstadisticas]
				SET [valor] = @valor
			WHERE [cli_id] = @cli_id
				 and [gru_id] = @gru_id
				 and [ter_id] = @ter_id
				 and [tipo_id] = @tipo_id
				 and [fecha] = convert(datetime, @date, 112)
	      END 	  
	  COMMIT TRANSACTION; 


	      
	 --BEGIN TRANSACTION;  
        -- A FOREIGN KEY constraint exists on this table. This   
        -- statement will generate a constraint violation error.  
      --delete from grupo where gru_id = '10094';

     -- If the delete operation succeeds, commit the transaction. The CATCH  
     -- block will not execute.  
     --COMMIT TRANSACTION; 

	 PRINT @startTranCount
	 set @rspCode = 0;

	END TRY
	BEGIN CATCH

		-- Test XACT_STATE for 0, 1, or -1.  
		-- If 1, the transaction is committable.  
		-- If -1, the transaction is uncommittable and should   
		--     be rolled back.  
		-- XACT_STATE = 0 means there is no transaction and  
		--     a commit or rollback operation would generate an error.  

		-- Test whether the transaction is uncommittable.  
		IF (XACT_STATE()) = -1 
		   set @rspCode = 5; 
			BEGIN  
				PRINT 'The transaction is in an uncommittable state.' +  
              ' Rolling back transaction.'  
			ROLLBACK TRANSACTION;  
			PRINT @startTranCount
			 INSERT INTO [dbo].[LogErrorBaseDatos]([bderr_fecha],[bderr_nombre_funcion],[bderr_mensaje_error],[bderr_datos_adicionales])
			  VALUES (GETDATE(), OBJECT_NAME(@@PROCID), ERROR_MESSAGE(), 
									 ', ter_id = ' + @ter_id + ', cli_id' + @cli_id)

				DECLARE @errMessage AS VARCHAR(4000) = ERROR_MESSAGE()
				DECLARE @errSeverity AS INT = ERROR_SEVERITY()
				DECLARE @errState AS INT = ERROR_STATE()

				RAISERROR (@errMessage, @errSeverity, @errState)
		END;  

		-- Test whether the transaction is active and valid.  
		IF (XACT_STATE()) = 1  
		BEGIN  
			PRINT 'The transaction is committable.' +   
              ' Committing transaction.'  
			COMMIT TRANSACTION;   
			PRINT @startTranCount  
		END;  

	END CATCH


END

GO