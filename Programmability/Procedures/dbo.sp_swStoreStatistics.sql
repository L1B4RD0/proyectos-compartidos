﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[sp_swStoreStatistics](


	@statistics VARCHAR(5000),
	@cli_id BIGINT,
	@gru_id BIGINT,
	@ter_id BIGINT,
	@rspCode VARCHAR(50) OUTPUT
)
 
AS
BEGIN

    SET XACT_ABORT ON
	SET NOCOUNT ON
    
	--VARIABLES LOCALES DEL SP
	DECLARE @startTranCount INT
	DECLARE @count INT	
	DECLARE @statistic_row VARCHAR(100)
	DECLARE @statistics_row_item VARCHAR(100)

	DECLARE	@date VARCHAR(50)
	DECLARE @tipo_id BIGINT
	DECLARE @event VARCHAR(50)
	DECLARE @valor VARCHAR(50)
	DECLARE @indexParameter INT	

	DECLARE @RC int





	BEGIN TRY
	  SELECT @startTranCount = @@TRANCOUNT
	  
	  	DECLARE statistics_table CURSOR FOR select * from sp_fnSplitString(@statistics, ';')

		OPEN statistics_table
		FETCH NEXT FROM statistics_table INTO @statistic_row
		WHILE @@fetch_status = 0
		BEGIN
			PRINT @statistic_row

			--CLEAR VALUES
			SET @indexParameter = 1
			SET @date = ''
			SET @tipo_id = 0
			SET @event = ''
			SET @valor = ''

			DECLARE statistics_row_table CURSOR FOR select * from sp_fnSplitString(@statistic_row, ',')
			OPEN statistics_row_table
			FETCH NEXT FROM statistics_row_table INTO @statistics_row_item
			WHILE @@fetch_status = 0
			BEGIN
			  PRINT @statistics_row_item
			  IF @indexParameter = 1
			  BEGIN
			     SET @date = @statistics_row_item
				 PRINT '@date: ' + @date
			  END
			  ELSE IF @indexParameter = 2
			  BEGIN
			     SET @tipo_id = @statistics_row_item
				 PRINT '@tipo_id: ' + convert(varchar(30), @tipo_id)
			  END
			  ELSE IF @indexParameter = 3
			  BEGIN
			     SET @event = @statistics_row_item
				 PRINT '@event: ' + @event
			  END
			  ELSE IF @indexParameter = 4
			  BEGIN
			     SET @valor = @statistics_row_item
				 PRINT '@valor: ' + @valor
			  END
			  
			  SET @indexParameter = @indexParameter + 1

			  IF @indexParameter = 5
			  BEGIN
			     SET @valor = @statistics_row_item
				 PRINT '@cli_id: ' + convert(varchar(30), @cli_id)
			     PRINT '@gru_id: ' + convert(varchar(30), @gru_id)
			     PRINT '@ter_id: ' + convert(varchar(30), @ter_id)
				 			  --ALMACENAMOS LOS VALORES
			     EXECUTE @RC = [dbo].[sp_swStoreStatistic] 
					@date
					,@tipo_id
					,@event
					,@valor
					,@cli_id
					,@gru_id
					,@ter_id
					,@rspCode OUTPUT
			  END


			  FETCH NEXT FROM statistics_row_table INTO @statistics_row_item
            END
			CLOSE statistics_row_table
			DEALLOCATE statistics_row_table




			FETCH NEXT FROM statistics_table INTO @statistic_row
		END
		CLOSE statistics_table
		DEALLOCATE statistics_table


	  PRINT @startTranCount
	  set @rspCode = 0;

	END TRY
	BEGIN CATCH

		-- Test XACT_STATE for 0, 1, or -1.  
		-- If 1, the transaction is committable.  
		-- If -1, the transaction is uncommittable and should   
		--     be rolled back.  
		-- XACT_STATE = 0 means there is no transaction and  
		--     a commit or rollback operation would generate an error.  

		-- Test whether the transaction is uncommittable.  
		IF (XACT_STATE()) = -1 
		   set @rspCode = 5; 
			BEGIN  
				PRINT 'The transaction is in an uncommittable state.' +  
              ' Rolling back transaction.'  
			ROLLBACK TRANSACTION;  
			PRINT @startTranCount
			 INSERT INTO [dbo].[LogErrorBaseDatos]([bderr_fecha],[bderr_nombre_funcion],[bderr_mensaje_error],[bderr_datos_adicionales])
			  VALUES (GETDATE(), OBJECT_NAME(@@PROCID), ERROR_MESSAGE(),  
									'@statistics = ' + @statistics )

				DECLARE @errMessage AS VARCHAR(4000) = ERROR_MESSAGE()
				DECLARE @errSeverity AS INT = ERROR_SEVERITY()
				DECLARE @errState AS INT = ERROR_STATE()

				RAISERROR (@errMessage, @errSeverity, @errState)
		END;  

		-- Test whether the transaction is active and valid.  
		IF (XACT_STATE()) = 1  
		BEGIN  
			PRINT 'The transaction is committable.' +   
              ' Committing transaction.'  
			COMMIT TRANSACTION;   
			PRINT @startTranCount  
		END;  

	END CATCH


END

GO