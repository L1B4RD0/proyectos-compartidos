﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[sp_webCrearAplicacionGrupo](
	@physicalFilename VARCHAR(50),
	@tmsFilename VARCHAR(50),
	@tmsChecksum VARCHAR(50),
	@hardDrivePath VARCHAR(250),
	@appDesc VARCHAR(250),
	@groupID BIGINT,
	@terminalModel BIGINT,
	@AplData VARCHAR(MAX)
)
AS
BEGIN

	SET XACT_ABORT, NOCOUNT ON
	DECLARE @startTranCount INT

	--VARIABLES LOCALES DEL SP
	DECLARE @responseCode AS INT
	DECLARE @sqlBulk AS NVARCHAR(1000)
	DECLARE @appId AS BIGINT
	SET @appId = (SELECT apl_id FROM Aplicacion WHERE apl_id = (SELECT MAX(apl_id)  FROM Aplicacion ));
	SET @appId = @appId + 1  
	SET @appId =  LTRIM (RTRIM(@appId))

	BEGIN TRY

	if @appId is null
	set @appId = '1'
		SELECT @startTranCount = @@TRANCOUNT
		if @terminalModel = -1
		BEGIN
	SET	@terminalModel = NULL
		END

		IF @startTranCount = 0
		BEGIN
			BEGIN TRANSACTION		
			--INICIO CUERPO DEL SP
			--VALICACIÓN PARA TMS DEL SYSTEM EN TERMINALES SPECTRA
			IF UPPER(@tmsFilename) = UPPER('SYSTEM.TMS')
			BEGIN
				SET @tmsFilename = UPPER('STEM.TMS')
			END
	
			IF NOT EXISTS(SELECT * FROM GrupoXAplicacion GXA INNER JOIN Aplicacion A ON (A.apl_id = GXA.aplicacion_id) WHERE GXA.grupo_id = @groupID AND A.apl_nombre_tms = @tmsFilename and GXA.Modelo_id = @terminalModel)
			BEGIN
			
				SET IDENTITY_INSERT [Aplicacion] ON

				INSERT INTO [Aplicacion](apl_id,apl_nombre_archivo_fisico, apl_nombre_tms, apl_checksum_tms, apl_descripcion, apl_fecha_creacion, apl_data) 
				VALUES (@appId,@physicalFilename, @tmsFilename,@tmsChecksum,@appDesc, GETDATE(), (convert(varbinary(max), @AplData, 2)))
				
				SET IDENTITY_INSERT [Aplicacion] OFF 

				--Relacion Grupo Con Aplicacion
				INSERT INTO GrupoXAplicacion(grupo_id, aplicacion_id, Modelo_id) VALUES(@groupID, @appId, @terminalModel)

				SELECT @responseCode = 1
			END
			ELSE
			BEGIN
				RAISERROR (50001, 10, 1, 'Aplicacion ya pertenece al grupo')
			End
			--FIN CUERPO DEL SP
		END

		IF @startTranCount = 0 
		BEGIN 
			COMMIT TRANSACTION

			--SELECT FINAL
			SELECT @responseCode AS RESPONSECODE
		END
	END TRY
	BEGIN CATCH
		IF XACT_STATE() <> 0 AND @startTranCount = 0 
			ROLLBACK TRANSACTION

		INSERT INTO [dbo].[LogErrorBaseDatos]([bderr_fecha],[bderr_nombre_funcion],[bderr_mensaje_error],[bderr_datos_adicionales])
			 VALUES (GETDATE(), OBJECT_NAME(@@PROCID), ERROR_MESSAGE(), /*Datos Extra, Parámetros del SP*/ 
																		'physicalFilename = ' + @physicalFilename + ', tmsfilename = ' + @tmsfilename + ', tmsChecksum = ' + @tmsChecksum + 
																		', hardDrivePath = ' + @hardDrivePath + ', groupID = ' + CAST(@groupID AS VARCHAR(20)))

		DECLARE @errMessage AS VARCHAR(4000) = ERROR_MESSAGE()
		DECLARE @errSeverity AS INT = ERROR_SEVERITY()
		DECLARE @errState AS INT = ERROR_STATE()

		RAISERROR (@errMessage, @errSeverity, @errState)
	END CATCH	

END

GO