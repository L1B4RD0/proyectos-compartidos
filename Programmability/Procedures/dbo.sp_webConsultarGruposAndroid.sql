﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[sp_webConsultarGruposAndroid]
(
	@customerID BIGINT,
	@groupName VARCHAR(50),
	@groupDesc VARCHAR(100)
)
 
AS
BEGIN
	
	SET NOCOUNT ON

	SELECT 
		G.gru_fecha_creacion,
		G.gru_id,
		c.cli_nombre,
		G.gru_nombre, 
		G.gru_descripcion,
		G.gru_mensaje_desplegar,
		G.gru_numero_consultas,
		G.gru_default_login,
		G.gru_default_Password,
		(SELECT COUNT(*) FROM Terminal T WHERE T.ter_grupo_id = G.gru_id) AS gru_total_terminales
	
	FROM Grupo G
		INNER JOIN cliente C ON (C.cli_id = G.gru_cliente_id)
	WHERE C.cli_id = @customerID AND G.gru_tipo_android = 1 /*Tipo Android*/ AND (G.gru_nombre LIKE '%' + @groupName + '%' OR @groupName = '-1')
			AND (G.gru_descripcion LIKE '%' + @groupDesc + '%' OR @groupDesc = '-1')

END

GO