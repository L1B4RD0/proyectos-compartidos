﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[sp_webCerrarSesionWebSeguridad](
	@UserLogin AS VARCHAR(100)
)
 
AS
BEGIN

	SET NOCOUNT ON;
		
	UPDATE LoginInfo
	SET login_fecha_fin = GETDATE()
	FROM LoginInfo LI
		INNER JOIN Usuario U ON (U.usu_id = LI.login_usuario_id)
	WHERE U.usu_login = @UserLogin AND CONVERT(VARCHAR, LI.login_fecha_inicio, 103) = CONVERT(VARCHAR, GETDATE(), 103) 
		AND LI.login_tipo_id = 1

END










GO