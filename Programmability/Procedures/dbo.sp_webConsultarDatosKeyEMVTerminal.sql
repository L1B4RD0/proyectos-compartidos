﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[sp_webConsultarDatosKeyEMVTerminal](
	@keyEMVId BIGINT
)
 
AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT [key_index]
      ,[key_application_id]
      ,[key_exponent]
      ,[key_size]
      ,[key_content]
      ,[key_expiry_date]
      ,[key_effective_date]
      ,[key_checksum]
	FROM TerminalXEMVKey
	WHERE [key_id] = @keyEMVId

END



GO