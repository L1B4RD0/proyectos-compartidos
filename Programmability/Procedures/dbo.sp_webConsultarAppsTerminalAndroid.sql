﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[sp_webConsultarAppsTerminalAndroid](
	@terminalId BIGINT
)
 
AS
BEGIN

	SET NOCOUNT ON;
	
	--VARIABLES LOCALES DEL SP
	DECLARE @backgroundColorGreen AS VARCHAR(10) = '#dff0d8'
	DECLARE @backgroundColorRed AS VARCHAR(10) = '#e47a7a'

	SELECT 
		TXAD.desc_id, 
		TXAD.desc_fecha, 
		TXAD.desc_apk_archivo, 
		TXAD.desc_apk_paquete, 
		TXAD.desc_apk_version,
		CASE WHEN TXAD.desc_apk_version = ISNULL((SELECT TOP 1 GXAD.desp_version FROM GrupoXArchivosDesplegados GXAD INNER JOIN Grupo G ON (G.gru_id = GXAD.desp_grupo_id) INNER JOIN Terminal T ON (T.ter_grupo_id = G.gru_id) WHERE GXAD.desp_tipo_despliegue_id = 1 AND T.ter_id = TXAD.desc_terminal_id AND GXAD.desp_nombre_archivo = TXAD.desc_apk_archivo AND GXAD.desp_nombre_paquete = TXAD.desc_apk_paquete ORDER BY 1 DESC), '0') THEN @backgroundColorGreen ELSE @backgroundColorRed END AS desc_color,
		ISNULL((SELECT TOP 1 GXAD.desp_version FROM GrupoXArchivosDesplegados GXAD INNER JOIN Grupo G ON (G.gru_id = GXAD.desp_grupo_id) INNER JOIN Terminal T ON (T.ter_grupo_id = G.gru_id) WHERE GXAD.desp_tipo_despliegue_id = 1 /*APK's*/ AND T.ter_id = TXAD.desc_terminal_id AND GXAD.desp_nombre_archivo = TXAD.desc_apk_archivo AND GXAD.desp_nombre_paquete = TXAD.desc_apk_paquete ORDER BY 1 DESC), '0')
	FROM TerminalXArchivosDescargados TXAD
	WHERE TXAD.desc_terminal_id = @terminalId AND TXAD.desc_zip_imgs IS NULL AND TXAD.desc_zip_xml IS NULL ORDER BY 1 DESC

END



GO