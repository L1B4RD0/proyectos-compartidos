﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[sp_webInsertarMensajeXObjetoMetodoLog](
	@UserID AS INT,
	@Object AS VARCHAR(100),
	@Method AS VARCHAR(100),	
	@Message1 AS VARCHAR(1000),
	@Message2 AS VARCHAR(1000),
	@SessionID AS VARCHAR(100)
)
 
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @FuncID AS INT

	SELECT @FuncID = F.func_id
	FROM Usuario U
		INNER JOIN Perfil P ON (U.usu_perfil_id = P.perf_id)
		INNER JOIN PerfilXFuncion PXF ON (P.perf_id = PXF.perf_id)
		INNER JOIN Funcion F ON (F.func_id = PXF.func_id)
		INNER JOIN Objeto O ON (O.id = F.func_objeto_id)
		INNER JOIN Metodo M ON (M.id = F.func_metodo_id)
	WHERE 
		U.usu_id = @UserID AND
		O.nombre = @Object AND
		M.nombre = @Method 

	INSERT INTO Log
		(log_usuario_id, log_fecha, log_funcion_id, log_tipo_log_id ,log_mensaje1, log_mensaje2, log_sessionID)
	VALUES
		(@UserID, GETDATE(), @FuncID, 4, @Message1, @Message2, @SessionID)
	
END


GO