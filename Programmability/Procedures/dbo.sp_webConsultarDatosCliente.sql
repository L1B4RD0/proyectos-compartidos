﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[sp_webConsultarDatosCliente](
	@customerID BIGINT
)
 
AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT C.cli_nombre, C.cli_descripcion, C.cli_ip_descarga, C.cli_puerto_descarga, cli_tipo_estado_id, C.cli_autoaprendizaje,
		ISNULL(T.terc_numero_identificacion, ''), ISNULL(T.terc_tipo_identificacion_id, -1), ISNULL(T.terc_nombre, ''), ISNULL(T.terc_direccion, ''), ISNULL(T.terc_celular, ''), ISNULL(t.terc_telefono_fijo, ''), ISNULL(T.terc_correo_electronico, '')
	FROM Cliente C
		LEFT OUTER JOIN Tercero T ON(T.terc_id = C.cli_tercero_id)
	WHERE C.cli_id = @customerID

END

GO