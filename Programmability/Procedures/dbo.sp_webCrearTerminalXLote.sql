﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

-- =============================================
-- Autor:				Elkin Beltrán
-- Fecha Creación:		26/Feb/2017
-- Histórico Cambios:
------------------------------------------------
--	v1.0	Versión Inicial
--  v1.1	Creación de nuevos campos (lat/long)
--			en archivo de Lote
------------------------------------------------
-- Campos de Respuesta:		SingleRow
-- RESPONSECODE		INT
-- =============================================

CREATE PROCEDURE [dbo].[sp_webCrearTerminalXLote](
	@customerID AS BIGINT,
	@groupName VARCHAR(30),
	@terminalSerial VARCHAR(30),
	@terminalType VARCHAR(30),
	@terminalBrand VARCHAR(30),
	@terminalModel VARCHAR(30),
	@terminalInterface VARCHAR(30),
	@serialSIM VARCHAR(30),
	@mobileNumberSIM VARCHAR(30),
	@IMEI VARCHAR(30),
	@chargerSerial VARCHAR(30),
	@batterySerial VARCHAR(30),
	@biometricSerial VARCHAR(30),
	@latitudeGPS VARCHAR(30),
	@longitudeGPS VARCHAR(30),
	@terminalDesc VARCHAR(100),
	--Contact Data
	@contactIDType VARCHAR(50),
	@contactIDNumber VARCHAR(20),
	@contactName VARCHAR(50),
	@contactAddress VARCHAR(100),
	@contactMobile VARCHAR(20),
	@contactPhone VARCHAR(20),
	@contactEmail VARCHAR(50)
)
 
AS
BEGIN

	SET XACT_ABORT, NOCOUNT ON
	DECLARE @startTranCount INT

	--VARIABLES LOCALES DEL SP
	DECLARE @responseCode AS INT
	DECLARE @groupId AS BIGINT
	DECLARE @contactId AS BIGINT
	DECLARE @terminalTypeId AS BIGINT
	DECLARE @terminalBrandId AS BIGINT
	DECLARE @terminalModelId AS BIGINT
	DECLARE @terminalInterfaceId AS BIGINT
	DECLARE @contactIDTipeInt AS BIGINT

	BEGIN TRY
		SELECT @startTranCount = @@TRANCOUNT

		IF @startTranCount = 0
		BEGIN
			BEGIN TRANSACTION		
			--INICIO CUERPO DEL SP
	
			--VALIDAR CREACIÓN DE CONTACTO
			IF (LEN(@contactIDNumber) <= 0)
			BEGIN
				--SIN DATOS DE CONTACTO
				SET @contactId = NULL
			END
			ELSE
			BEGIN
				--CON DATOS DE CONTACTO, VALIDAR SI ES EXISTENTE O ES NUEVO
				SELECT @contactId = T.terc_id FROM Tercero T WHERE T.terc_numero_identificacion = LTRIM(RTRIM(@contactIDNumber))

				IF @contactId IS NULL
				BEGIN
					
					--OBTENER EL TIPO DE IDENTIFICACIÓN
					SELECT @contactIDTipeInt = TI.id FROM TipoIdentificacion TI WHERE TI.descripcion = LTRIM(RTRIM(@contactIDType))

					--ES NUEVO CONTACTO, CREARLO
					INSERT INTO Tercero(terc_tipo_identificacion_id, terc_numero_identificacion, terc_nombre, terc_direccion, terc_celular, terc_telefono_fijo, terc_correo_electronico, terc_fecha_creacion)
						VALUES (@contactIDTipeInt, @contactIDNumber, @contactName, @contactAddress, @contactMobile, @contactPhone, @contactEmail, GETDATE())

					SET @contactId = SCOPE_IDENTITY()

				END
			END

			--OBTENER ID DEL GRUPO SEGÚN NOMBRE Y CLIENTE ID
			SELECT @groupId = G.gru_id FROM Grupo G WHERE G.gru_nombre = LTRIM(RTRIM(@groupName)) AND G.gru_cliente_id = @customerID

			--OBTENER IDs TIPO, MARCA, MODELO E INTERFACE DE LA TERMINAL
			SELECT @terminalTypeId = TT.id FROM TipoTerminal TT WHERE TT.descripcion = LTRIM(RTRIM(@terminalType))
			SELECT @terminalBrandId = MT.id FROM MarcaTerminal MT WHERE MT.descripcion = LTRIM(RTRIM(@terminalBrand))
			SELECT @terminalModelId = MoT.id FROM ModeloTerminal MoT WHERE MoT.descripcion = LTRIM(RTRIM(@terminalModel))
			SELECT @terminalInterfaceId = IT.id FROM InterfaceTerminal IT WHERE IT.descripcion = LTRIM(RTRIM(@terminalInterface))

			--INSERTAR NUEVA TERMINAL
			INSERT INTO [dbo].[Terminal]([ter_serial], [ter_tipo_terminal_id], [ter_marca_terminal_id], [ter_modelo_terminal_id], [ter_interface_terminal_id], [ter_serial_sim], [ter_telefono_sim], [ter_imei], [ter_serial_cargador]
						,[ter_serial_bateria], [ter_serial_lector_biometrico], [ter_descripcion], [ter_grupo_id], [ter_tercero_id], [ter_prioridad_grupo], [ter_actualizar], [ter_fecha_programada_actualizacion]
						,[ter_ip_descarga], [ter_puerto_descarga], [ter_estado_actualizacion_id], [ter_mensaje_actualizacion], [ter_tipo_estado_id], [ter_validar_imei], [ter_validar_sim], [ter_latitud_gps], [ter_longitud_gps], [ter_fecha_creacion])
			VALUES (@terminalSerial, @terminalTypeId, @terminalBrandId, @terminalModelId, @terminalInterfaceId, @serialSIM, @mobileNumberSIM, @IMEI, @chargerSerial, @batterySerial, @biometricSerial, @terminalDesc, @groupId, @contactId, 1 /*Prioridad grupo*/,
					0 /*No Actualizar Manual terminal*/, NULL, '', '', 1 /*Estado = Actualizada (Nueva)*/, '', 1 /*Estado = Activa (Nueva)*/, 0 /*No Validar Imei*/, 0 /*No Validar SIM*/, (CASE WHEN @latitudeGPS='' THEN NULL ELSE @latitudeGPS END), (CASE WHEN @longitudeGPS='' THEN NULL ELSE @longitudeGPS END), GETDATE())

			SELECT @responseCode = 1

			--FIN CUERPO DEL SP
		END

		IF @startTranCount = 0 
		BEGIN 
			COMMIT TRANSACTION

			--SELECT FINAL
			SELECT @responseCode AS RESPONSECODE
		END
	END TRY
	BEGIN CATCH
		IF XACT_STATE() <> 0 AND @startTranCount = 0 
			ROLLBACK TRANSACTION

		INSERT INTO [dbo].[LogErrorBaseDatos]([bderr_fecha],[bderr_nombre_funcion],[bderr_mensaje_error],[bderr_datos_adicionales])
			 VALUES (GETDATE(), OBJECT_NAME(@@PROCID), ERROR_MESSAGE(), /*Datos Extra, Parámetros del SP*/ 
																		'groupName = ' + @groupName + ', terminalSerial = ' + @terminalSerial + ', terminalType = ' + @terminalType + 
																		', terminalBrand = ' + @terminalBrand + ', terminalModel = ' + @terminalModel + ', terminalInterface = ' + @terminalInterface)

		DECLARE @errMessage AS VARCHAR(4000) = ERROR_MESSAGE()
		DECLARE @errSeverity AS INT = ERROR_SEVERITY()
		DECLARE @errState AS INT = ERROR_STATE()

		RAISERROR (@errMessage, @errSeverity, @errState)
	END CATCH	

END

GO