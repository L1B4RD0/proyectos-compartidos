﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
-- Batch submitted through debugger: SQLQuery1.sql|7|0|C:\Users\WPOSS\AppData\Local\Temp\~vs8150.sql
CREATE PROCEDURE [dbo].[sp_webDesplegarArchivoGrupoAndroid](
	@groupID INT,
	@filename VARCHAR(100),
	@packageName VARCHAR(100),
	@version VARCHAR(100),
	@deployType INT	/*1-APK, 2-ZIP DE XML's, 3-ZIP DE IMG's*/
)
 
AS
BEGIN

	SET XACT_ABORT, NOCOUNT ON
	DECLARE @startTranCount INT

	--VARIABLES LOCALES DEL SP
	DECLARE @responseCode AS INT
	DECLARE @serial varchar(100)
	DECLARE @apps varchar(500)
	DECLARE @Con int
	DECLARE @con2 int
	DECLARE @APP VARCHAR(100)
	DECLARE @validar int
	DECLARE @AppListTableDB TABLE (Id BIGINT NOT NULL PRIMARY KEY IDENTITY(1,1), AppName VARCHAR(100) NOT NULL)

	BEGIN TRY
		SELECT @startTranCount = @@TRANCOUNT

		IF @startTranCount = 0
		BEGIN
			BEGIN TRANSACTION		
			--INICIO CUERPO DEL SP

			IF @deployType IN (2,3)	--XML's E IMG's
			BEGIN
				IF NOT EXISTS(SELECT * FROM GrupoXArchivosDesplegados WHERE desp_grupo_id = @groupID AND desp_nombre_archivo = @filename)
				BEGIN

					--INSERTAR NUEVO ARCHIVO DESPLEGADO POR GRUPO
					INSERT INTO GrupoXArchivosDesplegados(desp_fecha, desp_grupo_id, desp_nombre_archivo, desp_nombre_paquete, desp_version, desp_tipo_despliegue_id)
							VALUES (GETDATE(), @groupID, @filename, @packageName, @version, @deployType)


					--ACTUALIZA ESTADO DE ACTUALIZACION A PENDIENTE CUANDO SE DESPLIEGA UNA NUEVA APLICACION
					

					SELECT @responseCode = 1

				END
				ELSE
				BEGIN
					RAISERROR (50001, 10, 1, 'Archivo ya existe en el grupo')
				END
			END
			ELSE
			BEGIN
				--APK's
				IF NOT EXISTS(SELECT * FROM GrupoXArchivosDesplegados WHERE desp_grupo_id = @groupID AND desp_nombre_archivo = @filename AND desp_nombre_paquete = @packageName AND desp_version = @version)
				BEGIN

					--INSERTAR NUEVO ARCHIVO DESPLEGADO POR GRUPO
					INSERT INTO GrupoXArchivosDesplegados(desp_fecha, desp_grupo_id, desp_nombre_archivo, desp_nombre_paquete, desp_version, desp_tipo_despliegue_id)
							VALUES (GETDATE(), @groupID, @filename, @packageName, @version, @deployType)
						
				--------------------------------------------------------
			
				INSERT INTO @AppListTableDB
						SELECT CONCAT(desp_nombre_paquete, '-', desp_version) 
						FROM GrupoXArchivosDesplegados WHERE desp_grupo_id = @groupID

				DECLARE cursorAplicaciones cursor
					for select ter_serial, ter_aplicaciones_instaladas from Terminal  where ter_grupo_id = @groupID
	
			

				OPEN cursorAplicaciones
				FETCH	NEXT FROM cursorAplicaciones INTO @serial, @apps

				WHILE (@@FETCH_STATUS = 0)

				BEGIN
					SET @con2 = 1
					SET @Con = 0
		
					WHILE (@con2 <= (SELECT MAX(Id) FROM @AppListTableDB))
					BEGIN
						 SELECT  @APP = AppName
						 FROM @AppListTableDB WHERE Id = @con2

						 SELECT @validar = CHARINDEX(@APP, @apps)

						 IF  @validar > 0
						 BEGIN
							SET @Con = @Con + 1
						 END
		 

						 SET @con2 = @con2 + 1
		
		
					END

				IF @Con = (SELECT MAX(Id) FROM @AppListTableDB)
				BEGIN
					UPDATE Terminal SET ter_estado_actualizacion_id = 1 WHERE ter_serial = @serial
				END
				else
					UPDATE Terminal SET ter_estado_actualizacion_id = 2 WHERE ter_serial = @serial

				FETCH NEXT FROM cursorAplicaciones INTO @serial, @apps

			END

			CLOSE cursorAplicaciones
			deallocate cursorAplicaciones
		


				----------------------------------------------------------
					SELECT @responseCode = 1

				END
				ELSE
				BEGIN
					RAISERROR (50001, 10, 1, 'Archivo ya existe en el grupo')
				END
			END

			--FIN CUERPO DEL SP
		END

		IF @startTranCount = 0 
		BEGIN 
			COMMIT TRANSACTION

			--SELECT FINAL
			SELECT @responseCode AS RESPONSECODE
		END
	END TRY
	BEGIN CATCH
		IF XACT_STATE() <> 0 AND @startTranCount = 0 
			ROLLBACK TRANSACTION

		INSERT INTO [dbo].[LogErrorBaseDatos]([bderr_fecha],[bderr_nombre_funcion],[bderr_mensaje_error],[bderr_datos_adicionales])
			 VALUES (GETDATE(), OBJECT_NAME(@@PROCID), ERROR_MESSAGE(), /*Datos Extra, Parámetros del SP*/ 
																		'groupID = ' + CAST(@groupID AS VARCHAR) + ', filename = ' + @filename + ', packageName = ' + @packageName + 
																		', version = ' + @version + ', deployType = ' + CAST(@deployType AS VARCHAR))

		DECLARE @errMessage AS VARCHAR(4000) = ERROR_MESSAGE()
		DECLARE @errSeverity AS INT = ERROR_SEVERITY()
		DECLARE @errState AS INT = ERROR_STATE()

		RAISERROR (@errMessage, @errSeverity, @errState)
	END CATCH	

END



GO