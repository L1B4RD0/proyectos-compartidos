﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

-- =============================================
-- Autor:				Elkin Beltrán
-- Fecha Creación:		06/Junio/2015
-- Histórico Cambios:
------------------------------------------------
--	v1.0	Versión Inicial
------------------------------------------------
-- Campos de Respuesta:		SingleRow
-- STATUS				INT
-- RNDTXT				VARCHAR(100)
-- =============================================

CREATE PROCEDURE [dbo].[sp_webIniciarProcesoRestauracionClave](
	@emailID VARCHAR(150),
	@IP VARCHAR(20)
)
 
AS
BEGIN

	SET XACT_ABORT, NOCOUNT ON
	DECLARE @startTranCount INT

	--VARIABLES LOCALES DEL SP	
	DECLARE @userID_DB AS INT
	DECLARE @status AS INT
	DECLARE @rndTxt AS VARCHAR(100)

	BEGIN TRY
		SELECT @startTranCount = @@TRANCOUNT

		IF @startTranCount = 0
		BEGIN
			BEGIN TRANSACTION		
			--INICIO CUERPO DEL SP

			SET @rndTxt = ''

			/*VERIFICAR DATOS CONTRA TABLA USUARIOS*/
			SELECT @userID_DB = U.usu_id FROM Usuario U
			WHERE U.usu_email = @emailID

			IF @userID_DB IS NOT NULL
			BEGIN

				--USUARIO EXISTE INSERTAR EN TABLA, SI NO HAY OTRO PROCESO PENDIENTE
				DELETE FROM UsuarioXRestauracionClave WHERE rest_usuario_id = @userID_DB;
				IF NOT EXISTS( SELECT * FROM UsuarioXRestauracionClave WHERE rest_usuario_id = @userID_DB AND rest_estado = 1 )
				BEGIN

					EXEC [spGenerarTextoRandomico] @rndTxt OUTPUT

					INSERT INTO UsuarioXRestauracionClave(rest_usuario_id, rest_fecha, rest_rand, rest_estado, rest_ip)
							VALUES ( @userID_DB, GETDATE(), @rndTxt, 1, @IP)

					SET @status = 1	/*OK*/
				END
				ELSE
				BEGIN
					SET @status = 0	/*ERROR*/ 
				END
		
			END
			ELSE
			BEGIN
				SET @status = 0	/*ERROR*/ 
			END

			--FIN CUERPO DEL SP

		END

		IF @startTranCount = 0 
		BEGIN 
			COMMIT TRANSACTION 

			--SELECT FINAL
			SELECT @status AS STATUS, @rndTxt AS RNDTXT
		END
	END TRY
	BEGIN CATCH
		IF XACT_STATE() <> 0 AND @startTranCount = 0 
			ROLLBACK TRANSACTION

		INSERT INTO [dbo].[LogErrorBaseDatos]([bderr_fecha],[bderr_nombre_funcion],[bderr_mensaje_error],[bderr_datos_adicionales])
			 VALUES (GETDATE(), OBJECT_NAME(@@PROCID), ERROR_MESSAGE(), /*Datos Extra, Parámetros del SP*/ 'emailID = ' + @emailID + ', IP = ' + @IP)

		DECLARE @errMessage AS VARCHAR(4000) = ERROR_MESSAGE()
		DECLARE @errSeverity AS INT = ERROR_SEVERITY()
		DECLARE @errState AS INT = ERROR_STATE()

		RAISERROR (@errMessage, @errSeverity, @errState)
	END CATCH

END

GO