﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[sp_webEditarTerminal](
	@terminalID AS INT,
	@terminalSerial VARCHAR(50),
	@terminalType INT,
	@terminalBrand INT,
	@terminalModel INT,
	@terminalInterface INT,
	@serialSIM VARCHAR(50),
	@mobileNumberSIM VARCHAR(50),
	@IMEI VARCHAR(50),
	@chargerSerial VARCHAR(50),
	@batterySerial VARCHAR(50),
	@biometricSerial VARCHAR(50),
	@terminalDesc VARCHAR(250),
	@groupPriority BIT,
	@updateTerminal BIT,
	@scheduleDate DATETIME,
	@updateIP VARCHAR(20),
	@updatePort VARCHAR(10),
	@updateBlockingMode BIT,
	@updateUpdateNow BIT,
	@validateIMEI BIT,
	@validateSerialSIM BIT,
	@terminalStatus INT,
	@terminalGroupId INT,
	--Contact Data
	@contactIDNumber VARCHAR(20),
	@contactIDType INT,
	@contactName VARCHAR(50),
	@contactAddress VARCHAR(100),
	@contactMobile VARCHAR(20),
	@contactPhone VARCHAR(20),
	@contactEmail VARCHAR(50),
	--MDM Fields
	@allowedApps VARCHAR(500) = NULL,
	@kioskoApp VARCHAR(500) = NULL
	--@Inicializacion BIT
)
 
AS
BEGIN

	SET XACT_ABORT, NOCOUNT ON
	DECLARE @startTranCount INT

	--VARIABLES LOCALES DEL SP
	DECLARE @responseCode AS INT
	DECLARE @responseMsg AS VARCHAR(500)
	DECLARE @contactId AS BIGINT
    DECLARE @imeiNumber INT
	DECLARE @serialIMEIDuplicado AS VARCHAR(500)

	BEGIN TRY
		--VALIDAR QUE EL IMEI NO ESTE REPETIDO
		IF @IMEI != ''
			BEGIN
				select @imeiNumber = count(*) from dbo.terminal where ter_id != @terminalID and ter_imei = @IMEI 
			END
		ELSE
			BEGIN 
				select @imeiNumber = 0
			END

		IF @imeiNumber = 0
		BEGIN
		   SELECT @startTranCount = @@TRANCOUNT

			IF @startTranCount = 0






			BEGIN
				BEGIN TRANSACTION	
			
			









				--INICIO CUERPO DEL SP
	
				--VALIDAR CREACIÓN DE CONTACTO
				IF (LEN(@contactIDNumber) <= 0)
				BEGIN
					--SIN DATOS DE CONTACTO
					SET @contactId = NULL
				END
				ELSE
				BEGIN
					--CON DATOS DE CONTACTO, VALIDAR SI ES EXISTENTE O ES NUEVO
					SELECT @contactId = T.terc_id FROM Tercero T WHERE T.terc_numero_identificacion = LTRIM(RTRIM(@contactIDNumber))

					IF @contactId IS NULL
					BEGIN
						--ES NUEVO CONTACTO, CREARLO
						INSERT INTO Tercero(terc_tipo_identificacion_id, terc_numero_identificacion, terc_nombre, terc_direccion, terc_celular, terc_telefono_fijo, terc_correo_electronico, terc_fecha_creacion)
						VALUES (@contactIDType, @contactIDNumber, @contactName, @contactAddress, @contactMobile, @contactPhone, @contactEmail, GETDATE())

						SET @contactId = SCOPE_IDENTITY()

					END
					ELSE
					BEGIN
						--EXISTE, ACTUALIZAR CONTACTO
						UPDATE Tercero
						SET 

						terc_tipo_identificacion_id = @contactIDType,
						terc_numero_identificacion = @contactIDNumber,
						terc_nombre = @contactName,
						terc_direccion = @contactAddress,
						terc_celular = @contactMobile,
						terc_telefono_fijo = @contactPhone,
						terc_correo_electronico = @contactEmail,
						terc_fecha_actualizacion = GETDATE()
						WHERE terc_id = @contactId
					END
				END


				--ACTUALIZAR TERMINAL EN GRUPO
				IF @groupPriority = 1
				BEGIN
					--ACTUALIZAR TERMINAL X GRUPO
					UPDATE Terminal
					SET 

					[ter_serial] = @terminalSerial, 
					[ter_tipo_terminal_id] = @terminalType, 
					[ter_marca_terminal_id] = @terminalBrand, 
					[ter_modelo_terminal_id] = @terminalModel, 
					[ter_interface_terminal_id] = @terminalInterface, 
					[ter_serial_sim] = @serialSIM, 
					[ter_telefono_sim] = @mobileNumberSIM, 
					[ter_imei] = @IMEI, 
					[ter_serial_cargador] = @chargerSerial, 
					[ter_serial_bateria] = @batterySerial, 
					[ter_serial_lector_biometrico] = @biometricSerial, 
					[ter_descripcion] = @terminalDesc, 
					[ter_tercero_id] = @contactId, 
					[ter_prioridad_grupo] = 1, 
					[ter_actualizar] = 0, 
					[ter_fecha_programada_actualizacion] = NULL, 
					[ter_ip_descarga] = '', 
					[ter_puerto_descarga] = '', 
					[ter_blocking_mode] = 0, 
					[ter_update_now] = 0,
					[ter_tipo_estado_id] = @terminalStatus, 
					[ter_validar_imei] = @validateIMEI, 
					[ter_validar_sim] = @validateSerialSIM, 
					[ter_grupo_id] = @terminalGroupId,
					--Campos MDM
					[ter_aplicaciones_permitidas] = @allowedApps,
					[ter_fecha_actualizacion] = GETDATE(),
					[ter_aplicacion_kiosko] = @kioskoApp
					WHERE ter_id = @terminalID
				END
				ELSE


				BEGIN
					IF @updateTerminal = 1 
					BEGIN
						--ACTUALIZAR TERMINAL MANUAL
						UPDATE Terminal
						SET 

						[ter_serial] = @terminalSerial, 
						[ter_tipo_terminal_id] = @terminalType, 
						[ter_marca_terminal_id] = @terminalBrand, 
						[ter_modelo_terminal_id] = @terminalModel, 
						[ter_interface_terminal_id] = @terminalInterface, 
						[ter_serial_sim] = @serialSIM, 
						[ter_telefono_sim] = @mobileNumberSIM, 
						[ter_imei] = @IMEI, 
						[ter_serial_cargador] = @chargerSerial, 
						[ter_serial_bateria] = @batterySerial, 
						[ter_serial_lector_biometrico] = @biometricSerial, 
						[ter_descripcion] = @terminalDesc, 
						[ter_tercero_id] = @contactId, 
						[ter_prioridad_grupo] = 0, 
						[ter_actualizar] = 1, 
						[ter_fecha_programada_actualizacion] = @scheduleDate, 
						[ter_ip_descarga] = @updateIP, 
						[ter_puerto_descarga] = @updatePort, 
						[ter_blocking_mode] = @updateBlockingMode, 
						[ter_update_now] = @updateUpdateNow, 
						[ter_tipo_estado_id] = @terminalStatus, 
						[ter_validar_imei] = @validateIMEI, 
						[ter_validar_sim] = @validateSerialSIM, 
						[ter_grupo_id] = @terminalGroupId,
						--Campos MDM
						[ter_aplicaciones_permitidas] = @allowedApps,
						[ter_fecha_actualizacion] = GETDATE(),
						[ter_aplicacion_kiosko] = @kioskoApp
						WHERE ter_id = @terminalID
					END
					ELSE
					BEGIN
						--NO ACTUALIZAR TERMINAL
						UPDATE Terminal
						SET 

						[ter_serial] = @terminalSerial, 
						[ter_tipo_terminal_id] = @terminalType, 
						[ter_marca_terminal_id] = @terminalBrand, 
						[ter_modelo_terminal_id] = @terminalModel, 
						[ter_interface_terminal_id] = @terminalInterface, 
						[ter_serial_sim] = @serialSIM, 
						[ter_telefono_sim] = @mobileNumberSIM, 
						[ter_imei] = @IMEI, 
						[ter_serial_cargador] = @chargerSerial, 
						[ter_serial_bateria] = @batterySerial, 
						[ter_serial_lector_biometrico] = @biometricSerial, 
						[ter_descripcion] = @terminalDesc, 
						[ter_tercero_id] = @contactId, 
						[ter_prioridad_grupo] = 0, 
						[ter_actualizar] = 0, 
						[ter_fecha_programada_actualizacion] = NULL, 
						[ter_ip_descarga] = '', 
						[ter_puerto_descarga] = '',  
						[ter_blocking_mode] = 0, 
						[ter_update_now] = 0, 
						[ter_tipo_estado_id] = @terminalStatus, 
						[ter_validar_imei] = @validateIMEI, 
						[ter_validar_sim] = @validateSerialSIM, 
						[ter_grupo_id] = @terminalGroupId,
						--Campos MDM
						[ter_aplicaciones_permitidas] = @allowedApps,
						[ter_fecha_actualizacion] = GETDATE(),
						[ter_aplicacion_kiosko] = @kioskoApp
						WHERE ter_id = @terminalID
					END
				END


				SELECT @responseCode = 1
				SELECT @responseMsg = 'Terminal actualizado de forma correcta.'

				--FIN CUERPO DEL SP
			END
			
			
				--UPDATE Terminal
				--SET ter_flag_inicializacion = @Inicializacion
				--WHERE ter_serial = @terminalSerial
			
			IF @startTranCount = 0 
			BEGIN 
				COMMIT TRANSACTION

				--SELECT FINAL
				SELECT @responseCode AS RESPONSECODE, @responseMsg AS RESPONSEMSG
			END
		END	
		ELSE
		BEGIN
		    SELECT @serialIMEIDuplicado = ter_serial from dbo.terminal where ter_imei = @IMEI
		  	SET @responseCode = 0
			SET @responseMsg = 'Terminal no actualizado de forma correcta - El IMEI: ' + @IMEI + ' ya existe asociado al terminal: ' + @serialIMEIDuplicado
			--SELECT FINAL
				SELECT @responseCode AS RESPONSECODE, @responseMsg AS RESPONSEMSG
				--SELECT @responseMsg AS RESPONSEMSG
		END

	
	END TRY
	BEGIN CATCH
	    SELECT @responseCode = 0
		SELECT @responseMsg = 'Terminal no actualizado de forma correcta.'
		--SELECT FINAL
				SELECT @responseCode AS RESPONSECODE, @responseMsg AS RESPONSEMSG

		IF XACT_STATE() <> 0 AND @startTranCount = 0 
			ROLLBACK TRANSACTION

		INSERT INTO [dbo].[LogErrorBaseDatos]([bderr_fecha],[bderr_nombre_funcion],[bderr_mensaje_error],[bderr_datos_adicionales])
			 VALUES (GETDATE(), OBJECT_NAME(@@PROCID), ERROR_MESSAGE(), /*Datos Extra, Parámetros del SP*/ 
																		'terminalSerial = ' + @terminalSerial + ', serialSIM = ' + @serialSIM + ', IMEI = ' + @IMEI + 
																		', updateIP = ' + @updateIP + ', updatePort = ' + @updatePort)

		DECLARE @errMessage AS VARCHAR(4000) = ERROR_MESSAGE()
		DECLARE @errSeverity AS INT = ERROR_SEVERITY()
		DECLARE @errState AS INT = ERROR_STATE()

		RAISERROR (@errMessage, @errSeverity, @errState)
	END CATCH	

END





GO