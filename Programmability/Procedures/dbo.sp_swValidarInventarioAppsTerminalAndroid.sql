﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[sp_swValidarInventarioAppsTerminalAndroid](
	/*INPUT*/
	@msgType VARCHAR(4),
	@procCode VARCHAR(6),
	@stan VARCHAR(6),
	@serialPos VARCHAR(20),
	@customerName VARCHAR(50),
	@applicationListPos VARCHAR(1000),
	@ipPos VARCHAR(15),
	@ipDetected VARCHAR(15),
	@tcpPortDetected VARCHAR(6),
	@imeiPos VARCHAR(50),
	@simPos VARCHAR(50),
	@latitude VARCHAR(50),
	@longitude VARCHAR(50),
	@apn VARCHAR(50),
	@signalLevel VARCHAR(50),
	@batteryLevel VARCHAR(50),
	@reservedUse1 VARCHAR(250),
	@reservedUse2 VARCHAR(250),
	@reservedUse3 VARCHAR(250),
	@softwareVersionPos VARCHAR(50),
	@isoField61 VARCHAR(250),
	@statistics VARCHAR(1000),
	/*OUTPUT*/
	@rspCode VARCHAR(2) OUTPUT,
	@rspMessage VARCHAR(200) OUTPUT,
	@dloadScheduledDatetime VARCHAR(20) OUTPUT,
	@dloadIp VARCHAR(15) OUTPUT,
	@dloadPort VARCHAR(6) OUTPUT,
	@dloadApplicationList VARCHAR(500) OUTPUT,
	@transactionId VARCHAR(20) OUTPUT,
	@configuracionTcp VARCHAR(500) OUTPUT,
	@messageServerIp VARCHAR(15) OUTPUT,
	@messageServerPort VARCHAR(6) OUTPUT,
	@group VARCHAR(100) OUTPUT,
	--@blockingMode VARCHAR(20) OUTPUT,
	@reserved1 VARCHAR(500) OUTPUT,
	@reserved2 VARCHAR(100) OUTPUT,
	@reserved3 VARCHAR(100) OUTPUT,
	@reserved4 VARCHAR(100) OUTPUT,
	@reserved5 VARCHAR(100) OUTPUT,
	@reserved6 VARCHAR(100) OUTPUT,
	@reserved7 VARCHAR(100) OUTPUT,
	@reserved8 VARCHAR(100) OUTPUT,
	@reserved9 VARCHAR(100) OUTPUT,
	@reserved10 VARCHAR(100) OUTPUT,
	@listaBlanca VARCHAR(500) OUTPUT,
	@modoKiosko VARCHAR(500) OUTPUT,
	@mensaje VARCHAR(500) OUTPUT,
	@cambioClave VARCHAR(500) OUTPUT,
	@nuevaClave VARCHAR(500) OUTPUT,
	@consultasXhora VARCHAR(500) OUTPUT,
	@bloqueoDispositivo VARCHAR(500) OUTPUT
)
AS
BEGIN

	SET XACT_ABORT, NOCOUNT ON
	DECLARE @startTranCount INT

	--VARIABLES LOCALES DEL SP	
	DECLARE @SeparatorApp VARCHAR(2) = CHAR(10) --\n
	DECLARE @SeparatorChecksum VARCHAR(2) = '-'
	DECLARE @Counter INT
	DECLARE @AppNameDB VARCHAR(100)
	DECLARE @AppChecksumDB VARCHAR(100)
	DECLARE @AppListTablePos TABLE (Id BIGINT NOT NULL PRIMARY KEY, AppName VARCHAR(100) NOT NULL, AppChecksum VARCHAR(100) NOT NULL)
	DECLARE @AppListTableDB TABLE (Id BIGINT NOT NULL PRIMARY KEY IDENTITY(1,1), AppName VARCHAR(100) NOT NULL,Apk VARCHAR(100) NOT NULL)
	DECLARE @TerminalId BIGINT = NULL
	DECLARE @UpdateStatus BIGINT = NULL
	DECLARE @AppListStringDB VARCHAR(1000) = ''
	DECLARE @FlagUpdTerminal BIT
	DECLARE @GrupoId BIGINT
	DECLARE @FlagUpdGrupo BIT
	DECLARE @FlagPrioridadGrupo BIT
	DECLARE @FlagProgramarFecha BIT
	DECLARE @GrupoActualizarFechaIni DATETIME
	DECLARE @GrupoActualizarFechaFin DATETIME
	DECLARE @GrupoRangoHoraIni TIME
	DECLARE @GrupoRangoHoraFin TIME
	DECLARE @FlagCheckForNewApps BIT
	DECLARE @TerminalStatus INT
	DECLARE @FlagValidateImei BIT
	DECLARE @TerminalImei VARCHAR(50)
	DECLARE @FlagValidateSim BIT
	DECLARE @TerminalSim VARCHAR(50)
	DECLARE @FlagContinueProcess BIT
	DECLARE @ExpLicenseDatetime DATETIME
	DECLARE @CustomerId BIGINT = NULL
	DECLARE @NextString NVARCHAR(40)
	DECLARE @Pos INT
	DECLARE @NextPos INT
	DECLARE @String NVARCHAR(250)
	DECLARE @Delimiter NVARCHAR(2) = '|'
	DECLARE @counterIdx INT
	DECLARE @posType VARCHAR(50)
	DECLARE @posBrand VARCHAR(50)
	DECLARE @posModel VARCHAR(50)
	DECLARE @posCommType VARCHAR(50)
	DECLARE @posUid VARCHAR(50)
	DECLARE @posName VARCHAR(50)
	DECLARE @posMac VARCHAR(50)
	DECLARE @posTypeId INT
	DECLARE @posBrandId INT
	DECLARE @posModelId INT
	DECLARE @posCommTypeId INT
	DECLARE @autoLearning BIT
	DECLARE @groupNumber INT
	DECLARE @groupUpdateNow BIT
	DECLARE @terminalUpdateNow BIT
	DECLARE @imeiNumber INT
	DECLARE @ter_codigo_producto varchar(50)
	DECLARE @TIPO_COFIGURACION varchar(100) 
	SET  @TIPO_COFIGURACION = ' CONFIGURACION INDETERMINADA'
	DECLARE @claveMDM varchar(8)
	BEGIN TRY

	    
		SELECT @startTranCount = @@TRANCOUNT

		--VALORES POR DEFECTO
		--SET @blockingMode = '0'
		SET @reserved1 = 'NA'
		SET @reserved2 = 'NA'
		SET @reserved3 = 'NA'
		SET @reserved4 = 'NA'
		SET @reserved5 = 'NA'
		SET @reserved6 = 'NA'
		SET @reserved7 = 'NA'
		SET @reserved8 = 'NA'
		SET @reserved9 = 'NA'
		SET @reserved10 = 'NA'
	
		IF @startTranCount = 0
		BEGIN
			BEGIN TRANSACTION		


	--DELETE FROM DEBUG
			--Capturamos el codigo de producto
			SET @ter_codigo_producto = @reservedUse2	

			--CORREGIR ERROR DEL IMEI
			IF @imeiPos is null
			BEGIN
				SET @imeiPos = 'T' + @serialPos
			END 
				ELSE IF @imeiPos = ''
			BEGIN
				SET @imeiPos = 'T' + @serialPos
			END
			ELSE IF CHARINDEX('+', @imeiPos) != 0
			BEGIN
				SET @imeiPos = 'E' + @serialPos
			END
			ELSE IF ISNUMERIC(@imeiPos) = 0
			BEGIN
			SET @imeiPos = 'E' + @serialPos
			END
			

			--INICIALIZAR PARÁMETROS DE SALIDA / VARIABLES LOCALES
			SELECT @rspCode = '', @rspMessage = '', @dloadScheduledDatetime = '', @dloadIp = '', @dloadPort = '', @dloadApplicationList = '', @transactionId = NULL, @FlagCheckForNewApps = 0, @FlagContinueProcess = 0;

			--SETEAR AUTOLEARNING FLAG
			--SELECT @autoLearning = 1
			SELECT @autoLearning = cli_autoaprendizaje, @CustomerId = cli_id FROM Cliente WHERE cli_nombre = @customerName;

			--VALIDAR CLIENTE
			IF EXISTS( SELECT * FROM Cliente WHERE cli_nombre = @customerName )
			BEGIN

				SELECT @ExpLicenseDatetime = DATEADD(DAY, TL.numero_dias + 1, lic_fecha_creacion) FROM LicenciaXCliente INNER JOIN TipoLicencia TL ON (lic_tipo_licencia_id = TL.id)  WHERE lic_cliente_id = (SELECT TOP 1 cli_id FROM Cliente WHERE cli_nombre = @customerName) AND lic_tipo_estado_id = 1 /*ACTIVA*/

				--VALIDAR FECHA DE EXPIRACIÓN DE LA LICENCIA POR CLIENTE
				IF NOT @ExpLicenseDatetime IS NULL AND @ExpLicenseDatetime > GETDATE()
				BEGIN
				    
					SET @FlagContinueProcess = 1	--CONTINUAR PROCESO

					--AUTOAPRENDIZAJE, VALIDAR FLAG SETEADO POR VARIABLE
					IF NOT EXISTS( SELECT * FROM Terminal WHERE ter_serial = @serialPos ) AND @autoLearning = 1
					BEGIN
						--CREAR TERMINAL SI EXISTE AL MENOS UN GRUPO CREADO PARA EL CLIENTE
						SELECT @CustomerId = cli_id FROM Cliente WHERE cli_nombre = @customerName

						--VALIDAR CLIENTE
						IF NOT @CustomerId IS NULL
						BEGIN


						    --VALIDAR QUE EXISTA EL GRUPO INVENTARIO EN AUTOLEARNING, EN CASO CONTRARIO DETENER EL PROCESO
							SELECT @groupNumber = count(*) FROM Grupo G INNER JOIN Cliente C ON (C.cli_id = G.gru_cliente_id) WHERE (C.cli_nombre = @customerName AND G.gru_tipo_android = 0) AND ( G.gru_nombre = 'DEFAULT_GROUP_5210' OR G.gru_nombre = 'DEFAULT_GROUP_9220')
							IF @groupNumber != 2 
							BEGIN
							  SELECT @rspCode = '01', @rspMessage = 'ACTUALIZACION DECLINADA - MODO APRENDIZAJE CONFIGURADO, SE DEBE CONFIGURAR EL GRUPO INVENTARIO'
							  SET @FlagCheckForNewApps = 0
							  SET @FlagContinueProcess = 0	--DETENER PROCESO
							  SET @GrupoId = NULL
							END
							ELSE
							BEGIN
							  	--OBTENER DATOS DEL GRUPO
							    SELECT TOP 1 @GrupoId = G.gru_id FROM Grupo G INNER JOIN Cliente C ON (C.cli_id = G.gru_cliente_id) WHERE (C.cli_nombre = @customerName AND G.gru_tipo_android = 0) AND ( G.gru_nombre = 'DEFAULT_GROUP_5210' OR G.gru_nombre = 'DEFAULT_GROUP_9220')
							END	
					
							
							--SE CREA TERMINAL SOLO SI EXISTE AL MENOS UN GRUPO CREADO PARA EL CLIENTE
							IF NOT @GrupoId IS NULL
							BEGIN 
								--CONSULTAR DATOS DE TIPO/MARCA/MODELO/TIPO COMUNICACIÓN/MAC/UID/NOMBRE_POS REPORTADO POR LA TERMINAL
								SET @String = @isoField61
								SET @String = @String + @Delimiter
								SET @Pos = CHARINDEX(@Delimiter,@String)
								SET @counterIdx = 0

								WHILE (@pos <> 0)
								BEGIN
									SET @NextString = SUBSTRING(@String,1,@Pos - 1)
									SET @String = SUBSTRING(@String,@pos+1,LEN(@String))
									SET @pos = CHARINDEX(@Delimiter,@String)
									SET @counterIdx = @counterIdx + 1
									IF @counterIdx = 2
										SET @posType = @NextString
									ELSE IF @counterIdx = 3
										SET @posBrand = @NextString
									ELSE IF @counterIdx = 4
										SET @posModel = @NextString
									ELSE IF @counterIdx = 5
										SET @posCommType = @NextString
									ELSE IF @counterIdx = 7
										SET @posMac = @NextString
									ELSE IF @counterIdx = 20
										SET @posUid = @NextString
									ELSE IF @counterIdx = 21
										SET @posName = @NextString
								END

								SELECT @posTypeId = id FROM TipoTerminal WHERE descripcion = @posType
								SELECT @posBrandId = id FROM MarcaTerminal WHERE descripcion = @posBrand
								SELECT @posModelId = id FROM ModeloTerminal WHERE descripcion = @posModel
								SELECT @posCommTypeId = id FROM InterfaceTerminal WHERE descripcion = @posCommType

								IF @posModelId IS NULL
								BEGIN
								    SELECT @rspCode = '01', @rspMessage = 'ACTUALIZACION DECLINADA - MODELO DE TERMINAL ' + @posModel + ' NO EXISTE'
									SET @FlagCheckForNewApps = 0
									SET @FlagContinueProcess = 0	--DETENER PROCESO
									SET @GrupoId = NULL
								END


								--VALORES POR DEFECTO
								IF @posTypeId IS NULL SET @posTypeId = 1 /*POS*/
								IF @posBrandId IS NULL SET @posBrandId = 1 /*SPECTRA*/
								--IF @posModelId IS NULL SET @posModelId = 3 /*T1000*/
								IF @posCommTypeId IS NULL SET @posCommTypeId = 1 /*GPRS*/


								--VALIDAR QUE EL IMEI NO ESTE REPETIDO
								select @imeiNumber = count(*) from dbo.terminal where ter_serial != @serialPos and ter_imei = @imeiPos
								IF @imeiNumber != 0
								BEGIN
									SELECT @rspCode = '01', @rspMessage = 'ACTUALIZACION DECLINADA - MODO APRENDIZAJE CONFIGURADO, EL IMEI: ' + @imeiPos + ' YA EXISTE'
									SET @FlagCheckForNewApps = 0
									SET @FlagContinueProcess = 0	--DETENER PROCESO
									SET @GrupoId = NULL
								END
								ELSE
								BEGIN	
								
								--MODIFICACION DE GRUPO SEGUN LA MARCA DE LA TERMINAL 03/12/2019
								--MODELO LEGACY
								IF @posModel = 'NEW9220'
								BEGIN
								 SELECT TOP 1 @GrupoId = G.gru_id FROM Grupo G INNER JOIN Cliente C ON (C.cli_id = G.gru_cliente_id) WHERE (C.cli_nombre = @customerName AND G.gru_tipo_android = 0) AND G.gru_nombre = 'DEFAULT_GROUP_9220'					
								END

								--MODELO T1000
								IF @posModel = 'NEW5210'
								BEGIN
								 SELECT TOP 1 @GrupoId = G.gru_id FROM Grupo G INNER JOIN Cliente C ON (C.cli_id = G.gru_cliente_id) WHERE (C.cli_nombre = @customerName AND G.gru_tipo_android = 0) AND G.gru_nombre = 'DEFAULT_GROUP_5210'
								END
									IF NOT @GrupoId IS NULL
									BEGIN 
								  --CREAR TERMINAL X GRUPO
								  INSERT INTO [dbo].[Terminal]([ter_serial], [ter_tipo_terminal_id], [ter_marca_terminal_id], [ter_modelo_terminal_id], [ter_interface_terminal_id], [ter_serial_sim], [ter_telefono_sim], [ter_imei], [ter_serial_cargador]
											,[ter_serial_bateria], [ter_serial_lector_biometrico], [ter_descripcion], [ter_grupo_id], [ter_tercero_id], [ter_prioridad_grupo], [ter_actualizar], [ter_fecha_programada_actualizacion]
											,[ter_ip_descarga], [ter_puerto_descarga], [ter_estado_actualizacion_id], [ter_mensaje_actualizacion], [ter_tipo_estado_id], [ter_validar_imei], [ter_validar_sim], [ter_fecha_creacion], [ter_codigo_producto],ter_mac,ter_UID,ter_nombre_pos)
								  VALUES (@serialPos, @posTypeId, @posBrandId, @posModelId, @posCommTypeId, @simPos, '', @imeiPos, '', '', '', '', @GrupoId, NULL, 1,
										0, NULL, '', '', 1, '', 1, 0, 0, GETDATE(), @ter_codigo_producto, @posMac,@posUid,@posName)  
								END
								ELSE
									BEGIN
										SELECT @rspCode = '01', @rspMessage = 'ACTUALIZACION DECLINADA - GRUPO NO EXISTE PARA EL MODELO: ' + @posModel
										SET @FlagCheckForNewApps = 0
										SET @FlagContinueProcess = 0	--DETENER PROCESO
										SET @GrupoId = NULL
								END
							END

						END

					END
				END

					--CONSULTAR DATOS DE TIPO/MARCA/MODELO/TIPO COMUNICACIÓN/MAC/UID/NOMBRE_POS REPORTADO POR LA TERMINAL
					SET @String = @isoField61
					SET @String = @String + @Delimiter
					SET @Pos = CHARINDEX(@Delimiter,@String)
					SET @counterIdx = 0

					WHILE (@pos <> 0)
					BEGIN
						SET @NextString = SUBSTRING(@String,1,@Pos - 1)
						SET @String = SUBSTRING(@String,@pos+1,LEN(@String))
						SET @pos = CHARINDEX(@Delimiter,@String)
						SET @counterIdx = @counterIdx + 1
						IF @counterIdx = 2
							SET @posType = @NextString
						ELSE IF @counterIdx = 3
							SET @posBrand = @NextString
						ELSE IF @counterIdx = 4
							SET @posModel = @NextString
						ELSE IF @counterIdx = 5
							SET @posCommType = @NextString
						ELSE IF @counterIdx = 7
							SET @posMac = @NextString
						ELSE IF @counterIdx = 20
							SET @posUid = @NextString
						ELSE IF @counterIdx = 21
							SET @posName = @NextString
					END

					SELECT @posModelId = id FROM ModeloTerminal WHERE descripcion = @posModel
		


					--VALIDAR QUE EXISTA EL MODELO.  SI NO EXISTE EL MODELO SE DECLINA LA TRANSACCION
					SELECT @posModelId = id FROM ModeloTerminal WHERE descripcion = @posModel
					IF @posModelId IS NULL
					BEGIN
						SELECT @rspCode = '01', @rspMessage = 'ACTUALIZACION DECLINADA - MODELO DE TERMINAL ' + @posModel + ' NO EXISTE'
						SET @FlagCheckForNewApps = 0
						SET @FlagContinueProcess = 0	--DETENER PROCESO
						SET @GrupoId = NULL
					END

					--VALIDAR QUE EL IMEI NO ESTE REPETIDO

					IF @FlagContinueProcess = 1 
					BEGIN
					select @imeiNumber = count(*) from dbo.terminal where ter_serial != @serialPos and ter_imei = @imeiPos
					IF @imeiNumber != 0
					BEGIN
						SELECT @rspCode = '01', @rspMessage = 'ACTUALIZACION DECLINADA - EL IMEI: ' + @imeiPos + ' YA EXISTE'
						SET @FlagCheckForNewApps = 0
						SET @FlagContinueProcess = 0	--DETENER PROCESO
						SET @GrupoId = NULL
					END
				END

					--VALIDAR TERMINAL
					IF @FlagContinueProcess = 1 
					BEGIN
						IF (EXISTS( SELECT * FROM Terminal WHERE ter_serial = @serialPos ))
						BEGIN

							--ACTUALIZAR VALORES REPORTADOS POR EL AGENTE DEL POS SIEMPRE QUE NO ESTÉN VACÍOS
							UPDATE Terminal 
								SET ter_imei = (CASE WHEN @imeiPos = '' THEN ter_imei ELSE @imeiPos END),
									ter_serial_sim = (CASE WHEN @simPos = '' THEN ter_serial_sim ELSE @simPos END),
									ter_latitud_gps = (CASE WHEN @latitude = '' THEN ter_latitud_gps ELSE @latitude END),
									ter_longitud_gps = (CASE WHEN @longitude = '' THEN ter_longitud_gps ELSE @longitude END),
									ter_apn = (CASE WHEN @apn = '' THEN ter_apn ELSE @apn END),
									ter_nivel_gprs = (CASE WHEN @signalLevel = '' THEN ter_nivel_gprs ELSE @signalLevel END),
									ter_nivel_bateria = (CASE WHEN @batteryLevel = '' THEN ter_nivel_bateria ELSE @batteryLevel END),
									ter_aplicaciones_instaladas = (CASE WHEN @applicationListPos = '' THEN ter_aplicaciones_instaladas ELSE @applicationListPos END),
									ter_fecha_actualizacion_auto = GETDATE(),
									ter_codigo_producto = (CASE WHEN @ter_codigo_producto = '' THEN ter_codigo_producto ELSE @ter_codigo_producto END),
									ter_mac = @posMac, ter_UID = @posUid, ter_nombre_pos = @posName
							WHERE ter_serial = @serialPos

							--VALIDAR TERMINAL / GRUPO / CLIENTE
							IF EXISTS( SELECT * FROM Terminal T INNER JOIN Grupo G ON (G.gru_id = T.ter_grupo_id) INNER JOIN Cliente C ON (C.cli_id = G.gru_cliente_id) WHERE T.ter_serial = @serialPos AND C.cli_nombre = @customerName )
							BEGIN

								--OBTENER DATOS DEL GRUPO Y DE LA TERMINAL
								SELECT @TerminalId = T.ter_id, @FlagUpdTerminal = T.ter_actualizar, 
										@GrupoId = G.gru_id, 
										@claveMDM = G.gru_clave_configurarMDM,
										@FlagUpdGrupo = G.gru_actualizar, 
										@groupUpdateNow = G.gru_update_now, 
										@terminalUpdateNow = T.ter_update_now,
										@FlagPrioridadGrupo = T.ter_prioridad_grupo,
										@FlagProgramarFecha = G.gru_programar_fecha,
										@GrupoActualizarFechaIni = G.gru_fecha_programada_actualizacion_ini,
										@GrupoActualizarFechaFin = G.gru_fecha_programada_actualizacion_fin,
										@GrupoRangoHoraIni = G.gru_rango_hora_actualizacion_ini,
										@GrupoRangoHoraFin = G.gru_rango_hora_actualizacion_fin,
										@TerminalStatus = T.ter_tipo_estado_id,
										@FlagValidateImei = T.ter_validar_imei,
										@FlagValidateSim = T.ter_validar_sim,
										@TerminalImei = T.ter_imei,
										@TerminalSim = T.ter_serial_sim
								FROM Terminal T 
									INNER JOIN Grupo G ON (G.gru_id = T.ter_grupo_id)
								WHERE T.ter_serial = @serialPos
								--COLACAR TIPO CONFIGURACION
								IF  @FlagPrioridadGrupo = '1'
								BEGIN
								SET @TIPO_COFIGURACION = ' - CONFIGURACION POR GRUPO'
								END
								ELSE
								 BEGIN	
									SET @TIPO_COFIGURACION = ' - CONFIGURACION POR TERMINAL'
								END
								--VALIDAR FLAGS DE LA TERMINAL
								IF @TerminalStatus = 1
								BEGIN

									--SET @FlagContinueProcess = 1	--CONTINUAR PROCESO

									IF @FlagValidateImei = 1
									BEGIN
										IF LTRIM(RTRIM(@TerminalImei)) = LTRIM(RTRIM(@imeiPos))
										BEGIN
											SET @FlagContinueProcess = 1	--CONTINUAR PROCESO
										END
										ELSE
										BEGIN
											SELECT @rspCode = '01', @rspMessage = 'ACTUALIZACION DECLINADA - VALOR DE IMEI ERRADO'
											SET @FlagCheckForNewApps = 0
											SET @FlagContinueProcess = 0	--DETENER PROCESO
										END
									END
							
									IF @FlagValidateSim = 1 AND @FlagContinueProcess = 1
									BEGIN
										IF LTRIM(RTRIM(@TerminalSim)) = LTRIM(RTRIM(@simPos))
										BEGIN
											SET @FlagContinueProcess = 1	--CONTINUAR PROCESO
										END
										ELSE
										BEGIN
											SELECT @rspCode = '01', @rspMessage = 'ACTUALIZACION DECLINADA - VALOR DE SERIAL SIM ERRADO'
											SET @FlagCheckForNewApps = 0
											SET @FlagContinueProcess = 0	--DETENER PROCESO
										END
									END	
								END
								ELSE
								BEGIN
									SELECT @rspCode = '01', @rspMessage = 'ACTUALIZACION DECLINADA - ESTADO TERMINAL INACTIVO'
									SET @FlagCheckForNewApps = 0
									SET @FlagContinueProcess = 0	--DETENER PROCESO
								END

								--VALIDAR SI SE DEBE CONTINUAR CON EL PROCESO
								IF @FlagContinueProcess = 1
								BEGIN												

									--VALIDAR SI TIENE PRIORIDAD EL GRUPO EN LA TERMINAL
									IF @FlagPrioridadGrupo = 1
									BEGIN 
										--VALIDAR DATOS DE GRUPO
										IF @FlagUpdGrupo = 1
										BEGIN
											--VALIDAR SI ESTÁ ACTIVADA OPCIÓN POR FECHAS
											IF @FlagProgramarFecha = 1
											BEGIN
												--FECHA_HORA ACTUAL ESTÁ DENTRO DEL RANGO PARA ACTUALIZAR
												SET @FlagCheckForNewApps = 1

												--VALIDAR FECHA_INI Y FECHA_FIN CON RANGO DE HORAS
												IF CONVERT(DATE,GETDATE()) BETWEEN CONVERT(DATE, @GrupoActualizarFechaIni) AND CONVERT(DATE,@GrupoActualizarFechaFin) AND
													CONVERT(TIME,GETDATE()) BETWEEN CONVERT(TIME, @GrupoActualizarFechaIni) AND CONVERT(TIME,@GrupoActualizarFechaFin)
												BEGIN
													--FECHA_HORA ACTUAL ESTÁ DENTRO DEL RANGO PARA ACTUALIZAR
													SET @FlagCheckForNewApps = 1
												END
												ELSE IF CONVERT(DATE,GETDATE()) > CONVERT(DATE,@GrupoActualizarFechaFin)
												BEGIN
													SELECT @rspCode = '01', @rspMessage = 'ACTUALIZACION DECLINADA CONFIGURACION X GRUPO - FECHA EXPIRO'
													SET @FlagCheckForNewApps = 0
												END
												ELSE
												BEGIN
													SELECT @rspCode = '01', @rspMessage = 'ACTUALIZACION DECLINADA CONFIGURACION X GRUPO - FECHA_HORA FUERA DE RANGO'
													SET @FlagCheckForNewApps = 0
												END
											END
											ELSE
											BEGIN
												--VALIDAR SOLO RANGO DE HORAS
												IF CONVERT(TIME, GETDATE()) BETWEEN @GrupoRangoHoraIni AND @GrupoRangoHoraFin
												BEGIN
													--HORA ACTUAL ESTÁ DENTRO DEL RANGO PARA ACTUALIZAR
													SET @FlagCheckForNewApps = 1
												END
												ELSE
												BEGIN
													SELECT @rspCode = '01', @rspMessage = 'ACTUALIZACION DECLINADA CONFIGURACION X GRUPO - HORA FUERA DE RANGO'
													SET @FlagCheckForNewApps = 0
												END
											END
										END
										ELSE
										BEGIN
											SELECT @rspCode = '01', @rspMessage = 'GRUPO CONFIGURADO PARA NO ACTUALIZAR'
											SET @FlagCheckForNewApps = 0
										END
									END
									ELSE
									BEGIN
										--VALIDAR DATOS DE TERMINAL

										---VALIDAR SI LA TERMINAL ESTÁ ACTIVA PARA ACTUALIZAR
										IF @FlagUpdTerminal = 1
										BEGIN
											--SE DEBE ACTUALIZAR TERMINAL, COMPARAR APLICACIONES Y CHECKSUM
											SET @FlagCheckForNewApps = 1
										END
										ELSE
										BEGIN
											SELECT @rspCode = '01', @rspMessage = 'TERMINAL CONFIGURADO PARA NO ACTUALIZAR'
											SET @FlagCheckForNewApps = 0
										END
									END

									--VALIDAR SI SE DEBE BUSCAR NUEVAS APLICACIONES A INSTALAR
									IF @FlagCheckForNewApps = 1
									BEGIN
										--DESEMPAQUETAR EN TABLA LISTA DE APLICACIONES REPORTADAS POR EL POS
										INSERT INTO @AppListTablePos
											SELECT * FROM SepararListaAplicacionesAndroid(@applicationListPos, @separatorApp, @separatorChecksum);
												

										--CONSULTAR APLICACIONES X GRUPO SEGÚN TERMINAL SERIAL
										DECLARE @APP INT
									    DECLARE @VALIDATOR_APP VARCHAR(100)
										SET @VALIDATOR_APP = (SELECT ((SELECT TOP 1  lTRIM(GXA.desp_nombre_paquete)+ '-' + GXA.desp_version FROM  GrupoXArchivosDesplegados GXA
										INNER JOIN Grupo G ON G.gru_id = gxa.desp_grupo_id 
										INNER JOIN Terminal T ON T.ter_grupo_id = gxa.desp_grupo_id WHERE T.ter_serial = @serialPos
										ORDER BY GXA.desp_fecha DESC))
										FROM Terminal T INNER JOIN Grupo G ON (T.ter_grupo_id = G.gru_id)
										WHERE T.ter_serial =@serialPos)

											IF @VALIDATOR_APP IS NULL 
												BEGIN 
												SET @APP = 0
											END
										ELSE
										BEGIN

										INSERT INTO @AppListTableDB
										SELECT lTRIM(GXA.desp_nombre_paquete)+ '-' + GXA.desp_version,GXA.desp_nombre_archivo
										FROM  GrupoXArchivosDesplegados GXA
										INNER JOIN Grupo G ON G.gru_id = gxa.desp_grupo_id 
										INNER JOIN Terminal T ON T.ter_grupo_id = gxa.desp_grupo_id WHERE T.ter_serial = @serialPos

										SET @APP = 1
									END
									
					
										--COMPARAR NOMBRE DE APLICACIÓN Y CHECKSUM

										IF @APP = 1
										BEGIN
										SET @Counter = 1

										WHILE (@Counter <= (SELECT MAX(Id) FROM @AppListTableDB))
										BEGIN
										DECLARE @AppNameApk VARCHAR(MAX)
											SELECT @AppNameDB = AppName, @AppListStringDB = @AppListStringDB + AppName + @SeparatorApp  FROM @AppListTableDB WHERE Id = @Counter
											SELECT @AppNameApk = Apk, @AppListStringDB = @AppListStringDB + AppName + @SeparatorApp  FROM @AppListTableDB WHERE Id = @Counter

											--BUSCAR POR NOMBRE DE APLICACIÓN EN LA LISTA DEL POS

											IF EXISTS(SELECT * FROM @AppListTablePos LP WHERE CONCAT(LP.AppName,'-', LP.AppChecksum) = @AppNameDB )
											BEGIN
												--ENCONTRADO, COMPARAR CHECKSUM
										    
												IF (SELECT CONCAT(LP.AppName,'-' ,LP.AppChecksum) FROM @AppListTablePos LP WHERE CONCAT(LP.AppName,'-',LP.AppChecksum) = @AppNameDB) <> @AppNameDB
												BEGIN
													
													--CHECKSUM DIFERENTES, ACTUALIZAR APPLICACION
													SET @dloadApplicationList = @dloadApplicationList + @AppNameDB + @SeparatorApp
													
												END
											END
											ELSE
											BEGIN
											
												--NO EXISTE ACTUALIZACIÓN EN LISTA DE POS, ACTUALIZARLA
												SET @dloadApplicationList = @dloadApplicationList + (SELECT STUFF((SELECT @SeparatorApp + CONCAT(G.gru_nombre ,'\' , @AppNameApk)
												FROM Grupo G
												INNER JOIN Terminal T on T.ter_grupo_id = G.gru_id WHERE T.ter_serial = @serialPos
												FOR XML PATH ('')),1,1,'')) + @SeparatorApp
				
											END

											SET @Counter = @Counter + 1
										END
									END
									ELSE
									BEGIN
									SELECT @rspCode = '00', @rspMessage = 'ACTUALIZACION DECLINADA - APLICACION NO DESPLEGADA EN EL GRUPO', @updateStatus = 1 
										SET @FlagContinueProcess = 0	--DETENER PROCESO
										------------------------------------------
										SELECT @dloadApplicationList = 'NA'

										IF @FlagPrioridadGrupo = 1 
										  BEGIN
											SELECT 
												@group = g.gru_nombre,
												@dloadIp = G.gru_ip_descarga,
												@dloadPort = G.gru_puerto_descarga,
												@bloqueoDispositivo = T.ter_bloqueo,
												@nuevaClave = T.ter_clave_bloqueo,
												@consultasXhora = CONCAT(G.gru_numero_consultas ,',',G.gru_frecuencia_horas),					
												@mensaje = G.gru_mensaje_desplegar,
												@listaBlanca = G.gru_aplicaciones_permitidas,					
												@modoKiosko = G.gru_aplicacion_kiosko
												--@reserved2 = CASE WHEN ter_flag_inicializacion = 1 THEN 'INICIALIZAR'
												--WHEN ter_flag_inicializacion = 0 THEN 'NA' END
												

											FROM Terminal T INNER JOIN Grupo G ON (T.ter_grupo_id = G.gru_id)
												WHERE T.ter_serial = @serialPos 
										 END
									 ELSE
									  BEGIN
									    SELECT 
												@group = g.gru_nombre,
												@dloadIp = T.ter_ip_descarga,
												@dloadPort = t.ter_puerto_descarga,
												@bloqueoDispositivo = T.ter_bloqueo,
												@nuevaClave = T.ter_clave_bloqueo,				
												@mensaje = T.ter_mensaje_desplegar,
												@listaBlanca = T.ter_aplicaciones_permitidas,					
												@modoKiosko = T.ter_aplicacion_kiosko,	@consultasXhora = CONCAT(G.gru_numero_consultas ,',',G.gru_frecuencia_horas)
												--@reserved2 = CASE WHEN ter_flag_inicializacion = 1 THEN 'INICIALIZAR'
												--WHEN ter_flag_inicializacion = 0 THEN 'NA' END
										FROM Terminal T INNER JOIN Grupo G ON (T.ter_grupo_id = G.gru_id)
												WHERE T.ter_serial = @serialPos 
									END

												UPDATE Terminal
											SET ter_estado_actualizacion_id = 1 /*Actualizado*/
											WHERE ter_id = @TerminalId

									END


										--VALIDAR SI HAY APLICACIONES POR ACTUALIZAR PARA LA TERMINAL
										IF @FlagContinueProcess = 1
										BEGIN
										IF @dloadApplicationList IS NULL
										BEGIN
											SET @dloadApplicationList = ''
										END

										IF LEN(@dloadApplicationList) > 0
										BEGIN

											--VALIDAR SI LA PROGRAMACIÓN ES POR GRUPO O POR TERMINAL
											IF @FlagPrioridadGrupo = 1
											BEGIN
										
										--IF @reservedUse3 = 'INICIALIZACION_EXITOSA'
										--BEGIN
										--	UPDATE Terminal 
										--	SET ter_flag_inicializacion = 0,
										--	ter_fecha_ultima_inicializacion = GETDATE() 
										--	WHERE ter_serial = @serialPos
										--END
										
												--BUSCAR DATOS DE PROGRAMACIÓN / IP / PUERTO EN GRUPO
												SELECT @rspCode = '00', @rspMessage = 'ACTUALIZACION APROBADA CONFIGURACION X GRUPO', @UpdateStatus = 2 /*Pendiente*/
												SELECT 
												@group = g.gru_nombre,
												@dloadIp = G.gru_ip_descarga,
												@dloadPort = G.gru_puerto_descarga,
												@bloqueoDispositivo = T.ter_bloqueo,
												@nuevaClave = T.ter_clave_bloqueo,
												@consultasXhora = CONCAT(G.gru_numero_consultas ,',',G.gru_frecuencia_horas),					
												@mensaje = G.gru_mensaje_desplegar,
												@listaBlanca = G.gru_aplicaciones_permitidas,					
												@modoKiosko = G.gru_aplicacion_kiosko
												--@reserved2 = CASE WHEN ter_flag_inicializacion = 1 THEN 'INICIALIZAR'
												--WHEN ter_flag_inicializacion = 0 THEN 'NA' END
												

											FROM Terminal T INNER JOIN Grupo G ON (T.ter_grupo_id = G.gru_id)
												WHERE T.ter_serial = @serialPos 
											
												--BUSCAR FECHA HORA, IP Y PUERTO DE DESCARGA POR GRUPO
												IF @groupUpdateNow = 0
												BEGIN
												  SELECT 
													--POR RANGO DE HORAS -> DESPUES DE 5 MINUTOS SE PROGRAMA LA DESCARGA - SEGÚN HORA ACTUAL DE CONSULTA
													--POR RANGO DE FECHAS -> AL SIGUIENTE DÍA SE PROGRAMA LA DESCARGA - SEGÚN HORA ACTUAL DE CONSULTA
													   @dloadScheduledDatetime = (CASE WHEN @FlagProgramarFecha = 1 AND GETDATE() BETWEEN G.gru_fecha_programada_actualizacion_ini AND G.gru_fecha_programada_actualizacion_fin THEN (CONVERT(VARCHAR, DATEADD(DAY, 1, GETDATE()), 112) + REPLACE(CONVERT(VARCHAR, DATEADD(DAY, 1, GETDATE()), 108), ':', '')) WHEN @FlagProgramarFecha = 0 AND CONVERT(TIME, GETDATE()) BETWEEN gru_rango_hora_actualizacion_ini AND gru_rango_hora_actualizacion_fin THEN (CONVERT(VARCHAR, DATEADD(MI,5,GETDATE()), 112) + REPLACE(CONVERT(VARCHAR, DATEADD(MI,5,GETDATE()), 108), ':', '')) ELSE 'NA' END),
													   @dloadIp = G.gru_ip_descarga,
													   @dloadPort = G.gru_puerto_descarga
													   --@blockingMode = G.gru_blocking_mode
												  FROM Terminal T 
													INNER JOIN Grupo G ON (G.gru_id = T.ter_grupo_id)
												  WHERE T.ter_id = @TerminalId
												END
												ELSE
												BEGIN
												  SELECT 
													--POR RANGO DE HORAS -> DESPUES DE 5 MINUTOS SE PROGRAMA LA DESCARGA - SEGÚN HORA ACTUAL DE CONSULTA
													--POR RANGO DE FECHAS -> AL SIGUIENTE DÍA SE PROGRAMA LA DESCARGA - SEGÚN HORA ACTUAL DE CONSULTA
													   @dloadScheduledDatetime = 'NA',
													   @dloadIp = G.gru_ip_descarga,
													   @dloadPort = G.gru_puerto_descarga
													   --@blockingMode = G.gru_blocking_mode
												  FROM Terminal T 
													INNER JOIN Grupo G ON (G.gru_id = T.ter_grupo_id)
												   WHERE T.ter_id = @TerminalId
												END
												
											END
											ELSE
											BEGIN
											

												--BUSCAR DATOS DE PROGRAMACIÓN / IP / PUERTO EN TERMINAL
												SELECT @rspCode = '00', @rspMessage = 'ACTUALIZACION APROBADA CONFIGURACION X TERMINAL', @UpdateStatus = 2 /*Pendiente*/
												SELECT 
												@group = g.gru_nombre,
												@dloadIp = T.ter_ip_descarga,
												@dloadPort = t.ter_puerto_descarga,
												@bloqueoDispositivo = T.ter_bloqueo,
												@nuevaClave = T.ter_clave_bloqueo,				
												@mensaje = T.ter_mensaje_desplegar,
												@listaBlanca = T.ter_aplicaciones_permitidas,					
												@modoKiosko = T.ter_aplicacion_kiosko,	@consultasXhora = CONCAT(G.gru_numero_consultas ,',',G.gru_frecuencia_horas)
												--@reserved2 = CASE WHEN ter_flag_inicializacion = 1 THEN 'INICIALIZAR'
												--WHEN ter_flag_inicializacion = 0 THEN 'NA' END
										FROM Terminal T INNER JOIN Grupo G ON (T.ter_grupo_id = G.gru_id)
												WHERE T.ter_serial = @serialPos 

												
												--BUSCAR FECHA HORA, IP Y PUERTO DE DESCARGA POR TERMINAL
												IF @terminalUpdateNow = 0
												BEGIN
												  SELECT 
													@dloadScheduledDatetime = (CASE WHEN T.ter_fecha_programada_actualizacion < GETDATE() THEN 'NA' ELSE (CONVERT(VARCHAR, T.ter_fecha_programada_actualizacion, 112) + REPLACE(CONVERT(VARCHAR, T.ter_fecha_programada_actualizacion, 108), ':', '')) END), 
													@dloadIp = T.ter_ip_descarga,
													@dloadPort = T.ter_puerto_descarga
													--@blockingMode = T.ter_blocking_mode
												  FROM Terminal T WHERE T.ter_id = @TerminalId
												END
												ELSE
												BEGIN
												 SELECT 
													@dloadScheduledDatetime = 'NA', 
													@dloadIp = T.ter_ip_descarga,
													@dloadPort = T.ter_puerto_descarga
													--@blockingMode = T.ter_blocking_mode
												  FROM Terminal T WHERE T.ter_id = @TerminalId
												END
												
											END

											--CAMBIAR ESTADO A REGISTRO DE TERMINAL
											UPDATE Terminal
											SET ter_estado_actualizacion_id = 2 /*Pendiente*/, ter_mensaje_actualizacion = 'APLICACIONES PENDIENTES: ' + REPLACE(@dloadApplicationList, CHAR(10), '{LF}')
											WHERE ter_id = @TerminalId

										END
										ELSE
										BEGIN
										SELECT @rspCode = '00', @rspMessage = CONCAT('TERMINAL ACTUALIZADO',@TIPO_COFIGURACION) , @updateStatus = 1 /*Actualizado*/
								
										 IF @FlagPrioridadGrupo = '0'
											BEGIN
											SELECT @dloadApplicationList = 'NA'
											SELECT
												@group = g.gru_nombre,
												@dloadIp = T.ter_ip_descarga,
												@dloadPort = t.ter_puerto_descarga,
												@bloqueoDispositivo = T.ter_bloqueo,
												@nuevaClave = T.ter_clave_bloqueo,		
												@mensaje = T.ter_mensaje_desplegar,
												@listaBlanca = T.ter_aplicaciones_permitidas,					
												@modoKiosko = T.ter_aplicacion_kiosko,
												@dloadScheduledDatetime = 'NA',
												@consultasXhora = CONCAT(G.gru_numero_consultas ,',',G.gru_frecuencia_horas)
												--@reserved2 = CASE WHEN ter_flag_inicializacion = 1 THEN 'INICIALIZAR'
												--WHEN ter_flag_inicializacion = 0 THEN 'NA' END
										FROM Terminal T INNER JOIN Grupo G ON (T.ter_grupo_id = G.gru_id)
												WHERE T.ter_serial = @serialPos 
										END
										ELSE 
										BEGIN
										
											SELECT @dloadApplicationList = 'NA'
											SELECT
												@group = g.gru_nombre,
												@dloadIp = G.gru_ip_descarga,
												@dloadPort = G.gru_puerto_descarga,
												@bloqueoDispositivo = T.ter_bloqueo,
												@nuevaClave = T.ter_clave_bloqueo,
												@consultasXhora = CONCAT(G.gru_numero_consultas ,',',G.gru_frecuencia_horas),					
												@mensaje = G.gru_mensaje_desplegar,
												@listaBlanca = G.gru_aplicaciones_permitidas,					
												@modoKiosko = G.gru_aplicacion_kiosko,
												@dloadScheduledDatetime = 'NA'
												--@reserved2 = CASE WHEN ter_flag_inicializacion = 1 THEN 'INICIALIZAR'
												--WHEN ter_flag_inicializacion = 0 THEN 'NA' END
											FROM Terminal T INNER JOIN Grupo G ON (T.ter_grupo_id = G.gru_id)
												WHERE T.ter_serial = @serialPos 

										END
										
											--CAMBIAR ESTADO A REGISTRO DE TERMINAL
											UPDATE Terminal
											SET ter_estado_actualizacion_id = 1 /*Actualizado*/, ter_mensaje_actualizacion = ''
											WHERE ter_id = @TerminalId

										END							
									END
								END
							END
						END
							ELSE
							BEGIN
								SELECT @rspCode = '01', @rspMessage = 'TERMINAL NO VALIDA PARA EL GRUPO'
							END

						END
						ELSE
						BEGIN
							SELECT @rspCode = '01', @rspMessage = 'TERMINAL NO EXISTE'
						END
					END
					

				END
				ELSE
				BEGIN
					SELECT @rspCode = '01', @rspMessage = 'LICENCIA DE USO EXPIRADA'

					--DESACTIVAR LICENCIA
					UPDATE LicenciaXCliente
					SET lic_tipo_estado_id = 2 /*INACTIVA*/
					WHERE lic_cliente_id = (SELECT TOP 1 cli_id FROM Cliente WHERE cli_nombre = @customerName)

				END

			END
			ELSE
			BEGIN
				SELECT @rspCode = '01', @rspMessage = 'CLIENTE NO EXISTE'
			END

			--FIN CUERPO DEL SP

		END

		IF @startTranCount = 0 
		BEGIN

			--REGISTRAR TRANSACCIÓN
			INSERT INTO TransaccionInventario( tra_fecha, tra_stan, tra_serial_pos, tra_terminal_id, tra_cliente_pos, tra_message_type, tra_processing_code,
						tra_codigo_respuesta, tra_mensaje_respuesta, tra_estado_actualizacion_id, tra_direccion_ip_pos, tra_direccion_ip_detectada, tra_puerto_tcp_atencion,
						tra_imei_pos, tra_sim_pos, tra_nivel_gprs, tra_nivel_bateria, tra_version_software_pos, tra_aplicaciones_pos, tra_aplicaciones_pendientes,
						tra_aplicaciones_a_instalar, tra_fecha_descarga, tra_ip_descarga, tra_puerto_descarga, tra_campo61_pos, tra_codigo_producto)
			VALUES (GETDATE(), @stan, @serialPos, @TerminalId, @customerName, @msgType, @procCode, @rspCode, @rspMessage, @UpdateStatus, @ipPos, @ipDetected, @tcpPortDetected,
						 @imeiPos, @simPos, @signalLevel, @batteryLevel, @softwareVersionPos, REPLACE(@applicationListPos, CHAR(10), '{LF}') , @AppListStringDB, @dloadApplicationList, @dloadScheduledDatetime, @dloadIp,
						 @dloadPort, @isoField61, @ter_codigo_producto);

			--REGISTRAR DATOS ADICIONALES EN LA TABLA DE TERMINALES
			UPDATE Terminal set 
					ter_fecha_consulta_sp = GETDATE(), 
					ter_aplicaciones_pendientes_sp = @AppListStringDB, 
					ter_ip_descarga_sp = @dloadIp, 
					ter_puerto_descarga_sp = @dloadPort, 
					ter_fecha_descarga_sp = @dloadScheduledDatetime 
			WHERE ter_id = @TerminalId;

			--OBTENER TRANSACCIÓN ID
			SET @transactionId = CAST(SCOPE_IDENTITY() AS VARCHAR(20))

			COMMIT TRANSACTION 
		END

		---ALMACENAMOS LAS ESTADISTICAS

		IF @statistics != ''
		DECLARE @RC int
		DECLARE @rs varchar(50)
		BEGIN
		  BEGIN TRY
			EXECUTE @RC = [dbo].[sp_swStoreStatistics] 
				@statistics
				,@CustomerId
				,@GrupoId
				,@TerminalId
				,@rs OUTPUT
		  END TRY
		  BEGIN CATCH
			PRINT ''
		  END CATCH
		END

	    ---ALMACENAMOS EL CONTADOR DE CONSULTAS
		BEGIN TRY
		  DECLARE @statNow varchar(100) = right('0000' + convert(varchar(30), datepart(year,getdate())), 4) + 
											right('00' + convert(varchar(30), datepart(month,getdate())), 2)  +
											right('00' + convert(varchar(30), datepart(day,getdate())), 2) + ',91,I,1'
	      DECLARE @RCs int
		  DECLARE @rss varchar(50)
	      EXECUTE @RCs = [dbo].[sp_swStoreStatistics] 
				@statNow
				,@CustomerId
				,@GrupoId
				,@TerminalId
				,@rss OUTPUT
		END TRY
		BEGIN CATCH
			PRINT ''
		END CATCH
		



	-----RETORNAMOS TAGS DE CONFIGURACION TCP Y CODIGO DE BARRAS
	--BEGIN 
	--DECLARE @tag1 as VARCHAR(2)= '01'
	--DECLARE @tag2 as VARCHAR(2)= '02'
	--DECLARE @tag3 as VARCHAR(2)= '03'
	--DECLARE @len1 as int
	--DECLARE @intentos as VARCHAR(50)
	--DECLARE @ID as VARCHAR(30)
	--DECLARE @OPERADOR as VARCHAR(30)
	--DECLARE @IPUNO as VARCHAR(30)
	--DECLARE @PUERTOUNO as VARCHAR(30)
	--DECLARE @IPDOS as VARCHAR(30)
	--DECLARE @PUERTODOS as VARCHAR(30) 
	--DECLARE @INFO as VARCHAR(100) 
	--DECLARE @INFOINTERNA  as VARCHAR(500) =''
	--DECLARE @INFOINTERNALEN  as int
	--DECLARE @binary as VARCHAR(10) 
	--select  @len1  =  len(opc_intentos), 
	--@intentos = opc_intentos from intentos_ip where opc_id=1
	----set @binary =  sys.fn_varbintohexstr(@len1)
	--set @binary = len(@len1)
	--if (@binary='3')
	--begin
	--set @binary = concat('0',@len1) 
	--end
	--if (@binary='2')
	--begin
	--set @binary = concat('00',@len1)
	--end
	--if (@binary='1')
	--begin
	--set @binary=  concat('000',@len1)
	--end
	--set @reserved1 = concat (@tag1,@binary,@intentos)
	--set @binary= ''
	--DECLARE @cursorv cursor
	--SET @cursorv = CURSOR FOR
	--SELECT RTRIM(opc_id),RTRIM(opc_operador),RTRIM(opc_ip_uno),RTRIM(opc_puerto_uno),RTRIM(opc_ip_dos),RTRIM(opc_puerto_dos)
	--FROM configuracion_tcp

	--OPEN @cursorv
	--FETCH NEXT FROM @cursorv
	--into @ID,@OPERADOR,@IPUNO,@PUERTOUNO,@IPDOS,@PUERTODOS
	--WHILE @@FETCH_STATUS = 0
	--BEGIN 
	--SELECT @INFO = concat (@ID,'@',@OPERADOR,'@',@IPUNO,'@',@PUERTOUNO,'@',@IPDOS,'@',@PUERTODOS)
	--set @INFOINTERNALEN = len(LTRIM(@INFO))
	----set @binary  = sys.fn_varbintohexstr(@INFOINTERNALEN)
	----SUBSTRING ( @binary ,len(@binary )-3 , len(@binary))
	--set @binary = len(@INFOINTERNALEN)
	--if (@binary='3')
	--begin
	--set @binary = concat('0',@INFOINTERNALEN) 
	--end
	--if (@binary='2')
	--begin
	--set @binary = concat('00',@INFOINTERNALEN)
	--end
	--if (@binary='1')
	--begin
	--set @binary=  concat('000',@INFOINTERNALEN)
	--end
	--set @INFOINTERNA = concat (LTRIM(@INFOINTERNA) ,@tag2,@binary,LTRIM(@INFO)) 
	--FETCH NEXT FROM @cursorv INTO @ID,@OPERADOR,@IPUNO,@PUERTOUNO,@IPDOS,@PUERTODOS
	--END
	--set @reserved1 = concat(RTRIM(@reserved1),RTRIM(@INFOINTERNA))
	--END
	
	set @reserved1 = 'NA'

		IF @group IS NULL
	BEGIN SET  @group = 'NA'
	END

	IF @dloadIp IS NULL
	BEGIN SET  @dloadIp = 'NA'
	END

	IF @dloadPort IS NULL
	BEGIN SET  @dloadPort = 'NA'
	END
	IF @consultasXhora IS NULL
	BEGIN SET  @consultasXhora = 'NA'
	END

	IF @modoKiosko IS NULL
	BEGIN SET  @modoKiosko = 'NA'
	END

		IF @mensaje IS NULL
	BEGIN SET  @mensaje = 'NA'
	END
		IF @listaBlanca IS NULL
	BEGIN SET  @listaBlanca = 'NA'
	END
		IF @nuevaClave IS NULL
	BEGIN SET  @nuevaClave = 'NA'
	END
		IF @bloqueoDispositivo IS NULL
	BEGIN SET  @bloqueoDispositivo = 'NA'
	END
		IF @reserved2 IS NULL
	BEGIN SET  @reserved2 = 'NA'
	END
	


		SET @configuracionTcp = @reserved1
		SET @reserved1 = 'NA'
		SET @messageServerIp = '192.168.0.10'
		SET @messageServerPort = '5020'
		SET @group = @group
		--SET @reserved2 = 'NA'
		SET @reserved3 = 'NA'
		SET @reserved4 = 'NA'
		SET @reserved5 = 'NA'
		SET @reserved6 = 'NA'
		SET @reserved7 = 'NA'
		SET @reserved8 = 'NA'
		SET @reserved9 = 'NA'
		SET @reserved10 = 'NA'
		--SET @rspCode = '00'
		--SET @rspMessage = 'ACTUALIZACION APROBADA CONFIGURACION X GRUPO'
		SET @dloadScheduledDatetime = @dloadScheduledDatetime
		SET @dloadIp = @dloadIp
		SET @dloadPort = @dloadPort
		SET @dloadApplicationList = @dloadApplicationList
		SET @transactionId = '5' 
		SET @listaBlanca = @listaBlanca
		SET @modoKiosko = @modoKiosko
		SET @mensaje = @mensaje
		SET @cambioClave = 'Y'
		SET @nuevaClave = @nuevaClave
		SET @consultasXhora = @consultasXhora
		SET @bloqueoDispositivo = @bloqueoDispositivo

		PRINT 'RangoXhoras '+ @dloadScheduledDatetime
		PRINT 'bloqueo pos '+ @bloqueoDispositivo
		PRINT 'consulta ' + @consultasXhora
		PRINT'nueva clave ' +  @nuevaClave
		PRINT 'mensaje ' + @mensaje
		PRINT 'modo kiosko ' + @modoKiosko
		PRINT 'lista blanca ' + @listaBlanca
		PRINT 'app a descargar ' + @dloadApplicationList
		PRINT 'configuracion tcp ' + @configuracionTcp
		PRINT 'grupo ' + @group
		PRINT 'ip ' + @dloadIp
		PRINT 'puerto ' + @dloadPort
		PRINT 'cabio clave ' + @cambioClave
	

	END TRY
	BEGIN CATCH
		IF XACT_STATE() <> 0 AND @startTranCount = 0 
			ROLLBACK TRANSACTION

					INSERT INTO [dbo].[LogErrorBaseDatos]([bderr_fecha],[bderr_nombre_funcion],[bderr_mensaje_error],[bderr_datos_adicionales])
			 VALUES (GETDATE(), OBJECT_NAME(@@PROCID), ERROR_MESSAGE(), /*Datos Extra, Parámetros del SP*/ 
									'stan = ' + @stan + ', serialPos = ' + @serialPos + ', customerName' + @customerName +
									'applicationListPos = ' + @applicationListPos + ', tcpPortDetected' + @tcpPortDetected)

		DECLARE @errMessage1 AS VARCHAR(4000) = ERROR_MESSAGE()
		DECLARE @errSeverity1 AS INT = ERROR_SEVERITY()
		DECLARE @errState1 AS INT = ERROR_STATE()

		RAISERROR (@errMessage1, @errSeverity1, @errState1)
	END CATCH

END
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Se realizan todas las validaciones relevantes para la transacción, si es válida, se registran los datos y la transacción se realiza en los terminales android, de lo contrario la transacción no es efectiva.', 'SCHEMA', N'dbo', 'PROCEDURE', N'sp_swValidarInventarioAppsTerminalAndroid'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Input variable', 'SCHEMA', N'dbo', 'PROCEDURE', N'sp_swValidarInventarioAppsTerminalAndroid', 'PARAMETER', N'@msgType'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Input variable', 'SCHEMA', N'dbo', 'PROCEDURE', N'sp_swValidarInventarioAppsTerminalAndroid', 'PARAMETER', N'@procCode'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Input variable', 'SCHEMA', N'dbo', 'PROCEDURE', N'sp_swValidarInventarioAppsTerminalAndroid', 'PARAMETER', N'@stan'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Input variable', 'SCHEMA', N'dbo', 'PROCEDURE', N'sp_swValidarInventarioAppsTerminalAndroid', 'PARAMETER', N'@serialPos'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Input variable', 'SCHEMA', N'dbo', 'PROCEDURE', N'sp_swValidarInventarioAppsTerminalAndroid', 'PARAMETER', N'@customerName'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Input variable', 'SCHEMA', N'dbo', 'PROCEDURE', N'sp_swValidarInventarioAppsTerminalAndroid', 'PARAMETER', N'@applicationListPos'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Input variable', 'SCHEMA', N'dbo', 'PROCEDURE', N'sp_swValidarInventarioAppsTerminalAndroid', 'PARAMETER', N'@ipPos'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Input variable', 'SCHEMA', N'dbo', 'PROCEDURE', N'sp_swValidarInventarioAppsTerminalAndroid', 'PARAMETER', N'@ipDetected'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Input variable', 'SCHEMA', N'dbo', 'PROCEDURE', N'sp_swValidarInventarioAppsTerminalAndroid', 'PARAMETER', N'@tcpPortDetected'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Input variable', 'SCHEMA', N'dbo', 'PROCEDURE', N'sp_swValidarInventarioAppsTerminalAndroid', 'PARAMETER', N'@imeiPos'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Input variable', 'SCHEMA', N'dbo', 'PROCEDURE', N'sp_swValidarInventarioAppsTerminalAndroid', 'PARAMETER', N'@simPos'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Input variable', 'SCHEMA', N'dbo', 'PROCEDURE', N'sp_swValidarInventarioAppsTerminalAndroid', 'PARAMETER', N'@latitude'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Input variable', 'SCHEMA', N'dbo', 'PROCEDURE', N'sp_swValidarInventarioAppsTerminalAndroid', 'PARAMETER', N'@longitude'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Input variable', 'SCHEMA', N'dbo', 'PROCEDURE', N'sp_swValidarInventarioAppsTerminalAndroid', 'PARAMETER', N'@apn'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Input variable', 'SCHEMA', N'dbo', 'PROCEDURE', N'sp_swValidarInventarioAppsTerminalAndroid', 'PARAMETER', N'@signalLevel'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Input variable', 'SCHEMA', N'dbo', 'PROCEDURE', N'sp_swValidarInventarioAppsTerminalAndroid', 'PARAMETER', N'@batteryLevel'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Input variable', 'SCHEMA', N'dbo', 'PROCEDURE', N'sp_swValidarInventarioAppsTerminalAndroid', 'PARAMETER', N'@reservedUse1'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Input variable', 'SCHEMA', N'dbo', 'PROCEDURE', N'sp_swValidarInventarioAppsTerminalAndroid', 'PARAMETER', N'@reservedUse2'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Input variable', 'SCHEMA', N'dbo', 'PROCEDURE', N'sp_swValidarInventarioAppsTerminalAndroid', 'PARAMETER', N'@reservedUse3'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Input variable', 'SCHEMA', N'dbo', 'PROCEDURE', N'sp_swValidarInventarioAppsTerminalAndroid', 'PARAMETER', N'@softwareVersionPos'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Input variable', 'SCHEMA', N'dbo', 'PROCEDURE', N'sp_swValidarInventarioAppsTerminalAndroid', 'PARAMETER', N'@isoField61'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Input variable', 'SCHEMA', N'dbo', 'PROCEDURE', N'sp_swValidarInventarioAppsTerminalAndroid', 'PARAMETER', N'@statistics'
GO