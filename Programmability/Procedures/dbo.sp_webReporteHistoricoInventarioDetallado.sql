﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

create PROCEDURE [dbo].[sp_webReporteHistoricoInventarioDetallado](
	@groupID AS BIGINT,
	@terminalID AS BIGINT,
	@customerID AS BIGINT,
	@startDate AS DATETIME,
	@endDate AS DATETIME
)
 
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @endDateReport AS DATETIME	
	SET @endDateReport = DATEADD(DAY, 1, @endDate)

	SELECT 
		CONVERT(VARCHAR, TI.tra_fecha, 103) + ' ' + CONVERT(VARCHAR, TI.tra_fecha, 108) AS tra_fecha,
		TI.tra_cliente_pos,
		G.gru_nombre,
		TI.tra_serial_pos,
		TI.tra_imei_pos,
		 tra_codigo_producto,
		TI.tra_message_type,
		TI.tra_processing_code,
		TI.tra_stan,
		TI.tra_direccion_ip_pos,
		TI.tra_direccion_ip_detectada,
		TI.tra_puerto_tcp_atencion,
		TI.tra_imei_pos,
		TI.tra_sim_pos,
		TI.tra_nivel_gprs,
		TI.tra_nivel_bateria,
		ISNULL(MT.descripcion,'') AS tra_marca_terminal,
		ISNULL(MoT.descripcion, '') AS tra_tipo_terminal,
		TI.tra_codigo_respuesta,
		TI.tra_mensaje_respuesta,
		ISNULL(EA.descripcion,'') AS tra_estado_actualizacion,
		TI.tra_version_software_pos,
		TI.tra_aplicaciones_pos,
		TI.tra_aplicaciones_a_instalar,
		TI.tra_fecha_descarga,
		(CASE WHEN TI.tra_ip_descarga = '' THEN '' ELSE TI.tra_ip_descarga + ':' + TI.tra_puerto_descarga END) AS tra_datos_descarga
	FROM TransaccionInventarioHistorico TI
		LEFT OUTER JOIN Terminal T ON (TI.tra_terminal_id = T.ter_id)
		LEFT OUTER JOIN Grupo G ON (T.ter_grupo_id = G.gru_id)
		LEFT OUTER JOIN MarcaTerminal MT ON (t.ter_marca_terminal_id = MT.id)
		LEFT OUTER JOIN ModeloTerminal MoT ON (t.ter_modelo_terminal_id = MoT.id)
		LEFT OUTER JOIN EstadoActualizacion EA ON (TI.tra_estado_actualizacion_id = EA.id)
	WHERE TI.tra_fecha >= @startDate AND TI.tra_fecha <= @endDateReport
		AND (T.ter_grupo_id = @groupID OR @groupID = -1) AND G.gru_cliente_id = @customerID AND (T.ter_id = @terminalID OR @terminalID = -1)
	ORDER BY TI.tra_fecha DESC
	
END
GO