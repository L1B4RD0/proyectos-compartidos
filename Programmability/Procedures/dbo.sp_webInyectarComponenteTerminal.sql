﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[sp_webInyectarComponenteTerminal](
	@terminalID BIGINT,
	@componentType INT,
	@componentData VARCHAR(250)
)
 
AS
BEGIN

	SET XACT_ABORT, NOCOUNT ON
	DECLARE @startTranCount INT

	--VARIABLES LOCALES DEL SP
	DECLARE @responseCode AS INT
	DECLARE @terminalSerial AS VARCHAR(50)

	BEGIN TRY
		SELECT @startTranCount = @@TRANCOUNT

		IF @startTranCount = 0
		BEGIN
			BEGIN TRANSACTION		
			--INICIO CUERPO DEL SP

			--VALIDAR TIPO DE COMPONENTE
			IF @componentType = 1 
			BEGIN
				--ACTUALIZAR COMPONENTE EN LA TERMINAL
				UPDATE Terminal
				SET
					ter_componente_1 = @componentData,
					ter_componente_1_inyectado = 1,
					ter_fecha_componente1 = GETDATE()
				WHERE ter_id = @terminalID

				SELECT @terminalSerial = ter_serial FROM Terminal WHERE ter_id = @terminalID

				--REGISTRAR EN TABLA LOG MDM
				INSERT INTO TransaccionMDM( [tra_fecha],[tra_operacion],[tra_terminal_id], [tra_terminal_id_device], [tra_estado_final])
				VALUES (GETDATE(), 'CARGAR_COMPONENTE_1', @terminalID, @terminalSerial, 'Carga OK');

			END
			ELSE IF @componentType = 2
			BEGIN
				--ACTUALIZAR COMPONENTE EN LA TERMINAL
				UPDATE Terminal
				SET
					ter_componente_2 = @componentData,
					ter_componente_2_inyectado = 1,
					ter_fecha_componente2 = GETDATE()
				WHERE ter_id = @terminalID

				SELECT @terminalSerial = ter_serial FROM Terminal WHERE ter_id = @terminalID

				--REGISTRAR EN TABLA LOG MDM
				INSERT INTO TransaccionMDM( [tra_fecha],[tra_operacion],[tra_terminal_id], [tra_terminal_id_device], [tra_estado_final])
				VALUES (GETDATE(), 'CARGAR_COMPONENTE_2', @terminalID, @terminalSerial, 'Carga OK');

			END

			SELECT @responseCode = 1

			--FIN CUERPO DEL SP
		END

		IF @startTranCount = 0 
		BEGIN 
			COMMIT TRANSACTION

			--SELECT FINAL
			SELECT @responseCode AS RESPONSECODE
		END
	END TRY
	BEGIN CATCH
		IF XACT_STATE() <> 0 AND @startTranCount = 0 
			ROLLBACK TRANSACTION

		INSERT INTO [dbo].[LogErrorBaseDatos]([bderr_fecha],[bderr_nombre_funcion],[bderr_mensaje_error],[bderr_datos_adicionales])
			 VALUES (GETDATE(), OBJECT_NAME(@@PROCID), ERROR_MESSAGE(), /*Datos Extra, Parámetros del SP*/ 
																		'terminalID = ' + CAST(@terminalID AS VARCHAR) + ', componentType = ' + CAST(@componentType AS VARCHAR))

		DECLARE @errMessage AS VARCHAR(4000) = ERROR_MESSAGE()
		DECLARE @errSeverity AS INT = ERROR_SEVERITY()
		DECLARE @errState AS INT = ERROR_STATE()

		RAISERROR (@errMessage, @errSeverity, @errState)
	END CATCH	

END


GO