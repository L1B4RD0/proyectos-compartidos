﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[sp_webReporteChartTerminalesBlocking](
	@customerID AS BIGINT,
	@groupID AS BIGINT
)
 
AS
BEGIN

	SET NOCOUNT ON;

	SELECT SUM(ter_Activado), SUM(ter_Desactivado), SUM(ter_totales) FROM 
	(
		SELECT 
			 SUM((CASE WHEN T.ter_blocking_mode = 1 THEN 1 ELSE 0 END)) AS ter_Activado,
			 SUM((CASE WHEN T.ter_blocking_mode = 0 THEN 1 ELSE 0 END)) AS ter_Desactivado,
			 COUNT(*) AS ter_totales
		FROM Terminal T
			INNER JOIN Grupo G ON (T.ter_grupo_id = G.gru_id)
		WHERE (G.gru_id = @groupID OR @groupID = -1) AND G.gru_cliente_id = @customerID
		GROUP BY T.ter_blocking_mode
	) AS REP
	
END


GO