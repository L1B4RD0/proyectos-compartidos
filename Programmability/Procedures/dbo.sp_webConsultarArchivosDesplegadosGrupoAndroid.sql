﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[sp_webConsultarArchivosDesplegadosGrupoAndroid](
	@groupId BIGINT,
	@deployTypeId INT
)
 
AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT desp_id, desp_fecha, desp_nombre_archivo, desp_nombre_paquete, desp_version
	FROM GrupoXArchivosDesplegados 
	WHERE desp_grupo_id = @groupId AND desp_tipo_despliegue_id = @deployTypeId
	ORDER BY desp_id DESC

END




GO