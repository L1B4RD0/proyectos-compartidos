﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

-- =============================================
-- Autor:				Elkin Beltrán
-- Fecha Creación:		06/Jun/2015
-- Histórico Cambios:
------------------------------------------------
--	v1.0	Versión Inicial
------------------------------------------------
-- Campos de Respuesta:		SingleRow
-- STATUS				INT
-- RNDTXT				VARCHAR(100)
-- =============================================

CREATE PROCEDURE [dbo].[sp_webContinuarProcesoRestauracionClave](
	@activationCode VARCHAR(100)
)
 
AS
BEGIN

	SET XACT_ABORT, NOCOUNT ON
	DECLARE @startTranCount INT

	--VARIABLES LOCALES DEL SP
	DECLARE @status AS INT
	DECLARE @restID AS INT
	DECLARE @rndTxt AS VARCHAR(100)

	SET @rndTxt = ''

	BEGIN TRY
		SELECT @startTranCount = @@TRANCOUNT

		IF @startTranCount = 0
		BEGIN
			BEGIN TRANSACTION		
			--INICIO CUERPO DEL SP

			--VERIFICAR QUE EXISTA UN PREPROCESO PARA REACTIVACIÓN DE CLAVE
			SELECT @restID = rest_id FROM UsuarioxRestauracionClave WHERE rest_rand = @activationCode AND rest_estado = 1

			IF @restID IS NOT NULL
			BEGIN

				EXEC [spGenerarTextoRandomico] @rndTxt OUTPUT

				/*ESTABLECER OTRO RANDÓMICO Y DEVOLVERLO*/
				UPDATE UsuarioXRestauracionClave
					SET rest_rand = @rndTxt
				WHERE rest_id = @restID

				SET @status = 1	/*OK*/
			END
			ELSE
			BEGIN
				SET @status = 0	/*ERROR*/ 
			END
			
			--FIN CUERPO DEL SP
		END

		IF @startTranCount = 0 
		BEGIN 
			COMMIT TRANSACTION 

			--SELECT FINAL
			SELECT @status AS STATUS, @rndTxt AS RNDTXT
		END
	END TRY
	BEGIN CATCH
		IF XACT_STATE() <> 0 AND @startTranCount = 0 
			ROLLBACK TRANSACTION

		INSERT INTO [dbo].[LogErrorBaseDatos]([bderr_fecha],[bderr_nombre_funcion],[bderr_mensaje_error],[bderr_datos_adicionales])
			 VALUES (GETDATE(), OBJECT_NAME(@@PROCID), ERROR_MESSAGE(), /*Datos Extra, Parámetros del SP*/ 'activationCode = ' + @activationCode)

		DECLARE @errMessage AS VARCHAR(4000) = ERROR_MESSAGE()
		DECLARE @errSeverity AS INT = ERROR_SEVERITY()
		DECLARE @errState AS INT = ERROR_STATE()

		RAISERROR (@errMessage, @errSeverity, @errState)
	END CATCH
	
END












GO