﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

-- =============================================
-- Autor:				Elkin Beltrán
-- Fecha Creación:		06/Junio/2015
-- Histórico Cambios:
------------------------------------------------
--	v1.0	Versión Inicial
------------------------------------------------
-- Campos de Respuesta:		SingleRow
-- USERID				INT
-- =============================================

CREATE PROCEDURE [dbo].[sp_webValidarUsuarioXCorreoElectronico](
	@emailID VARCHAR(150)
)
 
AS
BEGIN

	DECLARE @userID AS INT

	SELECT @userID = U.usu_id FROM Usuario U WHERE U.usu_email = @emailID

	IF @userID IS NULL
	BEGIN
		SET @userID = -1
	END

	SELECT @USERID AS USERID

END


GO