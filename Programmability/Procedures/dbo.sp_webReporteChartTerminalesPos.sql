﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[sp_webReporteChartTerminalesPos](
	@customerID AS BIGINT,
	@groupID AS BIGINT
)
 
AS
BEGIN

	SET NOCOUNT ON;

	SELECT SUM(ter_activa), SUM(ter_inactiva), SUM(ter_totales) FROM 
	(
		SELECT 
			 SUM((CASE WHEN T.ter_tipo_estado_id = 1 THEN 1 ELSE 0 END)) AS ter_activa,
			 SUM((CASE WHEN T.ter_tipo_estado_id = 2 THEN 1 ELSE 0 END)) AS ter_inactiva,
			 COUNT(*) AS ter_totales
		FROM Terminal T
			INNER JOIN Grupo G ON (T.ter_grupo_id = G.gru_id)
		WHERE (G.gru_id = @groupID OR @groupID = -1) AND G.gru_cliente_id = @customerID
		GROUP BY T.ter_tipo_estado_id
	) AS REP
	
end


GO