﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[sp_webDesplegarArchivoTerminalAndroid](
	@terminalID INT,
	@filename VARCHAR(100),
	@packageName VARCHAR(100),
	@version VARCHAR(100),
	@deployType INT	/*1-APK, 2-ZIP DE XML's, 3-ZIP DE IMG's*/
)
 
AS
BEGIN

	SET XACT_ABORT, NOCOUNT ON
	DECLARE @startTranCount INT

	--VARIABLES LOCALES DEL SP
	DECLARE @responseCode AS INT

	BEGIN TRY
		SELECT @startTranCount = @@TRANCOUNT

		IF @startTranCount = 0
		BEGIN
			BEGIN TRANSACTION		
			--INICIO CUERPO DEL SP

			IF @deployType IN (2,3)	--XML's E IMG's
			BEGIN
				IF NOT EXISTS(SELECT * FROM [dbo].[TerminalesXArchivosDesplegados] WHERE desp_terminal_id = @terminalID AND desp_nombre_archivo = @filename)
				BEGIN

					--INSERTAR NUEVO ARCHIVO DESPLEGADO POR TERMINAL
					INSERT INTO TerminalesXArchivosDesplegados(desp_fecha, desp_terminal_id, desp_nombre_archivo, desp_nombre_paquete, desp_version, desp_tipo_despliegue_id)
							VALUES (GETDATE(), @terminalID, @filename, @packageName, @version, @deployType)

					SELECT @responseCode = 1

				END
				ELSE
				BEGIN
					RAISERROR (50001, 10, 1, 'Archivo ya existe en la Terminal')
				END
			END
			ELSE
			BEGIN
				--APK's
				IF NOT EXISTS(SELECT * FROM [dbo].[TerminalesXArchivosDesplegados] WHERE desp_terminal_id = @terminalID AND desp_nombre_archivo = @filename AND desp_nombre_paquete = @packageName AND desp_version = @version)
				BEGIN

					--INSERTAR NUEVO ARCHIVO DESPLEGADO POR TERMINAL
					INSERT INTO TerminalesXArchivosDesplegados(desp_fecha, desp_terminal_id, desp_nombre_archivo, desp_nombre_paquete, desp_version, desp_tipo_despliegue_id)
							VALUES (GETDATE(), @terminalID, @filename, @packageName, @version, @deployType)

					SELECT @responseCode = 1

				END
				ELSE
				BEGIN
					RAISERROR (50001, 10, 1, 'Archivo ya existe en el Terminal')
				END
			END

			--FIN CUERPO DEL SP
		END

		IF @startTranCount = 0 
		BEGIN 
			COMMIT TRANSACTION

			--SELECT FINAL
			SELECT @responseCode AS RESPONSECODE
		END
	END TRY
	BEGIN CATCH
		IF XACT_STATE() <> 0 AND @startTranCount = 0 
			ROLLBACK TRANSACTION

		INSERT INTO [dbo].[LogErrorBaseDatos]([bderr_fecha],[bderr_nombre_funcion],[bderr_mensaje_error],[bderr_datos_adicionales])
			 VALUES (GETDATE(), OBJECT_NAME(@@PROCID), ERROR_MESSAGE(), /*Datos Extra, Parámetros del SP*/ 
																		'groupID = ' + CAST(@terminalID AS VARCHAR) + ', filename = ' + @filename + ', packageName = ' + @packageName + 
																		', version = ' + @version + ', deployType = ' + CAST(@deployType AS VARCHAR))

		DECLARE @errMessage AS VARCHAR(4000) = ERROR_MESSAGE()
		DECLARE @errSeverity AS INT = ERROR_SEVERITY()
		DECLARE @errState AS INT = ERROR_STATE()

		RAISERROR (@errMessage, @errSeverity, @errState)
	END CATCH	

END


GO